#encoding: utf-8
Dado(/^que eu esteja logado no sistema Gliese como username e password$/) do |table|
  @user = table.hashes[0]['username']
  @pwd = table.hashes[0]['password']
  LogarSistema.new.logado_sistema_gliese(@user, @pwd)
end

Quando(/^acesso a tela de Impressao de RPS\-NF$/) do
  sleep 3 
  @tela_imp_rps = FuncionalidadeImpressaoRPS.new
  @tela_imp_rps.menu_RPS_NF.click
  @tela_imp_rps.submenu_impressao_RPS.click
end

Então(/^valido se o texto "([^"]*)" e exibido na mesma$/) do |descricao_tbl_observacao|
  sleep 2
  @result_tabela = @tela_imp_rps.pega_valores_tabela_impressao_rps
  expect(@result_tabela).to have_content(descricao_tbl_observacao)
end

Então(/^valido se o combo de obervação é exibido para empresa status Ativo usando "([^"]*)" e "([^"]*)"$/) do |cip, visita|
  @tela_imp_rps.preencher_cip(cip)
  @tela_imp_rps.preencher_visita(visita)
  @tela_imp_rps.combo_rsp_existe
end

Então(/^valido se o campo observação é habilitado para digitação para empresa status não Ativo usando "([^"]*)" e "([^"]*)"$/) do |cip, visita|
  @tela_imp_rps.preencher_cip(cip)
  @tela_imp_rps.preencher_visita(visita)
  @tela_imp_rps.valida_campo_observacao
end

Então(/^valido campo observação carrega informações da rps e não permite digitação usando "([^"]*)" e "([^"]*)"$/) do |cip, visita|
  @tela_imp_rps.preencher_cip(cip)
  @tela_imp_rps.preencher_visita(visita)
  # @tela_imp_rps.combo_rsp_desabilitado
end

Então(/^solicito a impressão e valido o combo Observação usando "([^"]*)" e "([^"]*)"$/) do |cip, visita|
  texto_observacao = ""
  util = Utils.new
  rps_pdf = ""
  encontrou_texto_radiobutton = false
  @tela_imp_rps.preencher_cip(cip)
  @tela_imp_rps.preencher_visita(visita)
  texto_observacao = @tela_imp_rps.pega_texto_campo_observacao
  if texto_observacao == ""
    puts 'Campo observação não preenchido para validação da  Impressão, favor adicionar...'
    break
  end
  if texto_observacao.include?("|")
    texto_observacao = texto_observacao.split('|')
    texto_observacao.each do |valor_combo|
      @tela_imp_rps.gerar_rps
      rps_pdf = util.carregar_pdf("GerarRelatorioRpsServlet.pdf").text
      if rps_pdf.include?(valor_combo)
        encontrou_texto_radiobutton = true
      end
    end
    expect(encontrou_texto_radiobutton).to be(true)
  end
  #Valida se texto está no campo editável
  @tela_imp_rps.gerar_rps
  sleep 4
  rps_pdf = util.carregar_pdf("GerarRelatorioRpsServlet.pdf").text
  expect(rps_pdf.include?(texto_observacao)).to be(true)
end


Então(/^solicito a impressão e valido o combo Observação usando "([^"]*)" e Visita RPS$/) do |cip|
  rps_pdf = ""
  texto_observacao = ""
  encontrou_texto_radiobutton = false
  @tela_imp_rps.preencher_cip(cip)
  @tela_imp_rps.preencher_visita($numero_visita)
  @tela_imp_rps.preencher_observacao(texto_observacao)
  texto_observacao = @tela_imp_rps.pega_texto_campo_observacao
  @tela_imp_rps.gerar_rps
  sleep 4
  @tela_imp_rps.validar_pdf_relatorio_rps
end
