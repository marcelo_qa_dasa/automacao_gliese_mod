Então(/^clico na aba Conclusão$/) do
  begin
    @admissao_conclusao.clicar_no_botao_conclusao    
  rescue Selenium::WebDriver::Error::UnhandledAlertError
    true
  end
end

Então(/^é exibida a mensagem de admissão sem pendências "([^"]*)"$/) do |msg|
  @admissao_conclusao.validar_exibicao_msg_admissao_sem_pendencias(msg)
end

Então(/^clico na apção Salvar$/) do
  begin
    @admissao_conclusao.clicar_no_botao_salvar
  rescue Selenium::WebDriver::Error::UnhandledAlertError
    true
  end
end

Então(/^é exibida a mensagem de confirmação de exibição "([^"]*)"$/) do |msg|
  wait_accept_alert
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
end

Então(/^é exibido o Espelho de Atendimento, Prazo de Exames para Visita e a mensagem "([^"]*)"$/) do |mensagem_visita_autorizada|
  @admissao_conclusao.wait_window_handles(4)
  sleep 2
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  page.driver.browser.close
  sleep 2
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  @admissao_conclusao.wait_until_cip_paciente_visible(3)
  @admissao_conclusao.validar_guia_prazo_de_exames_para_visita($table_exame)
  page.driver.browser.close
  sleep 2
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[0])
  #expect(@admissao_conclusao.validar_msg_visita_autorizada).to eq mensagem_visita_autorizada
  @admissao_conclusao.validar_espelho_de_atendimento

end

Então(/^é exibido o Prazo de Exames para Visita e a mensagem "([^"]*)"$/) do |mensagem_admissao_autorizada|
  @admissao_conclusao.wait_window_handles(5)
  sleep 2
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  sleep 1
  @admissao_conclusao.wait_until_cip_paciente_visible(3)
  @admissao_conclusao.validar_guia_prazo_de_exames_para_visita($table_exame)
  sleep 2
  page.driver.browser.close
  sleep 1
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[0])
  #expect(@admissao_conclusao.validar_msg_visita_autorizada).to eq mensagem_admissao_autorizada
end

Então(/^é exibido o protocolo de exames de imagem "([^"]*)"$/) do |mensagem_admissao_autorizada|
  @admissao_conclusao.wait_window_handles(5)
  sleep 2
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  sleep 2
  page.driver.browser.close
  sleep 2
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  @admissao_conclusao.validar_guia_prazo_de_exames_para_visita($table_exame)
  sleep 2
  page.driver.browser.close
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[0])
  @admissao_conclusao.validar_protocolo_de_atendimento_paciente
  #expect(@admissao_conclusao.validar_msg_visita_autorizada).to eq mensagem_admissao_autorizada
end


Então(/^é exibido o Espelho de Atendimento, Prazo de Exames para Visita e protocolo de exames de imagem e a mensagem "([^"]*)"$/) do |mensagem_admissao_autorizada|
  @admissao_conclusao.wait_window_handles(5)
  sleep 2
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[3])
  @admissao_conclusao.validar_guia_prazo_de_exames_para_visita($table_exame)
  page.driver.browser.close
  sleep 1
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  sleep 1
  page.driver.browser.close
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  sleep 1
  page.driver.browser.close
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[0])
  @admissao_conclusao.validar_guia_tiss
  @admissao_conclusao.validar_protocolo_de_atendimento_paciente
  #expect(@admissao_conclusao.validar_msg_visita_autorizada).to eq mensagem_admissao_autorizada
end

Então(/^é exibido o Espelho de Atendimento, protocolo de exames de imagem e Prazo de Exames para Visita e a mensagem "([^"]*)"$/) do |mensagem_admissao_autorizada|
  @admissao_conclusao.wait_window_handles(5)
  sleep 2
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  @admissao_conclusao.validar_guia_prazo_de_exames_para_visita($table_exame)
  page.driver.browser.close
  sleep 1
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  sleep 1
  page.driver.browser.close
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[1])
  sleep 1
  page.driver.browser.close
  page.driver.browser.switch_to.window(page.driver.browser.window_handles[0])
  @admissao_conclusao.validar_protocolo_de_atendimento_paciente
  #expect(@admissao_conclusao.validar_msg_visita_autorizada).to eq mensagem_admissao_autorizada
end

Então(/^é exibida a mensagem "([^"]*)" e são informadas pendências na visita$/) do |msg, table|
  expect(@admissao_conclusao.validar_exibicao_msg_admissao_sem_pendencias(msg)).to eql true
  @admissao_conclusao.validar_informacoes_pendencias_visita(table)
end

Então(/^é exibida a mensagem de preenchimento do campo convenio "([^"]*)"$/) do |msg|
  wait_accept_alert
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept
end

Então(/^é exibida a mensagem de visita sem exames válidos "([^"]*)"$/) do |msg|
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  
end

Então(/^clico no botão Cancelar$/) do
  page.driver.browser.switch_to.alert.dismiss
end

Então(/^é exibida a mensagem de admissão sem pendências "([^"]*)" no cancelamento$/) do |msg|
  expect(@admissao_conclusao.validar_exibicao_msg_admissao_sem_pendencias(msg)).to eql true
end

Então(/^clico na apção Abandonar$/) do
  begin
    @admissao_conclusao.clicar_botao_abandonar
  rescue Selenium::WebDriver::Error::UnhandledAlertError
    true
  end
end

Quando(/^é exibida a mensagem de confirmação de abandono "([^"]*)" e clico no botão ok$/) do |msg|
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept               
end                                                                                                

Então(/^a aba Paciente é carregada com os dados do paciente$/) do
 expect(@admissao.validar_dados_paciente_aba_exames).to eql true
end

Quando(/^é exibida a mensagem de confirmação de abandono "([^"]*)"$/) do |msg|
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
end
