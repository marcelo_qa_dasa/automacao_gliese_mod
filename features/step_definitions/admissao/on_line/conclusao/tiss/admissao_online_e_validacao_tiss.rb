Então(/^valido o campo CID no relatorio tiss$/) do
  @utils = Utils.new
  # ****************************** Relatório Espelho atendimento ******************************
  result_espelho = @utils.carregar_pdf("EspelhoAtendimento.pdf").raw_content
  expect(result_espelho).to include $cid # valor do campo
  expect(result_espelho).to include 'CID' # Nome do campo
  @utils.remover_pdf("EspelhoAtendimento.pdf")
  # ****************************** Relatório TISS ******************************
  result_tiss = @utils.carregar_pdf("GuiaTissCvn.pdf").raw_content
  expect(result_tiss).to include $cid # valor do campo
  expect(result_tiss).to include '23- CID 10' # Nome do campo
end

Dado (/^que eu esteja na home do sistema Gliese pela empresa e unidade "([^"]*)" e "([^"]*)"$/) do |empresa, unidade|
  login = LogarSistema.new
  login.login_gliese_por(empresa, unidade)
end

Então(/^confirmo as mensagens até realizar impressao da guia tiss$/) do
  step 'clico no botão OK'
  sleep 2
  wait = Selenium::WebDriver::Wait.new ignore: Selenium::WebDriver::Error::NoAlertPresentError
  alert = wait.until { page.driver.browser.switch_to.alert }
  alert.dismiss
  step 'clico no botão OK'
  sleep 5 # :TODO melhorar no futuro para aguardar download do .pdf e tirar o sleep.
end
