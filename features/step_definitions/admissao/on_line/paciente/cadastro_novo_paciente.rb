
  Dado(/^que eu esteja na home do sistema Gliese$/) do |table|
    @user = table.hashes[0]['username']
    @pwd = table.hashes[0]['password']
    @empr = table.hashes[0]['empresa']
    @unid = table.hashes[0]['unidade']  
    LogarSistema.new.steps_login(@user, @pwd, @empr,  @unid)
  end
  
  
  Dado(/^clico no menu Admissão > On Line$/) do
    @admissao.link_admissao.click
    sleep 1
    @admissao.link_admissao_on_line.click
    sleep 1
    @unidade.menu_empresa_tela_home.click  
    sleep 1
  end

  Dado(/^clico na opção Cadastrar novo paciente$/) do
    @unidade.validar_informar_atendimento 
    @admissao.botao_cadastrar_novo_paciente
  end

  Dado(/^Preencho os campos de cadastro de cliente com sucesso$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    @admissao.cadastrar_novo_clientes_com_sucesso
  end

  Dado(/^preencho o CIP para editar os dados respectivos ao paciente$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    @admissao.campo_cadastro_paceinte_cip.send_keys($massa_editar['CIP'])
    @admissao.btn_novo_criar_um_cip.click
  end

  Dado(/^edito o cadastro de paciente$/) do  
    @admissao.editar_cadastro_novo_clientes_com_sucesso
  end

  Quando(/^clico no botão cadastrar$/) do 
    @admissao.btn_cadastrar_novo_paciente.click
    sleep 1
  end

  Então(/^e apresentado a mensagem de cadastro de paciente "([^"]*)"$/) do |msg_cadastro_com_sucesso|
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_cadastro_com_sucesso)
    page.driver.browser.switch_to.alert.accept
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)
    sleep 2
    expect( @admissao.validar_texto_campo_nome).to have_text $nome.titleize
  end

  Quando(/^preencho o campo nome do paciente$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    @admissao.campo_novo_nome_paciente.click
  end

  Então(/^é apresentado a mensagem "([^"]*)"$/) do |msg|
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept
  page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)
  end

  Quando(/^preencho os campos validos sem preencher o campo nome$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    @admissao.btn_novo_criar_um_cip.click
  end

  Quando(/^preencho os campos validos sem preencher o campo Data de Nascimento$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    @admissao.btn_novo_criar_um_cip.click
    $nome = Faker::Name.name_with_middle
    @admissao.campo_novo_nome_paciente.send_keys($nome)
  end

  Quando(/^preencho os campos validos sem preencher o campo Sexo$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    @admissao.btn_novo_criar_um_cip.click
    $nome = Faker::Name.name_with_middle
    @admissao.campo_novo_nome_paciente.send_keys($nome)
    @admissao.campo_novo_data_nascimento_paciente.send_keys('29091991')
  end

  Quando(/^preencho os campos validos sem preencher o Motivo de Email Não Informado$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    @admissao.btn_novo_criar_um_cip.click
    $nome = Faker::Name.name_with_middle
    @admissao.campo_novo_nome_paciente.send_keys($nome)
    @admissao.campo_novo_data_nascimento_paciente.send_keys('29091991')
    @admissao.selecionar_novo_sexo_paciente.select("M")
  end

  Quando(/^preencho os campos validos sem preencher o telefone$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    @admissao.btn_novo_criar_um_cip.click
    $nome = Faker::Name.name_with_middle
    @admissao.campo_novo_nome_paciente.send_keys($nome)
    @admissao.campo_novo_data_nascimento_paciente.send_keys('29091991')
    @admissao.selecionar_novo_sexo_paciente.select("M")
    @admissao.campo_novo_email_paciente.send_keys(Faker::Internet.email)
  end