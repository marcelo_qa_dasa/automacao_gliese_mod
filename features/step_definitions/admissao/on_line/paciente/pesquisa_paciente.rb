Dado(/^preencho os campos Data de nascimento e CIP com dados validos$/) do
  @unidade.validar_informar_atendimento
   @admissao.preencher_campo_data_nascimento_tela_paciente
   @admissao.preencher_campo_cip_paciente
  sleep 1
   @admissao.clicar_para_exibir_consulta_por_cip
end

Dado(/^preencho os dados do paciente com data de nascimento "([^"]*)" e CIP "([^"]*)"$/) do |data_nascimento, cip|
  @admissao.preencher_campo_data_nascimento_tela_paciente_com(data_nascimento.to_s)
  @admissao.preencher_campo_cip_paciente_com(cip)
end

Entao(/^é apresentado na tela de paciente as informações respectivas a consulta por Data de nascimento e CIP$/) do
  sleep 1
  aggregate_failures do
  expect(@admissao.validar_texto_campo_nome).to eq $massa['nome']
  expect(@admissao.retorno_campo_paciente_cpf.to_s).to eq $massa['CPF'].to_s
  end
end

Dado(/^clico na opção Pesq\. Nome\.\.\.$/) do
  @unidade.validar_informar_atendimento
   @admissao.clicar_botao_pesquisa_por_nome
end

Dado(/^Preencho os campos Data de nascimento e nome$/) do
   @admissao.consultar_paciente_por_nome
end

Entao(/^é apresentado as infoações respectivas ao paciente CIP, Nome, Data Nascimento, CPF, Telefone, Endereço e status VIP$/) do
  @elemento_visivel_page.wait_element_visible('#resNomePaciente')
  aggregate_failures do
  expect(@admissao.retorno_pesquisa_por_nome_campo_cip($massa['CIP'].to_s)).to eq true
  expect(@admissao.retorno_pesquisa_por_nome_campo_nome($massa['nome'])).to eq true
  expect(@admissao.retorno_pesquisa_por_nome_campo_data_nasc($massa['Data_nasc'].to_s)).to eq true
  end
end


Dado(/^clico na opção Pesq\. RG\.\.\.$/) do
  @unidade.validar_informar_atendimento
   @admissao.clicar_botao_pesquisar_por_rg
end

Dado(/^Preencho o campo com RG valido de uma paciente$/) do
  @admissao.preencher_campo_consulta_por_rg_campo_rg($massa['RG'])
  @admissao.clicar_botao_para_consultar_rg
 sleep 1
end

Entao(/^é apresentado as infoações respectivas ao paciente, CIP, nome e Data de nascimento$/) do
  aggregate_failures do
  expect(@admissao.retorno_pesquisa_por_rg_campo_cip($massa['CIP'].to_s)).to eq true
  expect(@admissao.retorno_pesquisa_por_rg_campo_nome($massa['nome'])).to eq true
  expect(@admissao.retorno_pesquisa_por_rg_campo_data_nascimento($massa['Data_nasc'])).to eq true
  end

end

Dado(/^clico na opção Pesq\. CPF\.\.\.$/) do
  @unidade.validar_informar_atendimento
   @admissao.clicar_botao_pesquisar_por_cpf
end

Dado(/^Preencho o campo com CPF valido de uma paciente$/) do
   @admissao.preencher_campo_cpf($massa['CPF'])
   @admissao.clicar_botao_consultar_cpf
  sleep 1
end

Entao(/^é apresentado as infoações respectivas a pesquisa por CPF: CIP, nome e Data de nascimento$/) do
  aggregate_failures do
  expect(@admissao.retorno_pesquisa_por_cpf_campo_cip($massa['CIP'].to_s)).to eq true
  expect(@admissao.retorno_pesquisa_por_cpf_campo_nome($massa['nome'])).to eq true
  expect(@admissao.retorno_pesquisa_por_cpf_campo_data_nascimento($massa['Data_nasc'].to_s)).to eq true
  end
end

Quando(/^clico na opção Limpa na tela de paciente$/) do
   @admissao.clicar_botao_limpar_dados
end

Então(/^os valores dos campos Data de nascimento e CIP são removidos\.$/) do
  aggregate_failures do
  expect(@admissao.retorno_campo_data_nascimento_tela_paciente).to eq ""
  expect(@admissao.retorno_campo_cip_paciente).to eq ""
  expect(@admissao.validar_texto_campo_nome).to eq ""
  end
end
