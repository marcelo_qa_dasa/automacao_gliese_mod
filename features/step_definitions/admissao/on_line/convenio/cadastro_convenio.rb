
Dado(/^que eu esteja logado no sistema Gliese como "([^"]*)" e "([^"]*)"$/) do |username, password|
  LogarSistema.new.logado_sistema_gliese(username, password)
end

Quando(/^seleciono a marca "([^"]*)" e a unidade "([^"]*)"$/) do |nome_empresa, nome_unidade|
 $empresa = nome_empresa
 $nome_unidade = nome_unidade
 $numero_empresa = @banco_dados.empresa_codigo(nome_empresa)
 @convenio.selecionar_empresa($numero_empresa)
 @unidade.selecionar_unidade_de_atendimento.select(nome_unidade)
 @unidade.btn_escolher_empresa.click
 @elemento_visivel.wait_element_visible('.empresa')
end

Quando(/^preencho os campos Data de nascimento "([^"]*)" e CIP "([^"]*)"$/) do |data_nasc, cip|
 @unidade.validar_informar_atendimento
 $cip = cip
 @convenio.preencher_campo_data_nascimento_tela_paciente(data_nasc)
 @convenio.preencher_campo_cip_paciente($cip)
 sleep 1
 @admissao.clicar_para_exibir_consulta_por_cip
 $nome_pacinete = @admissao.validar_texto_campo_nome
end

Quando(/^clico na aba Convênio$/) do
 @convenio.clicar_no_botao_convenio
 @elemento_visivel_page.wait_element_visible('#Convenio_convenio')
end

Quando(/^informo o convênio "([^"]*)"$/) do |nome_convenio|
 $nome_convenio = nome_convenio
 @convenio.preencher_o_campo_convenio_aba_convenio(nome_convenio)
 sleep 1
end

Quando(/^clico no botão Seleciona$/) do
 @convenio.clicar_no_botao_selecionar_convenio
end

Quando(/^são exibidas as instruções do convênio Golden Cross$/) do
 sleep 1
 expect(@convenio.validar_texto_instrucoes_do_convenio[0..-2]).to eql(@banco_dados.selecionar_instrucoes_do_convenio($nome_convenio))
end

Quando(/^preencho as informações do convênio para o convênio Golden Cross "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)"$/) do |arg1, arg2, arg3, arg4, select_01, arg6, checked, arg8|
 $data_atual = @utils.validar_data_atual
 @convenio.preencher_campos_para_o_convenio_golden_cross(arg1, arg2, arg3, arg4, select_01, arg6, checked, arg8, $data_atual, $data_atual)
end

Quando(/^são exibidas as instruções do convênio Sul America$/) do
 expect(@convenio.validar_texto_instrucoes_do_convenio).to eql(@banco_dados.selecionar_instrucoes_do_convenio($nome_convenio))
end

Quando(/^preencho as informações do convênio para o convênio Sul America "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)"$/) do |arg1, arg2, arg3, checked, arg5, arg6, select_01|
 $data_atual = @utils.validar_data_atual
 @convenio.preencher_campos_para_o_convenio_sul_america(arg1, $data_atual, arg3, checked, arg5, arg6, select_01)
end

Quando(/^são exibidas as instruções do convênio Unimed Seguros$/) do
 sleep 1
 expect(@convenio.validar_texto_instrucoes_do_convenio[0..-2]).to eql(@banco_dados.selecionar_instrucoes_do_convenio($nome_convenio))
end

Quando(/^preencho as informações do convênio para o convênio Unimed Seguros "([^"]*)", "([^"]*)", "([^"]*)" e "([^"]*)"$/) do |arg1, arg2, checked, arg4|
 $data_atual = @utils.validar_data_atual
 @convenio.preencher_campos_para_o_convenio_unimed_seguros(arg1, $data_atual, checked, arg4)
end

Dado(/^informo o convênio "([^"]*)" no campo Convênio da aba Convênio$/) do |nome_convenio|
 @convenio.preencher_o_campo_convenio_aba_convenio(nome_convenio)

end

Dado(/^clico no ícone binóculo do Convênio da aba Convênio$/) do
 @convenio.clicar_no_binoculo_da_aba_convenio
end

Dado(/^são exibidas as informações de Mnemônico e Descrição do convênio$/) do
expect(@convenio.validar_campo_descricao_mnemonico_aba_convenio).to eq true
expect(@convenio.validar_campo_mnemonico_aba_convenio).to eq true
end

Dado(/^clico no mnemônico "([^"]*)" da tela de resultado de pesquisa de convênio$/) do |mnemonico|
 @convenio.clicar_no_mnemonico_da_aba_convenio(mnemonico)
end

Dado(/^é exibido o mnemônico "([^"]*)" no campo Convênio da aba Convênio$/) do |mnemonico|
 expect(@convenio.validar_texto_do_campo_convenio_aba_convenio).to eql mnemonico
end

Dado(/^clico no botão Seleciona da aba Convênio$/) do
 @convenio.clicar_no_botao_selecionar_convenio
end

Dado(/^são exibidos os campos do convênio "([^"]*)" na aba Convênio$/) do |convenio|
 expect(@convenio.validar_campos_para_o_convenio_unimed_seguros(true)).to eql true
end

Quando(/^clico no botão Limpa da aba Convênio$/) do
 @convenio.clicar_botao_limpar_tela_convenio
end

Então(/^as informações do Convênio são removidas da aba Convênio$/) do
 expect(@convenio.validar_campos_para_o_convenio_unimed_seguros(false)).to eql true
end

Dado(/^clico na opção Pesq\. Por Nome da aba Convênio$/) do
@convenio.clicar_botao_pesquisa_convenio_por_nome
end

Dado(/^informo o convênio "([^"]*)" no campo Convênio da popup Pesquisa Convênio$/) do |convenio_nome|
 $convenio_nome = convenio_nome
@convenio.preencher_campo_pesquisar_convenio_por_nome(convenio_nome)
end

Dado(/^clico no ícone binóculo da popup Pesquisa Convênio$/) do
 @convenio.clicar_botao_binoculo_pesquisar_convenio_por_nome
end

Dado(/^são exibidas as informações de Mnemônico e Descrição do convênio na popup de resultado de pesquisa de convênio$/) do
 expect(@convenio.validar_campo_resultado_mnemonico_pesquisa_por_nome_convenio).to eql $convenio_nome
end

Quando(/^clico no mnemônico "([^"]*)" da tela de resultado de pesquisa de convênio por nome$/) do |arg1|
 @convenio.clicar_no_mnemonico_pesquisa_por_nome_convenio
end

Dado(/^clico na opção Pesq\. Operadora da aba Convênio$/) do
 @convenio.clicar_no_botao_pesquisar_por_operadora
end

Dado(/^informo a operadora "([^"]*)" no campo Operadora da popup Pesquisa Convênio Por Operadora$/) do |nome_operadora|
 $nome_operadora = nome_operadora
 @convenio.preencher_campo_pesquisa_por_operadora($nome_operadora)
end

Dado(/^clico no ícone binóculo do campo Operadora da popup Pesquisa Convênio Por Operadora$/) do
 @convenio.clicar_no_binoculo_pesquisar_por_operadora
end

Dado(/^é exibido o nome da operadora na tela de resultado de pesquisa de operadora$/) do
 expect(@convenio.validar_resultato_nome_operadora).to eql $nome_operadora
end

Quando(/^clico no nome da operadora "([^"]*)" da tela de resultado de pesquisa de operadora$/) do |operadora|
 @convenio.clicar_no_resultato_nome_operadora(operadora)
end

Então(/^é exibido o nome da operadora "([^"]*)" no campo Operadora da popup Pesquisa Convênio Por Operadora$/) do |nome_operado_campo|
 expect(@convenio.validar_exibicao_do_nome_da_operadora_no_campo).to eql nome_operado_campo
end

Então(/^informo o plano "([^"]*)" no campo Plano da popup Pesquisa Convênio Por Operadora$/) do |plano|
 $plano = plano
 @convenio.preencher_campo_nome_do_pano_aba_pesquisa_por_operadora($plano)
end

Então(/^clico no ícone binóculo do campo Plano da popup Pesquisa Convênio Por Operadora$/) do
@convenio.clicar_no_binoculo_pesquisar_por_operadora_pesquisa_por_plano
end

Então(/^são exibidas as informações de Convênio e Plano na tela de resultado de pesquisa de planos$/) do
 expect(@convenio.validar_resultado_nome_do_plano_pesquisa_por_operadora).to eql $plano
end

Quando(/^clico no plano "([^"]*)" da tela de resultado de pesquisa de planos$/) do |arg1|
 @convenio.clicar_no_resultado_nome_do_plano_pesquisa_por_operadora
end

Então(/^é exibido o convênio "([^"]*)" no campo Convênio da aba Convênio$/) do |nome_convenio|
expect(@convenio.validar_texto_do_campo_convenio_aba_convenio).to eql(nome_convenio)
end

Dado(/^são exibidas todas as operadoras$/) do
expect(@convenio.resultado_lista_de_operadoras_para_uma_empresa).to eql(@banco_dados.selecionar_nome_operadora_por_empresa("Sergio Franco"))
end

Então(/^clico no ícone binóculo do campo Plano da popup Pesquisa Convênio Por Operadora e é exibida a mensagem de pesquisar operadora "([^"]*)"$/) do |msg_pesquisa_operadora|
 begin
   page.driver.wait_until(@convenio.clicar_no_binoculo_pesquisar_por_operadora_pesquisa_por_plano)
 rescue Selenium::WebDriver::Error::UnhandledAlertError
   expect(page.driver.browser.switch_to.alert.text).to have_text(msg_pesquisa_operadora)
   page.driver.browser.switch_to.alert.accept
 end
end

Dado(/^informo "([^"]*)" no campo Convênio da aba Convênio$/) do |convenio|
 @convenio.preencher_o_campo_convenio_aba_convenio(convenio)
end

Então(/^nenhum registro de convênio é exibido$/) do
expect(@convenio.validar_nao_exibicao_de_convenios_invalidos).to eql true
end

Então(/^nenhuma operadora é exibida na tela de resultado de pesquisa de operadora$/) do
 expect(@convenio.validar_nao_exibicao_de_operadoras_invalidas).to eql true
end

Então(/^nenhum plano é exibido na tela de resultado de pesquisa de planos$/) do
 expect(@convenio.validar_nao_exibicao_de_plano_pesquisa_convenio_por_operadora).to eql true
end

Então(/^é exibida a mensagem de convênio recuperado com sucesso "([^"]*)"\.$/) do |msg_convenio|
 @convenio.clicar_no_botao_selecionar_convenio
 sleep 1
 expect(@convenio.validar_mensagem_convenio_recuperado_com_sucesso). to eql(msg_convenio)
end

Quando(/^são exibidas as instruções do convênio Particular$/) do
 sleep 1
 expect(@convenio.validar_texto_instrucoes_do_convenio).to eql(@banco_dados.selecionar_instrucoes_do_convenio($nome_convenio))
end

E(/^Preencho os dados do convenio$/) do |tabela|
  @convenio.preencher_campos_para_qualquer_convenio(tabela)
end
