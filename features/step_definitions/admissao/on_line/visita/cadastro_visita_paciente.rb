Dado(/^clico na aba Visita$/) do
  @visita.clicar_no_botao_visita
  @elemento_visivel_page.wait_element_visible('#Visita_cmbUnidadeAtendimento')
end

Então(/^o campo Posto exibe a unidade atual, o campo Data exibe a data atual, o campo Tipo de Coleta exibe "([^"]*)" e o campo Empresa exibe a empresa\.$/) do |tipo_coleta|
  expect(@visita.retorno_visita_campo_posto_unidade_atendimento).to eql(@visita.unidade_nova.text)
  expect(@visita.retorno_visita_data_atual).to eql(@utils.validar_data_atual)
  expect(@visita.retorno_visita_tipo_de_coleta).to eql(tipo_coleta)
end

Quando(/^digito "([^"]*)" no campo Número$/) do |numero|
  @visita.preencher_campo_numero_tela_visita(numero)
end

Quando(/^pressiono enter$/) do
 @visita.clicar_enter
end

Então(/^as informações de Posto, Data, Hora, Tipo de Coleta e Empresa da visita são exibidas\.$/) do
  sleep 1
  @Lista_de_visitas = @banco_dados.listagem_de_visitas_paciente($massa['CIP'].to_s)
  $resultado_empresa_visita = @banco_dados.validar_uf_da_unidade_selecionada($retorno[0][2])
  expect(@visita.retorno_visita_campo_posto_unidade_atendimento).to include(@Lista_de_visitas[0][2])
  expect(@visita.retorno_visita_data_atual).to eql(@Lista_de_visitas[0][1])
  expect(@visita.retorno_visita_tipo_de_coleta).to eql("Posto")
  expect(@visita.retorno_visita_campo_empresa).to include($resultado_empresa_visita[0])
  expect(@visita.retorno_visita_hora).to eql true
end

Então(/^as informações de Posto, Data, Hora, Tipo de Coleta e Empresa da visita são exibidas respectivas a pesquisa\.$/) do
  sleep 1
  @Lista_de_visitas = @banco_dados.listagem_de_visitas_paciente($massa['CIP'].to_s)
  $resultado_empresa_visita = @banco_dados.validar_uf_da_unidade_selecionada($retorno.last[2])
  expect(@visita.retorno_visita_campo_posto_unidade_atendimento).to include(@Lista_de_visitas.last[2])
  expect(@visita.retorno_visita_data_atual).to eql(@Lista_de_visitas.last[1])
  expect(@visita.retorno_visita_tipo_de_coleta).to eql("Posto")
  expect(@visita.retorno_visita_campo_empresa).to include($resultado_empresa_visita[0])
  expect(@visita.retorno_visita_hora).to eql true
end

Então(/^é exibida a mensagem "([^"]*)"$/) do |mensagem_visita|
 expect(@visita.mensagem_vista_nao_existe).to eq mensagem_visita
end

Quando(/^clico na opção Nova$/) do
 @visita.clicar_botao_nova_visita
end

Então(/^o campo Número exibe o próximo número sequencial, o campo Posto exibe a unidade atual, o campo Data exibe a data atual, o campo Hora exibe a hora atual, o campo Tipo de Coleta exibe "([^"]*)" e o campo Empresa exibe a empresa\.$/) do |tipo_coleta|
  expect(@visita.validar_numero_visita_existente).to eq true
  expect(@visita.retorno_visita_campo_posto_unidade_atendimento).to eql(@visita.unidade_nova.text)
  expect(@visita.retorno_visita_data_atual).to eql(@utils.validar_data_atual)
  expect(@visita.retorno_visita_tipo_de_coleta).to eql(tipo_coleta)
end

Quando(/^clico no ícone binóculo$/) do
  @visita.clicar_botao_binoculo_resumo_visitas
end

Quando(/^clico no Número da visita$/) do
 $valor_numero_visita = @visita.retornar_numero_total_visitas
 @visita.clicar_numero_ultima_visita
 @visita.wait_element_validar_numero_visita_existente
 expect(@visita.validar_numero_visita_existente).to eq true
end

Quando(/^clico na opção Documento$/) do
 @visita.clicar_botao_documento_visita
end

Então(/^são exibidas as informações de Paciente CIP, Visita e a mensagem "([^"]*)"$/) do |mensagem_documento_nao_existe|
  page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
  expect(@visita.retornar_text_cip_numero_visitas).to include($valor_numero_visita)
  expect(@visita.retornar_text_cip_numero_visitas).to include(($massa['CIP'].to_s))
  expect(@visita.retornar_mensagem_nao_exite_documento_visita).to eq mensagem_documento_nao_existe
end

Então(/^visitas já cadastradas para o cliente são exibidas com as informações de Número, Data, Unidade, Convênio, Exames e Docs\.$/) do
  expect(@visita.validar_documentos_existente_lista_visitas).to eq true
  expect(@visita.validar_convenio_existente_lista_visitas).to eq true
  expect(@visita.validar_unidade_existente_lista_visitas).to eq true
  expect(@visita.validar_data_existente_lista_visitas).to eq true
  expect(@visita.validar_numero_existente_lista_visitas).to eq true
end

Quando(/^clico na opção lista de visitas completa$/) do
  @visita.clicar_link_lista_de_visitas_completas
  @elemento_visivel_page.wait_element_visible('#visTotalVisita_resumoVisitas')
  expect(@visita.validar_resumo_total_de_visitas_do_paciente).to include "total de #{$valor_numero_visita}"
end

Quando(/^clico na opção Limpa na tela de visita$/) do
  @visita.clicar_botao_Limpar
end

Então(/^os valores dos campos Número e Hora são removidos\.$/) do
  expect(@visita.validar_numero_visita_existente).to eq false
  expect(@visita.validar_hora_atual_existente).to eq false
end

Quando(/^altero o Posto, Tipo de Coleta e Empresa$/) do
  @visita.selecionar_um_tipo_de_coleta_visita("Domiciliar")
  sleep 1
  @visita.selecionar_um_novo_posto_visita("NAM - Shopping Nova America")
  sleep 1
  @visita.selecionar_uma_empresa_visita("19 - Dasa Sp")    
end

Então(/^as informações de Posto, Tipo de Coleta e Empresa da visita são alterados\.$/) do
  expect(@visita.retorno_visita_campo_posto_unidade_atendimento).to eq "NAM - Shopping Nova America"
  sleep 1
  expect(@visita.retorno_visita_tipo_de_coleta).to eq "Domiciliar"
  sleep 1
  expect(@visita.retorno_visita_campo_empresa).to eq "19 - Dasa Sp"
end


