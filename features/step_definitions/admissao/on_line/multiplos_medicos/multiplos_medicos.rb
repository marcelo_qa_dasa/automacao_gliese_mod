
  Então(/^clico na aba Múltiplos Médicos$/) do
    begin 
      @multiplos_medicos.clicar_aba_multiplos_medicos
     rescue Selenium::WebDriver::Error::UnhandledAlertError 
      true 
    end
  end
  
  Então(/^informo o Número do Registro "([^"]*)" na aba Múltiplos Médicos$/) do |numero_registro|
    @multiplos_medicos.inserir_numero_registro_aba_multiplos_medicos(numero_registro)
    @multiplos_medicos.clico_no_binocolo_pesquisa_crm_medico
  end
  
  Então(/^o nome do médico "([^"]*)" é carregado no campo Nome da aba Múltiplos Médicos$/) do |nome_medico|
    sleep 1
    $nome_medico = nome_medico
    expect(@multiplos_medicos.validar_descricao_campo_nome_medido($nome_medico)).to eql true
  end
  
  Então(/^clico na opção Adiciona da aba Múltiplos Médicos$/) do
    @multiplos_medicos.clicar_botao_adicionar_aba_multiplos_medicos
  end
  
  Quando(/^o médico é adicionado na seção Exames\/Médico\(s\) da aba Múltiplos Médicos$/) do
    expect(@multiplos_medicos.validar_medico_adicionado_sessao_exames($nome_medico)).to eql true
    @multiplos_medicos.clicar_botao_fechar_popup_adicionar_miltplos_medicos
  end
  
  Então(/^seleciono os Exames\/Médico\(s\)$/) do |table|
    @multiplos_medicos.seleciona_exames_para_especifico_medico(table)
  end

  Então(/^é exibida a UF da unidade selecionada no campo UF na aba Múltiplos Médicos$/) do
    @banco_dados = FuncionalidadeBancoDados.new
    posto_local = @medico.validar_unidade_de_atendimento_com_unidade_local_do_medico.slice 0 .. 2
    expect(@banco_dados.validar_uf_da_unidade_selecionada(posto_local)[4]).to eql(@multiplos_medicos.validar_exibicao_uf)
  end

  Então(/^é incrementado o número de Sangue e Total no Resumo da aba Exames$/) do |table|
    $table = table
    @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table)
  end
  
  Então(/^é exibida e mensagem de seleção da opção de mais de um crm "([^"]*)"$/) do |mensagem|
    expect(page.driver.browser.switch_to.alert.text).to eq(mensagem)
    page.driver.browser.switch_to.alert.accept
  end

  Quando(/^clico na opção Limpa da aba Múltiplos Médicos$/) do
    @multiplos_medicos.clicar_botao_limpar_aba_multiplos_medicos
  end
  
  Então(/^as informações do Médico são removidas da aba Múltiplos Médicos$/) do
    expect(@multiplos_medicos.validar_descricao_campo_nome_medido($nome_medico)).to eql false
  end

  Então(/^clico no ícone binóculo do Número de Registro da aba Múltiplos Médicos$/) do
    begin 
        @multiplos_medicos.clicar_binocolo_nome_aba_multiplos_medicos
       rescue Selenium::WebDriver::Error::UnhandledAlertError 
        true 
    end
  end
  
  Então(/^são exibidas as informações de Número de Registro e Nome do médico na aba Múltiplos Médicos$/) do
   expect(@multiplos_medicos.validar_crm_existente_medico).to eql true
   expect(@multiplos_medicos.validar_medico_existente_aba_multiplos_medicos).to eql true
  end

  Então(/^médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação aba Múltiplos Médicos$/) do
    expect(@multiplos_medicos.validar_numero_registro_medico).to eql true
  end
  
  Quando(/^clico no CRM da tela de resultado de pesquisa de médico na aba Múltiplos Médicos$/) do
    @multiplos_medicos.clicar_crm_lista_medicos_multiplos_medicos
  end
  
  Então(/^digito "([^"]*)" no campo Nome da aba Múltiplos Médicos$/) do |valor|
    @multiplos_medicos.inserir_nome_medico(valor)
  end

  Então(/^clico no ícone binóculo do Nome do Médico da aba Múltiplos Médicos$/) do
    @multiplos_medicos.clicar_binocolo_nome_medico_aba_multiplos_medicos
  end

  Então(/^são exibidas as informações de Número de Registro e Nome do médico aba Múltiplos Médicos$/) do  
    expect(@multiplos_medicos.validar_crm_existente_medico).to eql true
    expect(@multiplos_medicos.validar_medico_existente_aba_multiplos_medicos).to eql true               
  end 
  
  Então(/^o médicos já cadastrado é exibido com as informações de CRM, Médico e Situação aba Múltiplos Médicos$/) do
    expect(@multiplos_medicos.validar_valores_resultado_medico(true)).to eql true
  end