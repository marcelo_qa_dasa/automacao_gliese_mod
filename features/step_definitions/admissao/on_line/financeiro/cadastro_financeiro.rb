  Quando(/^clico na aba Financeiro$/) do
    @financeiro.clicar_no_botao_financeiro
  end
  
  Quando(/^preencho a forma de pagamento, clico no botão Salvar$/) do |table|
    $table = table
    @financeiro.preencher_forma_de_pagamento(table)
  end
  
  Então(/^valido a forma de pagamento na Relação de formas de pagamento, o Total, Total Recebido, Falta e Troco$/) do
    @financeiro.validar_preenchimento_forma_de_pagamento($table)
  end
  
  Quando(/^clico na opção N\.F\. Terceiro na aba Financeiro$/) do
    @financeiro.clicar_no_botao_nota_fiscal_terceiro
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    Capybara.page.driver.browser.manage.window.maximize     
  end
  
  Quando(/^são preenchidas as informações da nota fiscal de terceiros$/) do |table|
    $table_dados_nf, $table = table
    @financeiro.preencher_dados_nota_fiscal_de_terceiro(table)
  end
  
  Quando(/^clico no botão Salvar$/) do
    @financeiro.clicar_no_botao_salvar
  end
  
  Então(/^é exibida a mensagem de dados salvos com sucesso "([^"]*)"$/) do |msg_salvos_com_sucesso|
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_salvos_com_sucesso)
    page.driver.browser.switch_to.alert.accept
    sleep 2
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)

  end
  
  Quando(/^incluo forma de pagamento cheque com valor superior ao exame$/) do |table|
    @financeiro.preencher_forma_de_pagamento(table)
  end
  
  Então(/^Valido Mensagem de erro "([^"]*)"$/) do |mensagem_erro|
    expect(@financeiro.retorna_msg_financeiro).to eql(mensagem_erro)
  end
  
  Então(/^é exibida a mensagem de campo logradouro obrigatório "([^"]*)"$/) do |msg_nota_fiscal|
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_nota_fiscal)
    page.driver.browser.switch_to.alert.accept
  end
  
  Então(/^é exibida a mensagem de campo número obrigatório "([^"]*)"$/) do |msg_nota_fiscal|
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_nota_fiscal)
    page.driver.browser.switch_to.alert.accept
  end
  
  Então(/^é exibida a mensagem de campo bairro obrigatório "([^"]*)"$/) do |msg_nota_fiscal|
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_nota_fiscal)
    page.driver.browser.switch_to.alert.accept
  end
  
  Então(/^é exibida a mensagem de campo CNPJ obrigatório "([^"]*)"$/) do |msg_nota_fiscal|
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_nota_fiscal)
    page.driver.browser.switch_to.alert.accept
  end
  
  Então(/^são carregadas as informações de Logradouro, Número, Bairro e CNPJ\/CPF$/) do
    @financeiro.validar_informacoes_existentes_nos_campos_preenchido($table)
  end
  
  Quando(/^clico no botão Excluir$/) do
    @elemento_visivel.wait_element_visible('#botExcluir')
    @financeiro.btn_excluir.click
  end
  
  Então(/^é exibida a mensagem de dados excluídos com sucesso "([^"]*)"$/) do |msg_nota_fiscal|
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_nota_fiscal)
    page.driver.browser.switch_to.alert.accept
  end
  
  Quando(/^clico na botão Limpar$/) do
    @financeiro.btn_limpar.click
  end
  
  Então(/^as informações são removidas dos campos Logradouro, Número, Bairro e CNPJ\/CPF$/) do
    @financeiro.validar_limpar_dados_campos_nota_fiscal
  end
  
  Então(/^clico no ícone binóculo do Município$/) do
    @financeiro.btn_pesquisar_municipio.click
  end

  Então(/^municípios são exibidos com as informações de Município, Estado e Código Município\.$/) do
    expect(@financeiro.validar_campos_existentes_resultado_municipio(true)).to eq true
  end
  

  Quando(/^clico no Município da tela de resultado de pesquisa de Município "([^"]*)"$/) do |municipio|
    @municipio = municipio
    @financeiro.selecionar_municipio(municipio)
  end
  
  Então(/^são exibidas o município é carregado no campo Município$/) do
    expect(@financeiro.validar_exibicao_municipio_selecionado).to eq @municipio
  end

  Então(/^informo "([^"]*)" no campo Município$/) do |tabela_municipio|
    @financeiro.input_municipio.set(tabela_municipio)
  end
  
  Então(/^municípios que iniciam por "([^"]*)" são exibidos com as informações de Município, Estado e Código Município\.$/) do |iniciais_municipio|
    @financeiro.validar_municipios_que_iniciam_com_informacoes_solicitada(iniciais_municipio)
  end

Quando(/^clico no botão Excluir da opção de pagamento da Relação de formas de pagamento da aba Financeiro$/) do
  @financeiro.clicar_no_botao_excluir
end

Então(/^a opção de pagamento é removida da Relação de formas de pagamento da aba Financeiro e o Total Recebido, Falta e Troco são atualizados$/) do
  sleep 1
  tabela_forma_pgto = @financeiro.pegar_registros_tabela_forma_pgto
  expect(tabela_forma_pgto).to eql(1)
  @financeiro.validar_valores_resumo
end

Quando (/^seleciono alguma forma de pagamento$/) do
  @financeiro.selecionar_forma_pgto('Dinheiro')
end

E(/^clico no botão Limpa da aba Financeiro$/) do
  @valor_atual_combo = @financeiro.limpar_forma_pgto
end

Então(/^o valor selecionado no campo Forma de Pagamento muda para Selecione... na aba Financeiro$/) do
  @financeiro.validar_valor_selecionado_forma_pagamento('Selecione...')
end

Então(/^Valido se a forma de pagamento foi aceita e o troco correto$/) do
  step 'valido a forma de pagamento na Relação de formas de pagamento, o Total, Total Recebido, Falta e Troco'
end

Então (/^edito a forma de pagamento salva e valido a alteração$/) do |tabela_edicao|
  @financeiro.clicar_botao_editar_forma_pgto
  @financeiro.preencher_forma_de_pagamento(tabela_edicao)
end

E(/^clico no botão Alterar da opção de pagamento em seguida clico em Limpar e valido se a forma de pagamento não foi altera$/) do
  @financeiro.clicar_botao_editar_forma_pgto
  @financeiro.limpar_forma_pgto
end

Então(/^o valor salvo não foi alterado e o campo Forma de Pagamento muda para Selecione$/) do |tabela_edicao|
  @financeiro.validar_preenchimento_forma_de_pagamento(tabela_edicao)
  @valor_atual_combo = @financeiro.validar_valor_selecionado_forma_pagamento('Selecione...')
end

Então(/^clico na opção Desconto$/) do  
  @financeiro.clicar_no_botao_desconto
end

Quando(/^são preenchidas as informações de desconto$/) do |table|
  $table_desconto = table
  @financeiro.preencher_desconto_supervisao(table)
end


Quando(/^o Percentual de Desconto e Total com Desconto é atualizado na aba Financeiro$/) do
 sleep 1
  @financeiro.validar_desconto_aplicado_financeiro
end

Então(/^é exibida a mensagem de preenchimento do usuário "([^"]*)"$/) do |msg|
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept
end

Então(/^é exibida a mensagem de senha incorreta ou usuário sem privilégios "([^"]*)"$/) do |msg|        
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept                           
end  

Então(/^é exibida a mensagem de preenchimento de senha "([^"]*)"$/) do |msg|
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept 
end

Então(/^é exibida a mensagem de desconto percentual vazio ou igual a zero "([^"]*)"$/) do |msg|
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept 
end

Então(/^é exibida a mensagem de desconto em reais vazio ou igual a zero "([^"]*)"$/) do |msg|
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept 
end

Então(/^é exibida a mensagem de preenchimento do motivo do desconto "([^"]*)"$/) do |msg|          
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept                        
end 

Então(/^é exibida a mensagem de remoção de desconto "([^"]*)"$/) do |msg|
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.accept
end

Então(/^é exibida a mensagem de remoção de desconto "([^"]*)" e clico no botão Cancelar$/) do |msg|                            
  expect(page.driver.browser.switch_to.alert.text).to have_text(msg)
  page.driver.browser.switch_to.alert.dismiss
end                                                                                                                             
                                                                                                                                
Então(/^é exibida a popup de desconto com os campos desabilitados contendo os dados do desconto previamente cadastrado$/) do    
  @financeiro.validar_popup_desconto_desabilitado                                                
end                                                                                                                             