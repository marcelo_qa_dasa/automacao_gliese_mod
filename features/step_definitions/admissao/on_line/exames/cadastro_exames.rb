  #encoding: utf-8
  Quando(/^são exibidas as instruções do convênio Amil$/) do
    sleep 1
    expect(@convenio.validar_texto_instrucoes_do_convenio[0..-2]).to eql(@banco_dados.selecionar_instrucoes_do_convenio($nome_convenio)[0..-2])
  end


  Quando(/^preencho as informações do convênio para o convênio Amil "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", (\d+)$/) do |valor_02, data_01, select01, checked, valor_03|
    expect(@convenio.validar_campos_para_o_convenio_amil(true)).to eql true
    @data_atual = @utils.validar_data_atual
    @convenio.preencher_campos_para_o_convenio_amil(valor_02, @data_atual, select01, checked, valor_03)
  end

  Quando(/^preencho as informações do convênio para o convênio Lm\-amil\/ami "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)"$/) do |valor_02, data_01, select01, checked, valor_03|
    expect(@convenio.validar_campos_para_o_convenio_lm_amil_ami(true)).to eql true
    $data_atual = @utils.validar_data_atual
    @convenio.preencher_campos_para_o_convenio_lm_amil_ami(valor_02, $data_atual, select01, checked, $data_atual)
  end

    Quando(/^clico na aba Exames$/) do
      @exames.clicar_no_botao_exames
    end

    Quando(/^é incrementado o número de Outros e Total no Resumo da aba Exames$/) do |table|
      $table_exame = table
      @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    Então(/^todos os exames foram adicionados na Relação de exames admitidos da aba Exames$/) do
      @exames.validar_lista_de_relacao_de_exames_admitidos_na_tela_de_exames($table_exame)
    end

    Quando(/^é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames$/) do |table|
      $table_exame = table
      @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    Quando(/^é incrementado o número de Imagem e Total no Resumo da aba Exames$/) do |table|
      $table_exame = table
      @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    Quando(/^clico na aba Recipientes e clico na aba Exames$/) do
      @recipientes.clicar_no_botao_recipiente
    end

    Quando(/^é incrementado o número de Sangue e o Total no Resumo da aba Exames$/) do |table|
      $table_exame = table
      @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    Então(/^é incrementado o número de Sangue e o Total no Resumo da aba Exames e são adicionados os exames filhos dos exames de perfil previamente admitidos$/) do |table|
      $table_exame_filho = table
      @exames.validar_exames_admitidos_filhos(table)
    end

    Quando(/^preencho um material e exame não realizado na unidade$/) do |table|
      $table_exame = table
      @exames.preencher_um_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    E(/^preencho exame com massa especifica para convenio com campo CID$/) do |table|
      @exames.preencher_material_para_qualquer_exame(table)
      # step 'preencho um material e exame não realizado na unidade', table
    end

    Quando(/^clico no botão valida da aba exames$/) do
      sleep 1
    @exames.clicar_no_botao_validar_exame
    end

    Quando(/^clico no botão valida da aba exames é exibida a mensagem de autorização "([^"]*)"$/) do |mensagem_de_autorizacao|
      begin
        @exames.clicar_no_botao_validar_exame
      rescue Selenium::WebDriver::Error::UnhandledAlertError
        expect(page.driver.browser.switch_to.alert.text).to eql(mensagem_de_autorizacao)
      end
    end

    Então(/^é exibida a mensagem de exame não realizado na unidade "([^"]*)"$/) do |mensagem_exame_nao_realizado|
      expect(@exames.validar_exame_nao_realizado_na_unidade_texto).to eql(mensagem_exame_nao_realizado)
    end

    Quando(/^são exibidas as instruções do convênio Lm\-amil\/ami$/) do
      sleep 1
      expect(@convenio.validar_texto_instrucoes_do_convenio[0..-2]).to eql(@banco_dados.selecionar_instrucoes_do_convenio($nome_convenio)[0..-2])
    end

    Quando(/^preencho um material e exame que exija senha de autorização do exame$/) do |table|
      $table_exame = table
      @exames.preencher_um_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    Quando(/^informo a identificação do médico "([^"]*)", data da agenda e a hora da agenda$/) do |identificacao_do_medico|
      @exames.validar_ident_do_medico(identificacao_do_medico)
    end

    Quando(/^clico no botão Salvar da aba Exames$/) do
      @exames.clicar_no_botao_salvar_exames
    end

    Quando(/^o exame é adicionado na Relação de exames admitidos$/) do
      @exames.validar_lista_de_relacao_de_exames_admitidos_na_tela_de_exames($table_exame)
    end

    Quando(/^todos os exames e exames filhos foram adicionados na Relação de exames admitidos da aba Exames$/) do
      @exames.validar_lista_de_relacao_de_exames_admitidos_na_tela_de_exames($table_exame)
    end

    Quando(/^clico na opção Senha da aba exames$/) do
    @exames.clicar_no_botao_para_informar_a_senha
    end

    Quando(/^informo a Senha do Convênio "([^"]*)"$/) do |senha|
      @exames.preencher_senha_do_convenio_aba_exames(senha)
    end

    Quando(/^clico no botão OK na pop\-up informe a senha do convênio$/) do
      @exames.confirmar_senha_convenio_botao_ok_aba_exames
    end

    Então(/^é exibida a mensagem de senha registrada "([^"]*)"$/) do |mensagem_senha_registrada|
      expect(@exames.validar_mensagem_senha_do_convenio_registrada).to eql mensagem_senha_registrada
    end


    Quando(/^são exibidas as instruções do convênio Lm\-amilfunc$/) do
      expect(@convenio.validar_texto_instrucoes_do_convenio[0..-2]).to eql(@banco_dados.selecionar_instrucoes_do_convenio($nome_convenio)[0..-2])
    end

    Quando(/^preencho as informações do convênio para o convênio Lm\-amilfunc "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)"$/) do |valor_01, valor_02, select01, checked|
    @data_atual = @utils.validar_data_atual
    @convenio.preencher_campos_para_o_convenio_lm_amilfunc(valor_01, valor_02, select01, checked, @data_atual)
    end

    Quando(/^preencho as informações do convênio para o convênio br_amilimagem "([^"]*)", "([^"]*)", "([^"]*)"$/) do |valor_02, select01, checked|
      data_atual = @utils.validar_data_atual
      @convenio.preencher_campos_para_o_convenio_br_amilimagem(valor_02, select01, checked, data_atual)
    end


    Quando(/^preencho um material e exame que não exibe o campo senha$/) do |table|
    $table_exame = table
    @exames.preencher_um_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    Então(/^a opção senha não existe na Relação de exames admitidos$/) do
      expect(@exames.validar_nao_exibicao_senha_convenio_aba_exames).to eql false
    end

    Quando(/^preencho as informações do convênio para o convênio Lm\-amil\/ami "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)"$/) do |valor_02, data_atual, select01, checked|
      expect(@convenio.validar_campos_para_o_convenio_lm_amil_ami(true)).to eql true
      $data_atual = @utils.validar_data_atual
      @convenio.preencher_campos_para_o_convenio_lm_amil_ami(valor_02, $data_atual, select01, checked, $data_atual)
    end

    Quando(/^clico na aba Exame$/) do
      @exames.clicar_no_botao_exames
    end

    Quando(/^preencho um material e exame que exibe o campo senha$/) do |table|
      $table_exame = table
      @exames.preencher_um_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    Quando(/^preencho um material e exame de sexo masculino a paciente do sexo feminino$/) do |table|
      $table_exame = table
      @exames.preencher_um_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    Então(/^é exibida a mensagem de exame masculino "([^"]*)"$/) do |mensagem_exame_sexo_masculino|
    expect(@exames.validar_mensagem_exame_so_e_feito_para_sexo_masculino).to eql mensagem_exame_sexo_masculino
    end

    Quando(/^clico no CRM da tela de resultado de pesquisa de médico da empresa Alta$/) do
      @medico.clicar_crm_lista_medicos($massa_medicos_tabela)
    end

    Quando(/^são exibidas as instruções do particular$/) do
      expect(@convenio.validar_texto_instrucoes_do_convenio[0..-2]).to eql(@banco_dados.selecionar_instrucoes_do_convenio($nome_convenio)[0..-2])
    end


    Quando(/^preencho as informações do particular Al\-partrj "([^"]*)", "([^"]*)", "([^"]*)"$/) do |valor_01, valor_02, checked|
      @convenio.preencher_campos_para_o_convenio_al_partrj_50(valor_01, valor_02, checked)
    end

    Quando(/^é incrementado o número de Sangue, Imagem e o Total no Resumo da aba Exames$/) do |table|
      $table_exame = table
      @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end


    Quando(/^preencho um material e exame de RDI$/) do |table|
      $table_exame = table
      @exames.preencher_um_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
    end

    Então(/^é exibida a mensagem de não permisão de exame de imagem de AC e RDI "([^"]*)"$/) do |mensagem_exame_AC_e_RDI|
      expect(@exames.validar_mensagem_exame_so_e_feito_para_sexo_masculino).to eql mensagem_exame_AC_e_RDI
    end


  Dado(/^Preencho as informações do convênio$/) do |table|
    convenio = table.hashes[0]['Convenio']
    Amil_Codigo_Associado = table.hashes[0]['Amil_Codigo_Associado']
    Amil_Data_Solicitacao = table.hashes[0]['Amil_Data_Solicitacao']
    Amil_Nome_Plano = table.hashes[0]['Amil_Nome_Plano']
    Amil_Plano_Empresa = table.hashes[0]['Amil_Plano_Empresa']
    Amil_Quantidade_Etiquetas = table.hashes[0]['Amil_Quantidade_Etiquetas']
    @convenio.preencher_o_campo_convenio_aba_convenio(convenio)
    sleep 1
    @convenio.clicar_no_botao_selecionar_convenio
    sleep 1
    expect(@convenio.validar_texto_instrucoes_do_convenio[0..-2]).to eql(@banco_dados.selecionar_instrucoes_do_convenio(convenio)[0..-2])
    expect(@convenio.validar_campos_para_o_convenio_amil(true)).to eql true
    @convenio.preencher_campos_para_o_convenio_amil(Amil_Codigo_Associado, Amil_Data_Solicitacao, Amil_Nome_Plano, Amil_Plano_Empresa, Amil_Quantidade_Etiquetas)

  end

  Então(/^informo "([^"]*)" no campo Material da aba Exames$/) do |material|
    @exames.preencher_material_aba_exames(material)
  end

  Então(/^informo "([^"]*)" no campo Exames da aba Exames$/) do |exame|
    @exames.preencher_campo_exame_na_aba_exame(exame)
  end

  Quando(/^clico no botão Limpa da aba Exames$/) do
    @exames.clicar_no_botao_limpar_aba_exames
  end

  Então(/^a informação do exame é removida do campo Exames da aba Exames$/) do
    @exames.validar_campo_exame_vazio
  end

  Então(/^clico na opção Pesquisa de Exame da aba Exames$/) do
    @exames.clicar_botao_pesquisar_exame_aba_exame
  end

  Então(/^informo o exame "([^"]*)" no campo Exame da popup Consulta Exame$/) do |exame|
    @exames.preencher_campo_exame_popup_exame(exame)
  end

  Então(/^clico no ícone binóculo do campo Exame da popup Consulta Exame$/) do
    @exames.clicar_lupa_pesquisa_popup_exame_aba_exame
    sleep 1
  end

  Então(/^são exibidas as informações de Mnemônico, Nome, Sinonimia, Status Convênio, AMB e TUS
  ado de pesquisa de exame$/) do |arg1|
    expect(@exames.div_resultado_pesquisa_popup_aba_exames.visible?).to be true
  end

  Quando(/^clico no mnemônico "([^"]*)" da tela de resultado de pesquisa de exame$/) do |arg1|
  @exames.clicar_elemento_hemograma_pesquisa_popup_aba_exames
  sleep 2
  end

  Então(/^é exibido o sub\-mnemônico "([^"]*)" no campo Exames da aba Exames$/) do |valor|
    expect(@exames.verificar_conteudo_exame_aba_exames).to eql valor
  end

  Então(/^são exibidas as informações de Mnemônico, Nome, Sinonimia, Status Convênio, AMB e TUSS dos exames que contenham "([^"]*)" na coluna Sinonimia da popup de resultado de pesquisa de exame$/) do |arg1|
    expect(@exames.validar_campos_resultado_da_pesquisa_exames_popup_aba_pesquisa(true)).to eql true
  end


  Então(/^informo o mnemônico "([^"]*)" no campo Mnemônico da popup Consulta Exame$/) do |valor|
    @exames.inseir_valor_mnemonico_popup_aba_exames(valor)
  end

  Então(/^clico no ícone binóculo do campo Mnemônico da popup Consulta Exame$/) do
    @exames.clicar_binoculo_mnemonico_popup_aba_exames
    sleep 2
  end

  Então(/^são exibidas as informações de Mnemônico, Nome, Sinonimia, AMB e TUSS dos exames que contenham "([^"]*)" na coluna Mnemônico da popup de resultado de pesquisa de exame$/) do |arg1|
    expect(@exames.validar_campos_resultado_da_pesquisa_mneumonico_popup_aba_pesquisa(true)).to eql true
  end

  Quando(/^clico no Sinonimia "([^"]*)" da tela de resultado de pesquisa de exame$/) do |sinonimia|
    @exames.clicar_elemento_hsa_pesquisa_campo_mnemonico_popup_aba_exames(sinonimia)
  end

  Então(/^é incrementado o número Sangue, Urina, Outros e o Total no Resumo da aba Exames$/) do |table|
    $table_exame = table
    @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
  end

  Então(/^todos os exames são adicionados na Relação de exames admitidos da aba Exames$/) do
    @exames.validar_lista_de_relacao_de_exames_admitidos_na_tela_de_exames($table_exame)
  end

  Então(/^clico na opção Excluir Exames da aba Exames$/) do
    begin
      @exames.clicar_botao_exluir_todos_exames_aba_exames
    rescue Selenium::WebDriver::Error::UnhandledAlertError
      true
    end
  end

  Então(/^é exibida a mensagem de confirmação de exclusão de todos os exames "([^"]*)"$/) do |mensagem_excluir_todos_exames|
      expect(page.driver.browser.switch_to.alert.text).to eql(mensagem_excluir_todos_exames)
  end

  Então(/^todos os exames são removidos da Relação de exames admitidos e é decrementado o número de Sangue, Urina, Outros e o Total no Resumo da aba Exames$/) do
    expect(@exames.validar_remocao_todos_exames_aba_exames(false)).to eql false
  end


  Então(/^é incrementado o número de exames de análises clínicas admitidos no Resumo da aba Exames$/) do |table|
    $table_exame = table
    @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
  end

  Então(/^clico na opção DayLab para Todos Exames da aba Exames$/) do
    begin
      @exames.clicar_botao_daylab_todos_exames_aba_exames
    rescue Selenium::WebDriver::Error::UnhandledAlertError
      true
    end
  end

  Então(/^é exibida a mensagem de confirmação de inclusão de daylab para todos os exames "([^"]*)"$/) do |mensagem_daylab_todos_exames_aba_exames|
    expect(page.driver.browser.switch_to.alert.text).to eql(mensagem_daylab_todos_exames_aba_exames)
  end

  Então(/^todos os exames são marcados na coluna Day Lab e com "([^"]*)" na coluna DL na Relação de exames admitidos$/) do |arg1|
    expect(@exames.validar_todos_daylabs_preenchidos_aba_exames($material.count)).to eql true
  end

  Então(/^clico na opção Alterar do exame "([^"]*)" na Relação de exames admitidos$/) do |nome_exame|
    sleep 1
    @exames.clicar_botao_editar_exames_aba_exames(nome_exame)
  end

  Então(/^informo a quantidade de lâminas "([^"]*)"$/) do |valor|
    @exames.inserir_valore_quantidade_laminas_aba_exames(valor)
  end

  Então(/^informo o material complementar "([^"]*)"$/) do |valor|
  @exames.selecionar_valor_combo_materiais_complementares(valor)
  end

  Então(/^o exame foi alterado com sucesso$/) do
  expect(@exames.validar_edicao_nome_exame_complemento).to eql("17-Esfregaco Cervico - Vaginal")
  end

  Então(/^clico na opção excluir do exame na Relação de exames admitidos$/) do
      begin
        @exames.clicar_no_botao_excluir_aba_exames
      rescue Selenium::WebDriver::Error::UnhandledAlertError
        true
      end
  end

  Então(/^é exibida a mensagem de confirmação de exclusão de exame "([^"]*)"$/) do |mensagem_Excluir_Exame|
    expect(page.driver.browser.switch_to.alert.text).to eql(mensagem_Excluir_Exame)
  end

  Então(/^o exame é removido da Relação de exames admitidos e é decrementado o número de Sangue e Total no Resumo da aba Exames$/) do
    expect(@exames.validar_remocao_todos_exames_aba_exames(false)).to eql false
  end


  Então(/^informo o mnemônico "([^"]*)" no campo Exames da aba Exames$/) do |mnemonico|
    @exames.inseir_valor_mnemonico_na_aba_exames(mnemonico)
  end

  Então(/^é exibida a mensagem de exame feminino "([^"]*)"$/) do |mensagem|
    expect(@exames.validar_mensagem_mensagem_de_exame_feminino).to eql(mensagem)
  end

  Então(/^é exibida a mensagem de exame inativo "([^"]*)"$/) do |mensagem|
    expect(@exames.validar_mensagem_mensagem_de_exame_inativo).to eql(mensagem)
  end

  Então(/^clico no botão valida da aba exames é exibida a mensagem de exame já admitido "([^"]*)"$/) do |mensagem_de_autorizacao|
    begin
      @exames.clicar_no_botao_validar_exame
    rescue Selenium::WebDriver::Error::UnhandledAlertError
      expect(page.driver.browser.switch_to.alert.text).to eql(mensagem_de_autorizacao)
    end
  end

  Quando(/^são exibidas as instruções do convênio particular$/) do
    sleep 2
    expect(@convenio.validar_texto_instrucoes_do_convenio).to eql(@banco_dados.selecionar_instrucoes_do_convenio($nome_convenio))
  end

  Quando(/^preencho as informações do convênio para o convênio particular$/) do
    @exames.clicar_input_plano_particular_aba_convenio
  end

  Então(/^o exame é adicionado na Relação de exames admitidos para HSA$/) do |table|
    @exames.validar_lista_de_relacao_de_exames_admitidos(table)
  end

  Então(/^os campos Hora da Coleta e Hora do Recebimento informam a hora atual$/) do
    @data_atual = @utils.validar_data_atual
    @hora_atual = @utils.validar_hora_atual
  @exames.validar_campos_hora_da_coleta_hora_do_recebimento_informam_a_hora_atual(@data_atual, @hora_atual, @hora_atual)
  end

  Quando(/^seleciono a Marca e a Unidade$/) do |table|
    nome_empresa = table.hashes[0]['Marca']
    nome_unidade = table.hashes[0]['Unidade']
    $numero_empresa = @banco_dados.empresa_codigo(nome_empresa)
    @convenio.selecionar_empresa($numero_empresa)
    @unidade.selecionar_unidade_de_atendimento.select(nome_unidade)
    @unidade.btn_escolher_empresa.click
    @elemento_visivel.wait_element_visible('.empresa')
  end

  Quando(/^preencho os campos Data_Nascimento e CIP$/) do |table|
    data_nasc = table.hashes[0]['Data_Nascimento']
    cip = table.hashes[0]['CIP']
    @unidade.validar_informar_atendimento
    @convenio.preencher_campo_data_nascimento_tela_paciente(data_nasc)
    @convenio.preencher_campo_cip_paciente(cip)
    sleep 1
    @admissao.clicar_para_exibir_consulta_por_cip
  end

  Quando(/^clico na opção Bloqueio da aba Exames$/) do
    @exames.clicar_no_botao_bloquear_aba_exames
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    Capybara.page.driver.browser.manage.window.maximize
  end

  Então(/^é exibido o CIP e o número da visita no do paciente da tela de Bloqueio$/) do
    expect(@exames.validar_texto_cip_existente_tela_de_bloqueio).to eql($cip)
    expect(@exames.validar_texto_visita_existente_tela_de_bloqueio).to eql($numero_visita)
  end

  Então(/^é exibido o campo Num Sequencial da tela de Bloqueio$/) do
    @elemento_visivel.wait_element_visible("#Bloqueio_numSeq")
    @exames.clicar_para_exibir_os_outros_campos
    @elemento_visivel.wait_element_visible("#Bloqueio_lovBlq")
  end

  Então(/^preencho os campos, tipo de bloqueio, prazo limite e observação com dados validos$/) do |table|
    tipo_do_bloqueio = table.hashes[0]['tipo_do_bloqueio']
    descricao_tipo_bloqueio = table.hashes[0]['descricao_tipo_bloqueio']
    @exames.preencher_campos_tela_bloqueio_tipo_de_bloqueio_prazo_limite_e_observacao(tipo_do_bloqueio, Faker::Lorem.sentence)
    expect(@exames.validar_descricao_tipo_de_bloqueio).to eql(descricao_tipo_bloqueio)
  end

  Então(/^a lista de exames é exibida na tela de Bloqueio informando um número sequencial começando em "([^"]*)" no campo Ordem$/) do |numero|
    @exames.validar_lista_de_exames_numero_sequencial(numero)
  end

  Então(/^marco a opção Bloqueio da tela de bloqueio$/) do |table|
    mnemonico_exame = table.hashes[0]['Mnemônico_Exame']
    @exames.bloqueio_um_exame_check(mnemonico_exame)
  end

  Então(/^clico na opção Cadastrar da tela de Bloqueio$/) do
    begin
      @exames.clicar_no_botao_cadastrar_tela_bloqueio
    rescue Selenium::WebDriver::Error::UnhandledAlertError
    true
    end
  end

  Então(/^é exibida a mensagem Confirma Bloqueio "([^"]*)"$/) do |confirma_bloqueio|
    expect(page.driver.browser.switch_to.alert.text).to eql(confirma_bloqueio)
  end

  Então(/^é exibida a mensagem Deseja imprimir o protocolo "([^"]*)"$/) do |imprimir_o_protocolo|
    expect(page.driver.browser.switch_to.alert.text).to eql(imprimir_o_protocolo)
  end


  Então(/^é exibido o protocolo do bloqueio$/) do
    expect(@utils.carregar_pdf("protBlq.pdf").raw_content).to include $cip
    expect(@utils.carregar_pdf("protBlq.pdf").raw_content).to include $nome_convenio
    expect(@utils.carregar_pdf("protBlq.pdf").raw_content).to include @utils.validar_data_atual_com_barras
    expect(@utils.carregar_pdf("protBlq.pdf").raw_content).to include $empresa
  end

  Então(/^é decrementado o número de Urina e o Total no Resumo da aba Exames e apenas os demais exames permanecem na Relação de exames admitidos da aba Exames$/) do |table|
    page.driver.browser.close
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)
    @exames.validar_lista_de_relacao_de_exames_admitidos_decrementados_bloqueio(table)
  end

  Então(/^é decrementado o número de Outros e o Total no Resumo da aba Exames$/) do |table|
    page.driver.browser.close
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)
    @exames.validar_lista_de_relacao_de_exames_admitidos_decrementados_bloqueio(table)
  end

  Então(/^é decrementado o número de Imagem e o Total no Resumo da aba Exames e apenas os demais exames permanecem na Relação de exames admitidos da aba Exames$/) do |table|
    page.driver.browser.close
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)
    @exames.validar_lista_de_relacao_de_exames_admitidos_decrementados_bloqueio(table)
  end

  Quando(/^é incrementado o número de Imagem e o Total no Resumo da aba Exames$/) do |table|
    $table_exame = table
    @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
  end

  Quando(/^clico na opção Senha da aba exames é exibida a mensagem de autorização "([^"]*)"$/) do |confirma_bloqueio|
    begin
      @exames.clicar_no_botao_salvar_exames
    rescue Selenium::WebDriver::Error::UnhandledAlertError
      expect(page.driver.browser.switch_to.alert.text).to eql(confirma_bloqueio)
    end
  end

  Quando(/^clico no botão OK é exibida a mensagem de autorização "([^"]*)"$/) do |confirma_bloqueio|
    begin
      page.driver.browser.switch_to.alert.accept
    rescue Selenium::WebDriver::Error::UnhandledAlertError
      expect(page.driver.browser.switch_to.alert.text).to eql(confirma_bloqueio)
    end
  end

  Quando(/^confirmo o procedimento de atendimento LMFPVTC(\d+)IM$/) do |arg1|
    @exames.validar_informe_de_exames
  end


  Dado(/^é incrementado o número Sangue e o Total no Resumo da aba Exames$/) do |table|
    $table_exame = table
    @exames.preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame($table_exame)
  end

  Dado(/^é exibido o CIP no campo CIP da tela de Bloqueio$/) do
  @exames.validar_campo_cip_tela_bloqueio_aba_exames
  end

  Dado(/^é exibida o número da visita no campo Visita da tela de Bloqueio$/) do
  @exames.validar_numero_visita_bloqueio_aba_exames
  end

  Dado(/^é exibido "([^"]*)" no campo Num\. Sequencial da tela de Bloqueio$/) do |valor|
    @elemento_visivel.wait_element_visible("#Bloqueio_numSeq")
    @exames.clicar_para_exibir_os_outros_campos
    @elemento_visivel.wait_element_visible("#Bloqueio_lovBlq")

  end

  Quando(/^informo "([^"]*)" no campo Tipo de Bloqueio da tela de Bloqueio$/) do |valor|
  @exames.inserir_valor_tipo_bloqueio_aba_exames(valor)
  end

  Então(/^é exibida a mensagem de bloqueio realizado pelo faturamento "([^"]*)" na tela de Bloqueio$/) do |mensagem|
    expect(@exames.validar_mensagem_tela_bloqueio(mensagem)).to eql true
  end

  Então(/^é exibida a mensagem de código inválido "([^"]*)" na tela de Bloqueio$/) do |mensagem|
    expect(@exames.validar_mensagem_tela_bloqueio(mensagem)).to eql true
  end

  Então(/^a descrição do tipo de bloqueio "([^"]*)" é exibida na tela de Bloqueio$/) do |mensagem|
    page.find("#Bloqueio_lovBlq").send_keys(:tab)
    expect(@exames.validar_descricao_bloqueio_aba_exames(mensagem)).to eql true
  end

  Então(/^informo o prazo limite no campo Prazo Limite da tela de Bloqueio$/) do
      @exames.preencher_prazo_limite_tela_de_bloqueio
  end

  Então(/^insiro uma observação no campo Observação da tela de Bloqueio$/) do
    @exames.input_observacao_tela_de_bloqueio.set('teste')
  end

  Quando(/^clico na opção Limpa da tela de Bloqueio$/) do
      @exames.clicar_botao_limpar_popup_bloqueio_aba_exames
  end

  Então(/^as informações Num\. Sequencial, Tipo de Bloqueio, Prazo Limite, Observação e a lista de exames são removidas da tela de Bloqueio$/) do
      expect(@exames.validar_campos_desabilitados_popup_bloqueio_aba_exames(false)).to eql true
  end

  Quando(/^clico na opção Novo Bloqueio da tela de Bloqueio$/) do
    @exames.clicar_botao_novo_bloqueio_popup_bloqueio_aba_exames
  end

  Então(/^as informações Tipo de Bloqueio, Prazo Limite, Observação e a lista de exames são removidas da tela de Bloqueio$/) do
      expect(@exames.validar_campos_desabilitados_popup_bloqueio_aba_exames(false)).to eql true
  end

  Então(/^é exibida a mensagem de bloqueio do tipo balcão "([^"]*)"$/) do |mensagem|
    expect(page.driver.browser.switch_to.alert.text).to eql(mensagem)
  end

  Então(/^clico na opção Bloqueio da aba Exames é exibida a mensagem de bloqueio do tipo balcão "([^"]*)"$/) do |mensagem|
      begin
        @exames.clicar_no_botao_bloquear_aba_exames
      rescue Selenium::WebDriver::Error::UnhandledAlertError
        expect(page.driver.browser.switch_to.alert.text).to eql(mensagem)
        page.driver.browser.switch_to.alert.accept
      end
  end

  Então("clico na opção excluir do exame selecionado na Relação de exames admitidos") do |table|
    begin
      @exames.clicar_botao_excluir_exames_selecionado_aba_exames(table)
    rescue Selenium::WebDriver::Error::UnhandledAlertError
      true
    end
    end

  Então("o exame de perfil e todos os seus exames filhos são removidos da Relação de exames admitidos e é decrementado o número de Sangue e Total no Resumo da aba Exames") do
    expect(@exames.validar_remocao_todos_exames_aba_exames(false)).to eql false
  end

  Então(/^é incrementado o número de Imagem e o Total no Resumo da aba Exames e são adicionados os exames filhos dos exames de perfil previamente admitidos$/) do |table|
    $table_exame_filho = table
    @exames.validar_exames_admitidos_filhos(table)
  end
