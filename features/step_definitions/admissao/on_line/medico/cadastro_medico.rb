  Dado(/^crio uma nova Visita$/) do
    @visita.clicar_botao_nova_visita
    @visita.wait_element_validar_numero_visita_existente
    $numero_visita = @visita.retornar_valor_numero_visita_atual
  end

  Dado(/^clico na aba Médico$/) do
    @medico.clicar_na_aba_medico
  end

  Então(/^é exibida a UF da unidade selecionada no campo UF$/) do
    $posto_local = @medico.validar_unidade_de_atendimento_com_unidade_local_do_medico.slice 0 .. 2
    expect(@banco_dados.validar_uf_da_unidade_selecionada($posto_local)[4]).to eql(@medico.validar_exibicao_uf_da_unidade_selecionada)
  end

  Então(/^informo os campos do registro da aba Médico$/) do
  @medico.preencher_crm_medico($massa_medicos['crm'])
  end

  Quando(/^pressiono enter na aba Médico$/) do
    @medico.pressionar_tecla_enter_exibir_medico
  end

  Então(/^são exibidas as informações de Nome e Telefones do médico\.$/) do
    sleep 1
    aggregate_failures do
    expect(@medico.validar_valor_campo_uf_medico).to eq true
    expect(@medico.validar_valor_campo_crm_medico).to eq true
    expect(@medico.validar_valor_campo_numero_de_crm_medico).to eq true
    expect(@medico.validar_nome_medico).to eq true
    end
  end
  Então(/^informo os campos do registro da aba Médico com "([^"]*)" no campo Número de Registro$/) do |numero_crm_invalido|
    @medico.preencher_crm_medico(numero_crm_invalido)
  end

  Então(/^é exibida a mensagem de médico inexistente "([^"]*)"$/) do |mensagem_crm_invalido|
  sleep 1
  expect(@medico.validar_mensagem_crm_inexistente).to eql(mensagem_crm_invalido)
  end

  Então(/^clico no ícone binóculo do Número de Registro$/) do
    @medico.clicar_no_botao_pesquisa_por_crm
  end

  Então(/^é exibida a mensagem de resultado de pesquisa "([^"]*)"$/) do |numero_resultado_pesquisa|
    wait_accept_alert
    expect(page.driver.browser.switch_to.alert.text).to eql(numero_resultado_pesquisa)
  end

  Então(/^clico no botão OK$/) do
    wait_accept_alert
    page.driver.browser.switch_to.alert.accept
  end
  Então(/^médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação na consulta por nome\.$/) do
    @elemento_visivel_page.wait_element_visible('#resultadonomeMedico')
    aggregate_failures do
    expect(@medico.validar_lista_situacao_medico_pesquisa_por_nome).to eql true
    expect(@medico.validar_lista_nome_do_medico_pesquisa_por_nome).to eql true
    expect(@medico.validar_lista_crm_medico_pesquisa_por_nome).to eql true
    end
  end

  Então(/^médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação\.$/) do
    aggregate_failures do
    expect(@medico.validar_lista_situacao_medico).to eql true
    expect(@medico.validar_lista_nome_do_medico).to eql true
    expect(@medico.validar_lista_crm_medico).to eql true
    end
  end

  Quando(/^clico no CRM da tela de resultado de pesquisa de médico na consulta por nome$/) do
    @medico.clicar_crm_lista_medicos_pesquisa_por_nome
  end
  Quando(/^clico no CRM da tela de resultado de pesquisa de médico$/) do
    @medico.clicar_crm_lista_medicos($massa_medicos_tabela)
  end

  E(/^seleciono daso do Medico sendo a uf "([^"]*)", selecione pelo "([^"]*)" e o codigo "([^"]*)"$/) do |uf, tipo_pesquisa, codigo_crm|
    @medico.seleciona_uf_medico_por(uf)
    @medico.selecionar_medico_por(tipo_pesquisa)
    @medico.preencher_crm_medico(codigo_crm)
  end

  Quando(/^clico no CRM de médico definitivo da tela de resultado de pesquisa de médico$/) do
    @medico.clicar_crm_lista_medicos($massa_medicos_definitivo['crm'])
  end

  Então(/^são exibidas as informações de Número de Registro, Nome e Telefones do médico definitivo\.$/) do
    aggregate_failures do
    expect(@medico.validar_valor_campo_uf_medico).to eq true
    expect(@medico.validar_valor_campo_crm_medico).to eq true
    expect(@medico.validar_valor_campo_numero_de_crm_medico_texto).to include($massa_medicos_definitivo['crm'])
    expect(@medico.validar_nome_medico_texto).to include($massa_medicos_definitivo['nome'])
    end
  end
  Então(/^são exibidas as informações de Número de Registro, Nome e Telefones do médico\.$/) do
    aggregate_failures do
    expect(@medico.validar_valor_campo_uf_medico).to eq true
    expect(@medico.validar_valor_campo_crm_medico).to eq true
    expect(@medico.validar_valor_campo_numero_de_crm_medico_texto).to include($massa_medicos['crm'])
    expect(@medico.validar_nome_medico_texto).to include($massa_medicos['nome'])
    end
  end
  Então(/^são exibidas as informações de Número de Registro, Nome e Telefones do médico quando selecionado pesquisa por nome\.$/) do
    aggregate_failures do
    expect(@medico.validar_valor_campo_uf_medico).to eq true
    expect(@medico.validar_valor_campo_crm_medico).to eq true
    expect(@medico.validar_valor_campo_numero_de_crm_medico_texto).to include("74362")
    expect(@medico.validar_nome_medico_texto).to include($nome_medico)
    end
  end
  Então(/^digito "([^"]*)" no campo Nome da aba Médico$/) do |nome_medico|
    $nome_medico = nome_medico
    @medico.preencher_nome_medico(nome_medico)
  end

  Então(/^clico no ícone binóculo do Nome da aba Médico$/) do
    @medico.clicar_para_pesquisar_por_nome_de_medico
  end

  Então(/^clico na opção Cadastro da aba Médico$/) do
    @medico.clicar_no_botao_cadastrar_medico
  end

  Então(/^é exibido o Cadastro de Profissional Solicitante$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    @elemento_visivel.wait_element_visible('#formCep')
    expect(@medico.tela_cadastro_de_procissional_solicitante.text).to eql("Cadastro de Profissional Solicitante")
  end

  Então(/^digito o código do médico no cadastro de profissional solicitante$/) do
    @medico.campo_cadastro_paciente_codico_crm.send_keys($massa_medicos['crm'])
  end

  Quando(/^mudo o foco para outro campo$/) do
    @medico.mudar_foco_para_outro_campo_texto_cep.click
  end

  Então(/^são carregados os dados do médico\.$/) do
    sleep 1
    aggregate_failures do
    expect(@medico.campo_nome_tela_cadastro.value).to eq($massa_medicos['nome'])
    expect(@medico.retorno_campo_selecao_uf_medico_tela_cadastro).to eq($massa_medicos['uf'])
    expect(@medico.retorno_campo_selecao_sexo_medico_tela_cadastro).to eq($massa_medicos['sexo'])
    end
  end
  Então(/^mudo o foco para outro campo no cadastro de profissional solicitante$/) do
    @medico.mudar_foco_para_outro_campo_texto_cep.click
  end

  Então(/^altero os campos de cadastro do médico no cadastro de profissional solicitante$/) do
    @medico.editar_medico_campos_da_tela_cadastro_profissional
  end

  Quando(/^clico na opção Cadastrar do Cadastro de Profissional Solicitante$/) do
    page.driver.browser.manage.window.resize_to(1000,720)
    @medico.btn_cadastrar_medico.click
  end

  Então(/^é exibida a mensagem de alteração de médico "([^"]*)"$/) do |msg_cadastro_com_sucesso|
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_cadastro_com_sucesso)
  end

  Então(/^são exibidas as informações de UF, Conselho, Número de Registro, Nome e Telefones do médico associado a visita\.$/) do
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)
    sleep 1
    expect(@medico.retorno_numero_telefone_medico_tela_pesquisa.gsub!(/\W+/, '')).to include($novo_numero_telefone_medico)
  end

  Então(/^removo o código$/) do
    sleep 1
    @medico.remover_infomacoes_campo_codigo_crm_medico
  end

  Quando(/^clico no botão Cadastrar$/) do
    page.driver.browser.manage.window.resize_to(1000,720)
    @medico.btn_cadastrar_medico.click
  end

  Então(/^é exibida a mensagem de código do profissional inválido "([^"]*)"$/) do |msg_codigo_profissional|
    sleep 1
    begin
      expect(page.driver.browser.switch_to.alert.text).to have_text(msg_codigo_profissional)
      page.driver.browser.switch_to.alert.accept
    rescue Exception => e
      true
    end
    end

  Então(/^removo o nome$/) do
    sleep 1
    @medico.remover_infomacoes_campo_nome_medico
  end

  Então(/^é exibida a mensagem de nome do médico inválido "([^"]*)"$/) do |msg_nome_medico|
    sleep 1
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_nome_medico)
    page.driver.browser.switch_to.alert.accept
  end

  Então(/^removo o DDD$/) do
    sleep 1
    @medico.remover_infomacoes_campo_ddd_medico
  end

  Então(/^é exibida a mensagem de DDD inválido "([^"]*)"$/) do |msg_ddd_invalido|
    sleep 1
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_ddd_invalido)
    page.driver.browser.switch_to.alert.accept
  end

  Então(/^removo o cliente preferencial$/) do
    @elemento_visivel.wait_element_visible('#clientePrefer')
    @medico.remover_infomacoes_selecionadas_cliente_prefecencial
  end

  Então(/^é exibida a mensagem de seleção de cliente preferencial "([^"]*)"$/) do |msg_cliente_preferencial|
    sleep 1
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_cliente_preferencial)
    page.driver.browser.switch_to.alert.accept
  end

  Então(/^digito "([^"]*)" no campo telefone$/) do |numero_telefone_invalido|
    @medico.cadastrar_numero_telefone_invalido_medico(numero_telefone_invalido)
  end

  Quando(/^clico no botão \+ do telefone$/) do
    @medico.btn_adcionar_novo_telefone_tela_cadastro.click
  end

  Então(/^é exibida a mensagem de número inválido "([^"]*)"$/) do |msg_numero_invalido|
    sleep 1
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_numero_invalido)
    sleep 1
    page.driver.browser.switch_to.alert.accept
  end

  Então(/^digito "([^"]*)" no campo fax$/) do |numero_fax_invalido|
    @medico.cadastrar_numero_fax_invalido_medico(numero_fax_invalido)
  end

  Quando(/^clico no botão \+ do fax$/) do
    @medico.btn_adcionar_novo_fax_tela_cadastro.click
  end

  Então(/^informo o CRM do médico com cadastro definitivo no campo Número de Registro da aba médico$/) do
    @medico.preencher_crm_medico($massa_medicos_definitivo['crm'])
  end

  Então(/^os valores dos campos da aba Médico \(Número de Registro, Nome e Telefones\) são removidos e é exibida a mensagem de profissional com cadastro definitivo "([^"]*)"$/) do |msg_cadastro_definitivo|
    sleep 1
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_cadastro_definitivo)
  end

  Então(/^os valores dos campos do Cadastro de Profissional Solicitante são removidos$/) do
    aggregate_failures do
    expect(@medico.validar_remover_infomacoes_campo_codigo_crm_medico).to eql false
    expect(@medico.validar_remover_infomacoes_campo_nome_medico).to eql false
    expect(@medico.validar_remover_infomacoes_selecionadas_cliente_prefecencial).to eql true
    end
  end

  Quando(/^clico no botão Limpa do Cadastro de Profissional Solicitante são removidos$/) do
    @medico.btn_limpar_tela_cadastro.click
  end

  Então(/^o botão Cadastrar é exibido\.$/) do
    @medico.validar_exibicao_botao_cadastrar_tela_cadastro_profissional
  end

  Quando(/^clico no botão Limpa da aba Médico$/) do
    @medico.clicar_botao_limpar_registros_de_medico
  end

  Então(/^as informações do Médico são removidas$/) do
    aggregate_failures do
    expect(@medico.validar_valor_campo_numero_de_crm_medico_nao_existente_na_tela).to eq true
    expect(@medico.validar_nome_medico_nao_existente_na_tela).to eq true
    end
  end

  Quando(/^informo o CRM "([^"]*)" do medico e realizo a pesquisa$/) do |crm|  
    $crm_medico_atendimento = crm
    @medico.preencher_crm_medico(crm) 
    @medico.wait_element_validar_preenchimento_medico
    $nome_Medico_atendimento = @medico.result_nome_medico_atendimento_paciente
  end