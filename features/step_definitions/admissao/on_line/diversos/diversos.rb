Quando(/^clico na aba Diversos$/) do
  @diversos = FuncionalidadeAdmissaoDiversos.new
  @diversos.clicar_no_botao_diversos
end

Então(/^são preenchidas as informações diversas dos exames$/) do |tabela|
  $tabela_diversos = tabela
  @diversos.preencher_as_informacoes_diversas_dos_exames(tabela)
end

Quando(/^clico na opção Senha Recurso da aba exames$/) do
  @exames.clicar_botao_senha_aba_exames
  page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
  Capybara.page.driver.browser.manage.window.maximize
end

Quando(/^valido a exibição do exame na pop-up Informe a Senha Recurso do exame$/) do
@exames.valida_existencia_popup_senha_convenio_aba_exames
end

Quando(/^informo a Senha "([^"]*)" na pop\-up Informe a Senha Recurso do exame$/) do |valor|
  @exames.inserir_valor_senha_convenio_popup_senha_aba_exames(valor)
end

Quando(/^clico no botão Salvar na pop-up Informe a Senha Recurso do exame$/) do
  @exames.clicar_botao_salvar_senha_convenio_popup_senha_aba_exames
end

Quando("clico no botão Limpa da aba Diversos") do
  @diversos.clicar_botao_limpar_aba_diversos
end

Então(/^o meio do resultado adicional é removido do campo Meio do resultado da aba Diversos e o "([^"]*)" permanece$/) do |valor|
  @diversos.validar_campos_aba_diversos(valor)
end

Quando(/^clico no botão Remover do Meio do resultado adicional da aba Diversos$/) do
  @diversos.clicar_botao_remover_resultado_adicional
end

Então(/^o meio do resultado adicional é removido do campo Meio do resultado da aba Diversos$/) do
  expect(@diversos.validar_existencia_campo_preenchimento(false)).to be false
end

Quando(/^clico no botão Remover do Meio do resultado "([^"]*)" da aba Diversos$/) do |valor|
  @diversos.clicar_botao_remover_do_meio_resultado_especifico(valor)
end

Então(/^o meio do resultado adicional não é removido do campo Meio do resultado da aba Diversos$/) do
  @diversos.clicar_botao_limpar_aba_diversos
  expect(@diversos.validar_existencia_campo_preenchimento(false)).to be false
end

Então(/^todos os campos voltam para as suas definições padrão na aba Diversos$/) do
  @diversos.validar_limpar_todos_os_dados_tela_diversos
end
