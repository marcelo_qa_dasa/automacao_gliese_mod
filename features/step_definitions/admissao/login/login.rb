  Dado(/^preencho os campos usuário e senha com dados validos$/) do
      @login.campo_usuario.send_keys($massa_dados['usuario'])
      @login.campo_senha.send_keys($massa_dados['senha'])
  end

  Então(/^deverá ser apresentado a tela de selenionar unidade$/) do
      @elemento_visivel.wait_element_visible('#labelInformaEmpresa')
      expect(@login.selecionar_empresa_tela_unidade_tx.visible?).to eq true
  end

  Dado(/^que eu esteja na tela principal do sistema Gliase$/) do
    SetUrl.new.load
    @elemento_visivel.wait_element_visible('#username')
  end

  Dado(/^preencho os campos usuário e senha com dados invalidos$/) do |table|
      @user = table.hashes[0]['Username']
      @pwd = table.hashes[0]['Password']
      @login.campo_usuario.send_keys(@user)
      @login.campo_senha.send_keys(@pwd)
  end

  Quando(/^clico no botão Entrar$/) do
      @login.btn_entra.click
  end

  Então(/^o usuário é apresentado uma mensagem$/) do |mensagem|
      expect(@login.mensagem_erro_login.text).to eq mensagem
  end

  Quando(/^seleciono uma unidade de atendimento$/) do
    @unidade.selecionar_empresa_tela_unidade_dasa_sp.click
    @unidade.selecionar_unidade_de_atendimento.select("ALB - Alcantara")
  end

  Quando(/^clico no botão Escolher unidade de atendimento$/) do
    @unidade.btn_escolher_empresa.click
  end

  Entao(/^é apresentado a home do sistema de atendimento$/) do
    expect(@unidade.menu_empresa_tela_home.visible?).to be true
  end


  Dado(/^clico em esqueceu a senha\?$/) do
    @login.campo_usuario.send_keys("@user") #colocar massa corresta
    @login.link_esqueceu_a_senha.click
  end

  Dado(/^preencho o campo com CPF valido$/) do
      @elemento_visivel.wait_element_visible('#cpf')
      @login.campo_cpf.send_keys('05148659548')
  end

  Dado(/^clico no botão Resetar Senha$/) do
    @login.btn_resetar_senha.click
  end

  Entao(/^é apresentado uma mensagem confirmação "([^"]*)"$/) do |msg_resetar_senha|
    wait_accept_alert
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_resetar_senha)
  end

  Entao(/^é apresentado uma mensagem de nova senha enviada "([^"]*)"$/) do |msg_novar_senha|
    wait_accept_alert
    expect(page.driver.browser.switch_to.alert.text).to have_text(msg_novar_senha)
  end

  Entao(/^clico no botão OK para confirmar o resetar senha$/) do
    wait_accept_alert
    page.driver.browser.switch_to.alert.accept
  end

  Entao(/^clico no botão OK para confirmar o envio do e\-mail da nova senha$/) do
    wait_accept_alert
    page.driver.browser.switch_to.alert.accept
  end