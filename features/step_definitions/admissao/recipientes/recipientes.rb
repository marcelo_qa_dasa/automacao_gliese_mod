Quando(/^clico na aba Recipientes$/) do
  @admissao_recipientes = FuncionalidadeAdmissaoRecipientes.new
  @admissao_recipientes.clicar_no_botao_recipiente
end

Então(/^são exibidos os recipientes dos exames$/) do |table|
  @admissao_recipientes.validar_lista_de_recipientes_dos_exames(table)
end
