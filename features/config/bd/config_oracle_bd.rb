    class FuncionalidadeBancoDados
        $DB = OCI8.new($dados_bd_gliese['usuario'], $dados_bd_gliese['senha'], "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = #{$dados_bd_gliese['host']})(PORT = #{$dados_bd_gliese['Porta']})))(CONNECT_DATA=(SID=#{$dados_bd_gliese['sid']})))")
        
        def validar_uf_da_unidade_selecionada(mnemonico_valor)
            $valor = mnemonico_valor       
            $DB.exec("SELECT  e.\"Empresa_codigo\",
            e.\"Empresa_descricao\",
            p.\"Pc_Mnemonico\",
            p.\"Pc_Nome\",
            p.\"Pc_Estado\"
            FROM GLIESE.\"Empresa\" e
            INNER JOIN GLIESE.\"Pc\" p ON p.\"Pc_Empresa\" = e.\"Empresa_codigo\"
            WHERE p.\"Pc_status\" = 1
            AND p.\"Pc_Mnemonico\" = '#{$valor}'
            order by e.\"Empresa_codigo\",
            p.\"Pc_Mnemonico\"") do |valor|
            $result = valor
            end
            $result
        end

        def empresa_codigo(empresa)
          $nome_empresa = empresa
          $DB.exec("SELECT \"EmpImg_codigo\"
            FROM \"EmpImg\"
            WHERE ROWNUM = 1 and \"EmpImg_nome\"
            LIKE '#{'%' + $nome_empresa + '%'}'") do |valor|
            $result = valor
          end
          return $result.first
        end

        def deletar_lock    
            $DB.exec("Delete \"Lock\" where \"Lock_usuario\" = 'MASTER'")
            $DB.exec("commit")
            $DB.exec("Delete \"Lock\" where \"Lock_usuario\" = 'TESTER03'")
            $DB.exec("commit")
            $DB.exec("Delete \"Lock\" where \"Lock_usuario\" = 'TESTER04'")
            $DB.exec("commit")
            $DB.exec("Delete \"Lock\" where \"Lock_usuario\" = 'TESTER01'")
            $DB.exec("commit")
            $DB.exec("Delete \"Lock\" where \"Lock_usuario\" = 'TESTER02'")
            $DB.exec("commit")    
        end

        def selecionar_instrucoes_do_convenio(convenio)
            $convenio = convenio    
            $result = Array.new   
            $DB.exec("Select \"Prcv_texto\" 
            from \"Prcv\" 
            where \"Prcv_convenio\" = '#{$convenio}'
            order by \"Prcv_sequencia\"") do |valor|
            $retorno = $result.push(valor) 
            end
            if $retorno == nil
            "Sem informação de Instruções de Convênio"
            else
            $result = $retorno.join("").gsub("®", "\n")
            end
        end

        def selecionar_nome_operadora_por_empresa(empresa)
            @empresa = FuncionalidadeBancoDados.new.empresa_codigo(empresa).to_i
            $result = Array.new 
            $DB.exec("select distinct \"GrpCvn_descricao\" as operadora from \"GrpCvn\"
            JOIN \"Cv\"  ON \"Cv_grupoConvenios\" = \"GrpCvn_codigo\"
            JOIN \"CvPlano\" ON \"CvPlano_convenio\" = \"Cv_mnemonico\"
            JOIN \"CriCv\" ON \"CriCv_codigoConvenio\" = \"Cv_mnemonico\"
            where \"CriCv_status\" = '1' and \"Cv_empresa\" = #{@empresa}
            order by \"OPERADORA\"") do |valor|
            $result.push(valor)
            end
            $result.join(", ")
        end


        def listagem_de_visitas_paciente(cip)   
            @cip = cip
            $result = Array.new 
            $DB.exec("select \"Resumo_visita\", \"Resumo_data\", \"Resumo_unidadeAtendimento\" from \"v_ResumoVisitas\" res where \"Resumo_cip\" = '#{@cip}' order by \"Resumo_visita\" DESC") do |valor|
            $retorno = $result.push(valor) 
            end
            $retorno

        end

        def listagem_dados_paciente(cip)
            $DB.exec("SELECT e.\"Pac_logradouro\",     
            e.\"Pac_numLogra\",
            e.\"Pac_bairro\",
            e.\"Pac_complemento\",
            e.\"Pac_uf\",
            e.\"Pac_cep\",
            e.\"Pac_ddd\",
            e.\"Pac_municipio\",
            e.\"Pac_email\",
            e.\"Pac_nome\",
            e.\"Pac_registro\",
            e.\"Pac_cpf\",
            p.\"PacTelefone_valor\",
            e.\"Pac_codSexo\",
            e.\"Pac_dtNasc\"
            FROM \"Pac\" e
            left JOIN \"PacTelefone\" p ON e.\"Pac_cip\" = p.\"PacTelefone_cip\"
            where e.\"Pac_cip\" = '#{cip}'") do |valor|
            return valor
            end
        end

        def lista_espelho_de_atendimento
            @result = Array.new  
            $DB.exec("SELECT '' AS \"ProdutoMM_unidade\",
            '1'     AS \"CriCv_tipoMatMed\",
            ''      AS \"ProdutoMM_descricao\",
            \"Exa_nome\",
            \"Qtdchcv_quantidadeCh\"  AS QTDCH,
            \"MPacExa_senhaConvenio\" AS SENHA,
            \"Qtdchcv_fatorMult\"     AS FATORMULT,
            \"CriCv_usaTuss\"         AS USATUSS,
            \"Qtdchcv_codigoAmb\"     AS CODAMB,
            \"Qtdchcv_codigoTuss\"    AS CODTUSS,
            \"MPacExa_exame\"         AS EXAME,
            \"MPacExa_numeroGuiaCv\"  AS Numguia,
            \"MPacCv_convenio\"       AS Convenio,
            1                       AS Qtdexa,
            (SELECT VAL_EXAME
            FROM \"v_ValorVisitaExame_V3\" v
            WHERE v.\"MPac_cip\"     = mpacExa.\"MPacExa_cip\"
            AND V.\"MPac_visita\"    = Mpacexa.\"MPacExa_visita\"
            AND v.\"MPacExa_seqAdm\" = mpacExa.\"MPacExa_seqAdm\"
            ) AS PRECO,
            (SELECT \"AutorizaCv_codAut\"
            FROM \"MPac\"
            JOIN \"AutorizaCv\"
            ON(\"AutorizaCv_cip\"          = \"MPac_cip\"
            AND \"MPac_dataHora\"          = CONCAT(CONCAT(\"AutorizaCv_dtVis\",' '),regexp_replace(\"AutorizaCv_hrVis\",'[^,]','',1,1)))
            WHERE \"MPac_cip\"             = '#{$cip}'
            AND \"MPac_visita\"            = '#{$numero_visita}'
            AND mpacExa.\"MPacExa_seqAdm\" = \"AutorizaCv_seqAdm\"
            )  AS SENHAAUTORIZA,
            '' AS TABELA_CODTUSS,
            \"CriCv_desconto\" as DESCONTO,
            \"CriCv_cobrancaAssociado\" AS COBRANCAASSOCIADO
            FROM \"MPac\"
            LEFT OUTER JOIN \"MPacExa\" mpacExa
            ON(\"MPacExa_cip\"  = \"MPac_cip\"
            AND \"MPac_visita\" = \"MPacExa_visita\")
            LEFT OUTER JOIN \"MPacCv\" mpacCv
            ON(\"MPacExa_cip\"    = \"MPacCv_cip\"
            AND \"MPacCv_visita\" = \"MPacExa_visita\")
            LEFT OUTER JOIN \"Qtdchcv\" qtdchcv
            ON (qtdchcv.\"Qtdchcv_mnmexa\" = mpacExa.\"MPacExa_exame\"
            AND qtdchcv.\"Qtdchcv_mnmcv\"  = mpacCv.\"MPacCv_convenio\")
            LEFT OUTER JOIN \"MPacDiv\"
            ON(\"MPacDiv_cip\"     = \"MPacCv_cip\"
            AND \"MPacDiv_visita\" = \"MPacExa_visita\")
            JOIN \"CriCv\" criCv1
            ON (criCv1.\"CriCv_codigoConvenio\" = qtdchcv.\"Qtdchcv_mnmcv\"
            AND criCv1.\"CriCv_dtRef\"          =
            (SELECT MAX(criCv2.\"CriCv_dtRef\")
            FROM \"CriCv\" criCv2
            WHERE criCv1.\"CriCv_codigoConvenio\" = criCv2.\"CriCv_codigoConvenio\"
            ))
            JOIN \"Exa\" exa1
            ON(exa1.\"Exa_codigo\" = \"MPacExa_exame\"
            AND exa1.\"Exa_dtRef\" =
            (SELECT MAX(exa2.\"Exa_dtRef\")
            FROM \"Exa\" exa2
            WHERE exa2.\"Exa_codigo\" = exa1.\"Exa_codigo\"
            AND exa2.\"Exa_dtRef\"   <=
              (SELECT TO_CHAR(SYSDATE, 'YYYYMMDD') \"NOW\" FROM DUAL
              )
            ) )
            WHERE \"MPacExa_cip\"  = '#{$cip}'
            AND \"MPacExa_visita\" = '#{$numero_visita}'
            AND \"Qtdchcv_dtRef\"  =
            (SELECT MAX(qtd2.\"Qtdchcv_dtRef\")
            FROM \"Qtdchcv\" qtd2
            WHERE qtd2.\"Qtdchcv_mnmcv\" = \"MPacCv_convenio\"
            AND qtd2.\"Qtdchcv_mnmexa\"  = \"MPacExa_exame\"
            )
            AND \"MPacExa_flagExclusaoLog\"      <> '1'
            AND (Upper(\"MPacExa_flagCobranca\") <> 'N'
            OR Upper(\"MPacExa_flagCobranca\")   IS NULL)
            UNION ALL
            SELECT \"ProdutoMM_unidade\",
            TO_CHAR(\"CriCv_tipoMatMed\"),
            prod2.\"ProdutoMM_descricao\",
            Prod.\"PProduto_descricao\" AS \"Exa_nome\",
            ''                        AS Qtdch,
            ''                        AS Senha,
            ''                        AS Fatormult,
            '1'                       AS Exigeamb,
            CASE
              WHEN \"CriCv_tipoMatMed\"='2'
              THEN LPAD(
                (SELECT \"TabelaCvExc_Codificacao\"
                FROM \"TabelaCvExc\"
                WHERE \"TabelaCvExc_codProd\" = \"MPacKit_codigoProdutoChave\"
                AND \"TabelaCvExc_mnmCv\"     = \"MPacCv_convenio\"
                AND \"TabelaCvExc_dtRef\"     =
                  (SELECT MAX(\"TabelaCvExc_dtRef\")
                  FROM \"TabelaCvExc\"
                  WHERE \"TabelaCvExc_codProd\"=\"MPacKit_codigoProdutoChave\"
                  AND \"TabelaCvExc_mnmCv\"    = \"MPacCv_convenio\"
                  )
                ), 10, '0')
              ELSE \"PProduto_codigoAmb\"
            END AS Codamb,
            CASE
              WHEN \"CriCv_tipoMatMed\"='2'
              THEN LPAD(
                (SELECT \"TabelaCvExc_Codificacao\"
                FROM \"TabelaCvExc\"
                WHERE \"TabelaCvExc_codProd\" = \"MPacKit_codigoProdutoChave\"
                AND \"TabelaCvExc_mnmCv\"     = \"MPacCv_convenio\"
                AND \"TabelaCvExc_dtRef\"     =
                  (SELECT MAX(\"TabelaCvExc_dtRef\")
                  FROM \"TabelaCvExc\"
                  WHERE \"TabelaCvExc_codProd\"=\"MPacKit_codigoProdutoChave\"
                  AND \"TabelaCvExc_mnmCv\"    = \"MPacCv_convenio\"
                  )
                ), 10, '0')
              ELSE \"PProduto_codigoAmb\"
            END                                                     AS Codtuss,
            Prod.\"PProduto_unidade\"                                 AS Exame,
            ''                                                      AS NUMGUIA,
            ''                                                      AS CONVENIO,
            kit.\"MPacKit_quantidade\"                                AS QTDEXA,
            TO_NUMBER(REPLACE(kit.\"MPacKit_valorUnitario\",'.',',')) AS PRECO,
            ''                                                      AS SENHAAUTORIZA,
            (SELECT \"TabelaProd_codTuss\"
            FROM
              (SELECT *
              FROM gliese.\"TabelaProd\" tab1
              WHERE \"TabelaProd_dtRef\" =
                (SELECT MAX(TAB2.\"TabelaProd_dtRef\")
                FROM gliese.\"TabelaProd\" tab2
                WHERE TAB2.\"TabelaProd_codProd\" = TAB1.\"TabelaProd_codProd\"
                )
              ORDER BY \"TabelaProd_codTabela\" DESC
              ) tb
            WHERE tb.\"TabelaProd_codProd\" = kit.\"MPacKit_codigoProdutoChave\"
            AND ROWNUM                    = 1
            ) AS TABELA_CODTUSS,
            \"CriCv_desconto\" as DESCONTO,
            \"CriCv_cobrancaAssociado\" AS COBRANCAASSOCIADO
            FROM \"MPacKit\" kit
            LEFT OUTER JOIN \"PProduto\" prod
            ON(\"PProduto_codigoProduto\" = TO_CHAR(\"MPacKit_codigoProdutoChave\"))
            JOIN \"MPacExa\"
            ON(\"MPacExa_cip\"              = \"MPacKit_cip\"
            AND \"MPacExa_visita\"          = \"MPacKit_visita\"
            AND \"MPacExa_seqAdm\"          = \"MPacKit_seqAdm\"
            AND \"MPacExa_flagExclusaoLog\" = 0)
            LEFT OUTER JOIN \"MPacCv\"
            ON(\"MPacCv_cip\"     = \"MPacExa_cip\"
            AND \"MPacCv_visita\" = \"MPacExa_visita\")
            INNER JOIN \"CriCv\" cri1
            ON(\"CriCv_codigoConvenio\" = \"MPacCv_convenio\"
            AND \"CriCv_dtRef\"         =
            (SELECT MAX(cri2.\"CriCv_dtRef\")
            FROM \"CriCv\" cri2
            WHERE cri2.\"CriCv_codigoConvenio\" = cri1.\"CriCv_codigoConvenio\"
            ))
            LEFT OUTER JOIN \"MPac\"
            ON(\"MPacExa_cip\"     = \"MPac_cip\"
            AND \"MPacExa_visita\" = \"MPac_visita\")
            LEFT OUTER JOIN \"ProdutoMM\" prod2
            ON (\"MPacKit_codigoProdutoChave\"                                                                         = \"ProdutoMM_codProd\"
            AND concat(\"ProdutoMM_codProd\",concat('|',concat(\"ProdutoMM_dtRef\",concat('|',\"ProdutoMM_afinidade\")))) IN
            (SELECT concat(\"ProdutoMM_codProd\",concat('|',concat(dtref,concat('|',\"ProdutoMM_afinidade\"))))
            FROM
              (SELECT \"ProdutoMM_codProd\",
                MAX(\"ProdutoMM_dtRef\") dtref,
                \"ProdutoMM_afinidade\"
              FROM \"ProdutoMM\" pprod
              WHERE pprod.\"ProdutoMM_codProd\" = \"ProdutoMM_codProd\"
              GROUP BY pprod.\"ProdutoMM_codProd\",
                \"ProdutoMM_afinidade\"
              )
            ) )
            AND \"ProdutoMM_status\" = 1
            WHERE \"MPacKit_cip\"    = '#{$cip}'
            AND \"MPacKit_visita\"   = '#{$numero_visita}'") do |valor|
            @retorno_espelho_de_atendimento = @result.push(valor)
            end
            return @retorno_espelho_de_atendimento
        end

    end   
