    class JiraApi
        include HTTParty
        format :json
        headers   'Authorization' => $dados_jira['authorization'],
                    'Content-Type' => $dados_jira['content_type']

        def initialize(feature_name, cenario_name, decricao_step)
            $request_body = { "fields": {
                "project": {"key": "ADT"},
                    "summary": "#{feature_name} - #{cenario_name}",
                    "description": "#{$decricao}",
                    "issuetype": {
                    "name": "Tarefa"}}}
        end

        def create_issue_problem
            self.class.post("https://jiradasa.atlassian.net/rest/api/latest/issue", :body => $request_body.to_json)
        end
    end

    class JiraApiAddAnexo  
        def initialize(id, timer_path, name_cenario)
            $id = id
            $timer_path = timer_path
            $name_cenario = name_cenario
            $request_body = "/home/dbs-note-260/Documentos/projeto_automacao_gliese/screenshots/test_failed/run_#{$timer_path}/#{name_cenario}.png"
        end
    
        def create_issue_attachment
            request = RestClient::Request.execute(method: :post, :url => "https://jiradasa.atlassian.net/rest/api/latest/issue/#{$id}/attachments", :headers => {'Authorization'  => $dados_jira['authorization'], 'X-Atlassian-Token' => 'no-check'}, :payload => { :multipart => true, :file => File.new($request_body, 'rb')})
        end
    end
