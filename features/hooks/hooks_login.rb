class LogarSistema < SitePrism::Page
  def steps_login(username, password,nome_empresa,nome_unidade)
    @login = FuncionalidadeLogin.new
    @convenio = FuncionalidadeAdmissaoConvenio.new
    @unidade = FuncionalidadeUnidade.new
    @elemento_visivel = ElementoVisivel.new
    @banco_dados = FuncionalidadeBancoDados.new
    SetUrl.new.load
    @elemento_visivel.wait_element_visible('#username')
      #validar se o user e o pass estiver vindo de uma tabela
      if password != "" && password != nil
        @password = password
      else
        @password = $massa_dados['senha']
      end
     
      if username != "" && username != nil
        @username = username
      else
        @username = $massa_dados['usuario']
      end
    @login.campo_usuario.set(@username)
    sleep 1
    @login.campo_senha.set(@password)
    sleep 1
    @login.btn_entra.click
    @elemento_visivel.wait_element_visible('.lista-10')

    if(nome_empresa != nil && nome_unidade != nil )
    $numero_empresa = @banco_dados.empresa_codigo(nome_empresa)
    @convenio.selecionar_empresa($numero_empresa)
    @unidade.selecionar_unidade_de_atendimento.select(nome_unidade)
    @unidade.btn_escolher_empresa.click
    @elemento_visivel.wait_element_visible('.empresa')
   
    else
    $numero_empresa = @banco_dados.empresa_codigo("Sergio Franco")
    @convenio.selecionar_empresa($numero_empresa)
    @unidade.selecionar_unidade_de_atendimento.select("LEA - Leblon - Cid Leblon") #LEA - Leblon - Cid Leblon   SED - Nto Caxias Rj
    @unidade.btn_escolher_empresa.click
    @elemento_visivel.wait_element_visible('.empresa')
    end
  end

 def logado_sistema_gliese(username, password)
    @login = FuncionalidadeLogin.new
    @elemento_visivel = ElementoVisivel.new
    SetUrl.new.load
    @elemento_visivel.wait_element_visible('#username')
    @login.campo_usuario.set(username)
    sleep 1
    @login.campo_senha.set(password)
    sleep 1
    @login.btn_entra.click
    @elemento_visivel.wait_element_visible('.lista-10')
 end

 def login_gliese_por(empresa, unidade)
   @login = FuncionalidadeLogin.new
   @convenio = FuncionalidadeAdmissaoConvenio.new
   @unidade = FuncionalidadeUnidade.new
   @elemento_visivel = ElementoVisivel.new
   @banco_dados = FuncionalidadeBancoDados.new
   SetUrl.new.load
   @elemento_visivel.wait_element_visible('#username')
      #validar se o user e o pass estiver vindo de uma tabela
      if password != "" && password != nil
        @password = password
      else
        @password = $massa_dados['senha']
      end
     
      if username != "" && username != nil
        @username = username
      else
        @username = $massa_dados['usuario']
      end
    @login.campo_usuario.set(@username)
    sleep 1
    @login.campo_senha.set(@password)
    sleep 1
   @login.btn_entra.click
   @elemento_visivel.wait_element_visible('.lista-10')
   $numero_empresa = @banco_dados.empresa_codigo(empresa)
   @convenio.selecionar_empresa($numero_empresa)
   @unidade.selecionar_unidade_de_atendimento.select(unidade) #LEA - Leblon - Cid Leblon   SED - Nto Caxias Rj
   @unidade.btn_escolher_empresa.click
   @elemento_visivel.wait_element_visible('.empresa')
 end

end
