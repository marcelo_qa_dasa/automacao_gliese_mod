class Utils
    include RSpec::Matchers
    require 'rspec'

    def validar_hora_atual
        return Time.now.strftime("%H%M%S")
    end

    def validar_data_atual
        return Time.now.strftime("%d%m%Y")
    end

    def validar_data_atual_com_barras
        return Time.now.strftime("%d/%m/%Y")
    end

    def validar_data_yesterday
        return Date.today.prev_day.strftime("%d%m%Y")
    end

    def validar_data_tomorrow
        tomorrow = Date.today + 1
        return tomorrow.strftime("%d%m%Y")
    end

    def validar_dia_atual
        return Date.today.strftime("%d")
    end

    def expressao_retornar_somente_letras(element)
        element.gsub!(/[^a-zA-Z]/, '')
    end

    def expressao_retornar_somente_numeros(element)
        element.gsub!(/[^0-9]/, '')
    end

    def expressao_remover_caractere_que_nao_seja_uma_palavra(element)
        element.gsub!(/\W+/, '')
    end

    def converte_letras_validas(valor)
        @valor = valor
        @letras = Utils.new.expressao_retornar_somente_letras(@valor)
        if @letras == nil
        return @valor
        else
        return @letras
        end
    end

    def carregar_pdf(path)
        sleep 1
        @file = File.expand_path("../../", File.dirname(__FILE__))
        reader = PDF::Reader.new("#{@file}/PDF/#{path}")
        reader.pages.each do |page|
            @result_pdf = page
        end
        return @result_pdf
    end

    def remover_pdf
        @file = File.expand_path("../../", File.dirname(__FILE__))
        FileUtils.rm_r Dir.glob("#{@file}/PDF/*") if File.directory?("PDF")
    end

    def scroll_page_window(x, y)
        execute_script "window.scrollBy(#{x},#{y})"
    end

    def scroll_to(element)
      page_height = Capybara.current_session.driver.browser.manage.window.size.height
      element_axis_y = element.native.location.y
      element_offset = element.native.size.height
      final_screen_axis = (element_axis_y + element_offset) - page_height / 2
      final_screen_axis = final_screen_axis >= 0 ? final_screen_axis : 0
      Capybara.current_session.driver.browser.execute_script("scrollTo(0,#{final_screen_axis})")
    end

    def formata_valores_float_para_string(value)
      return sprintf('%05.2f', value)
    end

    def formata_valor_inteiro_em_data(valor)
        Time.at(valor).strftime("%d/%m/%Y")
    end

    def calculo_desconto_porcentagem(valor_total, valor_desconto)
        return valor_total.to_f - (valor_desconto.to_i*valor_total.to_f)/100
    end


end
