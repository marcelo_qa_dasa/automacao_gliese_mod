World(Helper)

After do |cenario|
    ## take screenshot
    cenario_name = cenario.name.gsub(/\s+/, '_').tr('/', '_')
    if cenario.failed?
      take_screenshot(cenario_name.downcase!, 'failed')
          if $habilitar_report_jira == true
            FuncionalidadeDescricaoJiraBug.new.capturar_decricao_passo_passo_steps_erro(cenario)
            $cenario_name = cenario.name
            $feature_name = cenario.feature.name
          
            @jira_api = JiraApi.new($feature_name, $cenario_name, $decricao_step)
            @resultado = @jira_api.create_issue_problem
            $id = @resultado.parsed_response['id']
          
            $timer_path = Time.now.strftime('%Y_%m_%d').to_s
            $cenarioname = cenario.name.gsub(/\s+/, '_').tr('/', '_') 
            $cenarioname = $cenarioname.downcase!
          
            $jira_anexo = JiraApiAddAnexo.new($id, $timer_path, $cenarioname)
            $response_anexo = $jira_anexo.create_issue_attachment
            $response_anexo.code
          end
    else
      take_screenshot(cenario_name.downcase!, 'passed')
    end
  end

  class FuncionalidadeDescricaoJiraBug
    def capturar_decricao_passo_passo_steps_erro(cenario)
        $decricao_step = Array.new
        cenario.test_steps.each do |step|
        $decricao_step.push(step.name).delete("Before hook")
        $decricao = ""
        $decricao_step.each_with_index do |element,index|
        $decricao << "#{index+1} -> #{element} \n"
      end
    end
  end
end