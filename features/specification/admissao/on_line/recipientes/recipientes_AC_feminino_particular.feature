#language: pt
#encoding: utf-8
@recipientes_ac_feminino_particular @done_recipientes
Funcionalidade: Manutenção de recipientes de exames de análises clínicas femininas no particular na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de recipientes de exames de análises clínicas femininas no particular na admissão on-line
 Para gerenciar recipientes de exames de análises clínicas femininas no particular na visita de paciente

Esquema do Cenario: Recipientes de exames de análises clínicas femininas no particular 
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Br-bpbarraplaza"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | MI             | COLPO           | COLPOCITOLOGICO | 1                  |                 | 18-Esfregaco Vaginal | false   | 00000000    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  E são exibidos os recipientes dos exames
  | Recipiente   | Compl_material       | Recipiente        | Unidade_Realização | Setor | Exames  |
  | 823274949413 | 18-Esfregaco Vaginal | FRASCOLPO         | DAS                | CITP  | COLPOMI |

  Exemplos:
  | username   | password | Marca       | Unidade                         | Data_Nascimento | CIP          | medico_nome            | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011912"      | "6003616571" | "Natalia A.c. Freitas" | "10144043" | "SP"      |
  #| "TESTER01" | "123456" | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011912"      | "6003616571" | "Natalia A.c. Freitas" | "10144043" | "SP"      |
