#language: pt
#encoding: utf-8
@recipientes_rdi_convenio @done_recipientes
Funcionalidade: Manutenção de recipientes de exames de RDI na admissão on-line pelo convênio
 Como atendente do sistema Gliese
 Posso manter o cadastro de recipientes de exames de RDI pelo convênio na admissão on-line
 Para gerenciar recipientes de exames de RDI pelo convênio na visita de paciente

Esquema do Cenario: Recipientes de exames de RDI pelo convenio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Lm-amil/ami"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio Lm-amil/ami <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Validade do Pedido>
  E clico na aba Exames
  E é incrementado o número de Imagem e Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão Valida da aba Exames
  #E são exibidas as informações de atendimento do exame na aba Exames
  #E clico no ícone fechar das informações de atendimento do exame na aba Exames
  #E são exibidas as informações do exame na aba Exames
  #E informo a identificação do médico "dpi00"
  #E informo a data da agenda e a hora da agenda
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                   | Quantidade_Laminas | Volume_Material | Complemento_Material  | Day_Lab | Codificação | Pg | Impr |
  | IM             | ABD1            | RX ABDOMEN SIMPLES (AP)           |                    |                 | material_complementar | false   | 40808017    |  S | S    |
  | IM             | UABT            | ULTRA SOM ABDOMEN TOTAL (ABD S... |                    |                 |                       | false   | 40901122    |  S | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  E são exibidos os recipientes dos exames
  | Recipiente   | Compl_material        | Recipiente         | Unidade_Realização | Setor | Exames    |
  | 1790061389   | material_complementar | RECIPIENTE PROPRIO | SED                | RADI  | BR-ABD1IM |
  | 1790029855   |                       | SEM RECIPIENTE     | SED                | USON  | BR-UABTIM |

  Exemplos:
  | username   | password | Marca       | Unidade                                   | Data_Nascimento | CIP          | medico_nome           | medico_crm | medico_uf | Amil_Codigo_Associado |Amil_Data_Solicitacao | Amil_Nome_Plano  | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Bronstein" | "150 - Bronstein - Coleta - Barra Plazza" | "09061941"      | "1222187404" | "Ivana Daniela Porto" | "5987"     | "MS"      | "121212122"           | "21092017"           | "Amil 140 Nac"   | "Sim"              | 3                         |
  | "TESTER01" | "123456" | "Bronstein" | "150 - Bronstein - Coleta - Barra Plazza" | "09061941"      | "1222187404" | "Ivana Daniela Porto" | "5987"     | "MS"      | "121212122"           | "21092017"           | "Amil 140 Nac"   | "Sim"              | 3                         |
