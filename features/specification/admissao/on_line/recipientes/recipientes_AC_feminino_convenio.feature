#language: pt
#encoding: utf-8
@recipientes_ac_feminino_convenio @done_recipientes
Funcionalidade: Manutenção dos recipientes de exames de análises clínicas femininas na admissão on-line pelo convênio
 Como atendente do sistema Gliese
 Posso manter o cadastro de recipientes de exames de análises clínicas na admissão on-line
 Para gerenciar recipientes de exames de análises clínicas na visita de paciente

Esquema do Cenario: Recipientes de exames de análises clínicas femininas pelo convênio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Amil"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio Amil <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Amil_Quantidade_Etiquetas>
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab |  Codificação | Pg | Impr |
  | MI             | COLPO           | COLPOCITOLOGICO | 1                  |                 | 18-Esfregaco Vaginal | false   | 40601137     | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  E são exibidos os recipientes dos exames
  | Recipiente   | Compl_material       | Recipiente        | Unidade_Realização | Setor | Exames  |
  | 823274949413 | 18-Esfregaco Vaginal | FRASCOLPO         | DAS                | CITP  | COLPOMI |

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome            | medico_crm | medico_uf | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "26051990"      | "6006024227" | "Natalia A.c. Freitas" | "10144043" | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "26051990"      | "6006024227" | "Natalia A.c. Freitas" | "10144043" | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
