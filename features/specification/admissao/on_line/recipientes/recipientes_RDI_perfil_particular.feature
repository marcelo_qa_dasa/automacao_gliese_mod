#language: pt
#encoding: utf-8
@recipientes_rdi_perfil_particular @done_recipientes
Funcionalidade: Manutenção de recipientes de exames de perfil RDI no particular na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de recipientes de exames de perfil RDI no particular na admissão on-line
 Para gerenciar recipientes exames de perfil RDI no particular na visita de paciente

Esquema do Cenario: Recipientes de exames de perfil RDI no particular
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Lm-ptarp"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  Quando é incrementado o número de Imagem e o Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                  | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | TCAT            | TC ABDOMEN TOTAL ABDOMEN SUP +...|                    |                 |                      | false   | 41001095    | N  | N    |
  E confirmo o procedimento de atendimento LMFPVTC1IM
  E todos os exames e exames filhos foram adicionados na Relação de exames admitidos da aba Exames
  Então é incrementado o número de Imagem e o Total no Resumo da aba Exames e são adicionados os exames filhos dos exames de perfil previamente admitidos
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame           | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | FPVTC1          | TC PELVICA                |                    |                 |                      | false   | 41001117    | S  | S    |
  | SA             | TCAS            | TC ABDOMEN SUPERIOR       |                    |                 |                      | false   | 41001109    | S  | S    |
  Quando clico na aba Recipientes
  Então são exibidos os recipientes dos exames
  | Recipiente   | Compl_material       | Recipiente        | Unidade_Realização | Setor | Exames                            |
  | 1790030046   |                      | SEMREC  	        | SED                | TOMO  | LM-TCATIM, LM-FPVTC1IM, LM-TCASIM |

  Exemplos:
  | username   | password | Marca                                              | Unidade                   | Data_Nascimento | CIP          | medico_nome             | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Diagnosticos da America - Lamina Med. Diag. Ltda" | "LPO - Lamina - Arpoador" | "08021949"      | "6000981914" | "Matheus Freitas Leite"  | "23479"   | "BA"      | 
  #| "TESTER01" | "123456" | "Diagnosticos da America - Lamina Med. Diag. Ltda" | "LPO - Lamina - Arpoador" | "08021949"      | "6000981914" | "Matheus Freitas Leite"  | "23479"   | "BA"      |
