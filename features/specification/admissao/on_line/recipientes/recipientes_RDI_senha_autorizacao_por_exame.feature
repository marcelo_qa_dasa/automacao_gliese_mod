#language: pt
#encoding: utf-8
@recipientes_rdi_senha_autorizacao @done_recipientes
Funcionalidade: Manutenção de recipientes de exames de RDI que exigem senha na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de recipientes de exames de RDI que exigem senha na admissão on-line
 Para gerenciar recipientes de exames de RDI que exigem senha na visita de paciente

Esquema do Cenario: Recipientes de exames que exijam senha de autorização do exame
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Br-amilimagem"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Lm-amil/ami
  E preencho as informações do convênio para o convênio Lm-amil/ami <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Validade do Pedido>
  E clico na aba Exames
  E preencho um material e exame que exija senha de autorização do exame
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame | Quantidade_Laminas |  Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | RMCR            | RM CRANIO       |                    |                  |                      | false   | 41101014    | S  | S    |
  E clico no botão valida da aba exames é exibida a mensagem de autorização "Este exame precisa de autorização. LM-RMCRIM"
  E clico no botão OK
  E informo a identificação do médico "dpi00", data da agenda e a hora da agenda
  E clico no botão Salvar da aba Exames
  E o exame é adicionado na Relação de exames admitidos
  E clico na aba Recipientes
  E são exibidos os recipientes dos exames
  | Recipiente   | Compl_material | Recipiente        | Unidade_Realização | Setor | Exames    |
  | 1790030059   |                | SEMREC            | SED                | RESS  | LM-RMCRIM |
  
  Exemplos:
  | username   | password | Marca        | Unidade                                   | Data_Nascimento | CIP          | medico_nome                | medico_crm | medico_uf | Amil_Passe_Cartão | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano     | Amil_Plano_Empresa | Validade do Pedido |
  | "TESTER04" | "1234"   | "Bronstein"  | "150 - Bronstein - Coleta - Barra Plazza" | "01011912"      | "1225164367" | "Patricia Albizu Piaskowy" | "28212"    | "PR"      | "121212122"       | "121212122"           | "27092017"            | "Amil 140 Nacional" | "Sim"              | "28092017"         |
  #| "TESTER01" | "123456" | "Bronstein"  | "150 - Bronstein - Coleta - Barra Plazza" | "01011912"      | "1225164367" | "Patricia Albizu Piaskowy" | "28212"    | "PR"      | "121212122"       | "121212122"           | "27092017"            | "Amil 140 Nacional" | "Sim"              | "28092017"         |
