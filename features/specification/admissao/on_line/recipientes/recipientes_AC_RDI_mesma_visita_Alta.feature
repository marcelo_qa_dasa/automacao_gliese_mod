#language: pt
#encoding: utf-8
@recipientes_ac_RDI_mesma_ficha_Alta @done_recipientes
Funcionalidade: Manutenção de recipientes de exames de AC e RDI na mesma visita na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de recipientes de exames de AC e RDI na mesma visita na admissão on-line
 Para gerenciar recipientes de exames de AC e RDI na mesma visita na visita de paciente

Esquema do Cenario: Recipientes de exames de AC e RDI na mesma visita na marca Alta
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Al-partrj50"
  E clico no botão Seleciona
  E são exibidas as instruções do particular
  E preencho as informações do particular Al-partrj <Motivo_do_desconto>, <Observacao>, <Plano_Empresa>
  E clico na aba Exames
  E é incrementado o número de Sangue, Imagem e o Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | HIV-1           | HIV 1+2 - ANTICORPOS       |                    |                 |                      | false   | 40307182    | S  | S    |
  | IM             | RPFS            | PERFUSAO CEREBRAL POR RM   |                    |                 |                      | true    | 00000000    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  Quando clico na aba Recipientes
  Então são exibidos os recipientes dos exames
  | Recipiente   | Compl_material | Recipiente        | Unidade_Realização | Setor | Exames    |
  | 1790029940   |                | SORO HIV          | SED                | BIOQ  | HIV-1SA   |
  | 1790029944   |                | SEMREC            | SED                | RESS  | RPFSIM    |

  Exemplos:
  | username   | password | Marca                          | Unidade                     | Data_Nascimento | CIP          | medico_nome            | medico_crm | medico_uf | Motivo_do_desconto | Observacao                        | Plano_Empresa |
  | "TESTER04" | "1234"   | "Alta Excelencia Diagnostica"  | "ABA - Alta Barra Shopping" | "06072005"      | "1222325285" | "Giovana Fontes Rosin" | "38923"    | "RS"      | "Medico"           | "Desconto solicitado pelo medico" | "Sim"         |
  | "TESTER01" | "123456" | "Alta Excelencia Diagnostica"  | "ABA - Alta Barra Shopping" | "06072005"      | "1222325285" | "Giovana Fontes Rosin" | "38923"    | "RS"      | "Medico"           | "Desconto solicitado pelo medico" | "Sim"         |
