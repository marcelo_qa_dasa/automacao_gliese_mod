#language: pt
#encoding: utf-8
@recipientes_ac_senha_recursos_positivos @done_recipientes
Funcionalidade: Manutenção de recipientes de exames de AC que exigem senha na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de recipientes de exames de AC que exigem senha na admissão on-line
 Para gerenciar recipientes de exames de AC que exigem senha na visita de paciente

Esquema do Cenario: Recipientes de exames de AC que exibem o campo senha
   Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Amil"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio Amil <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Amil_Quantidade_Etiquetas>
  E clico na aba Exames
  E preencho um material e exame que exibe o campo senha
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame      | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | HIV-1           | HIV 1+2 - ANTICORPOS |                    |                 |                      | false   | 40307174    | S  | S    |   
  E clico no botão valida da aba exames
  E clico no botão Salvar da aba Exames
  E o exame é adicionado na Relação de exames admitidos
  E clico na opção Senha Recurso da aba exames
  E valido a exibição do exame na pop-up Informe a Senha Recurso do exame
  E informo a Senha "111111" na pop-up Informe a Senha Recurso do exame
  E clico no botão Salvar na pop-up Informe a Senha Recurso do exame
  E é exibida a mensagem de senha registrada "Senha do convênio registrada"
  Quando clico na aba Recipientes
  Então são exibidos os recipientes dos exames
  | Recipiente   | Compl_material | Recipiente        | Unidade_Realização | Setor | Exames    |
  | 1790030056   |                | SORO HIV          | SED                | BIOQ  | HIV-1SA   |

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome            | medico_crm | medico_uf | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01011912"      | "2600335540" | "Marcio Alves Tannure" | "52773794" | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01011912"      | "2600335540" | "Marcio Alves Tannure" | "52773794" | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
