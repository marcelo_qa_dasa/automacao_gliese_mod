#language: pt
#encoding: utf-8
@recipientes_vc_particular_vc @to_do
Funcionalidade: Manutenção de recipientes de vacinas na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de recipientes de vacinas na admissão on-line
 Para gerenciar recipientes de vacinas na visita de paciente

Esquema do Cenario: Recipiente de vacinas no particular
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão Valida da aba Exames
  #E são exibidas as informações de atendimento do exame na aba Exames
  #E clico no ícone fechar das informações de atendimento do exame na aba Exames
  #E são exibidas as informações do exame na aba Exames
  #E informo a identificação do médico "dpi00"
  #E informo a data da agenda e a hora da agenda
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | VC             | VACMB           |                                |                    |                 |                      |         |             |    |      |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  Quando clico na aba Recipientes
  Então são exibidos os recipientes dos exames
  | Recipiente   | Compl_material | Recipiente        | Unidade_Realização | Setor | Exames    |
  |              |                |                   |                    |       |           |

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome            | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01011912"      | "1224189291" | "Marcio Alves Tannure" | "52773794" | "PE"      |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01011912"      | "1224189291" | "Marcio Alves Tannure" | "52773794" | "PE"      |
