#language: pt
#encoding: utf-8
@recipientes_rdi_perfil_convenio @done_recipientes
Funcionalidade: Manutenção de recipientes de exames de perfil RDI na admissão on-line pelo convênio
 Como atendente do sistema Gliese
 Posso manter o cadastro de recipientes de exames de perfil RDI na admissão on-line
 Para gerenciar recipientes de exames de perfil RDI na visita de paciente

Esquema do Cenario: Recipientes de exames de perfil RDI pelo convênio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Br-amilimagem"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio br_amilimagem <Amil_Codigo_Associado>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>
  E clico na aba Exames
  E preencho um material e exame que exija senha de autorização do exame
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão valida da aba exames
  #E é exibida a mensagem de autorização "Este exame precisa de autorização. LM-TCATIM"
  #E clico no botão OK
  #E informo a identificação do médico "dpi00"
  #E informo a data da agenda e a hora da agenda
  #E clico no botão Salvar da aba Exames
  #E é exibida a mensagem de autorização "Este exame precisa de autorização. LM-FPVTC1IM"
  #E clico no botão OK
  #E é exibida a mensagem de autorização "Este exame precisa de autorização. LM-TCASIM"
  #E clico no botão OK
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                  | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | TCAT            | TC ABDOMEN TOTAL ABDOMEN SUP +...|                    |                 |                      | false   | 0           | N  | N    |
  E clico no botão valida da aba exames é exibida a mensagem de autorização "Este exame precisa de autorização. BR-TCATIM"
  E clico no botão OK
  E informo a identificação do médico "dpi00", data da agenda e a hora da agenda
  E clico na opção Senha da aba exames é exibida a mensagem de autorização "Este exame precisa de autorização. BR-FPVTC1IM"
  E clico no botão OK é exibida a mensagem de autorização "Este exame precisa de autorização. BR-TCASIM"
  E clico no botão OK
  E confirmo o procedimento de atendimento LMFPVTC1IM
  Então é incrementado o número de Imagem e o Total no Resumo da aba Exames e são adicionados os exames filhos dos exames de perfil previamente admitidos
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame           | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | FPVTC1          | TC PELVICA                |                    |                 |                      | false   | 41001117    | S  | S    |
  | SA             | TCAS            | TC ABDOMEN SUPERIOR       |                    |                 |                      | false   | 41001109    | S  | S    |
  Quando clico na aba Recipientes
  Então são exibidos os recipientes dos exames
  | Recipiente   | Compl_material       | Recipiente        | Unidade_Realização | Setor | Exames                            |
  | 1790030046   |                      | SEMREC  	        | SED                | TOMO  | BR-TCATIM, BR-FPVTC1IM, BR-TCASIM |

  Exemplos:
  | username   | password | Marca                                               | Unidade                   | Data_Nascimento | CIP          | medico_nome             | medico_crm | medico_uf | Amil_Passe_Cartão | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano | Amil_Plano_Empresa |
  | "TESTER04" | "1234"   | "Diagnosticos da America - Lamina Med. Diag. Ltda"  | "LPO - Lamina - Arpoador" | "01011912"      | "6006817892" | "Liana Moreira Freitas" | "17231"    | "CE"      | "121212122"       | "121212122"           | "27092017"            | "Amil 150"      | "Sim"              |
  #| "TESTER01" | "123456" | "Diagnosticos da America - Lamina Med. Diag. Ltda"  | "LPO - Lamina - Arpoador" | "01011912"      | "6006817892" | "Liana Moreira Freitas" | "17231"    | "CE"      | "121212122"       | "121212122"           | "27092017"            | "Amil 150"      | "Sim"              |
