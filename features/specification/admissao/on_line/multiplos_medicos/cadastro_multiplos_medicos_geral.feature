
#language: pt
#encoding: utf-8
@multiplos_medicos_geral @done_multiplos_medicos
Funcionalidade: Manutenção de informações de múltiplos médicos de exames na admissão on-line no particular
 Como atendente do sistema Gliese
 Posso manter o cadastro de informações de múltiplos médicos de exames na admissão on-line
 Para gerenciar múltiplos médicos de exames na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja na home do sistema Gliese
  | username | password |
  | TESTER04 | 1234     |
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento e CIP com dados validos
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  Então clico na aba Exames
  E é incrementado o número de Sangue e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames

@clicar_aba_multiplos_medicos_sem_selecionar_opcao @done_multiplos_medicos
Cenário: Clicar aba multiplos medicos sem selecionar opcao
  Quando clico na aba Múltiplos Médicos
  Então é exibida e mensagem de seleção da opção de mais de um crm "Selecione a opção mais de 1 crm em 'Diversos'"
  
@pesquisar_cadastro__multiplos_medicos_inexistente @done_multiplos_medicos
Cenário: Pesquisar cadastro de múltiplos médicos inexistente
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  |                                  |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  |                               | Sim           |                          |                               |             |            |                 |                      |                      |
  E clico na aba Múltiplos Médicos
  E é exibida a UF da unidade selecionada no campo UF na aba Múltiplos Médicos
  E informo o Número do Registro "112233" na aba Múltiplos Médicos
  Então é exibida a mensagem de médico inexistente "Crm informado inexistente em nossos registros"

@limpar_cadastro__multiplos_medicos @to_do
Cenário: Limpar cadastro de múltiplos médicos
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  |                                  |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  |                               | Sim           |                          |                               |             |            |                 |                      |                      |
  E clico na aba Múltiplos Médicos
  E é exibida a UF da unidade selecionada no campo UF na aba Múltiplos Médicos
  E informo o Número do Registro "52167094" na aba Múltiplos Médicos
  E o nome do médico "Benedito Cassu" é carregado no campo Nome da aba Múltiplos Médicos
  Quando clico na opção Limpa da aba Múltiplos Médicos
  Então as informações do Médico são removidas da aba Múltiplos Médicos

@selecionar_cadastro_multiplos_medico_crm @to_do
Cenário: Selecionar cadastro de múltiplos médicos pelo CRM
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  |                                  |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  |                               | Sim           |                          |                               |             |            |                 |                      |                      |
  E clico na aba Múltiplos Médicos
  E é exibida a UF da unidade selecionada no campo UF na aba Múltiplos Médicos
  E clico no ícone binóculo do Número de Registro da aba Múltiplos Médicos
  Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
  E clico no botão OK
  Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação aba Múltiplos Médicos
  Quando clico no CRM da tela de resultado de pesquisa de médico na aba Múltiplos Médicos
  Então são exibidas as informações de Número de Registro e Nome do médico na aba Múltiplos Médicos

@selecionar_cadastro_multiplos_medico_nome @to_do
Cenário: Selecionar cadastro de múltiplos médicos pelo nome
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  |                                  |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  |                               | Sim           |                          |                               |             |            |                 |                      |                      |
  E clico na aba Múltiplos Médicos
  E é exibida a UF da unidade selecionada no campo UF na aba Múltiplos Médicos
  E clico no ícone binóculo do Número de Registro da aba Múltiplos Médicos
  Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
  E clico no botão OK
  Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação aba Múltiplos Médicos
  Quando clico no CRM da tela de resultado de pesquisa de médico na aba Múltiplos Médicos
  Então são exibidas as informações de Número de Registro e Nome do médico na aba Múltiplos Médicos

@pesquisar_cadastro_multiplos_medicos_crm @to_do
Cenário: Pesquisar cadastro de múltiplos médicos pelo CRM
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  |                                  |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  |                               | Sim           |                          |                               |             |            |                 |                      |                      |
  E clico na aba Múltiplos Médicos
  E é exibida a UF da unidade selecionada no campo UF na aba Múltiplos Médicos
  E informo o Número do Registro "52167094" na aba Múltiplos Médicos
  E clico no ícone binóculo do Número de Registro da aba Múltiplos Médicos
  Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação aba Múltiplos Médicos
  Quando clico no CRM da tela de resultado de pesquisa de médico na aba Múltiplos Médicos
  Então são exibidas as informações de Número de Registro e Nome do médico na aba Múltiplos Médicos

@pesquisar_cadastro_multiplos_medicos_nome @to_do
Cenário: Pesquisar cadastro de múltiplos médicos pelo nome
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  |                                  |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  |                               | Sim           |                          |                               |             |            |                 |                      |                      |
  E clico na aba Múltiplos Médicos
  E é exibida a UF da unidade selecionada no campo UF na aba Múltiplos Médicos
  E digito "Benedito Cassu" no campo Nome da aba Múltiplos Médicos
  E clico no ícone binóculo do Nome do Médico da aba Múltiplos Médicos
  Então o médicos já cadastrado é exibido com as informações de CRM, Médico e Situação aba Múltiplos Médicos
  Quando clico no CRM da tela de resultado de pesquisa de médico na aba Múltiplos Médicos
  Então são exibidas as informações de Número de Registro e Nome do médico aba Múltiplos Médicos
