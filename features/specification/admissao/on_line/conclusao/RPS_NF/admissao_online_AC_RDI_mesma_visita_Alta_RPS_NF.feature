#language: pt
#encoding: utf-8
@admissao_online_ac_rdi_mesma_visita_Alta_RPS_NF @to_do
Funcionalidade: Conclusão do cadastro de exames de AC e RDI na mesma visita na admissão on-line e validação da RPS
 Como atendente do sistema Gliese
 Posso manter a conclusão do cadastro de exames de AC e RDI na mesma visita na admissão on-line
 Para gerenciar a conclusão do cadastro de exames de AC e RDI na mesma visita na visita de paciente

Esquema do Cenario: Conclusão do cadastro de exames de AC e RDI na mesma visita na marca Alta e validação da RPS
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Al-partrj50"
  E clico no botão Seleciona
  E são exibidas as instruções do particular
  E preencho as informações do particular Al-partrj <Motivo_do_desconto>, <Observacao>, <Plano_Empresa>
  E clico na aba Exames
  E é incrementado o número de Sangue, Imagem e o Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | HIV-1           | HIV 1+2 - ANTICORPOS       |                    |                 |                      | false   | 40307182    | S  | S    |
  | IM             | RPFS            | PERFUSAO CEREBRAL POR RM   |                    |                 |                      | true    | 00000000    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  Quando são exibidos os recipientes dos exames
  | Recipiente   | Compl_material | Recipiente        | Unidade_Realização | Setor | Exames    |
  | 1790029940   |                | SORO HIV          | SED                | BIOQ  | HIV-1SA   |
  | 1790029944   |                | SEMREC            | SED                | RESS  | RPFSIM    |
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado                         | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 51 - Express-paciente Entrega A Domicilio | A7                | Não             | Comentário observações/técnicas |                          | Não            |                    | 80      | 180    |                   |                  |                  | Comentário indicação/clínica. | Não           | Não                      | Comentário exames anteriores. |             | Sim        | Jantar          | 25102017             | 200000               |
  E clico na aba Financeiro
  E preencho a forma de pagamento, clico no botão Salvar
  | forma_de_pgto    | banco | agencia | conta | cheque | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut |
  | Cartao de Debito |       |         |       |        |          |          | Visa Electron |           |            |          | 222    |
  E valido a forma de pagamento na Relação de formas de pagamento, o Total, Total Recebido, Falta e Troco
  E clico na aba Conclusão
  E é exibida a mensagem de admissão sem pendências "admissão sem pendências"
  E clico na apção Salvar
  E é exibida a mensagem de confirmação de exibição "Confirma Admissao"
  E clico no botão OK
    E é exibida a mensagem de confirmação de exibição "Deseja imprimir o protocolo de exames de imagem?"
  E clico no botão OK
  E é exibida a mensagem de confirmação de exibição "Este convenio imprime R P S, utilize a opção adequada no Sistema!"
  Quando clico no botão OK
  Então é exibido o protocolo de exames de imagem "Admissão Autorizada"
  E acesso a tela de Impressao de RPS-NF
  E valido se o texto "Observação NFE/RPS" e exibido na mesma
  Então solicito a impressão e valido o combo Observação usando <CIP> e Visita RPS
  
  Exemplos:
  | username   | password | Marca                          | Unidade                     | Data_Nascimento | CIP          | medico_nome            | medico_crm | medico_uf | Motivo_do_desconto | Observacao                        | Plano_Empresa |
  | "TESTER04" | "1234"   | "Alta Excelencia Diagnostica"  | "ABA - Alta Barra Shopping" | "06072005"      | "1222325285" | "Giovana Fontes Rosin" | "38923"    | "RS"      | "Medico"           | "Desconto solicitado pelo medico" | "Sim"         |
  | "TESTER01" | "123456" | "Alta Excelencia Diagnostica"  | "ABA - Alta Barra Shopping" | "06072005"      | "1222325285" | "Giovana Fontes Rosin" | "38923"    | "RS"      | "Medico"           | "Desconto solicitado pelo medico" | "Sim"         |
