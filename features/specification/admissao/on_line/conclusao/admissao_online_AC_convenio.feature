#language: pt
#encoding: utf-8
@conclusao_do_cadastro_ac_convenio @done_admissao_conclusao
Funcionalidade: Conclusão do cadastro de exames de análises clínicas na admissão on-line pelo convênio
 Como atendente do sistema Gliese
 Posso manter a conclusão do cadastro de exames de análises clínicas na admissão on-line
 Para gerenciar a conclusão do cadastro de exames de análises clínicas na visita de paciente

Esquema do Cenario: Conclusão do cadastro de exames de análises clínicas pelo convênio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  #E informo o convênio "Amil"
  E informo o convênio "Sul America"
  E clico no botão Seleciona
  #E são exibidas as instruções do convênio Amil
  #E preencho as informações do convênio para o convênio Sul America <Sul_America_Codigo_Associado>, <Sul_America_Data_Solicitação>, <Sul_America_Nome_Plano>, <Sul_America_Plano_Empresa>, <Sul_America_Nome_Plano>, <Sul_America_Quantidade_Etiquetas>, <Sul_America_Produto>
  E preencho as informações do convênio para o convênio Sul America <Sul_America_Codigo_Associado>, <Sul_America_Data_Solicitação>, <Sul_America_Nome_Plano>, <Sul_America_Plano_Empresa>, <Sul_America_Quantidade_Etiquetas>, <Sul_America_Validade_Cartao>, <Sul_America_Produto>
  E clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação  | Pg | Impr |
  | SU             | CLCR            | CLEARANCE DE CREATININA    |                    | 0               |                      | false   | 40301508     | S  | S    |
  | SV             | CORT            | CORTISOL SALIVAR           |                    |                 |                      | false   | 40316190     | S  | S    |
  | UR             | EAS             | EAS - URINA TIPO 1         |                    |                 |                      | true    | 40311210     | S  | S    |
  | EP             | EGA             | ESPERMOGRAMA AUTOMATIZADO  |                    |                 |                      | false   | 40309320     | S  | S    |
  | FE             | EP              | PARASITOLOGICO             |                    |                 |                      | false   | 40303110     | S  | S    |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361     | S  | S    |
  | HI             | PAT             | HISTOPATOLOGICO            |                    |                 | 6-Biopsia de Pele    | false   | 40601110     | S  | S    |
  | LI             | PT              | PROTEINAS                  |                    |                 |                      | true    | 40302377     | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  Quando são exibidos os recipientes dos exames
  | Recipiente   | Compl_material | Recipiente        | Unidade_Realização | Setor | Exames |
  | 1790028020   | 1,3,4,5        | U-24H             | SED                | BIOQ  | CLCRSU |
  | 1790028021	 | 2              | SORO GEL          | SED                | BIOQ  | CLCRSU |
  | 823274781648 |                | SALIVETTE ESPE    | ALV                | HORM  | CORTSV |
  | 1790028022   |                | EAS-RJ            | SED                | URIN  | EASUR  |
  | 1790028023   |                | FRASCO P/ ESPERMA | SED                | URIN  | EGAEP  |
  | 1790028024   |                | SAF               | SED                | PARA  | EPFE   |
  | 1790028026   |                | EDTA              | SED                | HEMA  | HSA    |
  | 823274781655 |                | MISC              | DAS                | HIST  | PATHI  |
  | 1790028025   |                | F-ESTERIL         | SED                | BIOQ  | PTLI   |
  E clico na aba Diversos
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado                         | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 51 - Express-paciente Entrega A Domicilio | A1                | Não             | Comentário observações/técnicas |                          | Não            |                    | 80      | 180    |                   |                  | 3                | Comentário indicação/clínica. | Sim           | Não                      |                   |             | Sim        | Jantar          | 10012018             | 200000               |
  E clico na aba Múltiplos Médicos
  E é exibida a UF da unidade selecionada no campo UF na aba Múltiplos Médicos 
  E informo o Número do Registro "52818402" na aba Múltiplos Médicos
  E o nome do médico "Joao Gabriel Garcia Alves" é carregado no campo Nome da aba Múltiplos Médicos
  E clico na opção Adiciona da aba Múltiplos Médicos
  E o médico é adicionado na seção Exames/Médico(s) da aba Múltiplos Médicos
  E seleciono os Exames/Médico(s)
  | exame                                       | medico1 | medico2 |
  | CLCRSU CLEARENCE DE CREATININA              | true    | false   |
  | CORTSV CORTISOL SALIVAR                     | true    | false   |
  | EASUR ELEMENTOS ANORMAIS E SEDIMENTO        | true    | false   |
  | EGAEP ESPERMOGRAMA AUTOMATIZADO             | true    | false   |
  | EPFE PARASITOLOGICO                         | false   | true    |
  | HSA HEMOGRAMA COMPLETO                      | false   | true    |
  | PATHI HISTOPATOLOGICO                       | false   | true    |
  | PTLI PROTEINAS                              | false   | true    |
  E clico na aba Conclusão
  E é exibida a mensagem de admissão sem pendências "Admissão sem pendências"
  E clico na apção Salvar
  E é exibida a mensagem de confirmação de exibição "Confirma Admissao"
  Quando clico no botão OK
  Então é exibido o Espelho de Atendimento, Prazo de Exames para Visita e a mensagem "Admissão Autorizada"

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome              | medico_crm | medico_uf | Sul_America_Codigo_Associado  | Sul_America_Data_Solicitação| Sul_America_Nome_Plano       | Sul_America_Plano_Empresa | Sul_America_Quantidade_Etiquetas | Sul_America_Validade_Cartao   | Sul_America_Produto |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "21021968"      | "2213192684" | "Marcio Alves Tannure"   | "52773794" | "RJ"      | "11111111111111110"           | "05012018"                  | "especial"                   | "Sim"                     | "1"                                | "01012020"                  | "204"               |
  | "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "21021968"      | "2213192684" | "Marcio Alves Tannure"   | "52773794" | "RJ"      | "11111111111111110"           | "05012018"                  | "especial"                   | "Sim"                     | "1"                                | "01012020"                  | "204"               |