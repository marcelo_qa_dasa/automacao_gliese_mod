#language: pt
#encoding: utf-8
@conclusao_geral @done_geral_conclusao
Funcionalidade: Conclusão do cadastro de exames na admissão on-line
 Como atendente do sistema Gliese
 Posso manter a conclusão do cadastro de exames na admissão on-line
 Para gerenciar a conclusão do cadastro de exames na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja na home do sistema Gliese
  | username | password |
  | TESTER04 | 1234   |
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento e CIP com dados validos
  E clico na aba Visita
  E crio uma nova Visita

@concluir_admissao_sem_informar_medico @done_geral_conclusao
Cenário: Concluir admissão on-line sem informar o médico
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame     | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | OS             | CDA             | COLETA DOMICILIAR A |                    |                 |                      | false   | 28170016    | S  | S    | 
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  Quando clico na aba Conclusão
  Então é exibida a mensagem "Existem Pendências na admissão" e são informadas pendências na visita
  | aba        | pendencia                     |
  | Diversos   | Medicamento obrigatório       |
  | Diversos   | Indicação clínica obrigatória |
  | Diversos   | última refeição               |
  | Financeiro | Visita sem forma de pagamento | 

@concluir_admissao_sem_informar_convenio @done_geral_conclusao
Cenário: Concluir admissão on-line sem informar o convênio
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  Quando clico na aba Conclusão
  Então é exibida a mensagem de preenchimento do campo convenio "Favor preencher o campo convênio"

@concluir_admissao_sem_informar_dados_convenio @done_geral_conclusao
Cenário: Concluir admissão on-line sem informar dados do convenio
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                  | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | TIBC            | CAPACIDADE DE COMBINACAO DO FE...|                    |                 |                      | false   | 40301427    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  Quando clico na aba Conclusão
  Então é exibida a mensagem "Existem Pendências na admissão" e são informadas pendências na visita
  | aba        | pendencia                     |
  | Convênio   | Prazo do recebimento expirado.|
  | Diversos   | Medicamento obrigatório       |
  | Diversos   | Meio de resultado obrigatório |
  | Diversos   | Indicação clínica obrigatória |
  | Diversos   | última refeição               |

@concluir_admissao_sem_cadastrar_exames @done_geral_conclusao
Cenário: Concluir admissão on-line sem cadastrar exames
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  Quando clico na aba Conclusão
  Então é exibida a mensagem de visita sem exames válidos "Visita sem exames válidos"

@concluir_admissao_particular_sem_informar_diversos @done_geral_conclusao
Cenário: Concluir admissão on-line no particular sem informar dados diversos
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                  | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | TIBC            | CAPACIDADE DE COMBINACAO DO FE...|                    |                 |                      | false   | 40301427    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  Quando clico na aba Conclusão
  Então é exibida a mensagem "Existem Pendências na admissão" e são informadas pendências na visita
  | aba        | pendencia                     |
  | Diversos   | Medicamento obrigatório       |
  | Diversos   | Meio de resultado obrigatório |
  | Diversos   | Indicação clínica obrigatória |
  | Diversos   | última refeição               |

@concluir_admissao_convenio_sem_informar_diversos @done_geral_conclusao
Cenário: Concluir admissão on-line pelo convênio sem informar dados diversos
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  Quando clico na aba Conclusão
  Então é exibida a mensagem "Existem Pendências na admissão" e são informadas pendências na visita
  | aba        | pendencia                     |
  | Diversos   | Medicamento obrigatório       |
  | Diversos   | Indicação clínica obrigatória |
  | Diversos   | última refeição               |

@concluir_admissao_convenio_sem_informar_data_hora_refeicao @done_geral_conclusao
Cenário: Concluir admissão on-line sem informar data e hora da última refeição
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  E clico na aba Exames
  E é incrementado o número de Sangue e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  | Comentário indicação/clínica. |               |                          |                   |             |            | Jantar          |                      |                      |
  Quando clico na aba Conclusão
  Então é exibida a mensagem "Existem Pendências na admissão" e são informadas pendências na visita
  | aba        | pendencia            |
  | Diversos   | Data última refeição |
  | Diversos   | Hora última refeição |

@concluir_admissao_convenio_sem_informar_dados_exames @done_geral_conclusao
Cenário: Concluir admissão on-line sem informar dados de exames
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  E clico na aba Exames
  E é incrementado o número de Sangue e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SU             | CLCR            | CLEARANCE DE CREATININA    |                    | 0               |                      | false   | 40301508    | S  | S    |
  | HI             | PAT             | HISTOPATOLOGICO            |                    |                 | 6-Biopsia de Pele    | false   | 40601110    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  | Comentário indicação/clínica. |               |                          |                   |             |            | Jantar          | 27112017             | 222222               |
  Quando clico na aba Conclusão
  Então é exibida a mensagem "Existem Pendências na admissão" e são informadas pendências na visita
  | aba        | pendencia                                          |
  | Exames     | Número do volume de material inválido - CLCRSU - 2 |
  | Exames     | Complemento de material obrigatório2 - PATHI - 3   |
  | Diversos   | Peso obrigatório                                   |
  | Diversos   | Altura obrigatório                                 |

@concluir_admissao_convenio_sem_informar_multiplos_medicos @done_geral_conclusao
Cenário: Concluir admissão on-line sem informar múltiplos médicos
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  E clico na aba Exames
  E é incrementado o número de Sangue e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  | Comentário indicação/clínica. | Sim           |                          |                   |             |            | Jantar          | 27112017             | 222222               |
  Quando clico na aba Conclusão
  Então é exibida a mensagem "Existem Pendências na admissão" e são informadas pendências na visita
  | aba               | pendencia                                                  |
  | Múltiplos Médicos | existe pelo menos um exame que está sem médico designado   |
  | Múltiplos Médicos | existe pelo menos um médico que está sem exame selecionado |

@concluir_admissao_particular_sem_informar_financeiro @done_geral_conclusao
Cenário: Concluir admissão on-line no particular sem informar dados financeiros
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Sangue e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. |                   |                   |                 |                      |                         |                |                    |         |        |                   |                  |                  | Comentário indicação/clínica. |               |                          |                   |             |            | Jantar          | 27112017             | 222222               |
  Quando clico na aba Conclusão
  Então é exibida a mensagem "Existem Pendências na admissão" e são informadas pendências na visita
  | aba        | pendencia                     |
  | Financeiro | Visita sem forma de pagamento |

@cancelar_admissao @done_geral_conclusao
Cenário: Cancelar admissão on-line
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "15122017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  Então clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado      | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	           | Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 503 - Correio Paciente | 1                 | Sim             | Comentário observações técnicas | Comentário observações atendimento | Sim            |                    |         |        |                   |                  |                  | Comentário indicação/clínica. | Não           | Sim                      |                   | 247 - BELEM | Sim        | Jantar          | 25102017             | 200000               |
  E clico na aba Conclusão
  E é exibida a mensagem de admissão sem pendências "admissão sem pendências" no cancelamento
  E clico na apção Salvar
  Quando é exibida a mensagem de confirmação de exibição "Confirma Admissao"
  Então clico no botão Cancelar

@abandonar_admissao @done_geral_conclusao
Cenário: Abandonar admissão on-line
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "15122017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  Então clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                  | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | TIBC            | CAPACIDADE DE COMBINACAO DO FE...|                    |                 |                      | false   | 40301427    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado      | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	           | Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 503 - Correio Paciente | 1                 | Sim             | Comentário observações técnicas | Comentário observações atendimento | Sim            |                    |         |        |                   |                  |                  | Comentário indicação/clínica. | Não           | Sim                      |                   | 247 - BELEM | Sim        | Jantar          | 25102017             | 200000               |
  E clico na aba Conclusão
  E é exibida a mensagem de admissão sem pendências "admissão sem pendências"
  E clico na apção Abandonar
  Quando é exibida a mensagem de confirmação de abandono "Abandonar admissão?" e clico no botão ok
  Então a aba Paciente é carregada com os dados do paciente

@cancelar_abandono_admissao @done_geral_conclusao
Cenário: Cancelar abandono de admissão on-line
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "15122017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  Então clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado      | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	           | Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 503 - Correio Paciente | 1                 | Sim             | Comentário observações técnicas | Comentário observações atendimento | Sim            |                    |         |        |                   |                  |                  | Comentário indicação/clínica. | Não           | Sim                      |                   | 247 - BELEM | Sim        | Jantar          | 25102017             | 200000               |
  E clico na aba Conclusão
  E é exibida a mensagem de admissão sem pendências "admissão sem pendências"
  E clico na apção Abandonar
  Quando é exibida a mensagem de confirmação de abandono "Abandonar admissão?"
  Então clico no botão Cancelar
