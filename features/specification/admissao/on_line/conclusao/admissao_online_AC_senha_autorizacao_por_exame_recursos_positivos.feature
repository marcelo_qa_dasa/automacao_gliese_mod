#language: pt
#encoding: utf-8
@admissao_online_ac_senha_recursos_positivos @to_do
Funcionalidade: Conclusão do cadastro de exames de AC que exigem senha na admissão on-line pelo convênio
 Como atendente do sistema Gliese
 Posso manter a conclusão do cadastro de exames de AC que exigem senha na admissão on-line
 Para gerenciar a conclusão do cadastro de exames de AC que exigem senha na visita de paciente

Esquema do Cenario: Conclusão do cadastro de exames de AC que exibem o campo senha pelo convênio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Amil"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio Amil <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Amil_Quantidade_Etiquetas>
  E clico na aba Exames
  # E preencho um material e exame que exibe o campo senha
  E é incrementado o número de Sangue e o Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame      | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | HIV-1           | HIV 1+2 - ANTICORPOS |                    |                 |                      | false   | 40307182    | S  | S    |     
  E clico na opção Senha Recurso da aba exames
  E valido a exibição do exame na pop-up Informe a Senha Recurso do exame
  E informo a Senha "111111" na pop-up Informe a Senha Recurso do exame
  E clico no botão Salvar na pop-up Informe a Senha Recurso do exame
  E é exibida a mensagem de senha registrada "Senha do convênio registrada"
  E clico na aba Recipientes
  Quando são exibidos os recipientes dos exames
  | Recipiente   | Compl_material | Recipiente        | Unidade_Realização | Setor | Exames    |
  | 1790030056   |                | SORO HIV          | SED                | BIOQ  | HIV-1SA   |
  E clico na aba Diversos
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado                         | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 51 - Express-paciente Entrega A Domicilio | A8                | Não             | Comentário observações/técnicas |                          | Não            |                    |         |        |                   |                  |                  | Comentário indicação/clínica. | Não           | Não                      |                   |             | Não        | Jantar          | 25102017             | 200000               |
  E clico na aba Conclusão
  E é exibida a mensagem de admissão sem pendências "admissão sem pendências"
  E clico na apção Salvar
  E é exibida a mensagem de confirmação de exibição "Confirma Admissao"
  Quando clico no botão OK
  Então é exibido o Espelho de Atendimento, Prazo de Exames para Visita e a mensagem "Admissão Autorizada"

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome                 | medico_crm | medico_uf | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01011912"      | "2600335540" | "Natalia A.c. Freitas" | "10144043"     | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01011912"      | "2600335540" | "Natalia A.c. Freitas" | "10144043"     | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
