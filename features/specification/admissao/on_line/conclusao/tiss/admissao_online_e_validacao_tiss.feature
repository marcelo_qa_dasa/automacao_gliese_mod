#language: pt
#encoding: utf-8
@conclusao_ac_convenio @to_do
Funcionalidade: Conclusão do cadastro de exames na admissão on-line
 Como atendente do sistema Gliese
 Posso manter a conclusão do cadastro de exames na admissão on-line
 Para gerenciar a conclusão das manutenções do relatório tiss

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja na home do sistema Gliese pela empresa e unidade "Image" e "ICG - Image Campo Grande"
  E clico no menu Admissão > On Line
  E preencho os dados do paciente com data de nascimento "01012000" e CIP "99999"
  # E preencho os campos Data de nascimento e CIP com dados validos
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E seleciono daso do Medico sendo a uf "BA", selecione pelo "CRM" e o codigo "10"
  E clico na aba Convênio
  E Preencho os dados do convenio
  | Convenio           | Codigo_Associado     | Validade_cartao | Nome_Plano                          | Plano_Empresa | Validade_pedido  |
  |  Im-apubim         | "11111111111111111"  | "21092019"      | Alfa                                |  Sim          |     "21092019"   |
  Então clico na aba Exames
  E preencho exame com massa especifica para convenio com campo CID
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | DA-AACE         | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado        | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	           | Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |    cid    |
  | Comentário medicamento/cirurgia. | 1 - Laudo - Posto Origem | 1                 | Sim             | Comentário observações técnicas | Comentário observações atendimento | Sim            |                    |   90    |        |       111111      |    "04102017"    |                  | Comentário indicação/clínica. | Não           | Sim                      |        FEZES      | 247 - BELEM | Sim        | Jantar          | 25102017             | 200000               |   test17  |
  E clico na aba Conclusão
  E clico na apção Salvar
  Então confirmo as mensagens até realizar impressao da guia tiss

@validar_campo_cid_no_tiss @to_do
Cenário: Validar existencia do campo novo CID no tiss
  Então valido o campo CID no relatorio tiss
