#language: pt
#encoding: utf-8
@admissao_online_rdi_particular @done_admissao_conclusao
Funcionalidade: Conclusão do cadastro de exames de RDI na admissão on-line no particular
 Como atendente do sistema Gliese
 Posso manter a conclusão do cadastro de exames de RDI pelo convênio na admissão on-line
 Para gerenciar a conclusão do cadastro de exames de RDI pelo convênio na visita de paciente

Esquema do Cenario: Conclusão do cadastro de exames de RDI no particular
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Cd-particular"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Imagem e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                   | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | 24              | COL CERVICAL 7INC                 |                    |                 |                      | false   | 32020031    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado    | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. |                      | A10               | Não             | Comentário observações/técnicas |                         | Não            |                    | 80      | 180    |                   |                  |                  | Comentário indicação/clínica. | Não           | Não                      | Comentário exames anteriores. |             | Sim        | Jantar          | 25102017             | 200000               |
  E clico na aba Financeiro
  E preencho a forma de pagamento, clico no botão Salvar
  | forma_de_pgto | banco | agencia | conta | cheque | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut |valor_opcional |
  | Dinheiro      |       |         |       |        |          |          |               |           |            |          |        |               |  
  E valido a forma de pagamento na Relação de formas de pagamento, o Total, Total Recebido, Falta e Troco
  E clico na opção Desconto
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | MASTER  | caxias2016 | Desconto NAC 5% |            |                |        |
  E o Percentual de Desconto e Total com Desconto é atualizado na aba Financeiro
  E clico na opção N.F. Terceiro na aba Financeiro
  E são preenchidas as informações da nota fiscal de terceiros
  | nome           | logradouro | numero | complemento | bairro                | municipio | estado | cep | cnpj_cpf      |
  | Diego da Silva | Av. Juruá  | 548    |             | Alphaville Industrial |           | SP     |     | 1111111111111 |
  E clico no botão Salvar
  E é exibida a mensagem de dados salvos com sucesso "Dados salvos com sucesso."
  E clico na aba Conclusão
  E é exibida a mensagem de admissão sem pendências "admissão sem pendências"
  E clico na apção Salvar
  E é exibida a mensagem de confirmação de exibição "Confirma Admissao"
  E clico no botão OK
  E é exibida a mensagem de confirmação de exibição "Deseja imprimir o protocolo de exames de imagem?"
  E clico no botão OK
  E é exibida a mensagem de confirmação de exibição "Este convenio imprime R P S, utilize a opção adequada no Sistema!"
  Quando clico no botão OK
  Então é exibido o protocolo de exames de imagem "Admissão Autorizada"
  
  Exemplos:
  | username   | password | Marca                                 | Unidade               | Data_Nascimento  | CIP          | medico_nome         | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Diagnosticos da America S.a - Cedic" | "T01 - Cedic - Barao" | "01081976"       | "2213151265" | "Joao Aguiar Brito" | "1605"     | "MA"      |
  #| "TESTER01" | "123456" | "Diagnosticos da America S.a - Cedic" | "T01 - Cedic - Barao" | "01081976"       | "2213151265" | "Joao Aguiar Brito" | "1605"     | "MA"      |
