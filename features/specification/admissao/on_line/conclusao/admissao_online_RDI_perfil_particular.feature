#language: pt
#encoding: utf-8
@admissao_online_rdi_perfil_particular @done_admissao_conclusao
Funcionalidade: Conclusão do cadastro de exames de perfil RDI na admissão on-line no particular
 Como atendente do sistema Gliese
 Posso manter a conclusão do cadastro de exames de perfil RDI na admissão on-line
 Para gerenciar a conclusão do cadastro de exames de perfil RDI na visita de paciente

Esquema do Cenario: Conclusão do cadastro de exames de perfil RDI no particular
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Lm-ptregiao1"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                  | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | TCAT            | TC ABDOMEN TOTAL ABDOMEN SUP +...|                    |                 |                      | false   | 41001095    | N  | N    |
  E confirmo o procedimento de atendimento LMFPVTC1IM
  E é incrementado o número de Imagem e o Total no Resumo da aba Exames e são adicionados os exames filhos dos exames de perfil previamente admitidos
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame           | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | FPVTC1          | TC PELVICA                |                    |                 |                      | false   | 41001117    | S  | S    |
  | IM             | TCAS            | TC ABDOMEN SUPERIOR       |                    |                 |                      | false   | 41001109    | S  | S    |
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado                | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 52 - Automatico - Email Paciente | A12               | Não             | Comentário observações/técnicas |                         | Não            |                    | 80      | 180    |                   |                  |                  | Comentário indicação/clínica. | Não           | Não                      |                               |             | Não        | Jantar          | 29102017             | 200000               |
  E clico na aba Financeiro
  Quando preencho a forma de pagamento, clico no botão Salvar
  | forma_de_pgto | banco | agencia | conta | cheque | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut |valor_opcional |
  | Dinheiro      |       |         |       |        |          |          |               |           |            |          |        |               |  
  Então valido a forma de pagamento na Relação de formas de pagamento, o Total, Total Recebido, Falta e Troco
  E clico na opção Desconto
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto | percentual | valor_em_reais | motivo          |
  | MASTER  | caxias2016 | Outros   |            | 1.00           | motivo_desconto |
  #E 
  E o Percentual de Desconto e Total com Desconto é atualizado na aba Financeiro
  E clico na aba Conclusão
  E é exibida a mensagem de admissão sem pendências "admissão sem pendências"
  E clico na apção Salvar
  E é exibida a mensagem de confirmação de exibição "Confirma Admissao"
  E clico no botão OK
  E é exibida a mensagem de confirmação de exibição "Deseja imprimir o protocolo de exames de imagem?"
  E clico no botão OK
  E é exibida a mensagem de confirmação de exibição "Confirma Impressão da Guia Tiss?"
  E clico no botão OK
  E é exibida a mensagem de confirmação de exibição "Este convenio imprime R P S, utilize a opção adequada no Sistema!"
  E clico no botão OK
  Então é exibido o protocolo de exames de imagem "Admissão Autorizada"

  Exemplos:
  | username   | password | Marca                                              | Unidade                   | Data_Nascimento | CIP          | medico_nome             | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Diagnosticos da America - Lamina Med. Diag. Ltda" | "LPO - Lamina - Arpoador" | "08021949"      | "6000981914" | "Matheus Freitas Leite"  | "23479"   | "BA"      | 
  #| "TESTER01" | "123456" | "Diagnosticos da America - Lamina Med. Diag. Ltda" | "LPO - Lamina - Arpoador" | "08021949"      | "6000981914" | "Matheus Freitas Leite"  | "23479"   | "BA"      |
