#language: pt
#encoding: utf-8
@conclusao_ac_particular @done_admissao_conclusao
Funcionalidade: Conclusão do cadastro de exames de análises clínicas na admissão on-line no particular
 Como atendente do sistema Gliese
 Posso manter a conclusão do cadastro de exames de análises clínicas na admissão on-line
 Para gerenciar a conclusão do cadastro de exames de análises clínicas na visita de paciente

Esquema do Cenario: Conclusão do cadastro de exames de análises clínicas no particular
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Br-bpbarraplaza"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação  | Pg | Impr |
  | SU             | CLCR            | CLEARANCE DE CREATININA    |                    | 0               |                      | false   | 40301508     | S  | S    |
  | SV             | CORT            | CORTISOL SALIVAR           |                    |                 |                      | false   | 40712192     | S  | S    |
  | UR             | EAS             | EAS - URINA TIPO 1         |                    |                 |                      | true    | 00000000     | S  | S    |
  #| EP             | EGA             | ESPERMOGRAMA AUTOMATIZADO  |                    |                 |                      | false   | 40309312     | S  | S    |
  | FE             | EP              | PARASITOLOGICO             |                    |                 |                      | false   | 00000000     | S  | S    |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 00000000     | S  | S    |
  | HI             | PAT             | HISTOPATOLOGICO            |                    |                 | 6-Biopsia de Pele    | false   | 40601056     | S  | S    |
  | LI             | PT              | PROTEINAS                  |                    |                 |                      | true    | 40302377     | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  Quando são exibidos os recipientes dos exames
  | Recipiente   | Compl_material  | Recipiente        | Unidade_Realização | Setor | Exames |
  | 1790028020   | 1,3,4,5         | U-24H             | SED                | BIOQ  | CLCRSU |
  | 1790028021	 | 2               | SORO GEL          | SED                | BIOQ  | CLCRSU |
  | 823274781648 |                 | SALIVETTE ESPE    | ALV                | HORM  | CORTSV |
  | 1790028022   |                 | EAS-RJ            | SED                | URIN  | EASUR  |
  #| 1790028023   |                 | FRASCO P/ ESPERMA | SED                | URIN  | EGAEP  |
  | 1790028024   |                 | SAF               | SED                | PARA  | EPFE   |
  | 1790028026   |                 | EDTA              | SED                | HEMA  | HSA    |
  | 823274781655 |6-Biopsia de Pele| MISC              | DAS                | HIST  | PATHI  |
  | 1790028025   |                 | F-ESTERIL         | SED                | BIOQ  | PTLI   |
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado                         | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 51 - Express-paciente Entrega A Domicilio | A4                | Não             | Comentário observações/técnicas |                          | Não            |                    | 80      | 180    |                   |                  | 3                | Comentário indicação/clínica. | Sim           | Não                      |                               |             | Não        | Jantar          | 25102017             | 200000               |
  E clico na aba Financeiro
  E preencho a forma de pagamento, clico no botão Salvar
  | forma_de_pgto | banco | agencia | conta | cheque | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut |
  | Cheque        | Itau  | 555     | 6666  | 77777  |          |          |               |           |            |          |        |
  E valido a forma de pagamento na Relação de formas de pagamento, o Total, Total Recebido, Falta e Troco
  E clico na opção Desconto
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto | percentual | valor_em_reais | motivo          |
  | MASTER  | caxias2016 | Outros   | 10         |                | motivo_desconto |
  E o Percentual de Desconto e Total com Desconto é atualizado na aba Financeiro
  E clico na aba Múltiplos Médicos
  E é exibida a UF da unidade selecionada no campo UF na aba Múltiplos Médicos 
  E informo o Número do Registro "52479251" na aba Múltiplos Médicos
  E o nome do médico "Sonia Silva Prado" é carregado no campo Nome da aba Múltiplos Médicos
  E clico na opção Adiciona da aba Múltiplos Médicos
  E o médico é adicionado na seção Exames/Médico(s) da aba Múltiplos Médicos
  E seleciono os Exames/Médico(s)
  | exame                                | medico1 | medico2 |
  | CLCRSU CLEARENCE DE CREATININA       | true    | true    |
  | CORTSV CORTISOL SALIVAR	             | true    | true    |
  | EASUR ELEMENTOS ANORMAIS E SEDIMENTO | true    | true    |
  | EPFE PARASITOLOGICO	                 | true    | true    |
  | HSA HEMOGRAMA COMPLETO	             | true    | true    |
  | PATHI HISTOPATOLOGICO	               | true    | true    |
  | PTLI PROTEINAS	                     | true    | true    |
  E clico na aba Conclusão
  E é exibida a mensagem de admissão sem pendências "admissão sem pendências"
  E clico na apção Salvar
  E é exibida a mensagem de confirmação de exibição "Confirma Admissao"
  E clico no botão OK
  E é exibida a mensagem de confirmação de exibição "Este convenio imprime R P S, utilize a opção adequada no Sistema!"
  Quando clico no botão OK
  Então é exibido o Prazo de Exames para Visita e a mensagem "Admissão Autorizada"
  
  Exemplos:
  | username   | password | Marca       | Unidade                         | Data_Nascimento | CIP          | medico_nome           | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011912"      | "1224189291" | "Marcio Alves Tannure"| "52773794" | "MG"      |
  #| "TESTER01" | "123456" | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011912"      | "1224189291" | "Marcio Alves Tannure"| "52773794" | "MG"      |
