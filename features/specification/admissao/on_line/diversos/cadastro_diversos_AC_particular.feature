#language: pt
#encoding: utf-8
@diversos_ac_particular @done_diversos
Funcionalidade: Manutenção de informações diversas de exames de análises clínicas na admissão on-line no particular
 Como atendente do sistema Gliese
 Posso manter o cadastro de informações diversas de exames de análises clínicas na admissão on-line
 Para gerenciar informações diversas de exames de análises clínicas na visita de paciente

Esquema do Cenario: Cadastro de informações diversas de exames de análises clínicas no particular
 E clico na aba Convênio
  E informo o convênio "Br-bpbarraplaza"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação  | Pg | Impr |
  | SU             | CLCR            | CLEARANCE DE CREATININA    |                    | 0               |                      | false   | 40301508     | S  | S    |
  | SV             | CORT            | CORTISOL SALIVAR           |                    |                 |                      | false   | 40316190     | S  | S    |
  | UR             | EAS             | EAS - URINA TIPO 1         |                    |                 |                      | true    | 40311210     | S  | S    |
  | EP             | EGA             | ESPERMOGRAMA AUTOMATIZADO  |                    |                 |                      | false   | 40309312     | S  | S    |
  | FE             | EP              | PARASITOLOGICO             |                    |                 |                      | false   | 40303110     | S  | S    |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361     | S  | S    |
  | HI             | PAT             | HISTOPATOLOGICO            |                    |                 | 6-Biopsia de Pele    | false   | 40601110     | S  | S    |
  | LI             | PT              | PROTEINAS                  |                    |                 |                      | true    | 40302377     | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  Quando são exibidos os recipientes dos exames
  | Recipiente   | Compl_material | Recipiente        | Unidade_Realização | Setor | Exames |
  | 1790028020   | 1,3,4,5        | U-24H             | SED                | BIOQ  | CLCRSU |
  | 1790028021	 | 2              | SORO GEL          | SED                | BIOQ  | CLCRSU |
  | 823274781648 |                | SALIVETTE ESPE    | ALV                | HORM  | CORTSV |
  | 1790028022   |                | EAS-RJ            | SED                | URIN  | EASUR  |
  | 1790028023   |                | FRASCO P/ ESPERMA | SED                | URIN  | EGAEP  |
  | 1790028024   |                | SAF               | SED                | PARA  | EPFE   |
  | 1790028026   |                | EDTA              | SED                | HEMA  | HSA    |
  | 823274781655 |                | MISC              | DAS                | HIST  | PATHI  |
  | 1790028025   |                | F-ESTERIL         | SED                | BIOQ  | PTLI   |
  Quando clico na aba Diversos
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado                         | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 51 - Express-paciente Entrega A Domicilio | A4                | Não             | Comentário observações/técnicas |                          | Não            |                    | 80      | 180    |                   |                  | 3                | Comentário indicação/clínica. | Sim           | Não                      |                               |             | Não        | Jantar          | 25102017             | 200000               |

  Exemplos:
  | username   | password | Marca       | Unidade                         | Data_Nascimento | CIP          | medico_nome           | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011912"      | "1224189291" | "Marcio Alves Tannure"| "52773794" | "MG"      |
  #| "TESTER01" | "123456" | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011912"      | "1224189291" | "Marcio Alves Tannure"| "52773794" | "MG"      |
