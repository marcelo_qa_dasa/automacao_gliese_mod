#language: pt
#encoding: utf-8
@diversos_rdi_perfil_convenio @done_diversos
Funcionalidade: Manutenção de informações diversas de exames de perfil RDI na admissão on-line pelo convênio
 Como atendente do sistema Gliese
 Posso manter o cadastro de informações diversas de exames de perfil RDI na admissão on-line
 Para gerenciar informações diversas de exames de perfil RDI na visita de paciente

Esquema do Cenario: Cadastro de informações diversas de exames de perfil RDI pelo convênio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Lm-amilfunc"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio br_amilimagem <Amil_Codigo_Associado>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>
  E clico na aba Exames
  E preencho um material e exame não realizado na unidade
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                  | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | TCAT            | TC ABDOMEN TOTAL ABDOMEN SUP +...|                    |                 |                      | false   | 0           | N  | N    |
  E clico no botão valida da aba exames é exibida a mensagem de autorização "Este exame precisa de autorização. LM-TCATIM"
  E clico no botão OK
  E informo a identificação do médico "dpi00", data da agenda e a hora da agenda
  E clico na opção Senha da aba exames é exibida a mensagem de autorização "Este exame precisa de autorização. LM-FPVTC1IM"
  E clico no botão OK é exibida a mensagem de autorização "Este exame precisa de autorização. LM-TCASIM"
  E clico no botão OK
  E confirmo o procedimento de atendimento LMFPVTC1IM
  Então é incrementado o número de Imagem e o Total no Resumo da aba Exames e são adicionados os exames filhos dos exames de perfil previamente admitidos
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame           | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | FPVTC1          | TC PELVICA                |                    |                 |                      | false   | 41001117    | S  | S    |
  | SA             | TCAS            | TC ABDOMEN SUPERIOR       |                    |                 |                      | false   | 41001109    | S  | S    |
  E clico na aba Recipientes
  E são exibidos os recipientes dos exames
  | Recipiente   | Compl_material       | Recipiente        | Unidade_Realização | Setor | Exames                            |
  | 1790030046   |                      | SEMREC  	        | SED                | TOMO  | LM-TCATIM, LM-FPVTC1IM, LM-TCASIM |
  Quando clico na aba Diversos
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado                | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 52 - Automatico - Email Paciente | A11               | Não             | Comentário observações/técnicas |                         | Não            |                    | 80      | 180    | 11111111111111111 | 30102017         |                  | Comentário indicação/clínica. | Não           | Sim                      | Comentário exames anteriores. |             | Não        | Jantar          | 29102017             | 200000               |

  Exemplos:
  | username   | password | Marca                                               | Unidade                   | Data_Nascimento | CIP          | medico_nome             | medico_crm | medico_uf | Amil_Passe_Cartão | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano | Amil_Plano_Empresa |
  | "TESTER04" | "1234"   | "Diagnosticos da America - Lamina Med. Diag. Ltda"  | "LPO - Lamina - Arpoador" | "01011912"      | "6006817892" | "Liana Moreira Freitas" | "17231"    | "CE"      | "121212122"       | "121212122"           | "27092017"            | "Amil 150"      | "Sim"              |
  #| "TESTER01" | "123456" | "Diagnosticos da America - Lamina Med. Diag. Ltda"  | "LPO - Lamina - Arpoador" | "01011912"      | "6006817892" | "Liana Moreira Freitas" | "17231"    | "CE"      | "121212122"       | "121212122"           | "27092017"            | "Amil 150"      | "Sim"              |
