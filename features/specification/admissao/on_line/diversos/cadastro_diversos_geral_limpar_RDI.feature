#language: pt
#encoding: utf-8
@diversos_geral_limpar_rdi
Funcionalidade: Manutenção de informações diversas de exames na admissão on-line (Limpar RDI)
 Como atendente do sistema Gliese
 Posso manter o cadastro de informações diversas de exames pelo convênio na admissão on-line
 Para gerenciar informações diversas de exames pelo convênio na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  E clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                   | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | UABT            | ULTRA SOM ABDOMEN TOTAL (ABD S... |                    |                 |                      | false   | 40901122    |  S | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos

@limpar_cadastro_diversos_rdi @done_diversos
Cenário: Limpar cadastro diversos de exames de RDI
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado                | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 52 - Automatico - Email Paciente | A9                | Não             | Comentário observações/técnicas |                         | Não            |                    | 80      | 180    |                   |                  |                  | Comentário indicação/clínica. | Não           | Sim                      | Comentário exames anteriores. |             | Não        | Jantar          | 25102017             | 200000               |
  Quando clico no botão Limpa da aba Diversos
  Então todos os campos voltam para as suas definições padrão na aba Diversos