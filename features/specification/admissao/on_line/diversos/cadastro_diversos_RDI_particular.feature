#language: pt
#encoding: utf-8
@diversos_rdi_particular @done_diversos
Funcionalidade: Manutenção de informações diversas de exames de RDI na admissão on-line no particular
 Como atendente do sistema Gliese
 Posso manter o cadastro de informações diversas de exames de RDI pelo convênio na admissão on-line
 Para gerenciar informações diversas de exames de RDI pelo convênio na visita de paciente

Esquema do Cenario: Cadastro de informações diversas de exames de RDI no particular
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Cd-particular"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Imagem e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                   | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | 24              | COL CERVICAL 7INC                 |                    |                 |                      | false   | 32020031    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  Quando clico na aba Diversos
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado    | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. |                      | A10               | Não             | Comentário observações/técnicas |                         | Não            |                    | 80      | 180    |                   |                  |                  | Comentário indicação/clínica. | Não           | Não                      | Comentário exames anteriores. |             | Sim        | Jantar          | 25102017             | 200000               |

  Exemplos:
  | username   | password | Marca                                 | Unidade               | Data_Nascimento  | CIP          | medico_nome         | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Diagnosticos da America S.a - Cedic" | "T01 - Cedic - Barao" | "01081976"       | "2213151265" | "Joao Aguiar Brito" | "1605"     | "MA"      |
  #| "TESTER01" | "123456" | "Diagnosticos da America S.a - Cedic" | "T01 - Cedic - Barao" | "01081976"       | "2213151265" | "Joao Aguiar Brito" | "1605"     | "MA"      |
