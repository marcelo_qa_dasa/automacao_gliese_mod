#language: pt
#encoding: utf-8
@diversos_geral  @done_diversos
Funcionalidade: Manutenção de informações diversas de exames na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de informações diversas de exames pelo convênio na admissão on-line
 Para gerenciar informações diversas de exames pelo convênio na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja na home do sistema Gliese
  | username | password |
  | TESTER04 | 1234     |
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento e CIP com dados validos
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro 
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  E clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  E são exibidos os recipientes dos exames
  | Recipiente   | Compl_material  | Recipiente        | Unidade_Realização | Setor | Exames |
  | 1790028026   |                 | EDTA              | SED                | HEMA  | HSA    |
  E clico na aba Diversos

@limpar_cadastro_diversos @done_diversos
Cenário: Limpar cadastro diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado      | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	           | Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 503 - Correio Paciente | 1                 | Sim             | Comentário observações técnicas | Comentário observações atendimento | Sim            |                    |         |        |                   |                  |                  | Comentário indicação/clínica. | Sim           | Sim                      |                   | 247 - BELEM | Sim        | Jantar          | 25102017             | 200000               |
  Quando clico no botão Limpa da aba Diversos
  Então todos os campos voltam para as suas definições padrão na aba Diversos

@remover_meio_resultado_adicional_cadastro_diversos  @done_diversos
Cenário: Remover meio do resultado adicional do cadastro diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado      | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	           | Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 503 - Correio Paciente | 1                 | Sim             | Comentário observações técnicas | Comentário observações atendimento | Sim            |                    |         |        |                   |                  |                  | Comentário indicação/clínica. | Sim           | Sim                      |                   | 247 - BELEM | Sim        | Jantar          | 25102017             | 200000               |
  Quando clico no botão Remover do Meio do resultado adicional da aba Diversos
  Então o meio do resultado adicional é removido do campo Meio do resultado da aba Diversos

@remover_meio_resultado_posto_origem_cadastro_diversos  @done_diversos
Cenário: Remover meio do resultado posto origem do cadastro diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado      | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	           | Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 503 - Correio Paciente | 1                 | Sim             | Comentário observações técnicas | Comentário observações atendimento | Sim            |                    |         |        |                   |                  |                  | Comentário indicação/clínica. | Sim           | Sim                      |                   | 247 - BELEM | Sim        | Jantar          | 25102017             | 200000               |
  Quando clico no botão Remover do Meio do resultado "1 - Laudo - Posto Origem" da aba Diversos
  Então o meio do resultado adicional não é removido do campo Meio do resultado da aba Diversos