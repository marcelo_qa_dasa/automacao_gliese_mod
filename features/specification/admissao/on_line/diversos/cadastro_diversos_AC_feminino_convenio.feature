#language: pt
#encoding: utf-8
@diversos_ac_feminino_convenio @done_diversos
Funcionalidade: Manutenção de informações diversas de exames de análises clínicas femininas na admissão on-line pelo convênio
 Como atendente do sistema Gliese
 Posso manter o cadastro de informações diversas de exames de análises clínicas na admissão on-line
 Para gerenciar informações diversas de exames de análises clínicas na visita de paciente

Esquema do Cenario: Cadastro de informações diversas de exames de análises clínicas femininas pelo convênio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
 E clico na aba Convênio
  E informo o convênio "Amil"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio Amil <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Amil_Quantidade_Etiquetas>
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab |  Codificação | Pg | Impr |
  | MI             | COLPO           | COLPOCITOLOGICO | 1                  |                 | 18-Esfregaco Vaginal | false   | 40601137     | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  E são exibidos os recipientes dos exames
  | Recipiente   | Compl_material       | Recipiente        | Unidade_Realização | Setor | Exames  |
  | 823274949413 | 18-Esfregaco Vaginal | FRASCOLPO         | DAS                | CITP  | COLPOMI |
  Quando clico na aba Diversos
  Então são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado                         | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas            | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia   | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. | 51 - Express-paciente Entrega A Domicilio | A2                | Não             | Comentário observações/técnicas |                          | Não            | 15102017           |         |        |                   |                  |                  | Comentário indicação/clínica. | Não           | Não                      |                               |               | Não        | Jantar          | 25102017             | 200000               |

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome            | medico_crm | medico_uf | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "26051990"      | "6006024227" | "Natalia A.c. Freitas" | "10144043" | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "26051990"      | "6006024227" | "Natalia A.c. Freitas" | "10144043" | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
