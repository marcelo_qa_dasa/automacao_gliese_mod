#language: pt
#encoding: utf-8
Funcionalidade: Cadastro de Médico 
 Como atendente do sistema Gliese
 Posso manter o cadastro de médicos 
 Para gerenciar médicos associados a visitas de pacientes

Contexto:
 Dado que eu esteja na home do sistema Gliese
 | username | password |
 | TESTER01 | 123456   |
 E clico no menu Admissão > On Line
 E preencho os campos Data de nascimento e CIP com dados validos
 E clico na aba Visita
 E crio uma nova Visita
 E clico na aba Médico
 Então é exibida a UF da unidade selecionada no campo UF

@limpar_cadastro_medico @done_medico
Cenário: Limpar cadastro de médico
 E informo os campos do registro da aba Médico
 E pressiono enter na aba Médico
 E são exibidas as informações de Nome e Telefones do médico.
 Quando clico no botão Limpa da aba Médico
 Então as informações do Médico são removidas

@pesquisar_cadastro_medico @done_medico
Cenário: Pesquisar cadastro de médico
 E informo os campos do registro da aba Médico
 Quando pressiono enter na aba Médico
 Então são exibidas as informações de Nome e Telefones do médico.

@pesquisar_cadastro_medico_inexistente @done_medico
Cenário: Pesquisar cadastro de médico inexistente
 E informo os campos do registro da aba Médico com "999" no campo Número de Registro
 Quando pressiono enter na aba Médico
 Então é exibida a mensagem de médico inexistente "Crm informado inexistente em nossos registros"

@selecionar_cadastro_medico_crm @done_medico
Cenário: Selecionar cadastro de médico pelo CRM
 E clico no ícone binóculo do Número de Registro
 Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
 E clico no botão OK
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação.
 Quando clico no CRM da tela de resultado de pesquisa de médico
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico.

@selecionar_cadastro_medico_nome @done_medico
Cenário: Selecionar cadastro de médico pelo nome
 E digito "Maria Amelia Carvalho da Silva Santos" no campo Nome da aba Médico
 E clico no ícone binóculo do Nome da aba Médico
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação na consulta por nome.
 Quando clico no CRM da tela de resultado de pesquisa de médico na consulta por nome
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico quando selecionado pesquisa por nome.

@carregar_dados_cadastro_medico @done_medico
Cenário: Carregar dados do cadastro do médico
 E clico na opção Cadastro da aba Médico
 Então é exibido o Cadastro de Profissional Solicitante
 E digito o código do médico no cadastro de profissional solicitante
 Quando mudo o foco para outro campo
 Então são carregados os dados do médico.

@editar_cadastro_medico @done_medico
Cenário: Editar cadastro do médico
 E clico na opção Cadastro da aba Médico
 Então é exibido o Cadastro de Profissional Solicitante
 E digito o código do médico no cadastro de profissional solicitante
 E mudo o foco para outro campo no cadastro de profissional solicitante
 E altero os campos de cadastro do médico no cadastro de profissional solicitante
 Quando clico na opção Cadastrar do Cadastro de Profissional Solicitante
 Então é exibida a mensagem de alteração de médico "Alteração Efetuada com Sucesso"
 Quando clico no botão OK
 Então são exibidas as informações de UF, Conselho, Número de Registro, Nome e Telefones do médico associado a visita.

@editar_cadastro_medico_associado_visita @done_medico
Cenário: Editar cadastro do médico associado a visita
 E clico no ícone binóculo do Número de Registro
 Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
 E clico no botão OK
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação.
 Quando clico no CRM da tela de resultado de pesquisa de médico
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico.
 E clico na opção Cadastro da aba Médico
 Então é exibido o Cadastro de Profissional Solicitante
 E altero os campos de cadastro do médico no cadastro de profissional solicitante
 E clico na opção Cadastrar do Cadastro de Profissional Solicitante
 Então é exibida a mensagem de alteração de médico "Alteração Efetuada com Sucesso"
 Quando clico no botão OK
 Então são exibidas as informações de UF, Conselho, Número de Registro, Nome e Telefones do médico associado a visita.

@editar_cadastro_definitivo_medico @to_do
Cenário: Editar cadastro definitivo do médico
 E informo o CRM do médico com cadastro definitivo no campo Número de Registro da aba médico
 E clico no ícone binóculo do Número de Registro
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação.
 Quando clico no CRM de médico definitivo da tela de resultado de pesquisa de médico
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico definitivo.
 E clico na opção Cadastro da aba Médico
 Então os valores dos campos da aba Médico (Número de Registro, Nome e Telefones) são removidos e é exibida a mensagem de profissional com cadastro definitivo "Profissional com Cadastro Definitivo, alteração somente pelo Suporte a Postos. Clique em Limpar para cadastrar outro Médico, ou feche esta janela."
 E clico no botão OK
 E os valores dos campos do Cadastro de Profissional Solicitante são removidos
 Quando clico no botão Limpa do Cadastro de Profissional Solicitante são removidos
 Então o botão Cadastrar é exibido.
 
@cadastrar_novo_medico_omitindo_codigo @done_medico
Cenário: Editar médico removendo o código
 E clico no ícone binóculo do Número de Registro
 Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
 E clico no botão OK
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação.
 Quando clico no CRM da tela de resultado de pesquisa de médico
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico.
 E clico na opção Cadastro da aba Médico
 Então é exibido o Cadastro de Profissional Solicitante
 E removo o código
 Quando clico no botão Cadastrar
 Então é exibida a mensagem de código do profissional inválido "Código do Profissional inválido!"

@cadastrar_novo_medico_omitindo_nome @done_medico
Cenário: Editar médico removendo o nome
 E clico no ícone binóculo do Número de Registro
 Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
 E clico no botão OK
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação.
 Quando clico no CRM da tela de resultado de pesquisa de médico
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico.
 E clico na opção Cadastro da aba Médico
 Então é exibido o Cadastro de Profissional Solicitante
 E removo o nome
 Quando clico no botão Cadastrar
 Então é exibida a mensagem de nome do médico inválido "Nome do Médico mínimo de 1 e máximo de 50 caracteres"
 
@cadastrar_novo_medico_omitindo_ddd @done_medico
Cenário: Editar médico removendo o DDD
 E clico no ícone binóculo do Número de Registro
 Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
 E clico no botão OK
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação.
 Quando clico no CRM da tela de resultado de pesquisa de médico
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico.
 E clico na opção Cadastro da aba Médico
 Então é exibido o Cadastro de Profissional Solicitante
 E removo o DDD
 Quando clico no botão Cadastrar
 Então é exibida a mensagem de DDD inválido "DDD inválido"

@cadastrar_novo_medico_omitindo_cliente_preferencial @done_medico
Cenário: Editar médico removendo o cliente preferencial 
 E clico no ícone binóculo do Número de Registro
 Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
 E clico no botão OK
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação.
 Quando clico no CRM da tela de resultado de pesquisa de médico
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico.
 E clico na opção Cadastro da aba Médico
 Então é exibido o Cadastro de Profissional Solicitante
 E removo o cliente preferencial
 Quando clico no botão Cadastrar
 Então é exibida a mensagem de seleção de cliente preferencial "Selecione o campo Cliente Preferencial"
 
@cadastrar_novo_medico_telefone_invalido @done_medico
Cenário: Editar médico informando telefone invalido 
 E clico no ícone binóculo do Número de Registro
 Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
 E clico no botão OK
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação.
 Quando clico no CRM da tela de resultado de pesquisa de médico
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico.
 E clico na opção Cadastro da aba Médico
 Então é exibido o Cadastro de Profissional Solicitante
 E digito "00" no campo telefone
 Quando clico no botão + do telefone
 Então é exibida a mensagem de número inválido "Número Inválido"
 
@cadastrar_novo_medico_fax_invalido @done_medico
Cenário: Editar médico informando fax invalido
 E clico no ícone binóculo do Número de Registro
 Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 50 itens, pode haver mais registros do que os exibidos"
 E clico no botão OK
 Então médicos já cadastrados são exibidos com as informações de CRM, Médico e Situação.
 Quando clico no CRM da tela de resultado de pesquisa de médico
 Então são exibidas as informações de Número de Registro, Nome e Telefones do médico.
 E clico na opção Cadastro da aba Médico
 Então é exibido o Cadastro de Profissional Solicitante
 E digito "00" no campo fax
 Quando clico no botão + do fax
 Então é exibida a mensagem de número inválido "Número Inválido"
 