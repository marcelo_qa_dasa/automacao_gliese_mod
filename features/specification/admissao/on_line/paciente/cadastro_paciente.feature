#language: pt
#encoding: utf-8
Funcionalidade: Cadastro de novos Clientes 
 Como atendente do sistema
 Posso efetuar o cadastro de novos clientes 
 Para que os mesmos possam ter acesso as informações após as suas consultas

Contexto:
 Dado que eu esteja na home do sistema Gliese
 | username | password |
 | TESTER04 | 1234     |
 E clico no menu Admissão > On Line
 E clico na opção Cadastrar novo paciente

@cadastro_paciente @done
Cenário: Cadastro de novo paciente com sucesso
 E Preencho os campos de cadastro de cliente com sucesso
 Quando clico no botão cadastrar
 Então e apresentado a mensagem de cadastro de paciente "Paciente cadastrado com sucesso"

@editar_cadastro_paciente @done
Cenário: Editar cadastro de paciente com sucesso
 E preencho o CIP para editar os dados respectivos ao paciente
 E edito o cadastro de paciente
 Quando clico no botão cadastrar
 Então e apresentado a mensagem de cadastro de paciente "Paciente cadastrado com sucesso"

@validar_cip @done
Cenário: Validar preenchimento de CIP
 Quando preencho o campo nome do paciente
 Então é apresentado a mensagem "Campo CIP obrigatório"

@validar_nome @done
Cenário: validar preenchimento do campo nome
 Quando preencho os campos validos sem preencher o campo nome
 E clico no botão cadastrar
 Então é apresentado a mensagem "Nome Paciente mínimo de 1 e máximo de 50 caracteres"

@validar_data @done
Cenário: validar preenchimento do campo Data de Nascimento
 Quando preencho os campos validos sem preencher o campo Data de Nascimento
 E clico no botão cadastrar
 Então é apresentado a mensagem "Data de Nascimento Inválida"

@validar_sexo @done
Cenário: validar preenchimento do campo Sexo
 Quando preencho os campos validos sem preencher o campo Sexo
 E clico no botão cadastrar
 Então é apresentado a mensagem "Sexo Inválido"

@validar_motivo_email @done
Cenário: validar preenchimento do campo Motivo de Email Não Informado
 Quando preencho os campos validos sem preencher o Motivo de Email Não Informado
 E clico no botão cadastrar
 Então é apresentado a mensagem "Preencha o motivo por não informar o e-mail."

@validar_telefone @done
Cenário: validar preenchimento do campo Telefone
 Quando preencho os campos validos sem preencher o telefone
 E clico no botão cadastrar
 Então é apresentado a mensagem "Preencha ao menos 1 telefone fixo."

@validar_ddd @to_do
Cenário: validar preenchimento do campo DDD
 Quando preencho os campos validos sem preencher o DDD
 E clico no botão cadastrar
 Então é apresentado a mensagem "Preencha um DDD."

@validar_rg @to_do
Cenário: validar preenchimento do campo RG
 Quando preencho os campos validos sem preencher o RG
 E clico no botão cadastrar
 Então é apresentado a mensagem "Preencha um RG valido."

@validar_cpf @to_do
Cenário: validar preenchimento do campo CPF
 Quando preencho os campos validos sem preencher o CPF
 E clico no botão cadastrar
 Então é apresentado a mensagem "Preencha um CPF valido."