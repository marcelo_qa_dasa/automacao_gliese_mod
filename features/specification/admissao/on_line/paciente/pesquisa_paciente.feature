#language: pt
#encoding: utf-8
Funcionalidade: Pesquisa de pacientes no sistema gliese
 Sendo usuário do sistema gliese
 Posso efetuar pesquisas por nome, data, RG, CPF entre outros 
 Para que eu possa ter acesso as infromações dos pacintes para consultas

Contexto:
 Dado que eu esteja na home do sistema Gliese
 | username | password |
 | TESTER04 | 1234     |
 E clico no menu Admissão > On Line

@limpar_campos_pesquisa_paciente @done
Cenário: Limpar campos de pesquisa de clientes
 E preencho os campos Data de nascimento e CIP com dados validos
 Quando clico na opção Limpa na tela de paciente
 Então os valores dos campos Data de nascimento e CIP são removidos. 

@pesquisa_data_cip @done
Cenário: pesquisa por Data de nascimento e CIP
 E preencho os campos Data de nascimento e CIP com dados validos
 Entao é apresentado na tela de paciente as informações respectivas a consulta por Data de nascimento e CIP

@consulta_por_nome @done
Cenário: Consulta paciente por nome
 E clico na opção Pesq. Nome...
 E Preencho os campos Data de nascimento e nome
 Entao é apresentado as infoações respectivas ao paciente CIP, Nome, Data Nascimento, CPF, Telefone, Endereço e status VIP

@consulta_por_rg @done
Cenário: Consulta paciente por RG
 E clico na opção Pesq. RG...
 E Preencho o campo com RG valido de uma paciente
 Entao é apresentado as infoações respectivas ao paciente, CIP, nome e Data de nascimento

@consulta_por_cpf @done
Cenário: Consulta paciente por CPF
 E clico na opção Pesq. CPF...
 E Preencho o campo com CPF valido de uma paciente
 Entao é apresentado as infoações respectivas a pesquisa por CPF: CIP, nome e Data de nascimento
