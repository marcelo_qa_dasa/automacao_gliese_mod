#language: pt
#encoding: utf-8
@financeiro_desconto_geral
Funcionalidade: Manutenção de desconto nas informações financeiras de exames na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de desconto nas informações financeiras de exames na admissão on-line
 Para gerenciar desconto nas informações financeiras de exames na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja na home do sistema Gliese
  | username | password | empresa     | unidade                         |
  | TESTER04 | 1234     | Bronstein   | 068 - Bronstein - Barra Plaza   |
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento e CIP com dados validos
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Br-bpbarraplaza"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  Então clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 00000000    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. |                   |                   |                 |                      |                         |                |                    | 80      | 180    |                   |                  |                  | Comentário indicação/clínica. |               | Não                      | Comentário exames anteriores. |             |            | Jantar          | 25102017             | 200000               |
  E clico na aba Financeiro
  Então clico na opção Desconto

@incluir_desconto_omitindo_usuario
Cenário: Incluir desconto omitindo o usuário
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  |         |            |                 |            |                |        |
  Então é exibida a mensagem de preenchimento do usuário "Preencha o Usuário"

@incluir_desconto_informando_usuario_invalido
Cenário: Incluir desconto informando um usuário inválido
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | xxx     | caxias2016 | Desconto NAC 5% |            |                |        |
  Então é exibida a mensagem de senha incorreta ou usuário sem privilégios "senha informada incorreta ou usuário não possui privilégios de supervisor"

@incluir_desconto_omitindo_senha
Cenário: Incluir desconto omitindo a senha do usuário
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | MASTER  |            |                 |            |                |        | 
  Então é exibida a mensagem de preenchimento de senha "Preencha a Senha"

@incluir_desconto_informando_senha_invalida
Cenário: Incluir desconto informando senha inválida
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | MASTER  | xxx        | Desconto NAC 5% |            |                |        | 
  Então é exibida a mensagem de senha incorreta ou usuário sem privilégios "senha informada incorreta ou usuário não possui privilégios de supervisor"

@incluir_desconto_outros_percentual_omitindo_percentual
Cenário: Incluir desconto outros omitindo o percentual 
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | MASTER  | caxias2016 | Outros          |            |                |        |
  Então é exibida a mensagem de desconto percentual vazio ou igual a zero "Selecione 'Desconto em Percentual' ou 'Desconto em Valor'"

@incluir_desconto_outros_percentual_igual_zero
Cenário: Incluir desconto outros informando percentual igual a zero
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | MASTER  | caxias2016 | Outros          | 0          |                |        |
  Então é exibida a mensagem de desconto percentual vazio ou igual a zero "Selecione 'Desconto em Percentual' ou 'Desconto em Valor'"

@incluir_desconto_outros_valor_reais_igual_zero
Cenário: Incluir desconto outros informando valor em reais igual a zero
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | MASTER  | caxias2016 | Outros          |            | 0.00           |        | 
  Então é exibida a mensagem de desconto em reais vazio ou igual a zero "Desconto em Reais não pode ser vazio ou igual a zero"

@incluir_desconto_outros_omitindo_motivo
Cenário: Incluir desconto outros omitindo o motivo
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | MASTER  | caxias2016 | Outros          | 1          |                |        |
  Então é exibida a mensagem de preenchimento do motivo do desconto "Selecione 'Desconto em Percentual' ou 'Desconto em Valor'"

@incluir_desconto_informando_usuario_sem_privilegios
Cenário: Incluir desconto informando usuário sem privilégios
  E são preenchidas as informações de desconto
  | usuario  | senha      | desconto        | percentual | valor_em_reais | motivo |
  | TESTER04 | 1234       | Desconto NAC 5% |            |                |        | 
  Então é exibida a mensagem de senha incorreta ou usuário sem privilégios "senha informada incorreta ou usuário não possui privilégios de supervisor"

@remover_desconto_aplicado
Cenário: Remover desconto 
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | MASTER  | caxias2016 | Desconto NAC 5% |            |                |        |
  E o Percentual de Desconto e Total com Desconto é atualizado na aba Financeiro
  E clico na opção Desconto
  Então é exibida a mensagem de remoção de desconto "Desconto já aplicado. Deseja remover o desconto?" 

@cancelar_remocao_desconto_aplicado
Cenário: Cancelar remoção de desconto
  E são preenchidas as informações de desconto
  | usuario | senha      | desconto        | percentual | valor_em_reais | motivo |
  | MASTER  | caxias2016 | Desconto NAC 5% |            |                |        | 
  E o Percentual de Desconto e Total com Desconto é atualizado na aba Financeiro
  E clico na opção Desconto
  E é exibida a mensagem de remoção de desconto "Desconto já aplicado. Deseja remover o desconto?" e clico no botão Cancelar 
  Então é exibida a popup de desconto com os campos desabilitados contendo os dados do desconto previamente cadastrado
