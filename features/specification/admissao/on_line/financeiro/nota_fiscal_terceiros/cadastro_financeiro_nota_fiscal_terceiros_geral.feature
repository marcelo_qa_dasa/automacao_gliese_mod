#language: pt
#encoding: utf-8
@financeiro_nota_fiscal_terceiros_geral 
Funcionalidade: Manutenção de nota fiscal de terceiros nas informações financeiras de exames na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de nota fiscal de terceiros nas informações financeiras de exames na admissão on-line
 Para gerenciar notas fiscais de terceiros nas informações financeiras de exames na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja na home do sistema Gliese
  | username | password |
  | TESTER04 | 1234     |
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento e CIP com dados validos
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro 
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  Então clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. |                   |                   |                 |                      |                         |                |                    | 80      | 180    |                   |                  |                  | Comentário indicação/clínica. |               | Não                      | Comentário exames anteriores. |             |            | Jantar          | 25102017             | 200000               |
  E clico na aba Financeiro
  Então clico na opção N.F. Terceiro na aba Financeiro

@cadastrar_nota_fiscal_terceiros_sem_informar_logradouro @done_financeiro_nota_fiscal
Cenário: Cadastrar nota fiscal de terceiros sem informar o logradouro
  E são preenchidas as informações da nota fiscal de terceiros
  | nome           | logradouro | numero | complemento | bairro | municipio | estado | cep | cnpj_cpf      |
  |                |            |        |             |        |           |        |     |               |
  Quando clico no botão Salvar
  Então é exibida a mensagem de campo logradouro obrigatório "O campo logradouro é obrigatório."

@cadastrar_nota_fiscal_terceiros_sem_informar_numero @done_financeiro_nota_fiscal
Cenário: Cadastrar nota fiscal de terceiros sem informar o número
  E são preenchidas as informações da nota fiscal de terceiros
  | nome           | logradouro | numero | complemento | bairro                | municipio | estado | cep | cnpj_cpf      |
  |                | Av. Juruá  |        |             |                       |           |        |     |               |
  Quando clico no botão Salvar
  Então é exibida a mensagem de campo número obrigatório "O campo número é obrigatório."

@cadastrar_nota_fiscal_terceiros_sem_informar_bairro @done_financeiro_nota_fiscal
Cenário: Cadastrar nota fiscal de terceiros sem informar o bairro
  E são preenchidas as informações da nota fiscal de terceiros
  | nome           | logradouro | numero | complemento | bairro                | municipio | estado | cep | cnpj_cpf      |
  |                | Av. Juruá  | 548    |             |                       |           |        |     |               |
  Quando clico no botão Salvar
  Então é exibida a mensagem de campo bairro obrigatório "O campo bairro é obrigatório."

@cadastrar_nota_fiscal_terceiros_sem_informar_cnpj @done_financeiro_nota_fiscal
Cenário: Cadastrar nota fiscal de terceiros sem informar o CNPJ
  E são preenchidas as informações da nota fiscal de terceiros
  | nome           | logradouro | numero | complemento | bairro                | municipio | estado | cep | cnpj_cpf      |
  |                | Av. Juruá  | 548    |             | Alphaville Industrial |           |        |     |               |
  Quando clico no botão Salvar
  Então é exibida a mensagem de campo CNPJ obrigatório "O campo CNPJ é obrigatório."

@excluir_nota_fiscal_terceiros @done_financeiro_nota_fiscal
Cenário: Excluir nota fiscal de terceiros
  E são preenchidas as informações da nota fiscal de terceiros
  | nome           | logradouro | numero | complemento | bairro                | municipio | estado | cep | cnpj_cpf      |
  |                | Av. Juruá  | 548    |             | Alphaville Industrial |           |        |     | 1111111111111 |
  E clico no botão Salvar
  E é exibida a mensagem de dados salvos com sucesso "Dados salvos com sucesso."
  E clico na opção N.F. Terceiro na aba Financeiro
  E são carregadas as informações de Logradouro, Número, Bairro e CNPJ/CPF
  Quando clico no botão Excluir
  Então é exibida a mensagem de dados excluídos com sucesso "Dados excluídos com sucesso."

@limpar_nota_fiscal_terceiros @done_financeiro_nota_fiscal
Cenário: Limpar nota fiscal de terceiros
  E são preenchidas as informações da nota fiscal de terceiros
  | nome           | logradouro | numero | complemento | bairro                | municipio | estado | cep | cnpj_cpf      |
  |                | Av. Juruá  | 548    |             | Alphaville Industrial |           |        |     | 1111111111111 |
  Quando clico na botão Limpar
  Então as informações são removidas dos campos Logradouro, Número, Bairro e CNPJ/CPF

@selecionar_municipio @done_financeiro_nota_fiscal
Cenário: Selecionar município
 E clico no ícone binóculo do Município
 Então é exibida a mensagem de resultado de pesquisa "Resultado da pesquisa limitado a 100 municípios, por favor, coloque letras."
 E clico no botão OK
 Então municípios são exibidos com as informações de Município, Estado e Código Município.
 Quando clico no Município da tela de resultado de pesquisa de Município "Ibicui"
 Então são exibidas o município é carregado no campo Município

@selecionar_municipio_informando_inicio_nome @done_financeiro_nota_fiscal
Cenário: Selecionar município informando início do nome
 E informo "Gua" no campo Município 
 E clico no ícone binóculo do Município
 Então municípios que iniciam por "Gua" são exibidos com as informações de Município, Estado e Código Município.
 Quando clico no Município da tela de resultado de pesquisa de Município "Guarara"
 Então são exibidas o município é carregado no campo Município