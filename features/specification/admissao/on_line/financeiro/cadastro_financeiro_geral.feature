#language: pt
#encoding: utf-8
@financeiro_geral
Funcionalidade: Manutenção de informações financeiras de exames na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de informações financeiras de exames na admissão on-line
 Para gerenciar informações financeiras de exames na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja na home do sistema Gliese
  | username | password|
  | TESTER04 | 1234   |
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento e CIP com dados validos
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  Então clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Diversos
  E são preenchidas as informações diversas dos exames
  | Medicamento_Cirurg               | Meio_do_resultado | Número_da_chamada | RG_CPF_no_Laudo | Observações_Técnicas | Observações_Atendimento	| Imprime_cartao | Última_menstruação | Peso_kg | Altura | Senha_do_Convenio | Data_Autorização | Dias_abst_sexual | Indicação_clinica             | Mais_de_1_CRM | Trouxe_Exames_Anteriores | Exames_Anteriores             | Procedencia | Prioridade | Última_Refeição | Data_Última_Refeição | hora_ultima_refeicao |
  | Comentário medicamento/cirurgia. |                   |                   |                 |                      |                         |                |                    | 80      | 180    |                   |                  |                  | Comentário indicação/clínica. |               | Não                      | Comentário exames anteriores. |             |            | Jantar          | 25102017             | 200000               |
  Então clico na aba Financeiro

@limpar_cadastro_financeiro @done_financeiro_geral
Cenário: Limpar cadastro financeiro
  Quando seleciono alguma forma de pagamento
  E clico no botão Limpa da aba Financeiro
  Então o valor selecionado no campo Forma de Pagamento muda para Selecione... na aba Financeiro

@editar_forma_pagamento_cadastro_financeiro @done_financeiro
Cenário: Editar forma de pagamento do cadastro financeiro
  E preencho a forma de pagamento, clico no botão Salvar
  | forma_de_pgto      | banco | agencia | conta | cheque | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut | valor_opcional |
  | Caucao em Dinheiro |       |         |       |        | 06112017 | 07112017 |               |           |            |          |        |                |
  E valido a forma de pagamento na Relação de formas de pagamento, o Total, Total Recebido, Falta e Troco
  Então edito a forma de pagamento salva e valido a alteração
  | forma_de_pgto      | banco | agencia | conta | cheque | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut | valor_opcional |
  | Caucao em Dinheiro |       |         |       |        | 06112018 | 07112019 |               |           |            |          |        |                |

@cancelar_editar_forma_pagamento_cadastro_financeiro @done_financeiro
Cenário: Cancelar a editação da forma de pagamento do cadastro financeiro
  E preencho a forma de pagamento, clico no botão Salvar
  | forma_de_pgto      | banco | agencia | conta | cheque | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut | valor_opcional |
  | Caucao em Dinheiro |       |         |       |        | 06112017 | 07112017 |               |           |            |          |        |                |
  E valido a forma de pagamento na Relação de formas de pagamento, o Total, Total Recebido, Falta e Troco
  E clico no botão Alterar da opção de pagamento em seguida clico em Limpar e valido se a forma de pagamento não foi altera
  Então o valor salvo não foi alterado e o campo Forma de Pagamento muda para Selecione
  | forma_de_pgto      | banco | agencia | conta | cheque | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut | valor_opcional |
  | Caucao em Dinheiro |       |         |       |        | 06112017 | 07112017 |               |           |            |          |        |                |

@remover_forma_pagamento_cadastro_financeiro @done_financeiro_geral
Cenário: Remover forma de pagamento do cadastro financeiro
  E preencho a forma de pagamento, clico no botão Salvar
  | forma_de_pgto      | banco | agencia | conta | cheque | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut | valor_opcional |
  | Caucao em Dinheiro |       |         |       |        |          |          |               |           |            |          |        |                |
  E valido a forma de pagamento na Relação de formas de pagamento, o Total, Total Recebido, Falta e Troco
  Quando clico no botão Excluir da opção de pagamento da Relação de formas de pagamento da aba Financeiro
  Então a opção de pagamento é removida da Relação de formas de pagamento da aba Financeiro e o Total Recebido, Falta e Troco são atualizados

@incluir_valor_cheque_acima_valor_exame @done_financeiro_geral
Cenário: Incluir valor de cheque acima do valor do exame.
  Quando preencho a forma de pagamento, clico no botão Salvar
  | forma_de_pgto      | banco | agencia | conta | cheque   | dt_dep   | dt_lim   | cartao        | no_cartao | val_cartao | num_parc | cv_aut | valor_opcional |
  | Cheque             | Itau  | 1111    | 1111  | 1111     |          |          |               |           |            |          |        |       80       |
  Então Valido Mensagem de erro "Valor total recebido excede o valor total da visita"
