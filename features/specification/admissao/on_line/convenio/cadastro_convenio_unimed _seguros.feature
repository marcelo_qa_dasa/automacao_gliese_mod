#language: pt
#encoding: utf-8
@convenio_unimed_seguros
Funcionalidade: Manutenção de convênio Unimed na visita de Paciente 
 Como atendente do sistema Gliese
 Posso manter o atendimento pelo convênio Unimed
 Para gerenciar atendimentos pelo convênio Unimed do paciente admitido 

Esquema do Cenario: selecionar operadora Unimed, convênio Unimed Seguros
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio  
  E informo o convênio "Unimed Seguros"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Unimed Seguros
  E preencho as informações do convênio para o convênio Unimed Seguros <Unimed_Codigo_Associado>, <Unimed_Data_Solicitacao>, <Unimed_Plano_Empresa> e <Unimed_Via_Cartao>
  Então é exibida a mensagem de convênio recuperado com sucesso "Convênio recuperado com sucesso".
  
  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | Unimed_Codigo_Associado | Unimed_Data_Solicitacao | Unimed_Plano_Empresa | Unimed_Via_Cartao |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "11041988"      | "41133895"   | "12121212121212121"     | "05092017"              |  "Sim"               | "001"             |
  | "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "11041988"      | "41133895"   | "12121212121212121"     | "05092017"              |  "Sim"               | "001"             |