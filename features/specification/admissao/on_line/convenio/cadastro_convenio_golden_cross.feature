#language: pt
#encoding: utf-8
@convenio_golden_cross
Funcionalidade: Manutenção de convênio Golden Cross na visita de Paciente 
 Como atendente do sistema Gliese
 Posso manter o atendimento pelo convênio Golden Cross
 Para gerenciar atendimentos pelo convênio Golden Cross do paciente admitido 

Esquema do Cenario: selecionar operadora Golden Cross, convênio Golden Cross 
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio  
  E informo o convênio "Golden Cross"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Golden Cross
  E preencho as informações do convênio para o convênio Golden Cross <Golden_Cross_Passe_Cartão>, <Golden_Cross_Codigo_Associado>, <Golden_Cross_Data_Solicitação>, <Golden_Cross_Código_Dependente>, <Golden_Cross_Especialidade_Médica>, <Golden_Cross_Nome_Plano>, <Golden_Cross_Plano_Empresa>, <Golden_Cross_Quantidade_Etiquetas>
  Então é exibida a mensagem de convênio recuperado com sucesso "Convênio recuperado com sucesso".

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | Golden_Cross_Passe_Cartão | Golden_Cross_Codigo_Associado | Golden_Cross_Data_Solicitação | Golden_Cross_Código_Dependente | Golden_Cross_Especialidade_Médica | Golden_Cross_Nome_Plano | Golden_Cross_Plano_Empresa | Golden_Cross_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "23061970"      | "9006996"    | "12121214"                | "12121214"                    | "05092017"                    | "11"                           | "0230 - Cirurgia Toracica"        | "golden basico"         | "Sim"                      | "1"                               |
  | "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "23061970"      | "9006996"    | "12121214"                | "12121214"                    | "05092017"                    | "11"                           | "0230 - Cirurgia Toracica"        | "golden basico"         | "Sim"                      | "1"                               |