#language: pt
#encoding: utf-8
@convenio_sul_america
Funcionalidade: Manutenção de convênio Sul America na visita de Paciente 
 Como atendente do sistema Gliese
 Posso manter o atendimento pelo convênio Sul America
 Para gerenciar atendimentos pelo convênio Sul America do paciente admitido 

Esquema do Cenario: selecionar operadora Sul America, convênio Sul America
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio  
  E informo o convênio "Sul America"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Sul America
  E preencho as informações do convênio para o convênio Sul America <Sul_America_Codigo_Associado>, <Sul_America_Data_Solicitação>, <Sul_America_Nome_Plano>, <Sul_America_Plano_Empresa>, <Sul_America_Quantidade_Etiquetas>, <Sul_America_Validade_Cartão>, <Sul_America_Produto>
  Então é exibida a mensagem de convênio recuperado com sucesso "Convênio recuperado com sucesso".

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | Sul_America_Codigo_Associado | Sul_America_Data_Solicitação | Sul_America_Nome_Plano | Sul_America_Plano_Empresa | Sul_America_Quantidade_Etiquetas | Sul_America_Validade_Cartão | Sul_America_Produto |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "21021968"      | "2213192684" | "11111111111111110"          | "05092017"                   | "especial"             | "Sim"                     | "1"                              | "01012020"                  | "204"               |
  | "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "21021968"      | "2213192684" | "11111111111111110"          | "05092017"                   | "especial"             | "Sim"                     | "1"                              | "01012020"                  | "204"               |