  #language: pt
  #encoding: utf-8
  @convenio_particular
  Funcionalidade: Manutenção de atendimento particular de Paciente 
  Como atendente do sistema Gliese
  Posso manter o atendimento particular
  Para gerenciar atendimentos particulares do paciente admitido

  Esquema do Cenario: selecionar atendimento particular
    Dado que eu esteja logado no sistema Gliese como <username> e <password>
    Quando seleciono a marca <Marca> e a unidade <Unidade>
    E clico no menu Admissão > On Line
    E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
    E clico na aba Visita
    E crio uma nova Visita
    E clico na aba Médico
    E clico no ícone binóculo do Número de Registro
    E clico no botão OK
    E clico no CRM da tela de resultado de pesquisa de médico
    E clico na aba Convênio  
    E informo o convênio "Part Nove"
    E clico no botão Seleciona
    E são exibidas as instruções do convênio Particular
    Então é exibida a mensagem de convênio recuperado com sucesso "Convênio recuperado com sucesso".

    Exemplos:
    | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          |
    | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "11041988"      | "41133895"   |
    | "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "11041988"      | "41133895"   |