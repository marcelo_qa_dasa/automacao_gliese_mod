#language: pt
#encoding: utf-8

Funcionalidade: Manutenção de convênio de Paciente 
 Como atendente do sistema Gliese
 Posso manter o cadastro de convênios
 Para gerenciar convênios do paciente admitido

Contexto:
 Dado que eu esteja na home do sistema Gliese
 | username | password |
 | TESTER04 | 1234     |
 E clico no menu Admissão > On Line
 E preencho os campos Data de nascimento e CIP com dados validos
 E clico na aba Visita
 E crio uma nova Visita
 E clico na aba Médico
 E clico no ícone binóculo do Número de Registro
 E clico no botão OK
 E clico no CRM da tela de resultado de pesquisa de médico
 E clico na aba Convênio

@limpar_cadastro_convenio @done_convenio
Cenário: Limpar cadastro de convênio
 E informo o convênio "Unimed" no campo Convênio da aba Convênio
 E clico no ícone binóculo do Convênio da aba Convênio
 E são exibidas as informações de Mnemônico e Descrição do convênio
 E clico no mnemônico "Unimed Seguros" da tela de resultado de pesquisa de convênio
 E é exibido o mnemônico "Unimed Seguros" no campo Convênio da aba Convênio
 E clico no botão Seleciona da aba Convênio
 E são exibidos os campos do convênio "Unimed Seguros" na aba Convênio
 Quando clico no botão Limpa da aba Convênio
 Então as informações do Convênio são removidas da aba Convênio

@pesquisar_convenio_nome @done_convenio
Cenário: Pesquisar convênio por nome
 E clico na opção Pesq. Por Nome da aba Convênio
 E informo o convênio "Unimed Seguros" no campo Convênio da popup Pesquisa Convênio
 E clico no ícone binóculo da popup Pesquisa Convênio
 E são exibidas as informações de Mnemônico e Descrição do convênio na popup de resultado de pesquisa de convênio
 Quando clico no mnemônico "Unimed Seguros" da tela de resultado de pesquisa de convênio por nome
 Então é exibido o mnemônico "Unimed Seguros" no campo Convênio da aba Convênio

@pesquisar_convenio_por_operadora_plano @done_convenio
Cenário: Pesquisar convênio por operadora e plano
 E clico na opção Pesq. Operadora da aba Convênio
 E informo a operadora "Unimed" no campo Operadora da popup Pesquisa Convênio Por Operadora
 E clico no ícone binóculo do campo Operadora da popup Pesquisa Convênio Por Operadora
 E é exibido o nome da operadora na tela de resultado de pesquisa de operadora
 Quando clico no nome da operadora "Unimed" da tela de resultado de pesquisa de operadora
 Então é exibido o nome da operadora "Unimed" no campo Operadora da popup Pesquisa Convênio Por Operadora
 E informo o plano "Unipart Especial" no campo Plano da popup Pesquisa Convênio Por Operadora
 E clico no ícone binóculo do campo Plano da popup Pesquisa Convênio Por Operadora
 E são exibidas as informações de Convênio e Plano na tela de resultado de pesquisa de planos
 Quando clico no plano "Unipart Especial" da tela de resultado de pesquisa de planos
 Então é exibido o convênio "Unimed Pedido" no campo Convênio da aba Convênio

@listar_operadoras @done_convenio
Cenário: Listar operadoras
 E clico na opção Pesq. Operadora da aba Convênio
 E clico no ícone binóculo do campo Operadora da popup Pesquisa Convênio Por Operadora
 E são exibidas todas as operadoras
 Quando clico no nome da operadora "Unimed" da tela de resultado de pesquisa de operadora
 Então é exibido o nome da operadora "Unimed" no campo Operadora da popup Pesquisa Convênio Por Operadora 

@pesquisar_convenio_por_operadora_plano_sem_informar_operadora @done_convenio
Cenário: Pesquisar convênio por operadora e plano sem informar a operadora
 E clico na opção Pesq. Operadora da aba Convênio
 E informo o plano "Unipart Especial" no campo Plano da popup Pesquisa Convênio Por Operadora
 Então clico no ícone binóculo do campo Plano da popup Pesquisa Convênio Por Operadora e é exibida a mensagem de pesquisar operadora "Pesquise Operadora antes de pesquisar o Plano" 

@pesquisar_convenio_inexistente @done_convenio
Cenário: Pesquisar convênio inexistente
 E informo "xxx" no campo Convênio da aba Convênio
 Quando clico no ícone binóculo do Convênio da aba Convênio
 Então nenhum registro de convênio é exibido

@pesquisar_operadora_inexistente @done_convenio
Cenário: Pesquisar operadora inexistente
 E clico na opção Pesq. Operadora da aba Convênio
 E informo a operadora "dfdfdf" no campo Operadora da popup Pesquisa Convênio Por Operadora
 Quando clico no ícone binóculo do campo Operadora da popup Pesquisa Convênio Por Operadora
 Então nenhuma operadora é exibida na tela de resultado de pesquisa de operadora

@pesquisar_plano_inexistente @done_convenio
Cenário: Pesquisar plano inexistente
 E clico na opção Pesq. Operadora da aba Convênio
 E informo a operadora "Unimed" no campo Operadora da popup Pesquisa Convênio Por Operadora
 E clico no ícone binóculo do campo Operadora da popup Pesquisa Convênio Por Operadora
 E é exibido o nome da operadora na tela de resultado de pesquisa de operadora
 E clico no nome da operadora "Unimed" da tela de resultado de pesquisa de operadora
 E é exibido o nome da operadora "Unimed" no campo Operadora da popup Pesquisa Convênio Por Operadora
 E informo o plano "asfddasfd" no campo Plano da popup Pesquisa Convênio Por Operadora
 Quando clico no ícone binóculo do campo Plano da popup Pesquisa Convênio Por Operadora
 Então nenhum plano é exibido na tela de resultado de pesquisa de planos