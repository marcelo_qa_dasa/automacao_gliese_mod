#language: pt
#encoding: utf-8
Funcionalidade: Manutenção de visita de Paciente 
 Como atendente do sistema Gliese
 Posso manter o cadastro de visitas
 Para gerenciar visitas do paciente admitido

Contexto:
 Dado que eu esteja na home do sistema Gliese
 | username | password |
 | TESTER01 | 123456   |
 E clico no menu Admissão > On Line
 E preencho os campos Data de nascimento e CIP com dados validos
 E clico na aba Visita

@validar_template_visita @done_visita
Cenário: Validar template de visita
 Então o campo Posto exibe a unidade atual, o campo Data exibe a data atual, o campo Tipo de Coleta exibe "Posto" e o campo Empresa exibe a empresa.

@cadastrar_nova_visita @done_visita
Cenário: Cadastrar nova visita
 Quando clico na opção Nova
 Então o campo Número exibe o próximo número sequencial, o campo Posto exibe a unidade atual, o campo Data exibe a data atual, o campo Hora exibe a hora atual, o campo Tipo de Coleta exibe "Posto" e o campo Empresa exibe a empresa.

@documentos_nao_cadastrados @done_visita
Cenário: Documentos não cadastrados
 Quando clico no ícone binóculo
 E clico no Número da visita
 E clico na opção Documento
 Então são exibidas as informações de Paciente CIP, Visita e a mensagem "Não existem documentos cadastrados para essa visita"

@listar_visitas @done_visita
Cenário: Listar visitas
 Quando clico no ícone binóculo
 Então visitas já cadastradas para o cliente são exibidas com as informações de Número, Data, Unidade, Convênio, Exames e Docs.

@visualizar_lista_completa_visitas @done_visita
Cenário: Visualizar lista completa de visitas 
 E clico no ícone binóculo
 Quando clico na opção lista de visitas completa
 Então visitas já cadastradas para o cliente são exibidas com as informações de Número, Data, Unidade, Convênio, Exames e Docs.

@selecionar_visita @done_visita
Cenário: Selecionar visita
 Quando clico no ícone binóculo
 E clico no Número da visita
 Então as informações de Posto, Data, Hora, Tipo de Coleta e Empresa da visita são exibidas.

@limpar_campos_visita @done_visita
Cenário: Limpar campos da visita
 E clico na opção Nova
 Quando clico na opção Limpa na tela de visita
 Então os valores dos campos Número e Hora são removidos.

@editar_visita @done_visita
Cenário: Editar visita
 Quando clico no ícone binóculo
 E clico no Número da visita
 E altero o Posto, Tipo de Coleta e Empresa
 Então as informações de Posto, Tipo de Coleta e Empresa da visita são alterados.

@pesquisar_visita @done_visita
Cenário: Pesquisar visita
 Quando digito "1" no campo Número 
 E pressiono enter
 Então as informações de Posto, Data, Hora, Tipo de Coleta e Empresa da visita são exibidas respectivas a pesquisa.

@listar_documentos @to_do
Cenário: Listar documentos
 Quando clico no ícone binóculo
 E clico no Número da visita
 E clico na opção Documento
 Então são exibidas as informações de Paciente CIP, Visita e Documentos.

@pesquisar_visita_inexistente @done_visita
Cenário: Pesquisar visita inexistente
 Quando digito "999" no campo Número 
 E pressiono enter
 Então é exibida a mensagem "Visita não existe"



