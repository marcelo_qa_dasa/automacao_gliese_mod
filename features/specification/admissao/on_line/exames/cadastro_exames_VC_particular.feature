#language: pt
#encoding: utf-8
@exames_vc_particular_vc @to_do
Funcionalidade: Manutenção de vacinas na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de vacinas na admissão on-line
 Para gerenciar vacinas na visita de paciente

Esquema do Cenario: vacinas
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Lm-ptarp"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  Quando é incrementado o número de Outros e Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão Valida da aba Exames
  #E são exibidas as informações de atendimento do exame na aba Exames
  #E clico no ícone fechar das informações de atendimento do exame na aba Exames
  #E são exibidas as informações do exame na aba Exames
  #E informo a identificação do médico "dpi00"
  #E informo a data da agenda e a hora da agenda
  #Quando clico no botão Salvar da aba Exames
  #Então o exame é adicionado na Relação de exames admitidos
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | VC             | VACMB           |                                |                    |                 |                      |         |             |    |      |
  Então todos os exames foram adicionados na Relação de exames admitidos da aba Exames

  Exemplos:
  | username   | password | Marca                                               | Unidade                   | Data_Nascimento | CIP          | medico_nome             | medico_crm | medico_uf | Amil_Passe_Cartão | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano | Amil_Plano_Empresa |
  | "TESTER04" | "1234"   | "Diagnosticos da America - Lamina Med. Diag. Ltda"  | "LPO - Lamina - Arpoador" | "01011912"      | "6006817892" | "Marcio Alves Tannure"  | "52773794" | "CE"      | "121212122"       | "121212122"           | "27092017"            | "Amil 150"      | "Sim"              |
  #| "TESTER01" | "123456" | "Diagnosticos da America - Lamina Med. Diag. Ltda"  | "LPO - Lamina - Arpoador" | "01011912"      | "6006817892" | "Marcio Alves Tannure"  | "52773794" | "CE"      | "121212122"       | "121212122"           | "27092017"            | "Amil 150"      | "Sim"              |
