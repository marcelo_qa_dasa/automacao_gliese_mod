#language: pt
#encoding: utf-8
@exames_ac_nao_realizado_unidade @done_exames
Funcionalidade: Manutenção de exames não realizados na unidade na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de exames não realizados na unidade na admissão on-line
 Para gerenciar exames não realizados na unidade na visita de paciente

Esquema do Cenario: Cadastrar exame não realizado na unidade
 Dado que eu esteja logado no sistema Gliese como <username> e <password>
 Quando seleciono a marca <Marca> e a unidade <Unidade>
 E clico no menu Admissão > On Line
 E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
 E clico na aba Visita
 E crio uma nova Visita
 E clico na aba Médico
 E clico no ícone binóculo do Número de Registro
 E clico no botão OK
 E clico no CRM da tela de resultado de pesquisa de médico
 E clico na aba Convênio
 E informo o convênio "Amil"
 E clico no botão Seleciona
 E são exibidas as instruções do convênio Amil
 E preencho as informações do convênio para o convênio Amil <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Amil_Quantidade_Etiquetas>
 E clico na aba Exames
 E preencho um material e exame não realizado na unidade
 | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas |  Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
 | EP             | EGA             | ESPERMOGRAMA AUTOMATIZADO  |                    |                  |                      | false   | 40309312    | S  | S    |
 E clico no botão valida da aba exames
 Então é exibida a mensagem de exame não realizado na unidade "Atenção Exame não é realizado nesta unidade. Unidades que realizam o exame: LEA - Leblon - Cid Leblon"
 
  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome              | medico_crm | medico_uf | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01021985"      | "6007186575" | "Antonio Afonso Dourado" | "52773794" | "RS"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01021985"      | "6007186575" | "Antonio Afonso Dourado" | "52773794" | "RS"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |