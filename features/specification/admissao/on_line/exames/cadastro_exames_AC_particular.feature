#language: pt
#encoding: utf-8
@exames_ac_particular @done_exames
Funcionalidade: Manutenção dos exames de análises clínicas no particular na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de exames de análises clínicas no particular na admissão on-line
 Para gerenciar exames de análises clínicas no particular na visita de paciente

Esquema do Cenario: Cadastro de exames AC no particular
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Br-bpbarraplaza"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação  | Pg | Impr |
  | SU             | CLCR            | CLEARANCE DE CREATININA    |                    | 0               |                      | false   | 40301508     | S  | S    |
  | SV             | CORT            | CORTISOL SALIVAR           |                    |                 |                      | false   | 40316190     | S  | S    |
  | UR             | EAS             | EAS - URINA TIPO 1         |                    |                 |                      | true    | 40311210     | S  | S    |
  | EP             | EGA             | ESPERMOGRAMA AUTOMATIZADO  |                    |                 |                      | false   | 40309312     | S  | S    |
  | FE             | EP              | PARASITOLOGICO             |                    |                 |                      | false   | 40303110     | S  | S    |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361     | S  | S    |
  | HI             | PAT             | HISTOPATOLOGICO            |                    |                 | 6-Biopsia de Pele    | false   | 40601110     | S  | S    |
  | LI             | PT              | PROTEINAS                  |                    |                 |                      | true    | 40302377     | S  | S    |
  Então todos os exames foram adicionados na Relação de exames admitidos da aba Exames

  Exemplos:
  | username   | password | Marca       | Unidade                         | Data_Nascimento | CIP          | medico_nome           | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011912"      | "1224189291" | "Marcio Alves Tannure"| "52773794" | "MG"      |
  #| "TESTER01" | "123456" | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011912"      | "1224189291" | "Marcio Alves Tannure"| "52773794" | "MG"      |
