#language: pt
#encoding: utf-8
@exames_ac_convenio @done_exames
Funcionalidade: Manutenção dos exames de análises clínicas pelo convênio na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de exames de análises clínicas na admissão on-line
 Para gerenciar exames de análises clínicas na visita de paciente

Esquema do Cenario: Cadastro de exames de AC pelo convênio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Amil"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio Amil <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Amil_Quantidade_Etiquetas>
  E clico na aba Exames
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação  | Pg | Impr |
  | SU             | CLCR            | CLEARANCE DE CREATININA    |                    | 0               |                      | false   | 40301508     | S  | S    |
  | SV             | CORT            | CORTISOL SALIVAR           |                    |                 |                      | false   | 40316190     | S  | S    |
  | UR             | EAS             | EAS - URINA TIPO 1         |                    |                 |                      | true    | 40311210     | S  | S    |
  | EP             | EGA             | ESPERMOGRAMA AUTOMATIZADO  |                    |                 |                      | false   | 40309312     | S  | S    |
  | FE             | EP              | PARASITOLOGICO             |                    |                 |                      | false   | 40303110     | S  | S    |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361     | S  | S    |
  | HI             | PAT             | HISTOPATOLOGICO            |                    |                 | 6-Biopsia de Pele    | false   | 40601110     | S  | S    |
  | LI             | PT              | PROTEINAS                  |                    |                 |                      | true    | 40302377     | S  | S    |
  Então todos os exames foram adicionados na Relação de exames admitidos da aba Exames

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome              | medico_crm | medico_uf | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01021985"      | "6007186575" | "Antonio Afonso Dourado" | "52773794" | "RS"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "01021985"      | "6007186575" | "Antonio Afonso Dourado" | "52773794" | "RS"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |