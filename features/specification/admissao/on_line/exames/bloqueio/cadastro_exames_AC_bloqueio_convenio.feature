#language: pt
#encoding: utf-8
@exames_ac_bloqueio_convenio @done_exames
Funcionalidade: Manutenção de bloqueio de exames de AC pelo convênio na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de bloqueio de exames de análises clínicas na admissão on-line
 Para gerenciar bloqueio de exames de análises clínicas na visita de paciente

Esquema do Cenario: Bloqueio de exame de AC pelo convênio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Amil"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio Amil <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Amil_Quantidade_Etiquetas>
  E clico na aba Exames
  Quando é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | UR             | EAS             | EAS - URINA TIPO 1         |                    |                 |                      | true    | 40311210    | S  | S    |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  | LI             | PT              | PROTEINAS                  |                    |                 |                      | true    | 40302377    | S  | S    |
  E todos os exames são adicionados na Relação de exames admitidos da aba Exames
  E clico na opção Bloqueio da aba Exames
  Então é exibido o CIP e o número da visita no do paciente da tela de Bloqueio
  E é exibido o campo Num Sequencial da tela de Bloqueio
  E preencho os campos, tipo de bloqueio, prazo limite e observação com dados validos
  | tipo_do_bloqueio | descricao_tipo_bloqueio |
  | 3                | Paciente Deve Material  |
  E a lista de exames é exibida na tela de Bloqueio informando um número sequencial começando em "1" no campo Ordem
  E marco a opção Bloqueio da tela de bloqueio 
  | Mnemônico_Exame |
  | EASUR           |
  E clico na opção Cadastrar da tela de Bloqueio
  E é exibida a mensagem Confirma Bloqueio "Confirma Bloqueio"
  E clico no botão OK
  E é exibida a mensagem Deseja imprimir o protocolo "Deseja imprimir o protocolo"
  E clico no botão OK
  E é exibido o protocolo do bloqueio
  E é decrementado o número de Urina e o Total no Resumo da aba Exames e apenas os demais exames permanecem na Relação de exames admitidos da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 28040481    | S  | S    |
  | LI             | PT              | PROTEINAS                  |                    |                 |                      | true    | 28011600    | S  | S    |
  E clico na opção Bloqueio da aba Exames
  Então é exibido o CIP e o número da visita no do paciente da tela de Bloqueio
  E é exibido o campo Num Sequencial da tela de Bloqueio
  E preencho os campos, tipo de bloqueio, prazo limite e observação com dados validos
  | tipo_do_bloqueio | descricao_tipo_bloqueio |
  | 3                | Paciente Deve Material  |
  E a lista de exames é exibida na tela de Bloqueio informando um número sequencial começando em "2" no campo Ordem
  E marco a opção Bloqueio da tela de bloqueio 
  | Mnemônico_Exame |
  | PTLI            |
  E clico na opção Cadastrar da tela de Bloqueio
  E é exibida a mensagem Confirma Bloqueio "Confirma Bloqueio"
  E clico no botão OK
  E é exibida a mensagem Deseja imprimir o protocolo "Deseja imprimir o protocolo"
  E clico no botão OK
  E é exibido o protocolo do bloqueio
  Então é decrementado o número de Outros e o Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 28040481    | S  | S    |

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "03071945"      | "1225399738" | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "03071945"      | "1225399738" | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |