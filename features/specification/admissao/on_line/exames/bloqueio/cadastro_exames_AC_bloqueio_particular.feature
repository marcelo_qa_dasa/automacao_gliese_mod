#language: pt
#encoding: utf-8
@exames_ac_bloqueio_particular @done_exames
Funcionalidade: Manutenção de bloqueio de exames de AC no particular na admissão on-line 
 Como atendente do sistema Gliese
 Posso manter o cadastro de bloqueio de exames na admissão on-line
 Para gerenciar bloqueio de exames na visita de paciente particular

Esquema do Cenario: Bloqueio de exame de AC no particular
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número Sangue e o Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão Valida da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | false   | 40304361    | S  | S    |
  E todos os exames são adicionados na Relação de exames admitidos da aba Exames
  Então clico na opção Bloqueio da aba Exames é exibida a mensagem de bloqueio do tipo balcão "Não é possível realizar Bloqueio para Convênios do Tipo Balcão"
                                                                                               
  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "20111990"      | "1225341985" |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "20111990"      | "1225341985" |