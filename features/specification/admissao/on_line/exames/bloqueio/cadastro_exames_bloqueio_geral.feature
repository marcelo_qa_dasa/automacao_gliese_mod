#language: pt
#encoding: utf-8
@exames_bloqueio_geral 
Funcionalidade: Manutenção de bloqueio de exames pelo convênio na admissão on-line 
 Como atendente do sistema Gliese
 Posso manter o cadastro de bloqueio de exames na admissão on-line
 Para gerenciar bloqueio de exames na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja na home do sistema Gliese
  | username | password |
  | TESTER04 | 1234     |
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento e CIP com dados validos
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro 
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano           | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 137 - Amil 140 Nacional   |  Sim               | "3"                       |
  Então clico na aba Exames

@bloqueio_exame_tipo_faturamento @done_exames
Cenário: Bloqueio de exame informando tipo de bloqueio realizado pelo faturamento
  E é incrementado o número Sangue e o Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão Valida da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SU             | CLCR            | CLEARANCE DE CREATININA    |                    | 0               |                      | false   | 40301508    | S  | S    |
  E todos os exames são adicionados na Relação de exames admitidos da aba Exames
  E clico na opção Bloqueio da aba Exames
  E é exibido o CIP no campo CIP da tela de Bloqueio
  E é exibida o número da visita no campo Visita da tela de Bloqueio
  E é exibido "1" no campo Num. Sequencial da tela de Bloqueio
  Quando informo "1" no campo Tipo de Bloqueio da tela de Bloqueio
  Então é exibida a mensagem de bloqueio realizado pelo faturamento "Este bloqueio só é realizado pelo Faturamento!!!" na tela de Bloqueio
                                                                    
@bloqueio_exame_tipo_invalido @done_exames
Cenário: Bloqueio de exame informando tipo de bloqueio inválido
  E é incrementado o número Sangue e o Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia   
  #E clico no botão Valida da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames são adicionados na Relação de exames admitidos da aba Exames
  E clico na opção Bloqueio da aba Exames
  E é exibido o CIP no campo CIP da tela de Bloqueio
  E é exibida o número da visita no campo Visita da tela de Bloqueio
  E é exibido "1" no campo Num. Sequencial da tela de Bloqueio
  Quando informo "000" no campo Tipo de Bloqueio da tela de Bloqueio
  Então é exibida a mensagem de código inválido "Código inválido" na tela de Bloqueio

@limpar_cadastro_bloqueio @done_exames
Cenário: Limpar cadastro de bloqueio
  E é incrementado o número Sangue, Urina, Outros e o Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão Valida da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames são adicionados na Relação de exames admitidos da aba Exames
  E clico na opção Bloqueio da aba Exames
  E é exibido o CIP no campo CIP da tela de Bloqueio
  E é exibida o número da visita no campo Visita da tela de Bloqueio
  E é exibido "1" no campo Num. Sequencial da tela de Bloqueio
  E informo "3" no campo Tipo de Bloqueio da tela de Bloqueio
  E a descrição do tipo de bloqueio "Paciente Deve Material" é exibida na tela de Bloqueio
  E a lista de exames é exibida na tela de Bloqueio informando um número sequencial começando em "1" no campo Ordem
  E informo o prazo limite no campo Prazo Limite da tela de Bloqueio
  E insiro uma observação no campo Observação da tela de Bloqueio
  Quando clico na opção Limpa da tela de Bloqueio
  Então as informações Num. Sequencial, Tipo de Bloqueio, Prazo Limite, Observação e a lista de exames são removidas da tela de Bloqueio

@cadastro_novo_bloqueio @done_exames
Cenário: Cadastro de novo bloqueio
  E é incrementado o número Sangue, Urina, Outros e o Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão Valida da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame            | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO         |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames são adicionados na Relação de exames admitidos da aba Exames
  E clico na opção Bloqueio da aba Exames
  E é exibido o CIP no campo CIP da tela de Bloqueio
  E é exibida o número da visita no campo Visita da tela de Bloqueio
  E é exibido "1" no campo Num. Sequencial da tela de Bloqueio
  E informo "3" no campo Tipo de Bloqueio da tela de Bloqueio
  E a descrição do tipo de bloqueio "Paciente Deve Material" é exibida na tela de Bloqueio
  E a lista de exames é exibida na tela de Bloqueio informando um número sequencial começando em "1" no campo Ordem
  E informo o prazo limite no campo Prazo Limite da tela de Bloqueio
  E insiro uma observação no campo Observação da tela de Bloqueio
  Quando clico na opção Novo Bloqueio da tela de Bloqueio
  Então as informações Tipo de Bloqueio, Prazo Limite, Observação e a lista de exames são removidas da tela de Bloqueio