#language: pt
#encoding: utf-8
@exames_taxa_particular @done_exames
Funcionalidade: Manutenção de taxas na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de taxas na admissão on-line
 Para gerenciar taxas na visita de paciente

Esquema do Cenario: Taxas
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Part-10 Lea"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame     | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | OS             | CDA             | COLETA DOMICILIAR A |                    |                 |                      | false   | 28170016    | S  | S    | 
  Então todos os exames foram adicionados na Relação de exames admitidos da aba Exames

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome                | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "15051990"      | "1225470464" | "Roberto Alcantara Farias" | "2126"     | "PE"      |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "15051990"      | "1225470464" | "Roberto Alcantara Farias" | "2126"     | "PE"      |
