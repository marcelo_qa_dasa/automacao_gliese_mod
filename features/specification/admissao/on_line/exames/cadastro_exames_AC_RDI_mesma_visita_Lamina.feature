#language: pt
#encoding: utf-8
@exames_ac_RDI_mesma_ficha_Lamina @done_exames
Funcionalidade: Manutenção de exames de AC e RDI na mesma visita na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de exames de AC e RDI na mesma visita na admissão on-line
 Para gerenciar exames de AC e RDI na mesma visita na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja logado no sistema Gliese

Esquema do Cenario: Cadastro de exames de AC e RDI na mesma visita na marca Lâmina
 Quando seleciono a marca <Marca> e a unidade <Unidade>
 E clico no menu Admissão > On Line
 E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
 E clico na aba Visita
 E crio uma nova Visita
 E clico na aba Médico
 E clico no ícone binóculo do Número de Registro
 E clico no botão OK
 E clico no CRM da tela de resultado de pesquisa de médico
 E clico na aba Convênio
 E informo o convênio "Lm-amilfunc"
 E clico no botão Seleciona
 E são exibidas as instruções do convênio Lm-amilfunc
 E preencho as informações do convênio para o convênio Lm-amil/ami <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Validade do Pedido>
 E clico na aba Exames
Quando é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
 | Material_Exame | Mnemônico_Exame | Sinonimia_Exame      | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
 | SA             | HIV-1           | HIV 1+2 - ANTICORPOS |                    |                 |                      | false   | 40307182    | S  | S    |
 E o exame é adicionado na Relação de exames admitidos
 E preencho um material e exame de RDI
 | Material_Exame | Mnemônico_Exame | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
 | IM             | UABT            |                    |                 |                      | false   | 40901122    | S  | S    |
 E clico no botão valida da aba exames
 Então é exibida a mensagem de não permisão de exame de imagem de AC e RDI "Não é permitida a admissão de Exames de AC e RDI na mesma visita."

Exemplos:
 | Marca                                               | Unidade                   | Data_Nascimento | CIP          | Amil_Passe_Cartão | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano | Amil_Plano_Empresa |
 | "Diagnosticos da America - Lamina Med. Diag. Ltda"  | "LPO - Lamina - Arpoador" | "11041988"      | "41133895"   | "121212122"       | "121212122"           | "27092017"            | "Amil 150"      | "Sim"              |