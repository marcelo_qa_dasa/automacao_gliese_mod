#language: pt
#encoding: utf-8
@exames_ac_perfil_particular @done_exames
Funcionalidade: Manutenção dos exames de perfil no particular na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de exames de perfil no particular na admissão on-line
 Para gerenciar exames de perfil no particular na visita de paciente

Esquema do Cenario: Cadastro de exames AC de perfil no particular
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Br-bpbarraplaza"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  E é incrementado o número de Sangue e o Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão Valida da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                  | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | TIBC            | CAPACIDADE DE COMBINACAO DO FE...|                    |                 |                      | false   | 40301427    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na aba Recipientes
  E clico na aba Exames
  Então é incrementado o número de Sangue e o Total no Resumo da aba Exames e são adicionados os exames filhos dos exames de perfil previamente admitidos
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame           | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | UIBCTIB         | UIBC PARA CALCULAR O TIBC |                    |                 |                      | false   |             | N  | N    |
  | SA             | FE              | FERRO                     |                    |                 |                      | false   | 40301842    | N  | N    |

  Exemplos:
  | username   | password | Marca       | Unidade                         | Data_Nascimento | CIP          | medico_nome            | medico_crm | medico_uf |
  | "TESTER04" | "1234"   | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011969"      | "1225174163" | "Fabiano Cardoso"      | "52888320" | "RJ"      |
  #| "TESTER01" | "123456" | "Bronstein" | "068 - Bronstein - Barra Plaza" | "01011969"      | "1225174163" | "Fabiano Cardoso"      | "52888320" | "RJ"      |
