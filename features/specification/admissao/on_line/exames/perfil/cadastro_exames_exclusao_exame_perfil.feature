#language: pt
#encoding: utf-8
@exclusao_exame_rdi_perfil @done_exames
Funcionalidade: Manutenção dos exames de perfil RDI no particular na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de exames de perfil RDI no particular na admissão on-line
 Para gerenciar exames de perfil RDI no particular na visita de paciente

Esquema do Cenario: Exclusão exame de perfil
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E informo o convênio "Lm-ptarp"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio particular
  E preencho as informações do convênio para o convênio particular
  E clico na aba Exames
  Quando é incrementado o número de Imagem e o Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame                  | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | TCAT            | TC ABDOMEN TOTAL ABDOMEN SUP +...|                    |                 |                      | false   | 41001095    | N  | N    |
  Então é incrementado o número de Sangue e o Total no Resumo da aba Exames e são adicionados os exames filhos dos exames de perfil previamente admitidos
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame           | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | IM             | FPVTC1          | TC PELVICA                |                    |                 |                      | false   | 41001117    | S  | S    |
  | SA             | TCAS            | TC ABDOMEN SUPERIOR       |                    |                 |                      | false   | 41001109    | S  | S    |
  E clico na opção excluir do exame selecionado na Relação de exames admitidos
  | Sinonimia_Exame                   |
  | TC ABDOMEN TOTAL ABDOMEN SUP +... |
  E é exibida a mensagem de confirmação de exclusão de exame "Deseja Excluir - LM-TCATIM" 
  Quando clico no botão OK
  Então o exame de perfil e todos os seus exames filhos são removidos da Relação de exames admitidos e é decrementado o número de Sangue e Total no Resumo da aba Exames

  Exemplos:
  | username   | password | Marca                                               | Unidade                   | Data_Nascimento | CIP          | medico_nome             | medico_crm | medico_uf | Amil_Passe_Cartão | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano | Amil_Plano_Empresa |
  | "TESTER04" | "1234"   | "Diagnosticos da America - Lamina Med. Diag. Ltda"  | "LPO - Lamina - Arpoador" | "01011912"      | "6006817892" | "Marcio Alves Tannure"  | "52773794" | "CE"      | "121212122"       | "121212122"           | "27092017"            | "Amil 150"      | "Sim"              |
  #| "TESTER01" | "123456" | "Diagnosticos da America - Lamina Med. Diag. Ltda"  | "LPO - Lamina - Arpoador" | "01011912"      | "6006817892" | "Marcio Alves Tannure"  | "52773794" | "CE"      | "121212122"       | "121212122"           | "27092017"            | "Amil 150"      | "Sim"              |
