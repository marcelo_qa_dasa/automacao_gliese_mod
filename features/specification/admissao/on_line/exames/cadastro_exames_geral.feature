#language: pt
#encoding: utf-8
@exames_geral 
Funcionalidade: Manutenção de exames na admissão on-line
 Como atendente do sistema Gliese
 Posso manter o cadastro de exames na admissão on-line
 Para gerenciar exames na visita de paciente

Contexto: possibilidade de acessar o sistema
  Dado que eu esteja na home do sistema Gliese
  | username | password |
  | TESTER04 | 1234     |
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento e CIP com dados validos
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro 
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  Então clico na aba Exames

@limpar_cadastro_exames @done_exames
Cenário: Limpar cadastro de exames
  E informo "SA" no campo Material da aba Exames
  E informo "H" no campo Exames da aba Exames 
  Quando clico no botão Limpa da aba Exames
  Então a informação do exame é removida do campo Exames da aba Exames

@pesquisa_exame_nome @done_exames
Cenário: Pesquisa de exame pelo nome
  E informo "SA" no campo Material da aba Exames
  E clico na opção Pesquisa de Exame da aba Exames
  E informo o exame "Hemograma" no campo Exame da popup Consulta Exame
  E clico no ícone binóculo do campo Exame da popup Consulta Exame
  E são exibidas as informações de Mnemônico, Nome, Sinonimia, Status Convênio, AMB e TUSS dos exames que contenham "Hemograma" na coluna Sinonimia da popup de resultado de pesquisa de exame
  Quando clico no mnemônico "HSA" da tela de resultado de pesquisa de exame
  Então é exibido o sub-mnemônico "H" no campo Exames da aba Exames

@pesquisa_exame_mnemonico @done_exames
Cenário: Pesquisa de exame pelo mnemônico
  E informo "SA" no campo Material da aba Exames
  E clico na opção Pesquisa de Exame da aba Exames
  E informo o mnemônico "HSA" no campo Mnemônico da popup Consulta Exame
  E clico no ícone binóculo do campo Mnemônico da popup Consulta Exame
  E são exibidas as informações de Mnemônico, Nome, Sinonimia, AMB e TUSS dos exames que contenham "HSA" na coluna Mnemônico da popup de resultado de pesquisa de exame
  Quando clico no Sinonimia "Hemograma" da tela de resultado de pesquisa de exame
  Então é exibido o sub-mnemônico "H" no campo Exames da aba Exames

@exclusao_todos_exames @done_exames
Cenário: Exclusão de todos os exames
  E é incrementado o número Sangue, Urina, Outros e o Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão valida da aba exames da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame     | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg  | Impr |
  | UR             | EAS             | EAS - URINA TIPO 1  |                    |                 |                      | true    | 28130367    |  S  | S    |
  | SA             | H               | HEMOGRAMA COMPLETO  |                    |                 |                      | true    | 28040481    |  S  | S    |
  | LI             | PT              | PROTEINAS           |                    |                 |                      | true    | 28011600    |  S  | S    |
  E todos os exames são adicionados na Relação de exames admitidos da aba Exames
  E clico na opção Excluir Exames da aba Exames
  E é exibida a mensagem de confirmação de exclusão de todos os exames "Deseja excluir todos os exames?" 
  Quando clico no botão OK
  Então todos os exames são removidos da Relação de exames admitidos e é decrementado o número de Sangue, Urina, Outros e o Total no Resumo da aba Exames

@daylab_para_todos_exames @done_exames
Cenário: Daylab para todos os exames
  E é incrementado o número de exames de análises clínicas admitidos no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão valida da aba exames da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame     | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | UR             | EAS             | EAS - URINA TIPO 1  |                    |                 |                      | true    | 40311210    | S  | S    |
  | SA             | H               | HEMOGRAMA COMPLETO  |                    |                 |                      | true    | 40304361    | S  | S    |
  | LI             | PT              | PROTEINAS           |                    |                 |                      | true    | 40302377    | S  | S    |
  E todos os exames são adicionados na Relação de exames admitidos da aba Exames
  E clico na opção DayLab para Todos Exames da aba Exames
  E é exibida a mensagem de confirmação de inclusão de daylab para todos os exames "Deseja Incluir DayLab para todos os Exames?" 
  Quando clico no botão OK
  Então todos os exames são marcados na coluna Day Lab e com "S" na coluna DL na Relação de exames admitidos

@editacao_exame_easur @done_exames
Cenário: Editação de exame EASUR
  E é incrementado o número de Sangue, Urina, Fezes, Outros e Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão valida da aba exames da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame     | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | UR             | EAS             | EAS - URINA TIPO 1  |                    |                 |                      | true    | 40311210    | S  | S    |
  Então todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  Quando clico na opção Alterar do exame "EAS - URINA TIPO 1" na Relação de exames admitidos
  Então os campos Hora da Coleta e Hora do Recebimento informam a hora atual

@exclusao_exame @done_exames
Cenário: Exclusão de exame
  E é incrementado o número Sangue, Urina, Outros e o Total no Resumo da aba Exames
  #E seleciono o material do exame e seleciono o mnemônico do exame da respectiva sinonimia
  #E clico no botão valida da aba exames da aba Exames
  #E são exibidas as informações do exame na aba Exames
  #Quando clico no botão Salvar da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame     | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | SA             | H               | HEMOGRAMA COMPLETO  |                    |                 |                      | true    | 40304361    | S  | S    |
  E todos os exames são adicionados na Relação de exames admitidos da aba Exames
  E clico na opção excluir do exame na Relação de exames admitidos
  E é exibida a mensagem de confirmação de exclusão de exame "Deseja Excluir - HSA" 
  Quando clico no botão OK
  Então o exame é removido da Relação de exames admitidos e é decrementado o número de Sangue e Total no Resumo da aba Exames

@exames_ac_sexo_feminino_paciente_masculino @done_exames
Cenário: Cadastro de exame de sexo feminino a paciente do sexo masculino
 E informo "MI" no campo Material da aba Exames
 E informo o mnemônico "COLPO" no campo Exames da aba Exames
 E clico no botão valida da aba exames
 Então é exibida a mensagem de exame feminino "Exame só é feito no sexo Feminino - COLPOMI - 1"

@cadastro_exame_inativo @done_exames
Cenário: Cadastro de exame inativo
 E informo "SA" no campo Material da aba Exames
 E informo o mnemônico "HEMPC" no campo Exames da aba Exames
 E clico no botão valida da aba exames
 Então é exibida a mensagem de exame inativo "Exame HEMPCSA inativo para o convênio Amil."

@cadastro_exame_ja_cadastrado @done_exames
Cenário: Cadastro de exame já cadastrado
 E informo "SA" no campo Material da aba Exames
 E informo o mnemônico "H" no campo Exames da aba Exames
 E clico no botão valida da aba exames
 E clico no botão Salvar da aba Exames
 E o exame é adicionado na Relação de exames admitidos para HSA
 | Material_Exame | Mnemônico_Exame | Sinonimia_Exame     | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
 | SA             | H               | HEMOGRAMA COMPLETO  |                    |                 |                      | true    | 40304361    | S  | S    |
 E informo "SA" no campo Material da aba Exames   
 E informo o mnemônico "H" no campo Exames da aba Exames
 Entao clico no botão valida da aba exames é exibida a mensagem de exame já admitido "Exame já admitido! Deseja Incluir"
 E clico no botão OK
