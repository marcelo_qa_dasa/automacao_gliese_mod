#language: pt
#encoding: utf-8
@exames_ac_feminino_convenio
Funcionalidade: Manutenção dos exames de análises clínicas femininas pelo convênio na admissão on-line 
 Como atendente do sistema Gliese
 Posso manter o cadastro de exames de análises clínicas femininas na admissão on-line
 Para gerenciar exames de análises clínicas femininas na visita de paciente

# esse exame so é para ser feito no sexo feminino e o que esta qui é padrão da automação.
@edicao_exame_colpomi @done_exames
Cenário: Edição de cadastro de exame COLPOMI
  Dado que eu esteja na home do sistema Gliese
  | username | password | empresa        | unidade                   |
  | TESTER04 | 1234     | Sergio Franco  | LEA - Leblon - Cid Leblon |
  E clico no menu Admissão > On Line
  E preencho os campos Data_Nascimento e CIP 
  | Data_Nascimento | CIP       |
  | 26051991        | 6005805395|
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E clico no ícone binóculo do Número de Registro 
  E clico no botão OK
  E clico no CRM da tela de resultado de pesquisa de médico
  E clico na aba Convênio
  E Preencho as informações do convênio 
  | Convenio  | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  |  Amil     | "121212122"           | "21092017"            | 100 - Amil Master I   |  Sim               | "3"                       |
  Então clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab | Codificação | Pg | Impr |
  | MI             | COLPO           | COLPOCITOLOGICO | 1                  |                 | 18-Esfregaco Vaginal | false   | 40601137    | S  | S    |
  E todos os exames foram adicionados na Relação de exames admitidos da aba Exames
  E clico na opção Alterar do exame "COLPOCITOLOGICO" na Relação de exames admitidos
  E informo a quantidade de lâminas "1"
  E informo o material complementar "17-Esfregaco Cervico - Vaginal"
  E clico no botão Salvar da aba Exames
  Então o exame foi alterado com sucesso

@done_exames
Esquema do Cenario: Cadastro de exames AC feminino pelo convênio
  Dado que eu esteja logado no sistema Gliese como <username> e <password>
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E clico no menu Admissão > On Line
  E preencho os campos Data de nascimento <Data_Nascimento> e CIP <CIP>
  E clico na aba Visita
  E crio uma nova Visita
  E clico na aba Médico
  E informo o CRM <medico_crm> do medico e realizo a pesquisa
  E clico na aba Convênio
  E informo o convênio "Amil"
  E clico no botão Seleciona
  E são exibidas as instruções do convênio Amil
  E preencho as informações do convênio para o convênio Amil <Amil_Codigo_Associado>, <Amil_Data_Solicitacao>, <Amil_Nome_Plano>, <Amil_Plano_Empresa>, <Amil_Quantidade_Etiquetas>
  E clico na aba Exames
  E é incrementado o número de Outros e Total no Resumo da aba Exames
  | Material_Exame | Mnemônico_Exame | Sinonimia_Exame | Quantidade_Laminas | Volume_Material | Complemento_Material | Day_Lab |  Codificação | Pg | Impr |
  | MI             | COLPO           | COLPOCITOLOGICO | 1                  |                 | 18-Esfregaco Vaginal | false   | 40601137     | S  | S    |
  Então todos os exames foram adicionados na Relação de exames admitidos da aba Exames

  Exemplos:
  | username   | password | Marca            | Unidade                     | Data_Nascimento | CIP          | medico_nome            | medico_crm | medico_uf | Amil_Codigo_Associado | Amil_Data_Solicitacao | Amil_Nome_Plano       | Amil_Plano_Empresa | Amil_Quantidade_Etiquetas |
  | "TESTER04" | "1234"   | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "26051990"      | "6006024227" | "Natalia A.c. Freitas" | "10144043" | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
  #| "TESTER01" | "123456" | "Sergio Franco"  | "LEA - Leblon - Cid Leblon" | "26051990"      | "6006024227" | "Natalia A.c. Freitas" | "10144043" | "SP"      | "121212122"           | "21092017"            | "100 - Amil Master I" | "Sim"              | 3                         |
