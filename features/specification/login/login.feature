#language: pt
#encoding: utf-8
Funcionalidade: Acesso ao sistema de Atendimento
 Sendo usuário do sistema gliese
 Posso efetuar o login no sistema 
 Para suprir as minhas necessidades e acessar o sistema de Atendimento

Contexto:
 Dado que eu esteja na tela principal do sistema Gliase

@login_sucesso @done
Cenário: validar Login com sucesso
 E preencho os campos usuário e senha com dados validos
 Quando clico no botão Entrar
 Então deverá ser apresentado a tela de selenionar unidade

@login_invalido @done
Cenário: validar Login sem sucesso com dados invalidos
 E preencho os campos usuário e senha com dados invalidos
 |Username  |Password |
 |pjkj.dfrf |123456   |
 Quando clico no botão Entrar
 Então o usuário é apresentado uma mensagem
 """
 Login inválido!
 """
@unidade_atendimento @done
Cenário: Selecionando a unidade de Atendimento após o login
 E preencho os campos usuário e senha com dados validos
 E clico no botão Entrar
 Quando seleciono uma unidade de atendimento
 E clico no botão Escolher unidade de atendimento
 Entao é apresentado a home do sistema de atendimento

@redefinir_senha @to_do
Cenário: Redefinir senha
 E clico em esqueceu a senha?
 E preencho o campo com CPF valido
 E clico no botão Resetar Senha
 Entao é apresentado uma mensagem confirmação "Confirma resetar a sua senha?"
 E clico no botão OK para confirmar o resetar senha
 Entao é apresentado uma mensagem de nova senha enviada "Sua nova senha será enviada para o e-mail cadastrado"
 E clico no botão OK para confirmar o envio do e-mail da nova senha
 Entao será exibido o e-mail com a nova senha

@validar_nova_senha @to_do
Cenário: Validar redefinição de nova senha
 E preencho os campos usuário e a nova senha
 E clico no botão Entrar
 Entao é apresentado a home do sistema de atendimento

