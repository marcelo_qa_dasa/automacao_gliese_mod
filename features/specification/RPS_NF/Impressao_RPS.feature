#language: pt
#encoding: utf-8
@impressao_de_mensagem_rps
@sprint_5
Funcionalidade: Impressão de Mensagem no RPS.
 Como atendente do sistema Gliese
 Posso solicitar a impressão da NFE/RPS
 Para Valiar os Dados Impressos da solicitação

Contexto: possibilidade de acessar o sistema
 Dado que eu esteja logado no sistema Gliese como username e password
 | username | password |
 | TESTER01 | 123456   |

@validar_texto_label_observacoes
@sprint_5
Esquema do Cenario: Validar texto da label de obesrvações
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E acesso a tela de Impressao de RPS-NF
  Então valido se o texto "Observação NFE/RPS" e exibido na mesma

  Exemplos:
  | Marca        | Unidade                                |
  | "Frischmann" | "FSF - Frischmann - Santa Felicidade"  |
# | "Diagnosticos da America - Lamina Med. Diag. Ltda" | "LPO - Lamina - Arpoador" |


@validar_combo_empresa_ativa
@sprint_5
Esquema do Cenario: Validar se o combo de observação é exibido para empresas com status ativo
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E acesso a tela de Impressao de RPS-NF
  Então valido se o combo de obervação é exibido para empresa status Ativo usando <CIP> e <visita>

  Exemplos:
  | Marca        | Unidade                               | CIP        | visita |
  | "Frischmann" | "FSF - Frischmann - Santa Felicidade" | "0000888"  |  "128" |


@validar_campo_nao_editavel
@empresa_nao_ativa
@sprint_5
Esquema do Cenario: Validar campo de edição é exibido para empresas com status não ativo
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E acesso a tela de Impressao de RPS-NF
  Então valido se o campo observação é habilitado para digitação para empresa status não Ativo usando <CIP> e <visita>

  Exemplos:
  | Marca                                                  | Unidade                                | CIP        | visita  |
  | "Diagnosticos da America - Lamina Med. Diag. Ltda"     | "DAA - Lamina - Club da Arpoador"      | "899999"   |  "3927" |


@validar_rps_existente
@sprint_5
Esquema do Cenario: Validar campo observação estará habilitado para digitação
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E acesso a tela de Impressao de RPS-NF
  Então valido campo observação carrega informações da rps e não permite digitação usando <CIP> e <visita>

  Exemplos:
  | Marca    | Unidade                   | CIP        | visita | Observacao |
  | "Diagnosticos da America - Lamina Med. Diag. Ltda" | "LPO - Lamina - Arpoador" | "888"      |  "128" |   "teste"  |


@validar_observacao_impressao_rps
@sprint_5
Esquema do Cenario: Validar campo observação se é exibido na Impressão
  Quando seleciono a marca <Marca> e a unidade <Unidade>
  E acesso a tela de Impressao de RPS-NF
  Então solicito a impressão e valido o combo Observação usando <CIP> e <visita>

  Exemplos:
  | Marca                                                   | Unidade                               | CIP        | visita |
  #| "Diagnosticos da America - Lamina Med. Diag. Ltda"     | "LPO - Lamina - Arpoador"             | "899999"   | "3927" |
  #| "Frischmann" | "FSF - Frischmann - Santa Felicidade"   | "888"      |  "127" |
  | "Diagnosticos da America - Lamina Med. Diag. Ltda"      | "LPO - Lamina - Arpoador"            | "899999"   |  "3927"|
