
class FuncionalidadeAdmissaoExames <SitePrism::Page
    include RSpec::Matchers
    require 'rspec'

    element :btn_exames, '#lnkExames'
    element :input_Material, '#Exame_idMnmMat'
    element :input_exames, '#Exame_idMnmExame'
    element :img_binoculos_material, '#Exame_idMnmMatImg'
    element :img_binoculos_exames, '#Exame_idMnmExameImg'
    element :btn_valida_aba_exames, '#Exame_valida'
    element :btn_procedimentos_aba_exames, '#Exame_procedimento'
    element :btn_pesquisa_de_exame_aba_exam, '#Exame_botPesqExameSin'
    element :input_consulta_por_exame_popup, '#idSinonimia'
    element :input_consulta_por_mnemonico_popup, '#idMnemonico'
    element :img_binoculos__mnemonico_popup, '#idMnemonicoImg'
    element :img_binoculos_exames_popup, '#idSinonimiaImg'
    element :btn_salvar_exames, '#Exame_salva'
    element :txt_nome_exame_relacao_de_exame_admitidos, :xpath, '//*[@id="tabExa"]/tbody/tr[3]/td[4]'
    element :input_total_exames_existentes, '#Exame_idMatTotal'
    element :btn_limpar_campos_aba_exames, '#Exame_botNovo'
    element :img_fechar_informe_de_exames, :xpath, '//*[@id="divResprocedimentoAtendimento"]/img'
    element :input_indentificador_do_medico, '#Exame_idIdentMedico'
    element :input_data_da_agenda, '#Exame_idDataAgenda'
    element :input_hora_da_agenda, '#Exame_idHoraAgenda'
    element :txt_hsa_exame_popup_aba_exames, :xpath, '//*[@id="exanSin_resultado"]/tr[3]/td[1]/a'
    element :div_resultado_pesquisa_popup_aba_exames, '#exanSin_tabelaResultado'
    element :txt_hsa_mnemonico_popup_aba_exames, :xpath, '//*[@id="ididMnemonicoMnemonicoExameHSA"]'
    element :btn_excluir_todos_exames_aba_exames, '#Exame_botExcluirExames'
    element :btn_daylab_todos_exames_aba_exames, '#Exame_botDayLabTodos'
    element :img_editar_exames_aba_exames, :xpath, '//*[@id="1"]'
    element :validar_text_realizado_nesta_unidade_01, :xpath, '//*[@id="bodyAdmissao"]/div[8]/span[1]/em'
    element :validar_text_realizado_nesta_unidade_02, :xpath, '//*[@id="bodyAdmissao"]/div[8]/span[3]'
    element :validar_text_realizado_nesta_unidade_03, :xpath, '//*[@id="bodyAdmissao"]/div[8]/span[4]'
    element :validar_text_realizado_nesta_unidade_04, :xpath, '//*[@id="bodyAdmissao"]/div[8]/span[6]'
    element :img_botao_senha, :xpath, '//*[@id="tabExa"]/tbody/tr[3]/td[11]/a/img'
    element :input_senha_covenio_aba_exames, '#valorSenhaConvenio'
    element :btn_ok_confirmar_senha, '#clickSenhaConvenio'
    element :msg_senha_do_convenio_registrada, '.avisoExecutado'
    element :input_quantidade_laminas_aba_exames, '#Exame_idQtdLaminas'
    element :combo_material_complementar_aba_exames, "#Exame_idComplementoCmb"
    element :input_convenio_plano_particular_aba_convenio, '#Convenio_planoEmpresaNao'
    element :msg_exame_so_e_feito_para_masculino, '.avisoErro'
    element :input_editar_data_da_coleta, '#Exame_idDataColeta'
    element :input_editar_hora_da_coleta, '#Exame_idHoraColeta'
    element :input_editar_hora_recebimento, '#Exame_idHoraRecebimento'
    element :input_valor_tabela_exames, :xpath, '//*[@id="tabExa"]/tbody/tr[3]/td[4]/em'
    element :btn_bloquear_exames, '#Exame_botBloqueio'
    element :input_campo_cip_tela_bloqueio, '#Bloqueio_cipPac'
    element :input_campo_visita_tela_bloqueio, '#Bloqueio_vis'
    element :input_campo_numero_sequencial_tela_bloqueio, '#Bloqueio_numSeq'
    element :tecla_tab_para_exibir_os_outros_campos, :xpath, '//*[@id="divConteinerBlq"]/table/tbody/tr[2]/td/table/tbody/tr/td[1]/form/table/tbody/tr[4]/td'
    element :img_binoculos_num_sequencial, :xpath, '//*[@id="divConteinerBlq"]/table/tbody/tr[2]/td/table/tbody/tr/td[1]/form/table/tbody/tr[3]/td/a/img'
    element :input_tipo_de_bloqueio_tela_bloqueio, '#Bloqueio_lovBlq'
    element :input_descricao_tipo_de_bloqueio_tela_de_bloqueio, '#Bloqueio_descricao'
    element :input_observacao_tela_de_bloqueio, '#Bloqueio_obs'
    element :input_prazo_limite_tela_de_bloqueio, '#Bloqueio_prazoLimite'
    element :input_complemento_material, '#Exame_idComplemento'
    element :input_volume_material, '#Exame_idVolumeMat'
    element :btn_cadastrar_exame_bloqueado, '#Bloqueio_botCadastrar'
    element :div_mensagem_popup_bloqueio_aba_exames, '#divMsg'
    element :input_descricao_bloqueio_aba_exames, '#Bloqueio_descricao'
    element :btn_limpar_popup_bloqueio_aba_exames, '#Bloqueio_botLimpar'
    element :btn_novo_popup_bloqueio_aba_exames, '#Bloqueio_botNovoBloqueio'
    element :input_resumo_sangue, '#Exame_idMatSangue'
    element :input_resumo_urina, '#Exame_idMatUrina'
    element :input_resumo_fezes, '#Exame_idMatFezes'
    element :input_resumo_imagem, '#Exame_idMatImagem'
    element :input_resumo_outros, '#Exame_idMatOutros'
    element :input_senha_popup_convenio_aba_exames, '#valorSenhaConvenio'
    element :btn_salvar_senha_popup_convenio_aba_exames, '#clickSenhaConvenio'
    element :input_numero_guia_cv, '#Exame_idNumGuiaCv'


    def valida_existencia_popup_senha_convenio_aba_exames
      within_frame('iframeAdmissao') do
      ElementoVisivel.new.wait_element_visible("#ui-dialog-title-dialogSenhaExame")
      end
    end

   def clicar_botao_salvar_senha_convenio_popup_senha_aba_exames
    within_frame('iframeAdmissao') do
      self.btn_salvar_senha_popup_convenio_aba_exames.click
     end
   end

   def inserir_valor_senha_convenio_popup_senha_aba_exames(valor)
    within_frame('iframeAdmissao') do
     self.input_senha_popup_convenio_aba_exames.set(valor)
    end
   end

  def clicar_botao_senha_aba_exames
    begin
      within_frame('iframeAdmissao') do
        find(:xpath, '//*[@id="tabExa"]/tbody/tr[3]/td[10]/a/img').click
        end
    rescue Capybara::ElementNotFound => e
      find(:xpath, '//*[@id="tabExa"]/tbody/tr[3]/td[10]/a/img').click
    end
  end

  def clicar_botao_novo_bloqueio_popup_bloqueio_aba_exames
    self.btn_novo_popup_bloqueio_aba_exames.click
  end


  def validar_campos_desabilitados_popup_bloqueio_aba_exames(valor)
    $valor = valor
    if(has_css?('#Bloqueio_prazoLimite', :wait => 1) == $valor &&
      has_css?('#Bloqueio_lovBlq', :wait => 1) == $valor)
      true
    else
      false
    end
  end

  def clicar_botao_limpar_popup_bloqueio_aba_exames
      self.btn_limpar_popup_bloqueio_aba_exames.click
  end

  def validar_descricao_bloqueio_aba_exames(valor)
      @mensagem = self.input_descricao_bloqueio_aba_exames.value
      if @mensagem == valor
        return true
      else
        return false
      end
  end

  def validar_mensagem_tela_bloqueio(valor)
    page.find("#Bloqueio_lovBlq").send_keys(:tab)
    @mensagem = find(:xpath, '//*[@id="divMsg"]/p').text
    if @mensagem == valor
      return true
    else
      return false
    end
  end

  def inserir_valor_tipo_bloqueio_aba_exames(valor)
      self.input_tipo_de_bloqueio_tela_bloqueio.set(valor)
  end

  def validar_numero_sequencial_bloqueio_aba_exames(valor)
    @numero_sequencial = self.input_campo_numero_sequencial_tela_bloqueio.value
    if(@numero_sequencial == valor)
    return true
    else
    return false
    end
  end

  def validar_numero_visita_bloqueio_aba_exames
    ElementoVisivel.new.wait_element_visible("#Bloqueio_vis")
  end

  def validar_campo_cip_tela_bloqueio_aba_exames
    ElementoVisivel.new.wait_element_visible("#Bloqueio_cipPac")
  end


  def validar_campos_hora_da_coleta_hora_do_recebimento_informam_a_hora_atual(data1, hora1, hora2)
      within_frame('iframeAdmissao') do
      if(self.input_editar_data_da_coleta.value == data1 &&
        self.input_editar_hora_da_coleta.value == hora1 &&
        self.input_editar_hora_recebimento.value == hora2)
        true
        else
        false
      end
      end
  end

  def clicar_input_plano_particular_aba_convenio
    within_frame('iframeAdmissao') do
      FuncionalidadeAdmissaoExames.new.input_convenio_plano_particular_aba_convenio.click
    end
  end

  def clicar_no_botao_excluir_aba_exames
    within_frame('iframeAdmissao') do
        @botao_excluir =  find(:css, ".botaoExcluirExame")
        sleep 1
        @botao_excluir.click
        sleep 1
    end
  end

  def selecionar_valor_combo_materiais_complementares(valor)
    within_frame('iframeAdmissao') do
    sleep 1
    find(:css, '#Exame_idComplementoCmb').click
    page.all(:xpath, '//*[@id="bodyAdmissao"]/div[7]/ul/li').each do |i|
      if i.text == valor
      i.click
      break
      end
    end
    end
  end

  def validar_edicao_nome_exame_complemento
    within_frame('iframeAdmissao') do
      self.input_valor_tabela_exames.text
    end
  end

  def inserir_valore_quantidade_laminas_aba_exames(valor)
    within_frame('iframeAdmissao') do
      FuncionalidadeAdmissaoExames.new.input_quantidade_laminas_aba_exames.set(valor)
    end
  end

  def clicar_botao_editar_exames_aba_exames(exame)
    @d = 3
    within_frame('iframeAdmissao') do
    (1..1000).each do |i|
        @valor = find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[4]").text
       if has_xpath?("//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[4]/em", :wait => 1) == true
           @count = find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[4]/em").text.size+2
        if @count <= 2
          @valor = find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[4]").text
        else
         @valor = @valor[0..-@count]
        end
       end
      if exame == @valor
      @botao_editar =  page.all(:xpath, "//*[@id=\"#{i}\"]").first
      @botao_editar.click
      break
      end
      @d +=1
    end
  end
  end

  def validar_todos_daylabs_preenchidos_aba_exames(numero)
    (1..numero).each do |i|
    @checkbox_true = find(:xpath, "//*[@id=\"#{i}dayLab\"]").text
    if @checkbox_true == "S"
    else
    ApplicationError.new.msg_excecao_daylab_nao_checados
    end
  end
  return true
  end

  def clicar_botao_daylab_todos_exames_aba_exames
    within_frame('iframeAdmissao') do
      self.btn_daylab_todos_exames_aba_exames.click
    end
  end

  def validar_remocao_todos_exames_aba_exames(valor)
    $valor = valor
    if(has_xpath?('//*[@id="tabExa"]/tbody/tr[3]/td[2]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="tabExa"]/tbody/tr[3]/td[2]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="3dayLab"]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="tabExa"]/tbody/tr[3]/td[4]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="tabExa"]/tbody/tr[3]/td[5]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="tabExa"]/tbody/tr[3]/td[6]', :wait => 1) == $valor)
      false
    else
      true
    end
  end

  def clicar_botao_exluir_todos_exames_aba_exames
    within_frame('iframeAdmissao') do
      self.btn_excluir_todos_exames_aba_exames.click
    end
  end

  def validar_campos_resultado_da_pesquisa_exames_popup_aba_pesquisa(valor)
    $valor = valor
    within_frame('iframeAdmissao') do
    if(has_xpath?('//*[@id="exanSin_resultado"]/tr[1]/td[1]/a', :wait => 1) == $valor &&
      has_xpath?('//*[@id="exanSin_resultado"]/tr[1]/td[2]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="exanSin_resultado"]/tr[1]/td[3]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="exanSin_resultado"]/tr[1]/td[4]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="exanSin_resultado"]/tr[1]/td[5]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="exanSin_resultado"]/tr[1]/td[6]', :wait => 1) == $valor)
      true
    else
      false
    end
  end
  end

  def validar_campos_resultado_da_pesquisa_mneumonico_popup_aba_pesquisa(valor)
    $valor = valor
    within_frame('iframeAdmissao') do
    if(has_xpath?('//*[@id="ididMnemonicoMnemonicoExameHSA"]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="MncComp_resultado"]/tr[1]/td[2]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="MncComp_resultado"]/tr[1]/td[3]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="MncComp_resultado"]/tr[1]/td[4]', :wait => 1) == $valor &&
      has_xpath?('//*[@id="MncComp_resultado"]/tr[1]/td[5]', :wait => 1) == $valor)
      true
    else
      false
    end
  end
  end


  def clicar_elemento_hsa_pesquisa_campo_mnemonico_popup_aba_exames(sinonimia)
      $sinonimia = sinonimia
      $d = 1
      page.execute_script "window.scrollBy(0,100)"
      within_frame('iframeAdmissao') do
      page.all(:css, "#ididMnemonicoMnemonicoExameHSA").each do |mnemonico|
      if find(:xpath, "//*[@id=\"MncComp_resultado\"]/tr[#{$d}]/td[3]").text == $sinonimia
          mnemonico.click
        break
        end
        $d +=1
      end
    end
  end

  def clicar_binoculo_mnemonico_popup_aba_exames
    within_frame('iframeAdmissao') do
      self.img_binoculos__mnemonico_popup.click
    end
  end

  def inseir_valor_mnemonico_na_aba_exames(valor)
    within_frame('iframeAdmissao') do
      self.input_exames.set(valor)
    end
  end

  def inseir_valor_mnemonico_popup_aba_exames(valor)
    within_frame('iframeAdmissao') do
    self.input_consulta_por_mnemonico_popup.set(valor)
    end
  end

  def verificar_conteudo_exame_aba_exames
    within_frame('iframeAdmissao') do
      self.input_exames.value
    end
  end

  def clicar_elemento_hemograma_pesquisa_popup_aba_exames
    within_frame('iframeAdmissao') do
      self.txt_hsa_exame_popup_aba_exames.click
    end
  end

  def validar_informe_de_exames
    if has_css?("#divResprocedimentoAtendimento", :wait => 1) == true
      self.img_fechar_informe_de_exames.click
    end
  end

  def inserir_valor_complemento_material_aba_exames(valor)
    if has_css?("#Exame_idComplemento", :wait => 1) == true
        self.input_complemento_material.set(valor)
    end
    if has_css?("#Exame_idComplementoCmb", :wait => 1) == true
      find(:css, '#Exame_idComplementoCmb').click
      (1..1000).each do |i|
      @select = find(:xpath, "//*[@id=\"bodyAdmissao\"]/div[7]/ul/li[#{i}]")
        if @select.text == valor
          @select.click
        break
        end
      end
    end
  end

  def inserir_valor_volume_material_aba_exames(valor)
    if has_css?("#Exame_idVolumeMat", :wait => 1) == true
          self.input_volume_material.set(valor)
    end
  end

  def inserir_valor_quantidade_laminas_aba_exames(valor)
    if has_css?("#Exame_idQtdLaminas", :wait => 1) == true
      find(:css, '#Exame_idQtdLaminas').click
      find(:css, '#Exame_idQtdLaminas').set(valor)
    end
  end

  def validar_ident_do_medico(valor)
    if has_css?("#Exame_idIdentMedico", :wait => 1) == true
    $hora_atual = Utils.new.validar_hora_atual
    @data_atual = Utils.new.validar_data_atual
    self.input_indentificador_do_medico.set(valor)
    self.input_data_da_agenda.set(@data_atual)
    self.input_hora_da_agenda.set($hora_atual)
    sleep 1
    end
  end

  def clicar_lupa_pesquisa_popup_exame_aba_exame
    within_frame('iframeAdmissao') do
      self.img_binoculos_exames_popup.click
    end
  end

  def clicar_botao_pesquisar_exame_aba_exame
    within_frame('iframeAdmissao') do
      self.btn_pesquisa_de_exame_aba_exam.click
    end
  end

  def preencher_campo_exame_popup_exame(exame)
    within_frame('iframeAdmissao') do
      self.input_consulta_por_exame_popup.set(exame)
      end
  end

  def validar_campo_exame_vazio
    within_frame('iframeAdmissao') do
      self.input_exames.value == "" ? true:false
    end
  end
  def preencher_material_aba_exames(material)
    within_frame('iframeAdmissao') do
    self.input_Material.set(material)
    end
  end

  def clicar_no_botao_limpar_aba_exames
    within_frame('iframeAdmissao') do
      self.btn_limpar_campos_aba_exames.click
    end
  end
  def preencher_campo_exame_na_aba_exame(exame)
    within_frame('iframeAdmissao') do
      self.input_exames.set(exame)
    end
  end

  def clicar_no_botao_exames
    within_frame('iframeAdmissao') do
    self.btn_exames.click
    end
  end

  def validar_total_de_exames_existente
    within_frame('iframeAdmissao') do
      self.input_total_exames_existentes.value.to_i
    end
  end

  def validar_total_de_exames_existente_admitidos
    self.input_total_exames_existentes.value.to_i
  end

  def preencher_material_do_exame_e_seleciono_o_mnemonico_do_exame(table)
    page.execute_script('window.scrollTo(0,110)')
    @sangue = Array.new
    @urina = Array.new
    @fezes = Array.new
    @imagem = Array.new
    @outros = Array.new
    within_frame('iframeAdmissao') do
    table.hashes.each do |hash|
      @sinonimia_Exame = hash['Sinonimia_Exame']
      @material_Exame = hash['Material_Exame']
      @quantidade_Laminas = hash['Quantidade_Laminas']
      @volume_Material = hash['Volume_Material']
      @complemento_Material = hash['Complemento_Material']
      @numero_guia = hash['Numero_Guia'] 
      self.input_Material.set(@material_Exame)
      self.contador_de_categorias_de_materiais_na_aba_exames(@material_Exame, @sangue, @urina, @fezes, @imagem, @outros)
      $quantdade_exames = @material_Exame.size
      @mnemonico_Exame = hash['Mnemônico_Exame']
      self.input_exames.set(@mnemonico_Exame)
      self.clicar_no_botao_validar_exame_sem_iframe
      self.validar_informe_de_exames
      if @numero_guia != nil
        self.input_numero_guia_cv.set(@numero_guia)
      end 
      self.inserir_valor_quantidade_laminas_aba_exames(@quantidade_Laminas)
      self.inserir_valor_volume_material_aba_exames(@volume_Material )
      self.inserir_valor_complemento_material_aba_exames(@complemento_Material)
      self.validar_ident_do_medico("dpi00")
      begin
        ElementoVisivel.new.wait_element_visible('#Exame_salva')
      rescue Capybara::ElementNotFound => e
        ApplicationError.new.msg_excecao_exame_incorreto
      end
      self.btn_salvar_exames.click
      self.validar_informe_de_exames
    end
  end
  end

  def contador_de_categorias_de_materiais_na_aba_exames(valor, sangue, urina, fezes, imagem, outros)
    "SA" == valor ? $sangue = sangue.push(1) : $sangue = sangue.push(0)
    "UR" == valor ? $urina = urina.push(1) : $urina = urina.push(0)
    "FE" == valor ? $fezes = fezes.push(1) : $fezes = fezes.push(0)
    "IM" == valor ? $imagem = imagem.push(1) : $imagem = imagem.push(0)
    if( "SA" != valor &&
        "UR" != valor &&
        "FE" != valor &&
        "IM" != valor)
        $outros = outros.push(1)
    else
        $outros = outros.push(0)
    end
  end

  def preencher_material_para_qualquer_exame(table)
    table.hashes.each do |hash|
      @sinonimia_Exame = hash['Sinonimia_Exame']
      @material_Exame = hash['Material_Exame']
      @quantidade_Laminas = hash['Quantidade_Laminas']
      @volume_Material = hash['Volume_Material']
      @complemento_Material = hash['Complemento_Material']
      @mnemonico_Exame = hash['Mnemônico_Exame']
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          preencher_material_para_qualquer_exame_auxiliar
        end
        rescue StandardError => ex
          preencher_material_para_qualquer_exame_auxiliar if ex.message.include? "Unable to find"
      end
    end
  end

  def preencher_material_para_qualquer_exame_auxiliar
    input_Material.set(@material_Exame) if has_input_Material?
    input_exames.set(@mnemonico_Exame) if has_input_exames?
    btn_valida_aba_exames.click if has_btn_valida_aba_exames?
    validar_ident_do_medico("dpi00") if has_input_indentificador_do_medico?
    btn_salvar_exames.click
  end

  def validar_resumo_sangue_exames
      self.input_resumo_sangue.value
  end

  def validar_resumo_urina_exames
      self.input_resumo_urina.value
  end

  def validar_resumo_fezes_exames
      self.input_resumo_fezes.value
  end

  def validar_resumo_imagem_exames
      self.input_resumo_imagem.value
  end

  def validar_resumo_outros_exames
      self.input_resumo_outros.value
  end


  def validar_resumo_de_categorias_de_materiais
    aggregate_failures do
      expect($sangue.inject(:+)).to eql self.validar_resumo_sangue_exames.to_i
      expect($urina.inject(:+)).to eql self.validar_resumo_urina_exames.to_i
      expect($fezes.inject(:+)).to eql self.validar_resumo_fezes_exames.to_i
      expect($imagem.inject(:+)).to eql self.validar_resumo_imagem_exames.to_i
      expect($outros.inject(:+)).to eql self.validar_resumo_outros_exames.to_i
      $material_total = ($sangue.inject(:+) + $urina.inject(:+) + $fezes.inject(:+) + $imagem.inject(:+) + $outros.inject(:+))
      expect($material_total).to eql self.validar_total_de_exames_existente_admitidos
    end
  end

  def clicar_no_botao_salvar_exames
    begin
      within_frame('iframeAdmissao', :wait => 1) do
        self.btn_salvar_exames.click
      end
    rescue Capybara::ElementNotFound => e
      if has_css?('#Exame_salva', :wait => 1) == true
        self.btn_salvar_exames.click
      end
    end
  end

  def validar_lista_de_relacao_de_exames_admitidos_na_tela_de_exames(table)
    @d = 3
    @i = 1
    table.hashes.each do |hash|
      @mnemonico_Exame = hash['Mnemônico_Exame']
      @sinonimia_Exame = hash['Sinonimia_Exame']
      @day_lab_disabled = hash['Day_Lab']
      @amb = hash['AMB']
      @tuss = hash['TUSS']
      @codificacao = hash['Codificação']
      @pg_Exame = hash['Pg']
      @impr_Exame = hash['Impr']
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          validar_lista_de_relacao_de_exames_admitidos_auxiliar
        end
      rescue StandardError => ex
        validar_lista_de_relacao_de_exames_admitidos_auxiliar if ex.message.include? "Unable to find"
      end
      @d +=1
      @i +=1
    end
  end

  def validar_lista_de_relacao_de_exames_admitidos_auxiliar
      self.validar_resumo_de_categorias_de_materiais
      @day_lab_disabled ==  "true" ? find(:xpath, "//*[@id=\"#{@i}Chk\"]").set(true) : false
    aggregate_failures do
      expect(Utils.new.expressao_retornar_somente_letras(find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[2]").text)[0..-3]).to include Utils.new.converte_letras_validas(@mnemonico_Exame)
      expect(find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[4]").text).to include @sinonimia_Exame
      expect(@codificacao).to eql find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[5]").text
      expect(@pg_Exame).to eql find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[6]").text
      expect(@impr_Exame).to eql find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[7]").text
    end
  end

  def validar_exames_admitidos_filhos(table)
    @d = 4
    @i = 2
    @c = 1
    table.hashes.each do |hash|
      @mnemonico_Exame = hash['Mnemônico_Exame']
      @sinonimia_Exame = hash['Sinonimia_Exame']
      @day_lab_disabled = hash['Day_Lab']
      @codificacao = hash['Codificação']
      @pg_Exame = hash['Pg']
      @impr_Exame = hash['Impr']
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.validar_exames_admitidos_filhos_auxiliar
        end
      rescue StandardError => ex
          self.validar_exames_admitidos_filhos_auxiliar if ex.message.include? "Unable to find"
      end
      @d +=1
      @i +=1
      @c +=1
    end
  end

  def validar_exames_admitidos_filhos_auxiliar
    @day_lab_disabled ==  "true" ? find(:xpath, "//*[@id=\"#{@c}Chk\"]").set(true) : false
    aggregate_failures do
      expect(Utils.new.expressao_retornar_somente_letras(find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[2]").text)[0..-3]).to include Utils.new.converte_letras_validas(@mnemonico_Exame)
      expect(find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[4]").text).to include @sinonimia_Exame
      if @codificacao != nil && @codificacao != ""
        @valorTela = find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[5]").text
        if  @valorTela != nil && @valorTela != ""
        expect(@codificacao).to eql @valorTela
        end
      end
      expect(@pg_Exame).to eql find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[6]").text
      expect(@impr_Exame).to eql find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[7]").text
    end
  end

  def clicar_no_botao_validar_exame_sem_iframe
    for i in 0..3
      if has_css?('#Exame_salva', :wait => 1)
        break
      else
        self.btn_valida_aba_exames.click
        #esta dando erro para cenarios de teste, por isso foi comentado
        #page.driver.browser.switch_to.alert.accept if page.driver.browser.switch_to.alert.exist?
      end
    end

  end

  def clicar_no_botao_validar_exame
    within_frame('iframeAdmissao') do
      for i in 0..3
        if has_css?('#Exame_salva', :wait => 1) == false
          self.btn_valida_aba_exames.click
        end
        if has_css?('#Exame_salva', :wait => 1) == true
          break
        end
      end
    end
  end


  def preencher_um_material_do_exame_e_seleciono_o_mnemonico_do_exame(table)
    page.execute_script('window.scrollTo(0,100)')
    @sangue = Array.new
    @urina = Array.new
    @fezes = Array.new
    @imagem = Array.new
    @outros = Array.new
    within_frame('iframeAdmissao') do
    table.hashes.each do |hash|
      @sinonimia_Exame = hash['Sinonimia_Exame']
      @material_Exame = hash['Material_Exame']
      @mnemonico_Exame = hash['Mnemônico_Exame']
      $quantdade_exames = @material_Exame.size
      self.input_Material.set(@material_Exame)
      self.input_exames.set(@mnemonico_Exame)
      self.contador_de_categorias_de_materiais_na_aba_exames(@material_Exame, @sangue, @urina, @fezes, @imagem, @outros)
  end
  end
  end

  def validar_exame_nao_realizado_na_unidade_texto
  within_frame('iframeAdmissao') do
  "#{self.validar_text_realizado_nesta_unidade_01.text} #{self.validar_text_realizado_nesta_unidade_02.text} #{self.validar_text_realizado_nesta_unidade_03.text} #{self.validar_text_realizado_nesta_unidade_04.text}"
  end
  end

  def clicar_no_botao_para_informar_a_senha
    within_frame('iframeAdmissao') do
    self.img_botao_senha.click
    end
  end

  def preencher_senha_do_convenio_aba_exames(valor)
    within_frame('iframeAdmissao') do
    self.input_senha_covenio_aba_exames.set(valor)
    end
  end

  def confirmar_senha_convenio_botao_ok_aba_exames
    within_frame('iframeAdmissao') do
    self.btn_ok_confirmar_senha.click
    end
  end

  def validar_mensagem_senha_do_convenio_registrada
    within_frame('iframeAdmissao') do
    FuncionalidadeAdmissaoExames.new.msg_senha_do_convenio_registrada.text
    end
  end

  def validar_nao_exibicao_senha_convenio_aba_exames
    within_frame('iframeAdmissao') do
    has_xpath?('//*[@id="tabExa"]/tbody/tr[3]/td[11]/a/img', :wait => 1)
    end
  end

  def validar_mensagem_exame_so_e_feito_para_sexo_masculino
    within_frame('iframeAdmissao') do
    self.msg_exame_so_e_feito_para_masculino.text
    end
  end

  def validar_mensagem_mensagem_de_exame_inativo
    within_frame('iframeAdmissao') do
    self.msg_exame_so_e_feito_para_masculino.text
    end
  end

  def validar_mensagem_mensagem_de_exame_feminino
    within_frame('iframeAdmissao') do
    self.msg_exame_so_e_feito_para_masculino.text
    end
  end

  def validar_mensagem_mensagem_de_exame_ja_admitido
    within_frame('iframeAdmissao') do
    self.msg_exame_so_e_feito_para_masculino.text
    end
  end

  def validar_descricao_tipo_de_bloqueio
    self.input_descricao_tipo_de_bloqueio_tela_de_bloqueio.value
  end

  def preencher_prazo_limite_tela_de_bloqueio
    data_atual = Utils.new.validar_dia_atual.to_i+1
    self.input_prazo_limite_tela_de_bloqueio.click
    page.all(:css, '.ui-state-default').each do |data|
      if data.text == data_atual.to_s
        data.click
        break
      end
    end
  end

    def preencher_campos_tela_bloqueio_tipo_de_bloqueio_prazo_limite_e_observacao(tipo_de_bloqueio, observacao)
      self.input_tipo_de_bloqueio_tela_bloqueio.set(tipo_de_bloqueio)
      self.preencher_prazo_limite_tela_de_bloqueio
      self.input_observacao_tela_de_bloqueio.set(observacao)
    end


  def bloqueio_um_exame_check(exame)
    @d = 3
    (1..1000).each do |i|
      @valor = find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[2]")
      @total_exames = page.all(:xpath, '//*[@id="tabExa"]/tbody/tr').count-2
      if exame == @valor.text
        if @total_exames < $material_total
        bloaquear = page.all(:xpath, "//*[@id=\"#{i+1}\"]").last
        bloaquear.set(true)
        break
        else
        bloaquear = page.all(:xpath, "//*[@id=\"#{i}\"]").last
        bloaquear.set(true)
        break
        end
      end
      @d +=1
    end
  end

  def clicar_no_botao_cadastrar_tela_bloqueio
    self.btn_cadastrar_exame_bloqueado.click
  end

  def validar_lista_de_exames_numero_sequencial(numero)
    has_css?("#tabExa", :wait => 1)
    @valor = page.all(:xpath, "//*[@id=\"#{numero}\"]").first.value
    if @valor == numero
    end
  end

  def clicar_para_exibir_os_outros_campos
    sleep 1
    page.find("#Bloqueio_numSeq").click
    page.find("#Bloqueio_numSeq").send_keys(:tab)
  end

  def clicar_no_numero_sequencial(numero_sequencial)
    self.img_binoculos_num_sequencial.click
    (1..1000).each do |i|
      @valor = find(:xpath, "//*[@id=\"resultadoConsultaSeqBlqBloqueio_numSeq\"]/tr[#{i}]/td[1]/a")
      if numero_sequencial == @valor.text
        @valor.click
      break
      end
    end
  end


  def validar_texto_cip_existente_tela_de_bloqueio
    self.input_campo_cip_tela_bloqueio.value
  end

  def validar_texto_visita_existente_tela_de_bloqueio
    self.input_campo_visita_tela_bloqueio.value
  end


  def clicar_no_botao_bloquear_aba_exames
    within_frame('iframeAdmissao') do
    self.btn_bloquear_exames.click
    end
  end


  def validar_lista_de_relacao_de_exames_admitidos_decrementados_bloqueio(table)
    @d = 3
    within_frame('iframeAdmissao') do
    table.hashes.each do |hash|
    @mnemonico_Exame = hash['Mnemônico_Exame']
    @sinonimia_Exame = hash['Sinonimia_Exame']
    @day_lab_disabled = hash['Day_Lab']
    @codificacao = hash['Codificação']
    @pg_Exame = hash['Pg']
    @impr_Exame = hash['Impr']
    aggregate_failures do
    expect(Utils.new.expressao_retornar_somente_letras(find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[2]").text)[0..-3]).to include Utils.new.converte_letras_validas(@mnemonico_Exame)
    expect(find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[4]").text).to include @sinonimia_Exame
    if @codificacao != nil && @codificacao != ""
      expect(@codificacao).to eql find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[5]").text
    end
    expect(@pg_Exame).to eql find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[6]").text
    expect(@impr_Exame).to eql find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[7]").text
    end
    @d +=1
  end
  end
  end



  def clicar_botao_excluir_exames_selecionado_aba_exames(table)
    @d = 3
    @i = 1
    within_frame('iframeAdmissao') do
      table.hashes.each do |hash|
      @exame = hash['Sinonimia_Exame']
       (1..1000).each do |i|
        @valor = find(:xpath, "//*[@id=\"tabExa\"]/tbody/tr[#{@d}]/td[4]").text
      if @exame  == @valor
      @botao_excluir =  page.all(:xpath, "//*[@id=\"#{@i}\"]").last
      @botao_excluir.click
      break
      end
      @d +=1
      @i +=1
    end
  end
  end
  end









end
