
class FuncionalidadeAdmissaoDiversos <SitePrism::Page
  include RSpec::Matchers
  require 'rspec'

  element :btn_diversos, '#lnkDiversos'
  element :input_cid, '#cid'
  element :textarea_medicamento_cirugico, '#Diversos_medicamentos'
  element :input_peso_kg, '#Diversos_peso'
  element :input_altura, '#Diversos_altura'
  element :input_dias_abst_sexual, '#Diversos_diasAbstSexual'
  element :select_meio_do_resultado, '#Diversos_meioRes'
  element :btn_incluir_meio_do_resultado, '#Diversos_botIncluiMeioRes'
  element :input_numero_do_chamado, '#Diversos_numeroChamada'
  element :input_radio_rg_cpf_no_laudo_sim, '#rdImpS'
  element :input_radio_rg_cpf_no_laudo_nao, '#rdImpN'
  element :textarea_observacoes_tecnicas, '#Diversos_observacoes_tecnicas'
  element :textarea_observacoes_atendimento, '#Diversos_observacoes_atendimento'
  element :input_radio_imprime_cartao_sim, '#Diversos_imprimeCartao_S'
  element :input_radio_imprime_cartao_nao, '#Diversos_imprimeCartao_N'
  element :textarea_indicacao_clinica, '#Diversos_indicacaoClinica'
  element :input_radio_mais_1_crm_sim, '#Diversos_mais1CRM_S'
  element :input_radio_mais_1_crm_nao, '#Diversos_mais1CRM_N'
  element :input_radio_trouxe_exames_anteriores_sim, '#idTrouxeExAntS'
  element :input_radio_trouxe_exames_anteriores_nao, '#idTrouxeExAntN'
  element :select_procedencia, '#Diversos_procedencia'
  element :select_prioridade, '#Diversos_prioridade'
  element :select_ultima_refeicao, '#Diversos_ultimaRefeicao'
  element :input_data_ultima_refeicao, '#Diversos_idDataUltRefeicao'
  element :input_hora_ultima_refeicao, '#Diversos_idHoraUltRefeicao'
  element :btn_limpar, '#Diversos_botLimpa'
  element :grid_meio_resultado, '#meioResGrid1'

  element :input_logradouro_meio_do_resultado, '.logradouroAutomatico'
  element :input_numero_meio_do_resultado, '.numeroLogradouroAutomatico'
  element :input_bairro_meio_do_resultado, '.bairroAutomatico'
  element :input_complemento_meio_do_resultado, '.complementoAutomatico'
  element :select_uf_meio_do_resultado, '.ufAutomatico'
  element :input_cep_meio_do_resultado, '.cepAutomatico'
  element :input_ddd_meio_do_resultado, '.dddAutomatico'
  element :input_telefone_meio_do_resultado, '.telefoneAutomatico'
  element :input_municipio_meio_do_resultado, '.municipioAutomatico'
  element :input_referencia_meio_do_resultado, '.referenciaAutomatico'
  elements :select_meio_resultado, '#Diversos_meioRes'
  element :senha_convenio, '#Diversos_idSenhaConvenio'
  element :data_autorizacao, '#Diversos_idDataAutorizacao'
  element :input_exames_anteriores, '#Diversos_exaAnteriores'

  element :input_email_meio_do_resultado, '.txtEmailAutomatico'
  element :input_ultima_menstruacao, '#Diversos_ultimaMenstruacao'
  

  def validar_e_preencher_informacoes_meio_do_resultado(valor)
    @lista_de_dados_paciente = FuncionalidadeBancoDados.new.listagem_dados_paciente($cip)
     if @meio_do_resultado == "51 - Express-paciente Entrega A Domicilio"
        aggregate_failures do
        @logradouro = self.input_logradouro_meio_do_resultado.value
        @numero = self.input_numero_meio_do_resultado.value
        @bairro = self.input_bairro_meio_do_resultado.value
        @complemento = self.input_complemento_meio_do_resultado.value
        @uf = page.find('.ufAutomatico').all('option').find(&:selected?).text
        @cep = self.input_cep_meio_do_resultado.value
        @ddd = self.input_ddd_meio_do_resultado.value
        @telefone = self.input_telefone_meio_do_resultado.value
        @municipio = self.input_municipio_meio_do_resultado.value
        expect(@logradouro).to eql @lista_de_dados_paciente[0] if  expect(@logradouro.size != 0).to eql valor
        expect(@numero).to eql @lista_de_dados_paciente[1] if expect(@numero.size != 0).to eql valor
        expect(@bairro).to eql @lista_de_dados_paciente[2] if expect(@bairro.size != 0).to eql valor
        expect(@complemento).to eql @lista_de_dados_paciente[3] if expect(@complemento.size != 0).to eql valor
        expect(@uf).to eql @lista_de_dados_paciente[4] if expect(@uf != "Selecione...").to eql valor
        expect(@cep).to eql @lista_de_dados_paciente[5] if expect(@cep.size != 0).to eql valor
        expect(@ddd).to eql @lista_de_dados_paciente[6] if expect(@ddd.size != 0).to eql valor
        expect(@telefone).to include @lista_de_dados_paciente[12] if expect(@telefone.size != 0).to eql valor
        expect(@municipio).to eql @lista_de_dados_paciente[7] if expect(@municipio.size != 0).to eql valor
        end
        self.input_referencia_meio_do_resultado.set("texto de Referência")
        page.find(".referenciaAutomatico").send_keys(:tab)
      end
      if @meio_do_resultado == "52 - Automatico - Email Paciente"
          @email = find(:css, '.txtEmailAutomatico').value
          expect(@email).to eql @lista_de_dados_paciente[8] if expect(@email.size != 0).to eql valor
      end
  end


  def clicar_botao_remover_do_meio_resultado_especifico(valor)
    within_frame('iframeAdmissao') do
      @valor_tela = page.find('#Diversos_meioRes').all('option').find(&:selected?).text
     if valor == @valor_tela
      find(:xpath, '//*[@id="1"]').click
     end
    end
  end


  def validar_existencia_campo_preenchimento(valor)
    $valor = valor
    if(has_css?('#meioResGrid1', :wait => 1) == $valor )
      false
    else
      true
    end
  end


  def clicar_botao_remover_resultado_adicional
    within_frame('iframeAdmissao') do
      find(:xpath, '//*[@id="1"]').click
    end
  end

  def clicar_botao_limpar_aba_diversos
    within_frame('iframeAdmissao') do
      self.btn_limpar.click
    end
  end

  def clicar_no_botao_diversos
    if has_btn_diversos?
      self.btn_diversos.click
    else
      within_frame('iframeAdmissao') do
        self.btn_diversos.click
      end
    end
  end

  def preencher_as_informacoes_diversas_dos_exames(tabela)  # TODO: metodo refatorado
    page.execute_script('window.scrollTo(0,800)')
    tabela.hashes.each do |hash|
      # Carregando valores da tabela
      @utils = Utils.new
      @data_ultima_refeicao = @utils.validar_data_yesterday
      @ultima_menstruacao = hash['Última_menstruação']
      @hora_ultima_refeicao = hash['hora_ultima_refeicao']
      @ultima_refeicao = hash['Última_Refeição']
      @meio_do_resultado = hash['Meio_do_resultado']
      @numero_do_chamado = hash['numero_do_chamado']
      @medicamento_cirurg = hash['Medicamento_Cirurg']
      @senha_do_Convenio = hash['Senha_do_Convenio']
      @data_autorizacao = hash['Data_Autorização']
      @exames_anteriores = hash['Exames_Anteriores']
      @numero_da_chamada = hash['Número_da_chamada']
      @peso_kg = hash['Peso_kg']
      @altura = hash['Altura']
      @dias_abst_sexual = hash['Dias_abst_sexual']
      @rg_cpf_no_laudo = hash['RG_CPF_no_Laudo']
      @observacoes_tecnicas = hash['Observações_Técnicas']
      @observacoes_atendimento = hash['Observações_Atendimento']
      @imprime_cartao = hash['Imprime_cartao']
      @indicacao_clinica = hash['Indicação_clinica']
      @mais_1_crm = hash['Mais_de_1_CRM']
      @trouxe_exames_anteriores = hash['Trouxe_Exames_Anteriores']
      @procedencia = hash['Procedencia']
      @prioridade = hash['Prioridade']
      $cid = hash['cid']
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.preencher_as_informacoes_diversas_dos_exames_auxiliar
        end
      rescue StandardError => ex
        self.preencher_as_informacoes_diversas_dos_exames_auxiliar if ex.message.include? "Unable to find"
      end
    end
  end

  def preencher_as_informacoes_diversas_dos_exames_auxiliar
    self.textarea_medicamento_cirugico.set(@medicamento_cirurg) if has_css?("#Diversos_medicamentos", :wait => 1) 
    self.input_ultima_menstruacao.set(@ultima_menstruacao) if has_css?("#Diversos_ultimaMenstruacao", :wait => 1)
    self.input_cid.set($cid) if has_css?("#cid", :wait => 1)
    self.input_peso_kg.set(@peso_kg) if has_css?("#Diversos_peso", :wait => 1)
    self.input_altura.set(@altura) if has_css?("#Diversos_altura", :wait => 1)
    self.input_dias_abst_sexual.set(@dias_abst_sexual) if has_css?("#Diversos_diasAbstSexual", :wait => 1)
    texto_selecionado_lista_resultados = page.find('#Diversos_meioRes').all('option').find(&:selected?).text
    if @meio_do_resultado != "" && @meio_do_resultado != nil && @meio_do_resultado != texto_selecionado_lista_resultados 
      find(:css, '#Diversos_meioRes').select(@meio_do_resultado)
      self.btn_incluir_meio_do_resultado.click
      validar_e_preencher_informacoes_meio_do_resultado(true)
    end
    self.input_numero_do_chamado.set(@numero_do_chamado) if has_input_numero_do_chamado?
    page.find("#Diversos_numeroChamada").send_keys(:tab)
    "Sim" == @rg_cpf_no_laudo ? self.input_radio_rg_cpf_no_laudo_sim.set(true) : self.input_radio_rg_cpf_no_laudo_nao.set(false) if has_css?("#rdImpS", :wait => 1) == true
    self.textarea_observacoes_tecnicas.set(@observacoes_tecnicas) if has_css?("#Diversos_observacoes_tecnicas", :wait => 1) == true
    self.textarea_observacoes_atendimento.set(@observacoes_atendimento) if has_css?("#Diversos_observacoes_atendimento", :wait => 1) == true
    "Sim" == @imprime_cartao ? self.input_radio_imprime_cartao_sim.set(true) : self.input_radio_imprime_cartao_nao.set(false) if has_css?("#Diversos_imprimeCartao_S", :wait => 1) == true
    self.textarea_indicacao_clinica.set(@indicacao_clinica) if has_css?("#Diversos_indicacaoClinica", :wait => 1) == true
    page.find("#Diversos_indicacaoClinica").send_keys(:tab)
    "Sim" == @mais_1_crm ? self.input_radio_mais_1_crm_sim.set(true) : self.input_radio_mais_1_crm_nao.set(false) if has_css?("#Diversos_mais1CRM_S", :wait => 1) == true
    page.find("#idTrouxeExAntS").send_keys(:tab)
    "Sim" == @trouxe_exames_anteriores ? self.input_radio_trouxe_exames_anteriores_sim.set(true) : self.input_radio_trouxe_exames_anteriores_nao.set(false) if has_css?("#idTrouxeExAntS", :wait => 1) == true
    self.select_procedencia.select(@procedencia) if has_css?("#Diversos_procedencia", :wait => 1) == true && (@procedencia != "" && @procedencia != nil)
    self.select_prioridade.select(@prioridade) if has_css?("#Diversos_prioridade", :wait => 1) == true && (@prioridade != "" && @prioridade != nil)
    self.input_exames_anteriores.set(@exames_anteriores) if has_input_exames_anteriores?
    self.select_ultima_refeicao.select(@ultima_refeicao)
    self.input_data_ultima_refeicao.set(@data_ultima_refeicao)
    self.input_hora_ultima_refeicao.set(@hora_ultima_refeicao)
    self.senha_convenio.set(@senha_do_Convenio) if has_css?("#Diversos_idSenhaConvenio", :wait => 1) == true
    self.data_autorizacao.set(@data_autorizacao) if has_css?("#Diversos_idDataAutorizacao", :wait => 1) == true
  end

  def validar_limpar_todos_os_dados_tela_diversos
    page.execute_script('window.scrollTo(0,800)')
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.validar_limpar_todos_os_dados_tela_diversos_auxiliar
        end
      rescue StandardError => ex
        self.validar_limpar_todos_os_dados_tela_diversos_auxiliar if ex.message.include? "Unable to find"
      end
  end

  def validar_limpar_todos_os_dados_tela_diversos_auxiliar
    aggregate_failures do
    expect(self.textarea_medicamento_cirugico.value).to eql ""
    expect(self.input_peso_kg.value).to eql "" if has_css?("#Diversos_peso", :wait => 1)
    expect(self.input_altura.value).to eql "" if has_css?("#Diversos_altura", :wait => 1)
    expect(self.input_dias_abst_sexual.value).to eql "" if has_css?("#Diversos_diasAbstSexual", :wait => 1)
    expect(self.input_numero_do_chamado.value).to eql ""
    expect(self.textarea_observacoes_tecnicas.value).to eql "" if has_css?("#Diversos_observacoes_tecnicas", :wait => 1) == true
    expect(self.textarea_observacoes_atendimento.value).to eql "" if has_css?("#Diversos_observacoes_atendimento", :wait => 1) == true
    expect(self.textarea_indicacao_clinica.value).to eql "" if has_css?("#Diversos_indicacaoClinica", :wait => 1) == true
    page.find("#Diversos_indicacaoClinica").send_keys(:tab)
    page.find("#idTrouxeExAntS").send_keys(:tab)
    expect(page.find('#Diversos_procedencia').all('option').find(&:selected?).text).to eql "Selecione" if has_css?("#Diversos_procedencia", :wait => 1) == true
    expect(page.find('#Diversos_prioridade').all('option').find(&:selected?).text).to eql "Selecione" if has_css?("#Diversos_prioridade", :wait => 1) == true
    expect(page.find('#Diversos_ultimaRefeicao').all('option').find(&:selected?).text).to eql "Selecione"
    expect(self.input_data_ultima_refeicao.value).to eql ""
    expect(self.input_hora_ultima_refeicao.value).to eql ""
    end
  end


 def validar_campos_aba_diversos(valor)
  within_frame('iframeAdmissao') do
    @valor_tela = page.find('#Diversos_meioRes').all('option').find(&:selected?).text
   expect(valor).to eql @valor_tela
  end
 end


end
