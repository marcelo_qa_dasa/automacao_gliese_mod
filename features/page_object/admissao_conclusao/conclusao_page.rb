
  class FuncionalidadeAdmissaoConclusao <SitePrism::Page
    include RSpec::Matchers
    require 'rspec'

    element :btn_conclusao, '#lnkConclusao'
    element :msg_admissao_sem_pendencias , '#bannerPend'
    element :btn_salvar, '#Conclusao_botConfirma'
    element :cip_paciente, '#cip'
    element :visita_paciente, '#visita'
    element :nome_paciente, '#nome'
    element :msg_aviso_informacao, '#avisoInformacao'
    element :msg_admissao, '.msg_aviso_informacao'
    element :div_msg, '#pendListaUL' 
    element :btn_abandonar, '#Conclusao_botAbandona'

    def clicar_botao_abandonar
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.btn_abandonar.click
        end
      rescue StandardError => ex
        self.btn_abandonar.click if ex.message.include? "Unable to find"
      end
    end

    def clicar_no_botao_conclusao
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.btn_conclusao.click
        end
      rescue StandardError => ex
        self.btn_conclusao.click if ex.message.include? "Unable to find"
      end
    end

    def clicar_no_botao_salvar
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.btn_salvar.click
        end
        rescue StandardError => ex
          self.btn_salvar.click if ex.message.include? "Unable to find"
      end
    end

    def validar_exibicao_msg_admissao_sem_pendencias(msg)
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          expect(self.msg_admissao_sem_pendencias.text).to eq msg
        end
      rescue StandardError => ex
        expect(self.msg_admissao_sem_pendencias.text).to eq msg if ex.message.include? "Unable to find"
      end
    end


    def validar_guia_prazo_de_exames_para_visita(table)
      @d = 2
      table.hashes.each do |hash|
        @mnemonico_Exame = hash['Mnemônico_Exame']
        @material_exame = hash['Material_Exame']
        @sinonimia_Exame = hash['Sinonimia_Exame']
        @day_lab_disabled = hash['Day_Lab']
        @day_lab_disabled == "true" ? @day_lab = "Sim" : @day_lab = "Nao"
      begin
          within_frame('iframeAdmissao', :wait => 1) do
            self.validar_guia_prazo_de_exames_para_visita_auxiliar
          end
        rescue StandardError => ex
            self.validar_guia_prazo_de_exames_para_visita_auxiliar if ex.message.include? "Unable to find"
        end
        @d +=1
      end
    end

    def validar_guia_prazo_de_exames_para_visita_auxiliar
      aggregate_failures do
        expect(self.cip_paciente.text).to eq $cip
        expect(self.visita_paciente.text).to eq $numero_visita
        expect(self.nome_paciente.text).to eq $nome_pacinete
        expect(find(:xpath, "//*[@id=\"tableResultado\"]/tbody/tr[#{@d}]/td[1]").text).to include @mnemonico_Exame
        expect(find(:xpath, "//*[@id=\"tableResultado\"]/tbody/tr[#{@d}]/td[1]").text).to include @material_exame
        ## esta vindo errado a escrita do banco, everton já foi comunicado
        #expect(find(:xpath, "//*[@id=\"tableResultado\"]/tbody/tr[#{@d}]/td[2]").text).to eq "#{@sinonimia_Exame}"
        expect(find(:xpath, "//*[@id=\"tableResultado\"]/tbody/tr[#{@d}]/td[3]").text).to eq "#{@day_lab}"
      end
    end

    def validar_espelho_de_atendimento
      @utils = Utils.new
      @banco = FuncionalidadeBancoDados.new
      @lista_de_dados_paciente = @banco.listagem_dados_paciente($cip)
      @lista_espelho_de_atendimento = @banco.lista_espelho_de_atendimento
      result_text_pdf = @utils.carregar_pdf("EspelhoAtendimento.pdf").text
      @lista_espelho_de_atendimento.reverse.each do |espelho_de_atendimento|
        aggregate_failures do
          expect(result_text_pdf).to include espelho_de_atendimento[0] if espelho_de_atendimento[0] != nil
          expect(result_text_pdf).to include espelho_de_atendimento[3] if espelho_de_atendimento[3] != nil #exame
          expect(result_text_pdf).to include espelho_de_atendimento[5] if espelho_de_atendimento[5] != nil #senha
          expect(result_text_pdf).to include espelho_de_atendimento[10] if espelho_de_atendimento[10] != nil
          expect(result_text_pdf).to include espelho_de_atendimento[11] if espelho_de_atendimento[11] != nil
          expect(result_text_pdf).to include espelho_de_atendimento[12] if espelho_de_atendimento[12] != nil #guia
          expect(result_text_pdf).to include espelho_de_atendimento[13] if espelho_de_atendimento[13] != nil && espelho_de_atendimento[13].class.to_s != "BigDecimal" #plano       
          expect(result_text_pdf).to include "R$ #{espelho_de_atendimento[14].to_f.to_s.gsub('.', ',')}" if espelho_de_atendimento[14] != nil
        end      
    end
        aggregate_failures do
          expect(result_text_pdf).to include @lista_de_dados_paciente[9]
          expect(result_text_pdf).to include @lista_de_dados_paciente[13]
          expect(result_text_pdf).to include ("#{@lista_de_dados_paciente[14][0..1]}/#{@lista_de_dados_paciente[14][2..3]}/#{@lista_de_dados_paciente[14][4..8]}")
          expect(result_text_pdf).to include @lista_de_dados_paciente[10]
        end
    end

    def validar_guia_tiss
      @utils = Utils.new
      @banco = FuncionalidadeBancoDados.new
      @lista_de_dados_paciente = @banco.listagem_dados_paciente($cip)
      @lista_espelho_de_atendimento = @banco.lista_espelho_de_atendimento
      @nome_paciente = @lista_de_dados_paciente[9] if @lista_de_dados_paciente[9] != nil
      result_tiss = @utils.carregar_pdf("GuiaTissCvn.pdf").raw_content
        aggregate_failures do
          expect(result_tiss).to include $nome_unidade[0..2] if $nome_unidade[0..2].size <= 3 #codígo posto atendimento
          expect(result_tiss).to include $cip
          expect(result_tiss).to include $numero_visita
          expect(result_tiss).to include $nome_convenio
          expect(result_tiss).to include @nome_paciente
          expect(result_tiss).to include $nome_Medico_atendimento
          expect(result_tiss).to include $crm_medico_atendimento
          expect(result_tiss).to include @lista_espelho_de_atendimento[0][3] #exame
          expect(result_tiss).to include @lista_espelho_de_atendimento[0][14].to_f.to_s.gsub('.', ',') #valor exame  
        end
    end

    def validar_protocolo_de_atendimento_paciente
      @utils = Utils.new
      @banco = FuncionalidadeBancoDados.new
      @lista_de_dados_paciente = @banco.listagem_dados_paciente($cip)
      @lista_espelho_de_atendimento = @banco.lista_espelho_de_atendimento
      result_text_pdf = @utils.carregar_pdf("ProtImagem.pdf").text
      @lista_de_dados_paciente[13] == "M" ? @sexo = "Masculino" : @sexo = "Feminino"
      @data_de_nascimento = "#{@lista_de_dados_paciente[14][0..1]}/#{@lista_de_dados_paciente[14][2..3]}/#{@lista_de_dados_paciente[14][4..8]}"
      @idade_paciente = @utils.validar_data_atual_com_barras.to_s[6..-1].to_i-@data_de_nascimento[6..-1].to_i 
      @telefone_paciente = @lista_de_dados_paciente[12] if @lista_de_dados_paciente[12] != nil
      @nome_paciente = @lista_de_dados_paciente[9] if @lista_de_dados_paciente[9] != nil
      if $table_exame != nil && $table_exame != ""
        @total_tabela = $table_exame.raw.size-2
          (0..@total_tabela).each do |index_tabela_exames|
            @tabela_exames_resultado = $table_exame.raw.reverse[index_tabela_exames].reject { |c| c.empty? }
              aggregate_failures do
              expect(result_text_pdf).to include @tabela_exames_resultado[0] if @tabela_exames_resultado[0] == "IM" #Material_Exame
              expect(result_text_pdf).to include @tabela_exames_resultado[1] if @tabela_exames_resultado[0] == "IM" #Mnemônico_Exame
              end
          end
        end
        if $table_exame_filho != nil && $table_exame_filho != ""
        @total_tabela = $table_exame_filho.raw.size-2
          (0..@total_tabela).each do |index_tabela_exames|
            @tabela_exames_resultado = $table_exame_filho.raw.reverse[index_tabela_exames].reject { |c| c.empty? }
              aggregate_failures do
              expect(result_text_pdf).to include @tabela_exames_resultado[0] if @tabela_exames_resultado[0] == "IM" #Material_Exame
              expect(result_text_pdf).to include @tabela_exames_resultado[1] if @tabela_exames_resultado[0] == "IM" #Mnemônico_Exame
              end
          end
        end
      aggregate_failures do
        expect(result_text_pdf).to include "#{$cip} / #{$numero_visita}"
        expect(result_text_pdf).to include @utils.validar_data_atual_com_barras.to_s
        expect(result_text_pdf).to include $nome_unidade
        expect(result_text_pdf).to include $empresa
        expect(result_text_pdf).to include $nome_convenio
        expect(result_text_pdf).to include @lista_de_dados_paciente[0] if @lista_de_dados_paciente[0] != nil #rua
        expect(result_text_pdf).to include @lista_de_dados_paciente[2] if @lista_de_dados_paciente[2] != nil#Bairro
        ## cidate esta vindo cortada no pdf. o dev falou que esta correto devido ao tamanho do campo
        expect(result_text_pdf).to include @lista_de_dados_paciente[7][0..5] if @lista_de_dados_paciente[7] != nil #cidade
        expect(result_text_pdf).to include @data_de_nascimento
        expect(result_text_pdf).to include @sexo
        expect(result_text_pdf).to include @idade_paciente.to_s
        expect(result_text_pdf).to include @telefone_paciente
        expect(result_text_pdf).to include @nome_paciente
        expect(result_text_pdf).to include "#{$tabela_diversos.raw.reverse[0][8]} Kg"
        expect(result_text_pdf).to include "#{$tabela_diversos.raw.reverse[0][9]} Cm"
        end
    end

    def validar_msg_visita_autorizada
      within_frame('iframeAdmissao', :wait => 1) do
      find(:css, '.avisoInformacao').text
      end
    end

    def wait_window_handles(time)
      5.times do |i|
          if page.driver.browser.window_handles.size > 1
              sleep 2
              break
          end
          sleep time
      end
    end

    def validar_informacoes_pendencias_visita(table)
       within_frame('iframeAdmissao', :wait => 1) do
         @valor_pendencias_tela_conclusao = find(:css, '#pendListaUL').text
       end
       table.hashes.each do |hash_pendencia_admissao| 
        @pendencias_aba = hash_pendencia_admissao['aba']
        @pendencias = hash_pendencia_admissao['pendencia']
        aggregate_failures do
        expect(@valor_pendencias_tela_conclusao).to include @pendencias_aba
        expect(@valor_pendencias_tela_conclusao).to include @pendencias
        end
      end
    end 

    def validar_exibicao_msg_admissao_pendencias(msg)
      within_frame('iframeAdmissao', :wait => 1) do
        @valor = self.msg_admissao_sem_pendencias.text
      end
        if(@valor.to_s == msg.to_s)
        return true
        else
        return false
        end    
    end 


  end
