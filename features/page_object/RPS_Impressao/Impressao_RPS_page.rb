include RSpec::Matchers
require 'rspec'

class FuncionalidadeImpressaoRPS < SitePrism::Page

  element :submenu_impressao_RPS, '#mnuRps2'
  element :input_observacao, '#txtObservacao'
  element :input_visita, '#txtVisita'
  # element :cmb_observacao, '/html/body/form/div[1]/table/tbody/tr[3]/td/input[1]' #'//div[@class="tabelaHorizontal"]//input[@type="radio"]' # '//form[@name="frmGeraRps"]//div[@class="tabelaHorizontal"]//input[@class="rpsObservacao"][1]'
  element :btn_gerar_impressao, '#btGerar'
  element :menu_RPS_NF, :xpath, '//div[@class="mPrincipal"]//a[text()="RPS/NF"]'
  element :btn_binoculo_visita_rps, '#Rps_lovVisitasfind'
  element :input_cip, '#txtCip'

  def pega_valores_tabela_impressao_rps
    result = Hash.new
    idx = 0
    begin
      within_frame('iframeAdmissao') do
        all(:xpath, '//form[@name="frmGeraRps"]//div[@class="tabelaHorizontal"]//tr').each do |tr|
          tr.text
          result[idx] = tr.text
          idx +=1
        end
      end
      return result
    rescue
      puts 'Problemas ao ler tabela com valores do menu RPS'
    end
  end

  def preencher_cip(cip)
    within_frame('iframeAdmissao') do
      input_cip.set(cip)
    end
  end

  def preencher_visita(visita)
    within_frame('iframeAdmissao') do
      input_visita.set(visita)
      input_cip.click
      sleep 1
    end
  end

  def preencher_observacao(observacao)
    if has_input_observacao?
      self.input_observacao.set(observacao)
    else
      within_frame('iframeAdmissao') do
        self.input_observacao.set(observacao)
      end
    end
    sleep 1
  end

  def valida_campo_observacao
    within_frame('iframeAdmissao') do
      expect(has_input_observacao?).to be(true)
    end
  end

  def gerar_rps
    within_frame('iframeAdmissao') do
      btn_gerar_impressao.click
    end
  end

  def campo_rps_desabilitado
    expect(cmb_observacao.disabled).to be(true)
  end

  def combo_rsp_existe
    within_frame('iframeAdmissao') do
      all(:xpath, "//div[@class='tabelaHorizontal']//input[@type='radio']").each do |input|
        expect(input['name']).to eql("rpsObservacao")
      end
    end
  end

  def combo_rsp_desabilitado
    within_frame('iframeAdmissao') do
      expect(input_observacao['disabled']).to eql("true")
    end
  end

  def pega_texto_campo_observacao
    observacao = ""
    within_frame('iframeAdmissao') do
      if has_input_observacao?
        return input_observacao.value
      else
        all(:xpath, "//div[@class='tabelaHorizontal']//input[@type='radio']").each do |input|
          observacao = observacao + input['value'] + "|"
        end
      end
      return observacao
    end
  end

  def validar_pdf_relatorio_rps
      @utils = Utils.new
      @banco = FuncionalidadeBancoDados.new
      @lista_de_dados_paciente = @banco.listagem_dados_paciente($cip)
      @lista_espelho_de_atendimento = @banco.lista_espelho_de_atendimento
      @nome_paciente = @lista_de_dados_paciente[9]  if @lista_de_dados_paciente[9] != nil
      @cep = @lista_de_dados_paciente[5]  if @lista_de_dados_paciente[5] != nil
      ps_pdf = @utils.carregar_pdf("GerarRelatorioRpsServlet.pdf").text
      aggregate_failures do
      expect(ps_pdf).to include @utils.validar_data_atual_com_barras.to_s
      expect(ps_pdf).to include $nome_unidade[0..2]
      expect(ps_pdf).to include $empresa
      expect(ps_pdf).to include "#{$cip} / #{$numero_visita}"
      if $table_dados_nf != nil
        @dados_pessoal_nf = $table_dados_nf.raw.reverse[0]
        expect(ps_pdf).to include @dados_pessoal_nf[1] if @dados_pessoal_nf[1] != ""
        expect(ps_pdf).to include @dados_pessoal_nf[2] if @dados_pessoal_nf[2] != ""
        expect(ps_pdf).to include @dados_pessoal_nf[3] if @dados_pessoal_nf[3] != ""
        expect(ps_pdf).to include @dados_pessoal_nf[4] if @dados_pessoal_nf[4] != ""
        expect(ps_pdf).to include @dados_pessoal_nf[5] if @dados_pessoal_nf[5] != ""
        expect(ps_pdf).to include @dados_pessoal_nf[7] if @dados_pessoal_nf[7] != ""
        expect(ps_pdf).to include @dados_pessoal_nf[8] if @dados_pessoal_nf[8] != ""
      else
        expect(ps_pdf).to include @nome_paciente
        expect(ps_pdf).to include @lista_de_dados_paciente[0] if @lista_de_dados_paciente[0] != nil #rua 
        expect(ps_pdf).to include @lista_de_dados_paciente[2] if @lista_de_dados_paciente[2] != nil#Bairro
        expect(ps_pdf).to include @lista_de_dados_paciente[7] if @lista_de_dados_paciente[7] != nil#cidade
        expect(ps_pdf).to include @cep
        expect(ps_pdf).to include @lista_de_dados_paciente[1] if @lista_de_dados_paciente[1] != nil#numero ap/casa
        expect(ps_pdf).to include @lista_de_dados_paciente[3] if @lista_de_dados_paciente[3] != nil#complemento
      end
        expect(ps_pdf).to include $valor_total_exames.gsub!('.', ',') #valor total do exame
      end
      @lista_espelho_de_atendimento.each do |espelho_de_atendimento|
        @valor_lista_espelho = espelho_de_atendimento
        aggregate_failures do
          expect(ps_pdf).to include @valor_lista_espelho[4].to_f.to_s.gsub('.', ',') if @valor_lista_espelho[4] != nil
          expect(ps_pdf).to include @valor_lista_espelho[3] if @valor_lista_espelho[3] != nil
        end      
      end
  end



end
