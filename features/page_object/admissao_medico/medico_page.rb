
  class FuncionalidadeAdmissaoMedico <SitePrism::Page
    element :btn_medico, '#lnkMedico'
    element :btn_limpar_medico, '#Medico_botLimpa'
    element :btn_cadastar_medico, '#Medico_botNovo'
    element :selecionar_uf_medico, '#cmbUf'
    element :selecionar_crm_medico, '#cmbConselho'
    element :valor_crm_medico, '#txtCrm'
    element :campo_nome_medico, '#nomeMedico'
    element :campo_telefone_medico, '#telMedico'
    element :pesquisar_por_crm_medico, '#idPesquisatxtCrm'
    element :pesquisar_por_nome_medico, '#idPesquisanomeMedico'
    element :unidade_nova, '#UnidadeNova'
    element :clicar_entre_medico, '.tabelaHorizontal'
    element :msg_crm_inexistente, '#divMsg'
    element :lista_crm_medico, :xpath, '//*[@id="resultadoConsultaCRMtxtCrm"]/tr[1]/td[1]'
    element :lista_nome_medico, :xpath, '//*[@id="resultadoConsultaCRMtxtCrm"]/tr[1]/td[2]'
    element :lista_situacao_medico, :xpath, '//*[@id="resultadoConsultaCRMtxtCrm"]/tr[1]/td[3]'
    element :lista_crm_medico_pesquisa_por_nome, :xpath, '//*[@id="resultadonomeMedico"]/tr[1]/td[1]'
    element :lista_nome_medico_pesquisa_por_nome, :xpath, '//*[@id="resultadonomeMedico"]/tr[1]/td[2]'
    element :lista_situacao_medico_pesquisa_por_nome, :xpath, '//*[@id="resultadonomeMedico"]/tr[1]/td[3]'
    element :btn_cadastra_medico, '#Medico_botNovo'
    element :tela_cadastro_de_procissional_solicitante, :xpath, '//*[@id="divMedico"]/div[1]'
    element :campo_cadastro_paciente_codico_crm, '#formCodigo'
    element :mudar_foco_para_outro_campo_texto_cep, :xpath, '//*[@id="divMiolo"]/table/tbody/tr[3]/th[1]'
    element :btn_limpar_dados_medico, '#Medico_botLimpa'
    element :btn_remover_telefone, :xpath, '//*[@id="lin3"]/td[2]'

    element :campo_cep_tela_cadastro, '#formCep'
    element :campo_nuremo_tela_cadastro, '#formNumero'
    element :campo_bairro_tela_cadastro, '#formBairro'
    element :campo_ddd_tela_cadastro, '#formDdd'
    element :campo_telefone_tela_cadastro, '#formMedico_telefone'
    element :btn_adcionar_novo_telefone_tela_cadastro, '#Medico_botIncluiTel'
    element :btn_cadastrar_medico, '#botCadas'
    element :campo_nome_tela_cadastro, '#formNome'
    element :selecao_cliente_prefecencial_tela_cadastro, '#clientePrefer'
    element :campo_fax_tela_cadastro, '#formMedico_fax'
    element :btn_adcionar_novo_fax_tela_cadastro, '#Medico_botIncluiFax'
    element :btn_limpar_tela_cadastro, '#limpa'


    def clicar_botao_limpar_registros_de_medico
      within_frame('iframeAdmissao') do
      self.btn_limpar_dados_medico.click
      end
    end

    def validar_exibicao_botao_cadastrar_tela_cadastro_profissional
        page.should_not have_css(FuncionalidadeAdmissaoMedico.new.btn_cadastrar_medico)
    end

    def cadastrar_numero_fax_invalido_medico(numero)
      FuncionalidadeAdmissaoMedico.new.campo_fax_tela_cadastro.set(numero)
    end

    def cadastrar_numero_telefone_invalido_medico(numero)
      FuncionalidadeAdmissaoMedico.new.campo_telefone_tela_cadastro.set(numero)
    end

    def validar_exibicao_uf_da_unidade_selecionada
      within_frame('iframeAdmissao') do
      page.find('#cmbUf').all('option').find(&:selected?).text
      end
  end

    def validar_remover_infomacoes_selecionadas_cliente_prefecencial
    begin
      if page.find('#clientePrefer').all('option').find(&:selected?) == nil
      true
      end
      false
      rescue NameError => exception
      ApplicationError.new.msg_excecao_cliente_prefecencial_inexistente
    end
    end

    def remover_infomacoes_selecionadas_cliente_prefecencial
    begin
      if page.find('#clientePrefer').all('option').find(&:selected?).text != "Selecione..."
        self.selecao_cliente_prefecencial_tela_cadastro.select("Selecione...")
      end
    rescue NameError => exception
        ApplicationError.new.msg_excecao_cliente_prefecencial_inexistente
    end
    end

    def remover_infomacoes_campo_ddd_medico
      self.campo_ddd_tela_cadastro.set("")
    end

    def remover_infomacoes_campo_codigo_crm_medico
      self.campo_cadastro_paciente_codico_crm.set("")
    end

    def validar_remover_infomacoes_campo_codigo_crm_medico
      self.campo_cadastro_paciente_codico_crm.text.size == 0 ? false : true
    end

    def remover_infomacoes_campo_nome_medico
      self.campo_nome_tela_cadastro.set("")
    end

    def validar_remover_infomacoes_campo_nome_medico
      self.campo_nome_tela_cadastro.text.size == 0 ? false : true
    end

    def editar_medico_campos_da_tela_cadastro_profissional
      $novo_numero_telefone_medico = Faker::Number.number(8)
      self.campo_cep_tela_cadastro.set(Faker::Number.number(8))
      self.campo_nuremo_tela_cadastro.set(Faker::Address.building_number)
      self.campo_bairro_tela_cadastro.set(Faker::Address.country)
      self.campo_ddd_tela_cadastro.set(Faker::PhoneNumber.subscriber_number(2))
      if has_css?('#lin3') == true
        FuncionalidadeAdmissaoMedico.new.btn_remover_telefone.click
      end
      self.campo_telefone_tela_cadastro.set($novo_numero_telefone_medico)
      self.btn_adcionar_novo_telefone_tela_cadastro.click
    end

    def retorno_campo_selecao_sexo_medico_tela_cadastro
      return page.find('#sexo').all('option').find(&:selected?).text
    end

    def retorno_campo_selecao_uf_medico_tela_cadastro
      return page.find('#uf').all('option').find(&:selected?).text
    end

    def retorno_numero_telefone_medico_tela_pesquisa
      within_frame('iframeAdmissao') do
      begin
        FuncionalidadeAdmissaoMedico.new.campo_telefone_medico.text
      rescue Capybara::ElementNotFound => e
        ApplicationError.new.msg_excecao_numero_telefone_medico_inexistente
      end
      end
    end

    def clicar_no_botao_cadastrar_medico
      within_frame('iframeAdmissao') do
      self.btn_cadastra_medico.click
      end
    end

    def clicar_para_pesquisar_por_nome_de_medico
      within_frame('iframeAdmissao') do
      self.pesquisar_por_nome_medico.click
      end
    end

    def preencher_nome_medico(nome)
      within_frame('iframeAdmissao') do
      self.campo_nome_medico.send_keys(nome)
      end
    end


    def clicar_crm_lista_medicos_pesquisa_por_nome
      within_frame('iframeAdmissao') do
      self.lista_crm_medico_pesquisa_por_nome.click
      end
    end

    def clicar_crm_lista_medicos(codigo_medico)
      page.execute_script "window.scrollBy(0,600)"
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          clicar_crm_lista_medicos_auxiliar(codigo_medico)
        end
        rescue StandardError => ex
          clicar_crm_lista_medicos_auxiliar(codigo_medico) if ex.message.include? "Unable to find"
      end
    end

    def clicar_crm_lista_medicos_auxiliar(codigo_medico)
      (1..codigo_medico.size).each do |count_medico|
        all(:xpath, '//*[@id="resultadoConsultaCRMtxtCrm"]/tr').each do |nome_crm_medico|
          $crm = Utils.new.expressao_retornar_somente_numeros(nome_crm_medico.text)
          $estado = codigo_medico["medico_0#{count_medico}"]['uf']
          if $crm == codigo_medico["medico_0#{count_medico}"]['crm']
            find(:css, "#idtxtCrmCRMMedico#{$estado}-CRM-#{$crm}").click
            break
          end
        end
      end
    end

    def validar_lista_situacao_medico
      within_frame('iframeAdmissao') do
      self.lista_situacao_medico.text.size == 0 ? false : true
      end
    end

    def validar_lista_situacao_medico_pesquisa_por_nome
      within_frame('iframeAdmissao') do
      self.lista_situacao_medico_pesquisa_por_nome.text.size == 0 ? false : true
      end
    end

    def validar_lista_nome_do_medico
      within_frame('iframeAdmissao') do
      self.lista_nome_medico.text.size == 0 ? false : true
      end
    end

    def validar_lista_nome_do_medico_pesquisa_por_nome
      within_frame('iframeAdmissao') do
      self.lista_nome_medico_pesquisa_por_nome.text.size == 0 ? false : true
      end
    end

    def validar_lista_nome_do_medico_texto_pesquisa_por_nome
      within_frame('iframeAdmissao') do
      self.lista_nome_medico_pesquisa_por_nome.text
      end
    end

    def validar_lista_nome_do_medico_texto
      within_frame('iframeAdmissao') do
      self.lista_nome_medico.text
      end
    end

    def validar_lista_crm_medico
      within_frame('iframeAdmissao') do
      self.lista_crm_medico.text.size == 0 ? false : true
      end
    end

    def validar_lista_crm_medico_pesquisa_por_nome
      within_frame('iframeAdmissao') do
      self.lista_crm_medico_pesquisa_por_nome.text.size == 0 ? false : true
      end
    end

    def validar_lista_crm_medico_texto_pesquisa_por_nome
      within_frame('iframeAdmissao') do
      FuncionalidadeAdmissaoMedico.new.lista_crm_medico_pesquisa_por_nome.text
      end
    end

    def validar_lista_crm_medico_texto
      within_frame('iframeAdmissao') do
      FuncionalidadeAdmissaoMedico.new.lista_crm_medico.text
      end
    end

    def clicar_no_botao_pesquisa_por_crm
      within_frame('iframeAdmissao') do
      self.pesquisar_por_crm_medico.click
      end
    end

    def validar_mensagem_crm_inexistente
      within_frame('iframeAdmissao') do
      self.msg_crm_inexistente.text
      end
    end

    def unidade_de_atendimento_logada
      within_frame('iframeAdmissao') do
      FuncionalidadeAdmissaoMedico.class.new.selecionar_uf_medico.value
      end
    end

    def seleciona_uf_medico_por(uf)
      if has_selecionar_uf_medico?
        selecionar_uf_medico.select(uf)
      else
        within_frame('iframeAdmissao') do
          selecionar_uf_medico.select(uf)
        end
      end
    end

    def validar_unidade_de_atendimento_com_unidade_local_do_medico
      self.unidade_nova.text
    end

    def preencher_crm_medico(valor)
      within_frame('iframeAdmissao') do
        valor_crm_medico.send_keys(valor)
        valor_crm_medico.send_keys(:tab)
      end
    end

  def pressionar_tecla_enter_exibir_medico
    within_frame('iframeAdmissao') do
    self.clicar_entre_medico.click
    end
  end

    def clicar_botao_limpar_medico
      within_frame('iframeAdmissao') do
      self.btn_limpar_medico.click
        end
      end

  def clicar_botao_limpar_medico
    within_frame('iframeAdmissao') do
    self.btn_limpar_medico.click
      end
    end

    def clicar_na_aba_medico
      begin
        within_frame('iframeAdmissao') do
          self.btn_medico.click
        end
        rescue StandardError => ex
            self.btn_medico.click if ex.message.include? "Unable to find"
      end
    end

    def validar_valor_campo_uf_medico
      within_frame('iframeAdmissao') do
      self.selecionar_uf_medico.value.size == 0 ? false : true
      end
    end

    def validar_valor_campo_crm_medico
      within_frame('iframeAdmissao') do
      self.selecionar_crm_medico.value.size == 0 ? false : true
      end
    end

    def selecionar_medico_por(opcao)
      if has_selecionar_crm_medico?
        selecionar_crm_medico.select(opcao)
      else
        within_frame('iframeAdmissao') do
          selecionar_crm_medico.select(opcao)
        end
      end
    end

    def validar_valor_campo_numero_de_crm_medico
      within_frame('iframeAdmissao') do
      self.valor_crm_medico.value.size == 0 ? false : true
      end
    end

    def validar_valor_campo_numero_de_crm_medico_nao_existente_na_tela
      within_frame('iframeAdmissao') do
      self.valor_crm_medico.value == "" ? true : false
      end
    end

    def validar_valor_campo_numero_de_crm_medico_texto
      within_frame('iframeAdmissao') do
      FuncionalidadeAdmissaoMedico.new.valor_crm_medico.value
      end
    end

    def validar_nome_medico
      within_frame('iframeAdmissao') do
      self.campo_nome_medico.value.size == 0 ? false : true
      end
    end

    def validar_nome_medico_nao_existente_na_tela
      within_frame('iframeAdmissao') do
      self.campo_nome_medico.value == "" ? true : false
      end
    end

    def validar_nome_medico_texto
      within_frame('iframeAdmissao') do
      self.campo_nome_medico.value
      end
    end

    def validar_numero_telefone_medico
      within_frame('iframeAdmissao') do
      self.campo_telefone_medico.text.size == 0 ? false : true
      end
    end

    def result_nome_medico_atendimento_paciente
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          return find(:css, '#nomeMedico').value
        end
        rescue StandardError => ex 
          return find(:css, '#nomeMedico').value if ex.message.include? "Unable to find"
        end 
    end
    def wait_element_validar_preenchimento_medico_auxiliar
      8.times do |i|  
         @valor = find(:css, '#nomeMedico').value
      if @valor.size > 0 
        break  
      end   
      if i.to_s == "7"
        ApplicationError.new.msg_medico_nao_encontrado 
      end 
      sleep 1 
	    end 
    end
    
    def wait_element_validar_preenchimento_medico 
      begin
      within_frame('iframeAdmissao', :wait => 1) do
        wait_element_validar_preenchimento_medico_auxiliar
      end
      rescue StandardError => ex 
        wait_element_validar_preenchimento_medico_auxiliar if ex.message.include? "Unable to find"
      end   
    end
    
    

  end
