
class FuncionalidadeAdmissaoRecipientes <SitePrism::Page
  include RSpec::Matchers
  require 'rspec'

  element :btn_recipientes, '#lnkRecipientes'

  def clicar_no_botao_recipiente
    if has_btn_recipientes?
      btn_recipientes.click
    else
      within_frame('iframeAdmissao') do
        btn_recipientes.click
      end
    end
  end

  def validar_lista_de_recipientes_dos_exames(table)
    @d = 2
    table.hashes.each do |hash|
      @compl_material = hash['Compl_material']
      @recipiente = hash['Recipiente']
      @unidade_realizacao = hash['Unidade_Realização']
      @setor = hash['Setor']
      exames = hash['Exames']
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          validar_lista_de_recipientes_dos_exames_auxiliar(exames)
        end
      rescue StandardError => ex
        validar_lista_de_recipientes_dos_exames_auxiliar(exames) if ex.message.include? "Unable to find"
      end
    @d +=1
    end
  end

  def validar_lista_de_recipientes_dos_exames_auxiliar(exames)
    aggregate_failures do
      expect(find(:xpath, "//*[@id=\"tabRec\"]/tbody/tr[#{@d}]/td[2]").text).to include @compl_material
      expect(@recipiente).to eql find(:xpath, "//*[@id=\"tabRec\"]/tbody/tr[#{@d}]/td[3]").text
      expect(@unidade_realizacao).to eql find(:xpath, "//*[@id=\"tabRec\"]/tbody/tr[#{@d}]/td[4]").text
      expect(@setor).to eql find(:xpath, "//*[@id=\"tabRec\"]/tbody/tr[#{@d}]/td[5]").text
      expect(exames).to eql find(:xpath, "//*[@id=\"tabRec\"]/tbody/tr[#{@d}]/td[6]").text
    end
  end
end
