    class FuncionalidadeAdmissaoVisita <SitePrism::Page
      element :btn_visita, '#lnkVisita'
      element :data_atual_visita, '#Visita_txtData'
      element :tipo_coleta_visita, '#Visita_selTipoColeta'
      element :campo_numero_visita, '#Visita_txtNumero'
      element :click_validar_nova_visita, '#divVisita'
      element :campo_hora_visita, '#Visita_txtHora'
      element :unidade_nova, '#UnidadeNova'
      element :mensagem_visita_nao_existe, '.avisoErro'
      element :btn_nova_visita, '#Visita_botNova'
      element :btn_binoculo_resumo_visitas, '#Visita_resumoVisitasfind'
      element :lista_numero_ultima_visita, :xpath, '//*[@id="resultadoResumoVisitas"]/tr[1]/td[1]'
      element :lista_data_da_visita, :xpath, '//*[@id="resultadoResumoVisitas"]/tr[1]/td[2]'
      element :lista_unidade_da_visita, :xpath, '//*[@id="resultadoResumoVisitas"]/tr[1]/td[3]'
      element :lista_convenio_da_visita, :xpath, '//*[@id="resultadoResumoVisitas"]/tr[1]/td[4]'
      element :lista_exames_da_visita, :xpath, '//*[@id="resultadoResumoVisitas"]/tr[1]/td[5]'
      element :lista_documentos_da_visita, :xpath, '//*[@id="resultadoResumoVisitas"]/tr[1]/td[6]'
      element :mensagem_nao_exite_documento_visita, :xpath, '/html/body/h2'
      element :btn_documento_visita, '#Visita_botDocumentos'
      element :text_cip_numero_visitas, :xpath, '//*[@id="titulo"]'
      element :lista_de_visitas_completas, '#visCompVisita_resumoVisitas'
      element :resultato_total_de_resumo_visitas, '#visTotalVisita_resumoVisitas'
      element :btm_limpar_visita, '#Visita_botLimpa'
      element :selecionar_posto_visita, '#Visita_cmbUnidadeAtendimento'
      element :selecionar_empresa_visita, '#Visita_txtEmpresa'

    def selecionar_uma_empresa_visita(empresa_nome)
      within_frame('iframeAdmissao') do
      self.selecionar_empresa_visita.select(empresa_nome)
      end
    end

    def selecionar_um_tipo_de_coleta_visita(coleta_nome)
      within_frame('iframeAdmissao') do
      self.tipo_coleta_visita.select(coleta_nome)
      end
    end

    def selecionar_um_novo_posto_visita(posto_nome)
      within_frame('iframeAdmissao') do
      self.selecionar_posto_visita.select(posto_nome)
      end
    end

    def clicar_botao_Limpar
      within_frame('iframeAdmissao') do
      self.btm_limpar_visita.click
      end
    end

    def validar_resumo_total_de_visitas_do_paciente
      within_frame('iframeAdmissao') do
      self.resultato_total_de_resumo_visitas.text
      end
    end

    def clicar_link_lista_de_visitas_completas
      within_frame('iframeAdmissao') do
      self.lista_de_visitas_completas.click
      end
    end

    def validar_hora_atual_existente
      within_frame('iframeAdmissao') do
      self.campo_hora_visita.text.size == 0 ? false : ApplicationError.new.msg_excecao_hora_existente
      end
    end

    def validar_documentos_existente_lista_visitas
      within_frame('iframeAdmissao') do
      self.lista_documentos_da_visita.text.size == 0 ? false : true
      end
    end

    def validar_convenio_existente_lista_visitas
      within_frame('iframeAdmissao') do
      self.lista_convenio_da_visita.text.size == 0 ? false : true
      end
    end

    def validar_unidade_existente_lista_visitas
      within_frame('iframeAdmissao') do
      self.lista_unidade_da_visita.text.size == 0 ? false : true
      end
    end

    def validar_data_existente_lista_visitas
      within_frame('iframeAdmissao') do
      self.lista_data_da_visita.text.size == 0 ? false : true
      end
    end

    def validar_numero_existente_lista_visitas
      within_frame('iframeAdmissao') do
      self.lista_numero_ultima_visita.text.size == 0 ? false : true
      end
    end

    def retornar_numero_total_visitas
      within_frame('iframeAdmissao') do
      self.lista_numero_ultima_visita.text
      end
    end

    def retornar_text_cip_numero_visitas
      self.text_cip_numero_visitas.text
    end

    def retornar_mensagem_nao_exite_documento_visita
      self.mensagem_nao_exite_documento_visita.text
    end

    def clicar_botao_documento_visita
      within_frame('iframeAdmissao') do
      self.btn_documento_visita.click
    end
    end

    def clicar_numero_ultima_visita
      within_frame('iframeAdmissao') do
      @numero = self.lista_numero_ultima_visita.text
      find(:css, "#idVisita_resumoVisitasVisita#{@numero}").click
    end
    end

    def clicar_botao_binoculo_resumo_visitas
      within_frame('iframeAdmissao') do
      self.btn_binoculo_resumo_visitas.click
    end
    end

    def validar_numero_visita_existente
      within_frame('iframeAdmissao') do
      self.campo_numero_visita.value.size == 0 ? false : true
    end
    end

    def retornar_valor_numero_visita_atual
      within_frame('iframeAdmissao') do
      self.campo_numero_visita.value
    end
    end

    def wait_element_validar_numero_visita_existente
    begin
        within_frame('iframeAdmissao', :wait => 1) do
          wait_element_validar_numero_visita_existente_auxiliar
        end
      rescue StandardError => ex
          wait_element_validar_numero_visita_existente_auxiliar if ex.message.include? "Unable to find"
      end
    end

    def wait_element_validar_numero_visita_existente_auxiliar
      8.times do |i|
        @valor = find(:css, '#Visita_txtNumero')
        if @valor.value.size > 0
          break
        end
        if i.to_s == "7"
        ApplicationError.new.msg_numero_nova_visita_existente
        end
        sleep 1
      end
    end

    def clicar_botao_nova_visita
      within_frame('iframeAdmissao') do
      self.btn_nova_visita.click
    end
    end


    def preencher_campo_numero_tela_visita(numero_visita)
      within_frame('iframeAdmissao') do
      self.campo_numero_visita.send_keys(numero_visita)
      self.click_validar_nova_visita.click
    end
    end

    def clicar_enter
      within_frame('iframeAdmissao') do
      self.click_validar_nova_visita.click
    end
    end

    def clicar_no_botao_visita
      within_frame('iframeAdmissao') do
      self.btn_visita.click
      end
    end

    def retorno_visita_campo_posto_unidade_atendimento
      within_frame('iframeAdmissao') do
      begin
        return page.find('#Visita_cmbUnidadeAtendimento').all('option').find(&:selected?).text
      rescue NameError => exception
        ApplicationError.new.msg_excecao_posto_nao_existente
      end
    end
    end

    def retorno_visita_campo_empresa
      within_frame('iframeAdmissao') do
      return page.find('#Visita_txtEmpresa').all('option').find(&:selected?).text
    end
    end


    def retorno_visita_data_atual
      within_frame('iframeAdmissao') do
      self.data_atual_visita.value
      end
    end

    def retorno_visita_tipo_de_coleta
      within_frame('iframeAdmissao') do
      page.find('#Visita_selTipoColeta').all('option').find(&:selected?).text
      end
    end

    def retorno_visita_hora
      within_frame('iframeAdmissao') do
      self.campo_hora_visita.value.size == 0 ? false : true
      end
    end

    def mensagem_vista_nao_existe
      within_frame('iframeAdmissao') do
      self.mensagem_visita_nao_existe.text
    end
    end

  end
