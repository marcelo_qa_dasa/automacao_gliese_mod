include RSpec::Matchers
require 'rspec'

class FuncionalidadeAdmissaoFinanceiro <SitePrism::Page
  element :btn_financeiro, '#lnkFinanceiro'
  element :select_forma_de_pagamento, '#Financeiro_foPgm'
  element :select_numero_do_banco, '#Financeiro_banco'
  element :input_numero_da_agencia, '#Financeiro_agencia'
  element :input_numero_da_conta, '#Financeiro_conta'
  element :input_numero_do_cheque, '#Financeiro_cheque'
  element :input_data_do_deposito, '#Financeiro_dataDeposito'
  element :input_data_limite, '#Financeiro_dataLimite'
  element :input_valor, '#Financeiro_valorPagamento'
  element :btn_salvar_financeiro, '#Financeiro_botGravar'
  element :select_cartao_de_credito, '#Financeiro_cartaoCredito'
  element :input_numero_de_parcela, '#Financeiro_numParcela'
  element :input_codigo_cv_aut, '#Financeiro_cvaut'
  element :select_cartao_de_debito, '#Financeiro_cartaoDebito'
  element :input_resumo_total, '#Financeiro_total'
  element :btn_nota_fiscal_terceiro, '#Financeiro_botNfTerc'
  element :div_msg, '#divMsg'

  element :input_nome, '#idNome'
  element :input_logradouro, '#idLogradouro'
  element :input_numero, '#idNumero'
  element :input_complemento, '#idComplemento'
  element :input_bairro, '#idBairro'
  element :input_municipio, '#idMunicipio'
  element :select_estado, '#cmbUf'
  element :input_cep, '#idCepTerceiro'
  element :input_cpf_cnpj, '#idCpf'
  element :btn_salvar, '#botCadastrar'
  element :btn_excluir, '#botExcluir'
  element :btn_limpar, '#botLimpar'
  element :btn_pesquisar_municipio, '#idPadrao'

  element :btn_excluir_forma_pgto, :xpath, '//input[@id="A" and @value="Excluir"]'
  elements :tbl_forma_pgto, '#tabFin'
  element :btn_limpar, '#Financeiro_botLimpa'
  elements :tbl_resumo, '#trFinanceiro_Resumo .cpoLeitura'
  element :btn_alterar_forma_pgto, :xpath, '//input[@id="A" and @value="A"]'
  
  element :btn_desconto, '#Financeiro_botDescontoSupervisao'
  element :input_desconto_usuario, '#usuarioDescontoSupervisao'
  element :input_desconto_senha, '#senhaDescontoSupervisao'
  element :select_desconto, '#motDescMarca'
  element :radio_input_desconto_percentual, '#descEmPercentual'
  element :input_desconto_percentual, '#percentualDescSupervisao'
  element :radio_input_valor_em_reais, '#descEmValor'
  element :input_valor_em_reais, '#valorEmReaisDescSupervisao'
  element :input_desconto_motivo, '#motDescOutros'
  element :btn_desconto_ok, '#btnDescontoSupervisao'
  element :input_campo_valor_desconto, '#Financeiro_totalComDesconto'


  def valor_desconto
      within_frame('iframeAdmissao', :wait => 1) do
        return self.input_campo_valor_desconto.value
      end
  end

  def clicar_no_botao_desconto_ok
    begin
      within_frame('iframeAdmissao', :wait => 1) do
        self.btn_desconto_ok.click
      end
      rescue StandardError => ex
        self.btn_desconto_ok.click if ex.message.include? "Unable to find"
    end
  end 

  def preencher_desconto_supervisao(table)
    table.hashes.each do |hash|
      @usuario = hash['usuario']
      @senha = hash['senha']
      @desconto = hash['desconto']
      @percentual = hash['percentual']
      @valor_em_reais = hash['valor_em_reais']
      @motivo = hash['motivo']
      begin
        within_frame('iframeAdmissao', :wait => 1) do
        self.preencher_desconto_supervisao_auxiliar
        end
      rescue StandardError => ex
        self.preencher_desconto_supervisao_auxiliar if ex.message.include? "Unable to find"
      end
    end
      self.clicar_no_botao_desconto_ok
  end


  def validar_popup_desconto_desabilitado
    @usuario = execute_script "return document.getElementById('usuarioDescontoSupervisao').disabled" 
    @senha = execute_script "return document.getElementById('motDescMarca').disabled" 
    @percentual = execute_script "return document.getElementById('percentualDescSupervisao').disabled" 
    aggregate_failures do
    expect(@usuario).to eql true
    expect(@senha).to eql true
    expect(@percentual).to eql true 
    expect(self.input_desconto_usuario.value.size > 0).to eql true
    expect(self.input_desconto_percentual.value.size > 0).to eql true
    expect(page.find('#motDescMarca').all('option').find(&:selected?).text.size > 0).to eql true
    end
  end  

  def preencher_desconto_supervisao_auxiliar
    self.input_desconto_usuario.set(@usuario)
    self.input_desconto_senha.set(@senha)
    self.select_desconto.select(@desconto)
    $opcao_de_desconto = page.find('#motDescMarca').all('option').find(&:selected?).text
    if @desconto == "Outros"
      if @percentual.size > 1 
         self.radio_input_desconto_percentual.set(true)
         self.input_desconto_percentual.set(@percentual) 
      elsif @valor_em_reais.size > 1 
        self.radio_input_valor_em_reais.set(true)
        self.input_valor_em_reais.set(@valor_em_reais)
      end
        self.input_desconto_motivo.set(@motivo)
    end
  end

  def validar_desconto_aplicado_financeiro
      @utils = Utils.new
      @valor_total_exames = self.retornar_valor_resumo_total
      if $table_desconto.raw.reverse[0][3].size > 1
          @valor_percentual_desconto = $table_desconto.raw.reverse[0][3] if $table_desconto != nil && $table_desconto.raw.reverse[0][3] != ""
          calculo_valor_com_desconto = @utils.calculo_desconto_porcentagem(@valor_total_exames, @valor_percentual_desconto)
          expect(self.valor_desconto.to_f).to eq calculo_valor_com_desconto
      elsif $table_desconto.raw.reverse[0][4].size > 1
          @valor_reais_desconto = $table_desconto.raw.reverse[0][4] if $table_desconto != nil && $table_desconto.raw.reverse[0][4] != ""
          calculo_valor_com_desconto = @valor_total_exames.to_f - $table_desconto.raw.reverse[0][4].to_i
          expect(self.valor_desconto.to_f).to eq calculo_valor_com_desconto
      elsif $opcao_de_desconto != "Outros"
          @valor_percentual_desconto = $table_desconto.raw.reverse[0][2] if $table_desconto != nil && $table_desconto.raw.reverse[0][2] != ""
          valor_desconto_numeros = @utils.expressao_retornar_somente_numeros(@valor_percentual_desconto)
          calculo_valor_com_desconto = @utils.calculo_desconto_porcentagem(@valor_total_exames, valor_desconto_numeros)
          expect(self.valor_desconto.to_f).to eq calculo_valor_com_desconto
      end
  end
     
  def clicar_no_botao_desconto
    begin
      within_frame('iframeAdmissao', :wait => 1) do
        btn_desconto.click
      end
      rescue StandardError => ex
        btn_desconto.click if ex.message.include? "Unable to find"
    end
  end 

  def clicar_no_botao_salvar
    begin
      within_frame('iframeAdmissao', :wait => 1) do
        btn_salvar.click
      end
      rescue StandardError => ex
        btn_salvar.click if ex.message.include? "Unable to find"
    end
  end

  def validar_campos_existentes_resultado_municipio(valor)
    @valor = valor
    if(has_xpath?('//*[@id="tabelaResultadoidMunicipio"]/table/thead/tr/th[1]', :wait => 1) == @valor &&
      has_xpath?('//*[@id="tabelaResultadoidMunicipio"]/table/thead/tr/th[2]', :wait => 1) == @valor &&
      has_xpath?('//*[@id="tabelaResultadoidMunicipio"]/table/thead/tr/th[3]', :wait => 1) == @valor)
      true
    else
      false
    end
  end

  def preencher_dados_nota_fiscal_de_terceiro(table)
    table.hashes.each do |hash|
      @nome = hash['nome']
      @logradouro = hash['logradouro']
      @numero = hash['numero']
      @complemento = hash['complemento']
      @bairro = hash['bairro']
      @municipio = hash['municipio']
      @estado = hash['estado']
      @cep = hash['cep']
      @cnpj_cpf = hash['cnpj_cpf']
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.preencher_dados_nota_fiscal_de_terceiro_auxiliar
        end
      rescue StandardError => ex
          self.preencher_dados_nota_fiscal_de_terceiro_auxiliar if ex.message.include? "Unable to find"
      end
    end
  end

  def validar_informacoes_existentes_nos_campos_preenchido(table)
    table.hashes.each do |hash|
      @nome = hash['nome']
      @logradouro = hash['logradouro']
      @numero = hash['numero']
      @complemento = hash['complemento']
      @bairro = hash['bairro']
      @municipio = hash['municipio']
      @estado = hash['estado']
      @cep = hash['cep']
      @cnpj_cpf = hash['cnpj_cpf']
      aggregate_failures do
        expect(self.input_nome.value).to eq @nome
        expect(self.input_logradouro.value).to eq @logradouro
        expect(self.input_numero.value).to eq @numero
        expect(self.input_complemento.value).to eq @complemento
        expect(self.input_bairro.value).to eq @bairro
        expect(self.input_municipio.value).to eq @municipio
        expect(self.input_cep.value).to eq @cep
        expect(self.input_cpf_cnpj.value).to eq @cnpj_cpf
      end
    end

  end


  def validar_limpar_dados_campos_nota_fiscal
    aggregate_failures do
      expect(self.input_logradouro.value).to eq("")
      expect(self.input_numero.value).to eq("")
      expect(self.input_bairro.value).to eq("")
      expect(self.input_cpf_cnpj.value).to eq("")
    end
end

  def preencher_dados_nota_fiscal_de_terceiro_auxiliar
        self.input_nome.set(@nome)
        self.input_logradouro.set(@logradouro)
        self.input_numero.set(@numero)
        self.input_complemento.set(@complemento)
        self.input_bairro.set(@bairro)
        self.selecionar_municipio_nota_fiscal_de_terceiro(@municipio)
        @estado != nil && @estado != "" ? self.select_estado.select(@estado) : false 
        self.input_cep.set(@cep)
        self.input_cpf_cnpj.set(@cnpj_cpf)
  end

  def selecionar_municipio_nota_fiscal_de_terceiro(tabela_municipio)
    if tabela_municipio != nil && tabela_municipio != ""
      self.input_municipio.set(tabela_municipio) if has_css?("#idMunicipio", :wait => 1) == true
      self.btn_pesquisar_municipio.click if has_css?("#idPadrao", :wait => 1) == true
        (1..100).each do |i|
         @valor_municipio = find(:xpath, "//*[@id=\"resultadoidMunicipio\"]/tr[#{i}]/td[1]/a")
         page.find(:xpath, "//*[@id=\"resultadoidMunicipio\"]/tr[#{i}]/td[1]/a").send_keys(:tab) 
          if @valor_municipio.text == tabela_municipio
           @valor_municipio.click
           break
          end
        end
    end
  end

  def validar_municipios_que_iniciam_com_informacoes_solicitada(tabela_municipio)
     @total_municipios = all(:xpath, '//*[@id="resultadoidMunicipio"]/tr').size
    (1..@total_municipios).each do |i|
      @valor_municipio = find(:xpath, "//*[@id=\"resultadoidMunicipio\"]/tr[#{i}]/td[1]/a")
      page.find(:xpath, "//*[@id=\"resultadoidMunicipio\"]/tr[#{i}]/td[1]/a").send_keys(:tab)
      expect(@valor_municipio.text[0..2]).to eq tabela_municipio
      i == @total_municipios ? break : true
    end
  end


  def selecionar_municipio(tabela_municipio)
    (1..100).each do |i|
     @valor_municipio = find(:xpath, "//*[@id=\"resultadoidMunicipio\"]/tr[#{i}]/td[1]/a")
     page.find(:xpath, "//*[@id=\"resultadoidMunicipio\"]/tr[#{i}]/td[1]/a").send_keys(:tab)
      if @valor_municipio.text == tabela_municipio
       @valor_municipio.click
       break
      end
    end
  end


  def validar_exibicao_municipio_selecionado
    self.input_municipio.value
  end

  def clicar_no_botao_financeiro
    if has_btn_financeiro?
      self.btn_financeiro.click
    else
      within_frame('iframeAdmissao') do
        self.btn_financeiro.click
      end
    end
  end

  def clicar_no_botao_nota_fiscal_terceiro
    if has_btn_nota_fiscal_terceiro?
      self.btn_nota_fiscal_terceiro.click
    else
      within_frame('iframeAdmissao') do
        self.btn_nota_fiscal_terceiro.click
      end
    end
  end

  def selecionar_valuer_codigo_de_banco_selecionado
      return page.find('#Financeiro_banco').all('option').find(&:selected?).value
  end


  def retornar_valor_resumo_total
    within_frame('iframeAdmissao', :wait => 1) do
    return self.input_resumo_total.value
    end
  end

  # Valor adicional é usado para somar com valor do Total do Exame, teste Negativo #LucasBarretto
  def preencher_forma_de_pagamento(table)
    $valor_total_exames = self.retornar_valor_resumo_total
    @utils = Utils.new
    @valor_total_esta_parcelado = false
    @tamanho_tabela_massa = table.hashes.size
    @index_hash = 1
    ultima_parcela = false
    table.hashes.each do |hash|
      @forma_de_pgto = hash['forma_de_pgto']
      @banco = hash['banco']
      @agencia = hash['agencia']
      @conta = hash['conta']
      @cheque = hash['cheque']
      if hash['dt_dep'] == nil
        @dt_dep = @utils.validar_data_atual
      else
        @dt_dep = hash['dt_dep']
      end
      if hash['dt_lim'] == nil
        @dt_lim = @utils.validar_data_tomorrow
      else
        @dt_lim = hash['dt_lim']
      end
      @cartao = hash['cartao']
      @no_cartao = hash['no_cartao']
      @val_cartao = hash['val_cartao']
      @num_parc = hash['num_parc']
      @cv_aut = hash['cv_aut']
      @valor_adicional = hash['valor_opcional'] # Se o mesmo estiver preenchido, será somado com o valor do campo Total.
      ultima_parcela = true if @index_hash == @tamanho_tabela_massa && @tamanho_tabela_massa > 1
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.preencher_forma_de_pagamento_auxiliar(ultima_parcela)
        end
      rescue StandardError => ex
        self.preencher_forma_de_pagamento_auxiliar(ultima_parcela) if ex.message.include? "Unable to find"
      end
      @index_hash += 1
    end
  end

  def preencher_forma_de_pagamento_auxiliar(ultima_parcela)
    self.select_forma_de_pagamento.select(@forma_de_pgto) if has_css?('#Financeiro_foPgm', :wait => 1) == true
    sleep 1
    self.select_numero_do_banco.select(@banco) if has_css?('#Financeiro_banco', :wait => 1) == true
    $codigo_banco = self.selecionar_valuer_codigo_de_banco_selecionado if has_css?('#Financeiro_banco', :wait => 1) == true
    self.input_numero_da_agencia.set(@agencia) if has_css?('#Financeiro_agencia', :wait => 1) == true
    self.input_numero_da_conta.set(@conta) if has_css?('#Financeiro_conta', :wait => 1) == true
    self.input_numero_do_cheque.set(@cheque) if has_css?('#Financeiro_cheque', :wait => 1) == true
    self.input_data_do_deposito.set(@dt_dep) if has_css?('#Financeiro_dataDeposito', :wait => 1) == true
    self.input_data_limite.set(@dt_lim) if has_css?('#Financeiro_dataLimite', :wait => 1) == true
    if @valor_adicional != nil
     $valor = self.input_resumo_total.value.to_f + @valor_adicional.to_f
      self.input_valor.set(@utils.formata_valores_float_para_string($valor))
    else
      if @valor_total_esta_parcelado == false
        self.input_valor.set(formatar_paracela_valor_total)
        @valor_total_esta_parcelado = true
      elsif ultima_parcela == true
        self.input_valor.set(find(:css, '#Financeiro_falta').value) if has_css?('#Financeiro_falta', :wait => 1)
      else
        self.input_valor.set(find(:css, '#Financeiro_total').value)
      end
    end
    find(:select, "Financeiro_cartaoCredito").first(:option, @cartao).select_option if has_css?('#Financeiro_cartaoCredito', :wait => 1) == true
    self.input_codigo_cv_aut.set(@cv_aut) if has_css?('#Financeiro_cvaut', :wait => 1) == true
    self.input_numero_de_parcela.set(@num_parc) if has_css?('#Financeiro_numParcela', :wait => 1) == true
    self.select_cartao_de_debito.select(@cartao) if has_css?('#Financeiro_cartaoDebito', :wait => 1) == true
    self.btn_salvar_financeiro.click
  end

  def validar_preenchimento_forma_de_pagamento(table)
    @utils = Utils.new
    @d = 4
    table.hashes.each do |hash|
      @forma_de_pgto = hash['forma_de_pgto']
      @banco = hash['banco']
      @agencia = hash['agencia']
      @conta = hash['conta']
      @cheque = hash['cheque']
      if hash['dt_dep'] == nil
        @dt_dep = @utils.validar_data_atual
      else
        @dt_dep = hash['dt_dep']
      end
      if hash['dt_lim'] == nil
        @dt_lim = @utils.validar_data_tomorrow
      else
        @dt_lim = hash['dt_lim']
      end
      @cartao = hash['cartao']
      @no_cartao = hash['no_cartao']
      @val_cartao = hash['val_cartao']
      @num_parc = hash['num_parc']
      @cv_aut = hash['cv_aut']
      begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.validar_preenchimento_forma_de_pagamento_auxiliar
        end
      rescue StandardError => ex
          self.validar_preenchimento_forma_de_pagamento_auxiliar if ex.message.include? "Unable to find"
      end
      @d +=1
    end
  end

  def validar_preenchimento_forma_de_pagamento_auxiliar
    aggregate_failures do
      expect(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[3]").text).to eq @forma_de_pgto
      coluna_banco = find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[4]").text
      expect(coluna_banco).to eq $codigo_banco if $codigo_banco != nil && coluna_banco != '' && @forma_de_pgto.upcase.include?('Cheque').upcase
      expect(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[5]").text).to eq @agencia
      expect(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[6]").text).to eq @conta
      expect(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[7]").text).to eq @cheque
      data_dep = find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[8]").text
      expect(data_dep).to eq @dt_dep if data_dep != nil && data_dep != ""
      data_limite = find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[9]").text
      expect(data_limite).to eq @dt_lim if data_limite != nil && data_limite != ""
      expect(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[10]").text).to eq @cartao
      expect(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[11]").text).to eq @no_cartao
      expect(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[12]").text).to eq @val_cartao
      expect(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[13]").text).to eq @num_parc
      expect(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[14]").text).to eq @cv_aut
      # expect(@utils.expressao_retornar_somente_numeros(find(:xpath,"//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[15]").text)).to eq $valor
      expect(find(:xpath, "//*[@id=\"tabFin\"]/tbody/tr[#{@d}]/td[15]").text.to_f).to eql($valor.to_f)
    end
  end

  def retorna_msg_financeiro
    if has_div_msg?
      self.div_msg.text
    else
      within_frame('iframeAdmissao') do
        self.div_msg.text
      end
    end
  end

  def clicar_no_botao_excluir
    if has_btn_excluir_forma_pgto?
      self.btn_excluir_forma_pgto.click
    else
      within_frame('iframeAdmissao') do
        self.btn_excluir_forma_pgto.click
      end
    end
  end

  def pegar_registros_tabela_forma_pgto
    if has_tbl_forma_pgto?
      self.tbl_forma_pgto.count
    else
      within_frame('iframeAdmissao') do
        self.tbl_forma_pgto.count
      end
    end
  end

  def selecionar_forma_pgto(forma_de_pgto)
    if has_select_forma_de_pagamento?
      self.select_forma_de_pagamento.select(forma_de_pgto)
    else
      within_frame('iframeAdmissao') do
        self.select_forma_de_pagamento.select(forma_de_pgto)
      end
    end
  end

  def limpar_forma_pgto
    if has_btn_limpar?
      self.btn_limpar.click
    else
      within_frame('iframeAdmissao') do
        self.btn_limpar.click
      end
    end
  end

  def validar_valor_selecionado_forma_pagamento(texto_para_validacao)
    if has_select_forma_de_pagamento?
      expect(has_select?('Financeiro_foPgm', :selected => texto_para_validacao, :wait => 1)).to be(true)
    else
      within_frame('iframeAdmissao') do
        expect(has_select?('Financeiro_foPgm', :selected => texto_para_validacao, :wait => 1)).to be(true)
      end
    end
  end

  def validar_valores_resumo
    if has_tbl_resumo?
      validar_valores_resumo_auxiliar
    else
      within_frame('iframeAdmissao') do
        validar_valores_resumo_auxiliar
      end
    end
  end

  def validar_valores_resumo_auxiliar
    $valor = $valor.gsub('.', '') if $valor.is_a? String
    expect(find(:css, '#Financeiro_total').value.to_f).to eql($valor)
    expect(find(:css, '#Financeiro_pago').value.to_i).to eql(0)
    expect(find(:css, '#Financeiro_falta').value.to_i).to eql(0)
    expect(find(:css, '#Financeiro_troco').value.to_i).to eql(0)
  end

  def validar_valor_grid_forma_pagamento_com_total_resumo
    if has_tbl_resumo? && has_tbl_forma_pgto?
      expect(find(:xpath, "//*[@id=\"tabFin\"]/tbody/tr[4]/td[15]").text).to eql(find(:css, '#Financeiro_total').value)
    else
      within_frame('iframeAdmissao') do
        expect(find(:xpath, "//*[@id=\"tabFin\"]/tbody/tr[4]/td[15]").text).to eql(find(:css, '#Financeiro_total').value)
      end
    end
  end

  def formatar_paracela_valor_total
    begin
      $valor = self.input_resumo_total.value
      $valor = $valor.to_f / @tamanho_tabela_massa.to_f
      $valor = @utils.formata_valores_float_para_string($valor)
    rescue StandardError => ex
      puts 'Problemas ao converter valor da parcale de forma de pagamento - ' + ex.message
    end
  end

  def clicar_botao_editar_forma_pgto
    if has_btn_alterar_forma_pgto?
      btn_alterar_forma_pgto.click
    else
      within_frame('iframeAdmissao') do
        btn_alterar_forma_pgto.click
      end
    end
  end
end
