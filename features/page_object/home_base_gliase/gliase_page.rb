class SetUrl < SitePrism::Page
  #set_url "http://atendehomolog.dasa.com.br/atendimento/taskforce/"
  set_url $base_url['base_url']
end

class ElementoVisivel < SitePrism::Page
    def wait_element_visible(element)
        find(element, :visible => true, wait: 10)
    end
end

class ElementoVisivelWithinFrame < SitePrism::Page
def wait_element_visible(element, texto)
  within_frame('iframeAdmissao') do
  wait_until do
    page.find(element).text.should contain(texto)
  end
  end
end
end

class ElementoVisivelPage < SitePrism::Page
  def wait_element_visible(element)
    begin
      within_frame('iframeAdmissao', :wait => 1) do
        find(element, :visible => true, wait: 10)
      end
      rescue StandardError => ex
        find(element, :visible => true, wait: 10) if ex.message.include? "Unable to find"
    end
  end
end

class FuncionalidadeLogin < SitePrism::Page
  element :campo_usuario, '#username'
  element :campo_senha, '.loginSenha'
  element :btn_entra, '#linkEntrar'
  element :mensagem_erro_login, '#mensagemErro'
  element :selecionar_empresa_tela_unidade_tx, '#labelInformaEmpresa'
  element :link_esqueceu_a_senha, '.esqueceuSenha'
  element :campo_cpf, '#cpf'
  element :btn_resetar_senha, '.btResetarSenha'
  element :btn_informar_atendimento, :xpath, '//*[@id="divLocalAtendimento"]/span/input'
  

  def validar_informar_atendimento
    sleep 2    
    within_frame('iframeAdmissao', :wait => 1) do
    self.wait_until_btn_informar_atendimento_visible(3) if has_xpath?('//*[@id="divLocalAtendimento"]/span/input', :wait => 1) == true
      if has_css?("#divLocalAtendimento", :wait => 1) == true  
      self.btn_informar_atendimento.click
      end
    end
  end
end

class FuncionalidadeUnidade < SitePrism::Page
  element :selecionar_empresa_tela_unidade_dasa_sp, '.lista-10'
  element :selecionar_unidade_de_atendimento, '#resCpUnidade'
  element :btn_escolher_empresa, '#btEscolherEmpresa'
  element :menu_empresa_tela_home, '.empresa'
  element :btn_informar_atendimento, :xpath, '//*[@id="divLocalAtendimento"]/span/input'
  
  def validar_informar_atendimento
    sleep 2    
    within_frame('iframeAdmissao', :wait => 1) do
    self.wait_until_btn_informar_atendimento_visible(3) if has_xpath?('//*[@id="divLocalAtendimento"]/span/input', :wait => 1) == true
      if has_css?("#divLocalAtendimento", :wait => 1) == true  
      self.btn_informar_atendimento.click
      end
    end
  end
end
