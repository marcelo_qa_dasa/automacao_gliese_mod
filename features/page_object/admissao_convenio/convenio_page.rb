
  class FuncionalidadeAdmissaoConvenio <SitePrism::Page
    element :btn_convenio, '#lnkConvenio'
    element :campo_informar_convenio, '#Convenio_convenio'
    element :btn_selecionar_convenio, '#Convenio_valida'
    element :text_instrucao_convenio, '#Convenio_procConv'
    element :img_binoculo_tela_convenio, '#findConvenio_convenio'
    element :btn_limpar_campos_tela_convenio, '#Convenio_botLimpa'
    element :btn_pesquisa_convenio_por_nome, '#Convenio_botPesqNome'
    element :campo_pesquisar_convenio_por_nome, '#Convenio_pesqCv'
    element :img_binoculo_pesquisar_convenio_por_nome, '#findConvenio_pesqCv'
    element :campo_resultado_mnemonico_pesquisa_por_nome_convenio, :xpath, '//*[@id="resultadoCvConvenio_pesqCv"]/tr/td[1]/a'

    element :campo_passe_o_cartao, '#Convenio_leCartaoMagnetico'
    element :campo_codigo_associado, '#Convenio_matricAssociado'
    element :campo_data_de_solicitacao, '#Convenio_dataSolicitacao'
    element :campo_codigo_do_dependente, '#Convenio_dependente'
    element :selecao_especialidade_medica, '#Convenio_especMedico'
    element :campo_nome_do_plano, '#Convenio_plano'
    element :checked_plano_empresa_sim, '#Convenio_planoEmpresaSim'
    element :checked_plano_empresa_nao, '#Convenio_planoEmpresaNao'
    element :campo_quantidade_de_etiquetas, '#Convenio_quantEtiqueta'
    element :campo_data_do_ultimo_pagamento, '#Convenio_ultPagamento'
    element :campo_validade_do_cartao, '#Convenio_valCartao'
    element :selecao_produto, '#Convenio_Produto'
    element :campo_via_do_cartao, '#Convenio_viaCartao'
    element :campo_mnemonico_aba_convenio, :xpath, '//*[@id="resultadoCvConvenio_convenio"]/tr[1]/td[1]'
    element :campo_descricao_mnemonico_aba_convenio, :xpath, '//*[@id="resultadoCvConvenio_convenio"]/tr[1]/td[2]'

    element :btn_pesquisar_por_operadora, '#Convenio_botPesqOperadora'
    element :campo_pesquisa_por_operadora, '#operadoraConvenio'
    element :img_binoculo_pesquisar_por_operadora, '#operadoraConveniofind'
    element :img_binoculo_pesquisar_por_operadora_pesquisa_por_plano, '#planoConveniofind'
    element :resultato_nome_operadora, :xpath, '//*[@id="divOperadoraoperadoraConvenio"]/table/tbody/tr/td'
    element :campo_nome_do_pano_aba_pesquisa_por_operadora, '#planoConvenio'
    elements :resultado_nome_do_plano_pesquisa_por_operadora, :xpath, '//*[@id="ancoraplanoConvenio"]'
    element :validar_msg_cadastro_com_sucesso, :xpath, '//*[@id="divMsg"]/p'

    element :input_codigo_associado, '#Convenio_matricAssociado'
    element :input_data_de_solicitacao, '#Convenio_dataSolicitacao'
    element :select_nome_do_plano, '#Convenio_planoCv'
    element :select_plano_empresa_sim, '#Convenio_planoEmpresaSim'
    element :select_plano_empresa_nao, '#Convenio_planoEmpresaNao'
    element :input_quantidade_de_etiquetas, '#Convenio_quantEtiqueta'
    element :input_validade_do_pedido, '#Convenio_valPedido'
    element :input_confirmar_associado, '#Convenio_matricAssociadoConfirmacao'
    element :select_motivo_do_desconto_exames, '#Convenio_motDesc'
    element :input_observacao_motivo_do_desconto_exames, '#Convenio_obsDesc'

    def validar_campos_para_o_convenio_lm_amil_ami(valor)
      $valor = valor
      within_frame('iframeAdmissao') do
        if(has_css?('#Convenio_leCartaoMagnetico', :wait => 1) == $valor &&
          has_css?('#Convenio_matricAssociado', :wait => 1) == $valor &&
          has_css?('#Convenio_dataSolicitacao', :wait => 1) == $valor &&
          has_css?('#Convenio_planoCv', :wait => 1) == $valor &&
          has_css?('#Convenio_planoEmpresaSim', :wait => 1) == $valor &&
          has_css?('#Convenio_planoEmpresaNao', :wait => 1) == $valor &&
          has_css?('#Convenio_valPedido', :wait => 1) == $valor)
          true
        else
          false
        end
      end
    end

    def preencher_campos_para_o_convenio_lm_amil_ami(valor_02, data_01, select01, checked, data_03)
      within_frame('iframeAdmissao') do
        self.input_codigo_associado.send_keys(valor_02)
        self.input_data_de_solicitacao.send_keys(data_01)
        self.select_nome_do_plano.select(select01)
          if checked == "Sim"
            self.select_plano_empresa_sim.set(true)
          else
            self.select_plano_empresa_nao.set(true)
          end
        self.input_validade_do_pedido.send_keys(data_03)
      end
    end

    def preencher_campos_para_o_convenio_lm_amilfunc(valor_01, valor_02, select01, checked, data_01)
      within_frame('iframeAdmissao') do
      #page.execute_script "window.scrollBy(0,100)"
      self.campo_passe_o_cartao.send_keys(valor_01)
      self.input_codigo_associado.send_keys(valor_02)
      self.input_confirmar_associado.send_keys(valor_02)
      self.input_data_de_solicitacao.send_keys(data_01)
      self.select_nome_do_plano.select(select01)
      if checked == "Sim"
        self.select_plano_empresa_sim.set(true)
      else
        self.select_plano_empresa_nao.set(true)
      end
      self.input_validade_do_pedido.send_keys(data_01)
      end
    end


    def preencher_campos_para_o_convenio_br_amilimagem(valor_02, select01, checked, data_01)
      within_frame('iframeAdmissao') do
      #page.execute_script "window.scrollBy(0,100)"
      self.input_codigo_associado.send_keys(valor_02)
      self.input_confirmar_associado.send_keys(valor_02)
      self.input_data_de_solicitacao.send_keys(data_01)
      self.select_nome_do_plano.select(select01)
      if checked == "Sim"
      self.select_plano_empresa_sim.set(true)
      else
      self.select_plano_empresa_nao.set(true)
      end
      self.input_validade_do_pedido.send_keys(data_01) if has_css?('#Convenio_valPedido', :wait => 1) == true
      end
    end

      def validar_campos_para_o_convenio_amil(valor)
        $valor = valor
        within_frame('iframeAdmissao') do
        if(has_css?('#Convenio_matricAssociado', :wait => 1) == $valor &&
          has_css?('#Convenio_dataSolicitacao', :wait => 1) == $valor &&
          has_css?('#Convenio_planoCv', :wait => 1) == $valor &&
          has_css?('#Convenio_planoEmpresaSim', :wait => 1) == $valor &&
          has_css?('#Convenio_planoEmpresaNao', :wait => 1) == $valor &&
          has_css?('#Convenio_quantEtiqueta', :wait => 1) == $valor)
          true
        else
          false
        end
        end
      end

    def preencher_campos_para_o_convenio_al_partrj_50(valor_01, valor_02, checked)
      within_frame('iframeAdmissao') do
      #page.execute_script "window.scrollBy(0,100)"
      self.select_motivo_do_desconto_exames.select(valor_01)
      self.input_observacao_motivo_do_desconto_exames.set(valor_02)
      if checked == "Sim"
      self.select_plano_empresa_sim.set(true)
      else
      self.select_plano_empresa_nao.set(true)
      end
      end
    end

    def preencher_campos_para_o_convenio_amil(valor_02, data_01, select01, checked, valor_03)
      within_frame('iframeAdmissao') do
      self.input_codigo_associado.send_keys(valor_02)
      self.input_data_de_solicitacao.send_keys(data_01)
      self.select_nome_do_plano.select(select01)
      if checked == "Sim"
      self.select_plano_empresa_sim.set(true)
      else
      self.select_plano_empresa_nao.set(true)
      end
      self.input_quantidade_de_etiquetas.send_keys(valor_03)
      end
    end



    def validar_mensagem_convenio_recuperado_com_sucesso
      within_frame('iframeAdmissao') do
      self.validar_msg_cadastro_com_sucesso.text
      end
    end

    def resultado_lista_de_operadoras_para_uma_empresa
      $operadoras = Array.new
      page.execute_script "window.scrollBy(0,200)"
      within_frame('iframeAdmissao') do
      all(:xpath, "//*[@id=\"ancoraoperadoraConvenio\"]").each do |i|
      $operadoras.push(i.text)
      end
      $operadoras.join(", ")
    end
    end

    def clicar_no_resultado_nome_do_plano_pesquisa_por_operadora
      within_frame('iframeAdmissao') do
      self.resultado_nome_do_plano_pesquisa_por_operadora.first.click
      end
    end

    def validar_resultado_nome_do_plano_pesquisa_por_operadora
      within_frame('iframeAdmissao') do
      FuncionalidadeAdmissaoConvenio.new.resultado_nome_do_plano_pesquisa_por_operadora.first.text
      end
    end

    def clicar_no_binoculo_pesquisar_por_operadora_pesquisa_por_plano
      within_frame('iframeAdmissao') do
      self.img_binoculo_pesquisar_por_operadora_pesquisa_por_plano.click
      end
    end

    def preencher_campo_nome_do_pano_aba_pesquisa_por_operadora(valor)
      within_frame('iframeAdmissao') do
      self.campo_nome_do_pano_aba_pesquisa_por_operadora.send_keys(valor)
      end
    end

    def validar_exibicao_do_nome_da_operadora_no_campo
      within_frame('iframeAdmissao') do
      self.campo_pesquisa_por_operadora.value
      end
    end

    def clicar_no_resultato_nome_operadora(operadora)
        $operadora = operadora
        page.execute_script "window.scrollBy(0,200)"
        within_frame('iframeAdmissao') do
        all(:css, '#ancoraoperadoraConvenio').each do |nome_operadora|
        if nome_operadora.text == $operadora
            nome_operadora.click
            break
          end
        end
      end
      end

    def validar_resultato_nome_operadora
      within_frame('iframeAdmissao') do
      self.resultato_nome_operadora.text
      end
    end

    def clicar_no_binoculo_pesquisar_por_operadora
      within_frame('iframeAdmissao') do
      self.img_binoculo_pesquisar_por_operadora.click
      end
    end

    def preencher_campo_pesquisa_por_operadora(valor)
      within_frame('iframeAdmissao') do
      self.campo_pesquisa_por_operadora.send_keys(valor)
      end
    end

    def clicar_no_botao_pesquisar_por_operadora
      within_frame('iframeAdmissao') do
      self.btn_pesquisar_por_operadora.click
      end
    end

  def clicar_no_mnemonico_pesquisa_por_nome_convenio
      within_frame('iframeAdmissao') do
      self.campo_resultado_mnemonico_pesquisa_por_nome_convenio.click
      end
    end

  def validar_campo_resultado_mnemonico_pesquisa_por_nome_convenio
      within_frame('iframeAdmissao') do
      self.campo_resultado_mnemonico_pesquisa_por_nome_convenio.text
      end
    end

    def clicar_botao_binoculo_pesquisar_convenio_por_nome
      within_frame('iframeAdmissao') do
      self.img_binoculo_pesquisar_convenio_por_nome.click
      end
    end

    def preencher_campo_pesquisar_convenio_por_nome(valor)
      within_frame('iframeAdmissao') do
      self.campo_pesquisar_convenio_por_nome.set(valor)
      end
    end

    def clicar_botao_pesquisa_convenio_por_nome
      within_frame('iframeAdmissao') do
      self.btn_pesquisa_convenio_por_nome.click
      end
    end

    def clicar_botao_limpar_tela_convenio
      within_frame('iframeAdmissao') do
      self.btn_limpar_campos_tela_convenio.click
      end
    end

    def validar_campo_descricao_mnemonico_aba_convenio
      within_frame('iframeAdmissao') do
      self.campo_descricao_mnemonico_aba_convenio.text.size == 0 ? false : true
      end
    end

    def validar_campo_mnemonico_aba_convenio
      within_frame('iframeAdmissao') do
      self.campo_mnemonico_aba_convenio.text.size == 0 ? false : true
      end
    end

    def clicar_no_mnemonico_da_aba_convenio(mnemonico)
      $mnemonico = mnemonico
      page.execute_script "window.scrollBy(0,200)"
        within_frame('iframeAdmissao') do
        (1..1000).each do |i|
        if find(:xpath, "//*[@id=\"resultadoCvConvenio_convenio\"]/tr[#{i}]/td[1]").text == $mnemonico
          find(:xpath, "//*[@id=\"resultadoCvConvenio_convenio\"]/tr[#{i}]/td[1]").click
          break
          end
        end
      end
    end

    def clicar_no_binoculo_da_aba_convenio
      within_frame('iframeAdmissao') do
        self.img_binoculo_tela_convenio.click
      end
    end

    def validar_nao_exibicao_de_plano_pesquisa_convenio_por_operadora
      within_frame('iframeAdmissao') do
      has_css?('#ancoraplanoConvenio') == false
    end
  end

    def validar_nao_exibicao_de_operadoras_invalidas
      within_frame('iframeAdmissao') do
      has_css?('#ancoraoperadoraConvenio') == false
    end
  end

    def validar_nao_exibicao_de_convenios_invalidos
      within_frame('iframeAdmissao') do
      has_css?('#resultadoCvConvenio_convenio') == false
    end
  end

    def validar_campos_para_o_convenio_unimed_seguros(valor)
      $valor = valor
      within_frame('iframeAdmissao') do
      if(has_css?('#Convenio_matricAssociado', :wait => 1) == $valor &&
        has_css?('#Convenio_dataSolicitacao', :wait => 1) == $valor &&
        has_css?('#Convenio_planoEmpresaSim', :wait => 1) == $valor &&
        has_css?('#Convenio_planoEmpresaNao', :wait => 1) == $valor &&
        has_css?('#Convenio_viaCartao', :wait => 1) == $valor)
        true
      else
        false
    end
    end
  end

    def preencher_campos_para_o_convenio_unimed_seguros(valor_02, data_01, checked, valor_03)
      within_frame('iframeAdmissao') do
      page.execute_script "window.scrollBy(0,100)"
      self.campo_codigo_associado.send_keys(valor_02)
      self.campo_data_de_solicitacao.send_keys(data_01)
      if checked == "Sim"
      self.checked_plano_empresa_sim.set(true)
      else
      self.checked_plano_empresa_nao.set(true)
      end
      self.campo_via_do_cartao.send_keys(valor_03)
      end
    end

    def preencher_campos_para_o_convenio_sul_america(valor_02, data_01, valor_05, checked, valor_06, valor_08, select_01)
      within_frame('iframeAdmissao') do
      page.execute_script "window.scrollBy(0,100)"
      self.campo_codigo_associado.send_keys(valor_02)
      self.campo_data_de_solicitacao.send_keys(data_01)
      self.campo_nome_do_plano.send_keys(valor_05)
      if checked == "Sim"
      self.checked_plano_empresa_sim.set(true)
      else
      self.checked_plano_empresa_nao.set(true)
      end
      self.campo_quantidade_de_etiquetas.send_keys(valor_06)
      self.campo_validade_do_cartao.send_keys(valor_08)
      self.selecao_produto.select(select_01)
      end
    end

    def preencher_campo_data_nascimento_tela_paciente(valor)
      within_frame('iframeAdmissao') do
        admissao_online_pacienete = FuncionalidadeAdmissao.new
        sleep 1
        admissao_online_pacienete.campo_novo_data_nascimento_tela_paciente.send_keys(valor)
      end
    end

    def preencher_campo_cip_paciente(valor)
      within_frame('iframeAdmissao') do
        FuncionalidadeAdmissao.new.campo_cip.send_keys(valor)
      end
    end

    def preencher_campos_para_o_convenio_golden_cross(valor_01, valor_02, data_01, valor_04, select_01, valor_05, checked, valor_06, valor_07, valor_08)
      within_frame('iframeAdmissao') do
      page.execute_script "window.scrollBy(0,100)"
      self.campo_passe_o_cartao.send_keys(valor_01)
      self.campo_codigo_associado.send_keys(valor_02)
      self.campo_data_de_solicitacao.send_keys(data_01)
      self.campo_codigo_do_dependente.send_keys(valor_04)
      self.selecao_especialidade_medica.select(select_01)
      self.campo_nome_do_plano.send_keys(valor_05)
      if checked == "Sim"
      self.checked_plano_empresa_sim.set(true)
      else
      self.checked_plano_empresa_nao.set(true)
      end
      self.campo_quantidade_de_etiquetas.send_keys(valor_06)
      self.campo_data_do_ultimo_pagamento.send_keys(valor_07)
      self.campo_validade_do_cartao.send_keys(valor_08)
      end
    end

    def validar_texto_instrucoes_do_convenio
      within_frame('iframeAdmissao') do
        self.text_instrucao_convenio.value
      end
    end

    def clicar_no_botao_selecionar_convenio
      within_frame('iframeAdmissao') do
      self.btn_selecionar_convenio.click
      end
    end

    def preencher_o_campo_convenio_aba_convenio(valor_convenio)
      within_frame('iframeAdmissao') do
      sleep 1
      self.campo_informar_convenio.set(valor_convenio)
      end
    end

    def validar_texto_do_campo_convenio_aba_convenio
      within_frame('iframeAdmissao') do
      self.campo_informar_convenio.value
      end
    end

    def clicar_no_botao_convenio
      within_frame('iframeAdmissao') do
      self.btn_convenio.click
      end
    end

    def selecionar_empresa(empresa)
      $id_empresa = empresa
      $d = 220
      $i = 0
      $num = 10
      while $i < $num  do
        if has_css?(".lista-#{$id_empresa}", :wait => 1) == true
        find(:css, ".lista-#{$id_empresa}").click
        $num = 0
      else
        execute_script("window.document.getElementsByClassName('overview')[0].setAttribute(\"style\", \"top: -#{$d}px;\");")
        $d +=220
      end
        $i +=1
        end
    end

    def preencher_campos_para_qualquer_convenio(tabela)
      tabela.hashes.each do |hash|
        # Carregando valores da tabela
        @convenio = hash['Convenio']
        @data_solicitacao = hash['Data_Solicitacao']
        @quantidade_etiquetas = hash['Quantidade_Etiquetas']
        @codigo_associado = hash['Codigo_Associado']
        @ultima_refeicao = hash['Última_Refeição']
        @nome_plano = hash['Nome_Plano']
        @plano_Empresa = hash['Plano_Empresa']
        @validade_cartao = hash['Validade_cartao']
        @validade_pedido = hash['Validade_pedido']
        $nome_convenio = @convenio
        begin
          within_frame('iframeAdmissao', :wait => 1) do
            self.preencher_campos_para_qualquer_convenio_auxiliar
          end
        rescue StandardError => ex
          self.preencher_campos_para_qualquer_convenio_auxiliar if ex.message.include? "Unable to find"
        end
      end
    end

    def preencher_campos_para_qualquer_convenio_auxiliar
      campo_informar_convenio.set(@convenio)
      btn_selecionar_convenio.click
      sleep 1
      input_codigo_associado.set(@codigo_associado) if has_input_codigo_associado?
      input_data_de_solicitacao.set(@data_solicitacao) if has_input_data_de_solicitacao?
      select_nome_do_plano.select(@nome_plano) if has_select_nome_do_plano?
      if @plano_Empresa == "Sim"
        select_plano_empresa_sim.set(true) if has_select_plano_empresa_sim?
      else
        select_plano_empresa_nao.set(true) if has_select_plano_empresa_nao?
      end
      campo_validade_do_cartao.set(@validade_cartao) if has_campo_validade_do_cartao?
      input_validade_do_pedido.set(@validade_pedido) if has_input_validade_do_pedido?
      input_quantidade_de_etiquetas.set(@quantidade_etiquetas) if has_input_quantidade_de_etiquetas?
    end
  end
