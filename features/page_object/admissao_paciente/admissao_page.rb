
  class FuncionalidadeAdmissao <SitePrism::Page
    element :link_admissao_on_line, '#mnuAtdAdm'
    element :btn_cadastrar_novo_paciente, '#Paciente_botCadastrar'
    element :btn_novo_criar_um_cip ,'#Paciente_botNovo'
    element :campo_novo_nome_paciente, '#idTxtNome'
    element :campo_novo_cep_paciente, '#idCep'
    element :campo_novo_numero_paciente, '#idNumero'
    element :campo_novo_bairro_paciente, '#idBairro'
    element :selecionar_novo_uf_paciente, '#cmbUf'
    element :campo_novo_rg_paciente, '#idRegistro'
    element :campo_novo_data_nascimento_paciente, '#idTxtDataNascimento'
    element :selecionar_novo_sexo_paciente, '#cmbSexo'
    element :campo_novo_ddd_paciente, '#idDdd'
    element :btn_add_contato_telefonico, '#Paciente_botIncluiTel'
    element :campo_novo_telefone_paciente, '#txt_Paciente_telefones'
    element :campo_novo_lagradouro_paciente, '#idLogradouro'
    element :campo_novo_municipio_paciente, '#idMunicipio'
    element :campo_novo_cpf_paciente, '#idCpf'
    element :campo_novo_orgao_emissor_paciente, '#idOrgaoEmissor'
    element :campo_novo_email_paciente, '#idEmail'
    element :campo_novo_data_nascimento_tela_paciente, '#Paciente_txtDtNasc'
    element :campo_cip, '#Paciente_txtCip'
    element :campo_consulta_por_nome_data_nasc, '#ConsultaNome_txtDtNasc'
    element :campo_consulta_por_nome, '#lovPacConNome'
    element :btn_pesquisar_por_nome, '#Paciente_botPesqNome'
    element :btn_pesquisar_nome_paciente, '#idInutilizadoVis'
    element :retorno_consulta_por_nome_retorno_cip, :xpath, '//*[@id="resNomePaciente"]/tr/td[1]'
    element :retorno_consulta_por_nome_retorno_nome, :xpath, '//*[@id="resNomePaciente"]/tr/td[2]'
    element :retorno_consulta_por_nome_retorno_data, :xpath, '//*[@id="resNomePaciente"]/tr/td[3]'
    element :btn_pesquisa_por_rg, '#Paciente_botPesqRG'
    element :campo_consulta_por_rg, '#txtRgPac'
    element :btn_consultar_por_rg, '#psqtxtRgPac'
    element :retorno_consulta_por_rg_retorno_cip, :xpath, '//*[@id="resultadoConsultaPactxtRgPac"]/tr/td[1]'
    element :retorno_consulta_por_rg_retorno_nome, :xpath, '//*[@id="resultadoConsultaPactxtRgPac"]/tr/td[2]'
    element :retorno_consulta_por_rg_retorno_data_nascimento, :xpath, '//*[@id="resultadoConsultaPactxtRgPac"]/tr/td[3]'
    element :btn_pesquisa_por_cpf, '#Paciente_botPesqCpf'
    element :campo_consulta_paciente_por_cpf, '#lovPacCpf'
    element :btn_consultar_cpf, '#idPadrao'
    elements :retorno_consulta_por_cpf_retorno_cip, :xpath, '//*[@id="resNomePacientelovPacCpf"]/tr/td[1]'
    elements :retorno_consulta_por_cpf_retorno_nome, :xpath, '//*[@id="resNomePacientelovPacCpf"]/tr/td[2]'
    element :retorno_consulta_por_cpf_retorno_data_nascimento, :xpath, '//*[@id="resNomePacientelovPacCpf"]/tr/td[3]'
    element :campo_cadastro_paceinte_cip, '#idTxtCip'
    element :btn_remover_telefone, :xpath, '//*[@id="lin3"]/td[2]/input'
    element :btn_Limpar, '#Paciente_botNovo'


  def link_admissao
    page.all(:css, '#linkAdmissao')[1] || page.all(:css, '#linkAdmissao')[0]
  end

  def cadastrar_novo_clientes_com_sucesso
    self.btn_novo_criar_um_cip.click
    $nome = Faker::Name.name_with_middle
    $nome = $nome.capitalize
    self.campo_novo_nome_paciente.send_keys($nome)
    self.campo_novo_cep_paciente.send_keys("40225290")
    self.campo_novo_numero_paciente.send_keys(Faker::Address.building_number)
    self.campo_novo_bairro_paciente.send_keys(Faker::Address.country)
    self.selecionar_novo_uf_paciente.select("SP")
    self.campo_novo_rg_paciente.send_keys(Faker::Number.number(9))
    self.campo_novo_data_nascimento_paciente.send_keys('29091991')
    self.selecionar_novo_sexo_paciente.select(["M", "F"].sample)
    self.campo_novo_ddd_paciente.send_keys(Faker::PhoneNumber.subscriber_number(2))
    self.campo_novo_telefone_paciente.send_keys(Faker::Number.number(8))
    self.btn_add_contato_telefonico.click
    self.campo_novo_lagradouro_paciente.send_keys(Faker::Address.street_name)
    self.campo_novo_municipio_paciente.send_keys(Faker::Address.city)
    self.campo_novo_cpf_paciente.send_keys(BRDocuments::CPF.generate)
    self.campo_novo_orgao_emissor_paciente.send_keys("SSP")
    self.campo_novo_email_paciente.send_keys(Faker::Internet.email)
  end

  def editar_cadastro_novo_clientes_com_sucesso
    $nome = $massa_editar['nome'].capitalize
    self.campo_novo_cep_paciente.set("")
    self.campo_novo_cep_paciente.send_keys(Faker::Number.number(8))
    self.campo_novo_numero_paciente.set("")
    self.campo_novo_numero_paciente.send_keys(Faker::Address.building_number)
    self.campo_novo_bairro_paciente.set("")
    self.campo_novo_bairro_paciente.send_keys(Faker::Address.country)
    self.selecionar_novo_uf_paciente.select("SP")
    self.selecionar_novo_sexo_paciente.select(["M", "F"].sample)
    self.campo_novo_ddd_paciente.set("")
    self.campo_novo_ddd_paciente.send_keys(Faker::PhoneNumber.subscriber_number(2))
    self.btn_remover_telefone.click
    self.campo_novo_telefone_paciente.send_keys(Faker::Number.number(8))
    self.btn_add_contato_telefonico.click
    self.campo_novo_telefone_paciente.send_keys(Faker::Number.number(8))
    self.btn_add_contato_telefonico.click
    self.campo_novo_lagradouro_paciente.set("")
    self.campo_novo_lagradouro_paciente.send_keys(Faker::Address.street_name)
    self.campo_novo_municipio_paciente.set("")
    self.campo_novo_municipio_paciente.send_keys(Faker::Address.city)
    self.campo_novo_cpf_paciente.set("")
    self.campo_novo_cpf_paciente.send_keys(BRDocuments::CPF.generate)
    self.campo_novo_orgao_emissor_paciente.set("")
    self.campo_novo_orgao_emissor_paciente.send_keys("SSP")
    self.campo_novo_email_paciente.set("")
    self.campo_novo_email_paciente.send_keys(Faker::Internet.email)
  end

  def clicar_para_exibir_consulta_por_cip
    within_frame('iframeAdmissao') do
    find(:css, '#Paciente_txtResponsavel').click
    end
  end

  def clicar_botao_limpar_dados
    within_frame('iframeAdmissao') do
    self.btn_Limpar.click
    end
  end

  def botao_cadastrar_novo_paciente
    within_frame('iframeAdmissao') do
    ElementoVisivel.new.wait_element_visible('#Paciente_botCadastrar')
    self.btn_cadastrar_novo_paciente.click
    end
  end

  def retorno_campo_paciente_cpf
    within_frame('iframeAdmissao') do
    find(:css, '#Paciente_cpf').value
    end
  end

    def validar_texto_campo_nome
    within_frame('iframeAdmissao') do
    return find(:css, '#Paciente_txtNome').value
    end
  end

  def preencher_campo_data_nascimento_tela_paciente
    within_frame('iframeAdmissao') do
    self.campo_novo_data_nascimento_tela_paciente.send_keys($massa['Data_nasc'])
    end
  end

  def preencher_campo_data_nascimento_tela_paciente_com(data)
    if has_campo_novo_data_nascimento_tela_paciente?
      campo_novo_data_nascimento_tela_paciente.set(data)
    else
      within_frame('iframeAdmissao') do
        campo_novo_data_nascimento_tela_paciente.set(data)
      end
    end
  end

  def preencher_campo_cip_paciente_com(cip)
    if has_campo_cip?
      campo_cip.set(cip)
    else
      within_frame('iframeAdmissao') do
        campo_cip.send_keys(cip)
      end
    end
  end

  def retorno_campo_data_nascimento_tela_paciente
    within_frame('iframeAdmissao') do
    return self.campo_novo_data_nascimento_tela_paciente.value
    end
  end

  def preencher_campo_cip_paciente
    within_frame('iframeAdmissao') do
    self.campo_cip.send_keys($massa['CIP'])
    end
  end

  def retorno_campo_cip_paciente
    within_frame('iframeAdmissao') do
    return self.campo_cip.value
  end
  end

  def clicar_botao_pesquisa_por_nome
    within_frame('iframeAdmissao') do
    self.btn_pesquisar_por_nome.click
    end
  end

  def consultar_paciente_por_nome
    within_frame('iframeAdmissao') do
    self.campo_consulta_por_nome_data_nasc.send_keys($massa['Data_nasc'])
    self.campo_consulta_por_nome.send_keys($massa['nome'])
    self.btn_pesquisar_nome_paciente.click
    end
  end

  def retorno_pesquisa_por_nome_campo_cip(valor_cip)
    within_frame('iframeAdmissao') do
      all(:xpath, '//*[@id="resNomePaciente"]/tr/td[1]').each do |nome_cip|
      $nome_cip = nome_cip.text
      if $nome_cip == valor_cip
        return true
        break
      end
      end
      ApplicationError.new.msg_excecao_cip_invalido(valor_cip)
    end
  end

  def retorno_pesquisa_por_nome_campo_nome(valor_nome)
    within_frame('iframeAdmissao') do
      all(:xpath, '//*[@id="resNomePaciente"]/tr/td[2]').each do |nome_nome|
      $nome_nome = nome_nome.text
      if $nome_nome == valor_nome
        return true
        break
      end
      end
      ApplicationError.new.msg_excecao_nome_invalido(valor_nome)
    end
  end

  def retorno_pesquisa_por_nome_campo_data_nasc(valor_data)
    within_frame('iframeAdmissao') do
      all(:xpath, '//*[@id="resNomePaciente"]/tr/td[3]').each do |nome_data|
      $nome_data = Utils.new.expressao_remover_caractere_que_nao_seja_uma_palavra(nome_data.text)
      if $nome_data == valor_data
        return true
        break
      end
      end
      ApplicationError.new.msg_excecao_data_invalido(valor_data)
    end
  end

  def clicar_botao_pesquisar_por_rg
    within_frame('iframeAdmissao') do
    self.btn_pesquisa_por_rg.click
    end
  end

  def preencher_campo_consulta_por_rg_campo_rg(valor_rg)
    within_frame('iframeAdmissao') do
    self.campo_consulta_por_rg.send_keys(valor_rg)
    end
  end

  def clicar_botao_para_consultar_rg
    within_frame('iframeAdmissao') do
    self.btn_consultar_por_rg.click
    end
  end

  def retorno_pesquisa_por_rg_campo_cip(valor_cip)
    within_frame('iframeAdmissao') do
      all(:xpath, '//*[@id="resultadoConsultaPactxtRgPac"]/tr/td[1]').each do |nome_cip|
      @nome_cip = nome_cip.text
      if @nome_cip == valor_cip
        return true
        break
      end
      end
      ApplicationError.new.msg_excecao_cip_invalido(valor_cip)
    end
  end

  def retorno_pesquisa_por_rg_campo_nome(valor_nome)
    within_frame('iframeAdmissao') do
      all(:xpath, '//*[@id="resultadoConsultaPactxtRgPac"]/tr/td[2]').each do |nome_nome|
      $nome_nome = nome_nome.text
      if $nome_nome == valor_nome
        return true
        break
      end
      end
      ApplicationError.new.msg_excecao_nome_invalido(valor_nome)
    end
  end

  def retorno_pesquisa_por_rg_campo_data_nascimento(valor_data)
    within_frame('iframeAdmissao') do
      all(:xpath, '//*[@id="resultadoConsultaPactxtRgPac"]/tr/td[3]').each do |nome_data|
      $nome_data = Utils.new.expressao_remover_caractere_que_nao_seja_uma_palavra(nome_data.text)
      if $nome_data == valor_data
        return true
        break
      end
      end
      ApplicationError.new.msg_excecao_data_invalido(valor_data)
    end
  end

  def clicar_botao_pesquisar_por_cpf
    within_frame('iframeAdmissao') do
    self.btn_pesquisa_por_cpf.click
    end
  end

  def preencher_campo_cpf(massa_cpf)
    within_frame('iframeAdmissao') do
    self.campo_consulta_paciente_por_cpf.send_keys(massa_cpf)
    end
  end

  def clicar_botao_consultar_cpf
    within_frame('iframeAdmissao') do
    self.btn_consultar_cpf.click
    end
  end

  def retorno_pesquisa_por_cpf_campo_cip(valor_cip)
    within_frame('iframeAdmissao') do
    all(:xpath, '//*[@id="resNomePacientelovPacCpf"]/tr/td[1]').each do |nome_cip|
      $nome_cip = nome_cip.text
      if $nome_cip == valor_cip
        return true
        break
      end
    end
    ApplicationError.new.msg_excecao_cip_invalido(valor_cip)
    end
  end

  def retorno_pesquisa_por_cpf_campo_nome(valor_nome)
    within_frame('iframeAdmissao') do
      all(:xpath, '//*[@id="resNomePacientelovPacCpf"]/tr/td[2]').each do |nome_nome|
        $nome_nome = nome_nome.text
        if $nome_nome == valor_nome
        return true
        break
        end
      end
      ApplicationError.new.msg_excecao_nome_invalido(valor_nome)
      end
  end

  def retorno_pesquisa_por_cpf_campo_data_nascimento(valor_data)
    within_frame('iframeAdmissao') do
      all(:xpath, '//*[@id="resNomePacientelovPacCpf"]/tr/td[3]').each do |nome_data|
        $nome_data = Utils.new.expressao_remover_caractere_que_nao_seja_uma_palavra(nome_data.text)
        if $nome_data == valor_data
        return true
        break
        end
      end
      ApplicationError.new.msg_excecao_data_invalido(valor_data)
      end
  end

  def validar_dados_paciente_aba_exames
    if ( find(:css, '#Paciente_txtNome').value != "" && find(:css, '#Paciente_txtCip').value != "")
      return true
    else
      return false  
    end  
  end

end




