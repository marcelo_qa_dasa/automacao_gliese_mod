   class FuncionalidadeAdmissaoMultiplosMedico <SitePrism::Page

    element :btn_aba_multiplos_medico, '#lnkMultiMed'
    element :btn_limpar_aba_multiplos_medicos, '#Paciente_botNovo'
    element :input_numero_registro, '#mmtxtCrm'
    element :input_nome_medico, '#mmnomeMedico'
    element :btn_binocolo_nome_medico_aba_multiplos_medicos, '#idPesquisammnomeMedico'
    element :combo_uf_aba_multiplos_medicos, '#mmcmbUf'
    element :btn_binocolo_crm_medico_aba_multiplos_medicos, '#idPesquisammtxtCrm'
    element :btn_adicionar_aba_multiplos_medicos, :xpath, '//*[@id="divMultiMed"]/table[1]/tbody/tr/td/div/ul/li[2]/a'
    element :div_resultado_adicionar_medicos, '#tbResultadoCRMmmtxtCrm'
    element :input_nome_medico_div_adicinar_multiplos_medicos, :xpath, '//*[@id="resultadoConsultaCRMmmtxtCrm"]/tr/td[2]'
    element :btn_fechar_popup_adicionar_medicos, '#idClosemmtxtCrm'
    element :lista_crm_medico_pesquisa_por_nome, :xpath, '//*[@id="resultadoConsultaCRMmmtxtCrm"]/tr[1]/td[1]'                                                    
    element :resultado_consulta_crm, :xpath, '//*[@id="resultadoConsultaCRMmmtxtCrm"]/tr[1]/td[1]'
    element :resultado_consulta_medico, :xpath, '//*[@id="resultadoConsultaCRMmmtxtCrm"]/tr[1]/td[2]'
    element :resultado_situacao_aba_multiplos_medicos, :xpath, '//*[@id="resultadoConsultaCRMmmtxtCrm"]/tr[1]/td[3]'
    element :medico_ja_cadastrado, :xpath, '//*[@id="mmMedTab"]/tr[1]/td'



     def validar_valores_resultado_medico(valor)
        $valor = valor
        within_frame('iframeAdmissao') do
          if(has_xpath?('//*[@id="resultadommnomeMedico"]/tr/td[1]', :wait => 1) == $valor &&
             has_xpath?('//*[@id="resultadommnomeMedico"]/tr/td[2]', :wait => 1) == $valor &&
             has_xpath?('//*[@id="resultadommnomeMedico"]/tr/td[3]', :wait => 1) == $valor )
             true
           else
             false
          end
        end
     end


    def clicar_binocolo_nome_medico_aba_multiplos_medicos
        within_frame('iframeAdmissao') do
            self.btn_binocolo_nome_medico_aba_multiplos_medicos.click
         end
    end

    def inserir_nome_medico(valor)
        within_frame('iframeAdmissao') do
           self.input_nome_medico.set(valor)
        end
    end    

    def validar_numero_registro_medico
      within_frame('iframeAdmissao') do
        find(:xpath, '//*[@id="resultadoConsultaCRMmmtxtCrm"]/tr[1]/td[1]').text.size == 0 ? false : true
      end
    end 

    def validar_crm_existente_medico
      within_frame('iframeAdmissao') do
        find(:xpath, '//*[@id="mmtxtCrm"]').value.size == 0 ? false : true
      end
    end    

    def validar_medico_existente_aba_multiplos_medicos
        within_frame('iframeAdmissao') do
         find(:xpath, '//*[@id="mmnomeMedico"]').value.size == 0 ? false : true
        end
    end

    def validar_situacao_existente_aba_multiplos_medicos
        within_frame('iframeAdmissao') do
           self.resultado_situacao_aba_multiplos_medicos.text.size == 0 ? false : true
        end
    end

    def validar_medico_ja_cadastrado
        within_frame('iframeAdmissao') do
         self.medico_ja_cadastrado.text.size == 0 ? false : true
        end
    end    

    def clicar_crm_lista_medicos_multiplos_medicos
        within_frame('iframeAdmissao') do
        find(:xpath, '//*[@id="resultadommnomeMedico"]/tr/td[1]').click  
        end 
    end

  def validar_existencia_grid_todos_medicos
    within_frame('iframeAdmissao') do
        ElementoVisivel.new.wait_element_visible("#tbResultadoCRMmmtxtCrm")
    end
  end  

  def clicar_binocolo_nome_aba_multiplos_medicos
    within_frame('iframeAdmissao', :wait => 1) do
        self.btn_binocolo_crm_medico_aba_multiplos_medicos.click
      end
  end

  def clicar_botao_limpar_aba_multiplos_medicos
    within_frame('iframeAdmissao', :wait => 1) do
        self.btn_limpar_aba_multiplos_medicos.click
      end
  end  

  def seleciona_exames_para_especifico_medico(table)
    @d = 2
    table.hashes.each do |hash|
        @exame = hash['exame']
        @medico1 = hash['medico1']
        @medico2 = hash['medico2']
    begin
        within_frame('iframeAdmissao', :wait => 1) do
          self.seleciona_exames_para_especifico_medico_auxiliar
        end
      rescue StandardError => ex
          self.seleciona_exames_para_especifico_medico_auxiliar if ex.message.include? "Unable to find"
      end
      @d +=1 
    end    
  end  

  def seleciona_exames_para_especifico_medico_auxiliar
    expect(find(:xpath, "//*[@id=\"mmMedTab\"]/tr[#{@d}]/td[1]").text).to eq @exame
    @medico1 == "true" ? find(:xpath, "//*[@id=\"mmMedTab\"]/tr[#{@d}]/td[2]/input").set(true) : false
    @medico2 == "true" ? find(:xpath, "//*[@id=\"mmMedTab\"]/tr[#{@d}]/td[3]/input").set(true) : false
  end

   def clicar_botao_fechar_popup_adicionar_miltplos_medicos
    within_frame('iframeAdmissao') do
        self.btn_fechar_popup_adicionar_medicos.click
    end
   end 

   def clicar_botao_adicionar_aba_multiplos_medicos
    within_frame('iframeAdmissao') do
        self.btn_adicionar_aba_multiplos_medicos.click
    end
   end 
   
   def clicar_aba_multiplos_medicos
    within_frame('iframeAdmissao') do
        self.btn_aba_multiplos_medico.click
    end
   end 

   def inserir_numero_registro_aba_multiplos_medicos(numero)
    within_frame('iframeAdmissao') do
        self.input_numero_registro.set(numero)
    end
   end 

   def validar_exibicao_uf
    within_frame('iframeAdmissao') do
    page.find('#mmcmbUf').all('option').find(&:selected?).text
    end
   end

   def clico_no_binocolo_pesquisa_crm_medico
    within_frame('iframeAdmissao') do
        self.btn_binocolo_crm_medico_aba_multiplos_medicos.click
    end
   end 

   def validar_descricao_campo_nome_medido(valor)
    within_frame('iframeAdmissao') do
      @mensagem = self.input_nome_medico.value
      if @mensagem == valor
        return true
      else
      return false
      end
   end
   end

  def validar_medico_adicionado_sessao_exames(valor)
    within_frame('iframeAdmissao') do
        @mensagem = find(:xpath, '//*[@id="resultadoConsultaCRMmmtxtCrm"]/tr/td[2]').text
        if @mensagem == valor
          return true
        else
        return false
        end
     end
  end 
  


   end  