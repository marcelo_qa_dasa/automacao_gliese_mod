class ApplicationError < StandardError
  
  def msg_excecao_listagem_medico(codigo_medico)
    if $crm != codigo_medico
      raise ArgumentError.new($exception['msg_erro_medico_nao_disponivel_lista'])
    end
  end

  def msg_excecao_cip_invalido(valor_cip)
    if $nome_cip != valor_cip
      raise ArgumentError.new($exception['msg_erro_cip_invalido'])
    end
  end

  def msg_excecao_nome_invalido(valor_nome)
    if $nome_nome != valor_nome
      raise ArgumentError.new($exception['msg_erro_nome_invalido'])
    end
  end

  def msg_excecao_data_invalido(valor_data)
    if $nome_data != valor_data
      raise ArgumentError.new($exception['msg_erro_data_invalida'])
    end
  end

  def msg_excecao_hora_existente
      raise ArgumentError.new($exception['msg_error_hora_existente'])
  end

  def msg_excecao_posto_nao_existente
    raise ArgumentError.new($exception['msg_error_posto_nao_existente'])
  end
    
  def msg_excecao_numero_telefone_medico_inexistente
    raise ArgumentError.new($exception['msg_error_numero_telefone_medico_inexistente'])
  end

  def msg_excecao_cliente_prefecencial_inexistente
    raise ArgumentError.new($exception['msg_error_cliente_prefecencial_inexistente'])
  end

  def msg_excecao_relacao_de_exames_admitidos_inexistente
    raise ArgumentError.new($exception['msg_error_relacao_de_exames_admitidos_inexistente'])
  end
  

  def  msg_excecao_exame_incorreto
    raise ArgumentError.new($exception['msg_error_exame_incorreto'])
  end
  
  def  msg_excecao_daylab_nao_checados
    raise ArgumentError.new($exception['msg_error_daylab_nao_checados'])
  end
  
  def msg_numero_nova_visita_existente
    raise Exception.new($exception['msg_error_numero_nova_visita_existente'])
  end

  def msg_medico_nao_encontrado
    raise Exception.new($exception['msg_erro_medico_nao_encontrado'])
  end
  
end