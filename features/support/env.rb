  require 'rspec'
  require 'capybara-screenshot'
  require 'capybara-screenshot/cucumber'
  require 'capybara/cucumber'
  require 'site_prism'
  require 'capybara'
  require 'selenium-webdriver'
  require 'yaml'
  require 'capybara/rspec'
  require 'pry'
  require 'phantomjs'
  require 'capybara/poltergeist'
  require 'faker'
  require 'br_documents'
  require 'special_char_remover'
  require 'titleize'
  require 'rake'
  require 'capybara/dsl'
  require 'httparty'
  require 'json'
  require 'base64'
  require 'rest-client'
  require "oci8"
  require 'rubygems'
  require 'pdf-reader'
  require 'date'
  require 'open-uri'


  if ENV['pdf_disabled'] == "false"
    @pdf_disabled = false
  else
    @pdf_disabled = true
  end

 # Rodar no browser Chrome
 Capybara.register_driver :selenium_chrome do |app|
   ENV['HTTP_PROXY'] = ENV['http_proxy'] = nil
   prefs = {
     download: {
       prompt_for_download: false,
       directory_upgrade: true,
       default_directory: File.expand_path('./PDF')
     },
     plugins: {
       always_open_pdf_externally: true,
       plugins_disabled: ["Chrome PDF Viewer", "Adobe Flash Player"]
     }
   }
   capabilities = Selenium::WebDriver::Remote::Capabilities.chrome('chromeOptions' => { 'prefs' => prefs, "args" => [ "--start-maximized",
   "--disable-infobars",
   "--disable-notifications",
   "--disable-password-generation --disable-password-manager-reauthentication",
   "--disable-password-separated-signin-flow",
   "--disable-popup-blocking",
   "--disable-translate",
   "--disable-save-password-bubble",
   "--ignore-certificate-errors",
   "--print-to-pdf" ] })
   Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities)
 end

 # Rodar no broser firefox
 Capybara.register_driver :selenium_firefox do |app|
  ENV['HTTP_PROXY'] = ENV['http_proxy'] = nil
  Capybara::Selenium::Driver.new(app, browser: :firefox,
  driver_path: Gem.bin_path("geckodriver-helper", "geckodriver")
  )
end

 # Rodar em headless chomedriver
 Capybara.register_driver :headless_chrome do |app|
   ENV['HTTP_PROXY'] = ENV['http_proxy'] = nil
   capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
     chromeOptions: { args: %w[headless no-sandbox disable-gpu disable-translate ignore-certificate-errors
     disable-popup-blocking
     start-maximized print-to-pdf] })
   Capybara::Selenium::Driver.new app,
     browser: :chrome,
     desired_capabilities: capabilities
 end

   #Rodar com o browser ativo em headless ou browser
   if "headless" == ENV['driver']
     Capybara.default_driver = :headless_chrome
     Capybara.javascript_driver = :headless_chrome
   else
     Capybara.default_driver = :selenium_chrome
     Capybara.javascript_driver = :selenium_chrome
   end

   # Limpar cookies e cache session browser
   browser = Capybara.current_session.driver.browser
   if browser.respond_to?(:clear_cookies)
     # Rack::MockSession
     browser.clear_cookies
     puts true
   elsif browser.respond_to?(:manage) and browser.manage.respond_to?(:delete_all_cookies)
     # Selenium::WebDriver
     browser.manage.delete_all_cookies
     puts false
   else
     raise "Don't know how to clear cookies. Weird driver?"
   end


 #Variavel de configuração de BASE_URL de ambiente homologação ou Produção.
 $ambiente = 'homo'
 $base_url = YAML.load_file('./features/config/environment.yml')[$ambiente]
 $base_url['base_url'] = $base_url['base_url'] + ENV['versao'] if ENV['versao'] != nil
 #Massa para logar no sistema Gliese
 $massa_dados = YAML.load_file('./features/config/massa_test/massa_login.yml')[$ambiente]

 #Massa de pacientes (Vip e não Vip)
 $paciente = 'vip1'
 $massa = YAML.load_file('./features/config/massa_test/massa_cip_por_perfil.yml')[$paciente]

 #Massa de paciente para o cenario editar peciente
 $paciente_editar = 'Paciente1'
 $massa_editar = YAML.load_file('./features/config/massa_test/massa_cip_por_perfil.yml')[$paciente_editar]

 #Massa de madicos descrito crm, uf, nome e telefone
 $inf_medico = 'medico_02'
 $massa_medicos = YAML.load_file('./features/config/massa_test/massa_medico.yml')['inform_medico'][$inf_medico]
 $massa_medicos_tabela = YAML.load_file('./features/config/massa_test/massa_medico.yml')['inform_medico']
 $inf_medico_definitivo = 'medico_cadastro_definitivo'
 $massa_medicos_definitivo = YAML.load_file('./features/config/massa_test/massa_medico.yml')['inform_medico'][$inf_medico_definitivo]

 # Arquivo de configuração de dados para acesso ao body e informações do headers para o jira
 $dados_jira = YAML.load_file('./features/config/servico_bug_jira/headers_valor.yml')['user_data']

 # informações de conexão ao banco gliese oracle
 $dados_bd = 'dados_bd'
 $dados_bd_gliese = YAML.load_file('./features/config/bd/conexao_dados_bd.yml')[$dados_bd]

 # Validar todas as exceções
 $exception = YAML.load_file('./features/support/exception/mensseger_exception.yml')

 #Criar reporte de bug no jira true ou false
 $habilitar_report_jira = false

 #Capybara.default_max_wait_time = 1
