# encoding: utf-8
# !/usr/bin/env ruby
module Helper
    # Screenshots
    def take_screenshot(file_name, result)
      timer_path = Time.now.strftime('%Y_%m_%d').to_s
      file_path = "log/screenshots/test_#{result}/run_#{timer_path}"
      screenshot = "#{file_path}/#{file_name}.png"
      page.save_screenshot(screenshot)
      embed(screenshot, 'image/png', 'SCREENSHOT')
    end
  end

After do |cenario|
  if cenario.failed?
    FuncionalidadeBancoDados.new.deletar_lock
    Capybara.current_session.driver.quit
  else
    FuncionalidadeBancoDados.new.deletar_lock
    Capybara.current_session.driver.quit
  end
end

After do |cenario|
  if cenario.failed?
    Utils.new.remover_pdf
  else
    Utils.new.remover_pdf
  end
end

Before do
  @login = FuncionalidadeLogin.new
  @convenio = FuncionalidadeAdmissaoConvenio.new
  @admissao = FuncionalidadeAdmissao.new
  @utils = Utils.new
  @exames = FuncionalidadeAdmissaoExames.new
  @visita = FuncionalidadeAdmissaoVisita.new
  @elemento_visivel_page = ElementoVisivelPage.new
  @unidade = FuncionalidadeUnidade.new
  @banco_dados = FuncionalidadeBancoDados.new
  @medico = FuncionalidadeAdmissaoMedico.new
  @elemento_visivel = ElementoVisivel.new
  @recipientes = FuncionalidadeAdmissaoRecipientes.new
  @financeiro = FuncionalidadeAdmissaoFinanceiro.new
  @multiplos_medicos= FuncionalidadeAdmissaoMultiplosMedico.new
  @admissao_conclusao = FuncionalidadeAdmissaoConclusao.new
end

def wait_accept_alert
  wait = Selenium::WebDriver::Wait.new(:timeout => 40)
  wait.until {
    begin
      page.driver.browser.switch_to.alert
      true
    rescue Selenium::WebDriver::Error::NoAlertPresentError
      false
    end
  }
end
